/*

  ---- TABLE OF CONTENTS ----
  I. Push Notifications
  II. Utility 
  III. Database Modificications
  IV. Sign-up Utilities
  V. Test Group Notificiations

*/

/*
	---- NOTES ----
	Please do NOT use console.log();
	Please do NOT try to log out anything.
	There is a really weird bug with logging you will be here all day tring to figure out the issue.
*/


// I. Push Notifications
// II. Utility
// III. Database Modificications
Parse.Cloud.job("deleteOldEntries", function(request, status) {
                Parse.Cloud.useMasterKey();
                 
                var Post = Parse.Object.extend("Post");
                var query = new Parse.Query(Post);
                var Notifications = Parse.Object.extend("Notifications");
                var notificationQuery = new Parse.Query(Notifications);
                var day = new Date();
                 
                day.setDate(day.getDate()-1);
                 
                query.lessThan("createdAt", day);
                notificationQuery.lessThan("createdAt", day);
                 
                query.find({
                           success:function(results) {
                            
                           // Run through posts that are 24 hours old, return those posts that are > 24 hours old. By > 24 hours we nean literally ANYTHING over 24 hours.
                           for (var i = 0; i < results.length; i++) {

                             var PostBackup = Parse.Object.extend("Post_Backup");
                             PostBackup = new PostBackup();
                             var result = results[i];
                             var id = result.get("objectId"); //added this
                              
                             // Copy data into post backup class
                             PostBackup.set("author", result.get("author"));
                             PostBackup.set("authorString", result.get("authorString"));
                             PostBackup.set("commenters", result.get("commenters"));
                             PostBackup.set("comments", result.get("comments"));
                             PostBackup.set("image", result.get("image"));
                             PostBackup.set("likers", result.get("likers"));
                             PostBackup.set("likes", result.get("likes"));
                             PostBackup.set("location", result.get("location"));
                             PostBackup.set("locationName", result.get("locationName"));
                             PostBackup.set("textContent", result.get("textContent"));
                             PostBackup.set("createdAt", result.get("createdAt"));

                             PostBackup.save(null, {
                                         success: function(result) {
                                          
                                         	// Destroy notifications after 24 hours.
								                notificationQuery.find({
								                                       success:function(notificationResults) {
								                                        
									                                       	for (var i = 0, len = notificationResults.length; i < len; i++) {
										                                       	var notificationResult = notificationResults[i];
										                                       	var id = notificationResult.get("objectId"); // added this
										                                       
										                                       	notificationResult.destroy({});
										                                       
									                                        }

									                                        response.success("Success");
								                                        
								                                       },
								                                        error:function(error) {
								                                        	response.error(error);
								                                        }
								                });

                                         },
                                         error: function(error) {
                                          response.error(error);
                                         }
                             });

                            // Delete the result after copying
                             result.destroy({});
                           
                           }
                           // status.success("Delete successfully.");
                           },
                           error: function(error) {
                            response.error("Nothing to delete");
                           }
            	});
                 
});

Parse.Cloud.define("setPendingBuddy", function(request, response) {
                   Parse.Cloud.useMasterKey();
                    
                   var username = request.params.username;
                   var requestingUsername = request.params.requestingUser;
                   var pendingBudddies = [];
                   var client = request.params.client;
                   var query = new Parse.Query(Parse.User);
                    
                   query.equalTo("username", username);
                   query.first({
                               success: function(user) {
                                
                               console.log(user);
                                
                               pendingBuddies = user.get("pendingBuddies");
                                
                               if (pendingBuddies === null || pendingBuddies === undefined) {
                               		pendingBuddies = [requestingUsername];
                               } else {
                               		pendingBuddies.splice(0, 0, requestingUsername);
                               }
                                
                               user.set("pendingBuddies", pendingBuddies);
                                
                               user.save(null, {
                                         success: function(result) {
                                          
                                         response.success("Success");
 											
 											if (client != "iOS") {

		 												// Add a notification to notifications centre on successful add
		                                         var NotificationForSave = Parse.Object.extend("Notifications");
		                                         var notificationForSave = new NotificationForSave();
		                                          
		                                         notificationForSave.set("type", "BuddyRequest");
		                                         notificationForSave.set("sendTo", username);
		                                          
		                                         notificationForSave.save(null, {
			                                                                  success: function(notificationForSave) {
			                                                                 		 // Execute any logic that should take place after the object is saved.
			                                                                  	console.log('Created a new buddy request notification with objectID: ' + notificationForSave.id);
		                                                                  },
			                                                                  error: function(notificationForSave, error) {
			                                                                  	console.log('Failed to create buddy request notification with error code: ' + error.message);
		                                                                  	  }
		                                                                  });

 											};
                   
                                         },
                                         error: function(error) {
                                          
                                         }
                                         });
                                
                               },
                               error: function() {
                               response.error("Failed");
                               }
                               });

});
 
Parse.Cloud.define("acceptBuddy", function(request, response) {
                   Parse.Cloud.useMasterKey();
                    
                   var username = request.params.username;
                   var currentUser = request.params.currentUser;
                    
                   var query = new Parse.Query(Parse.User);
                    
                   query.equalTo("username", username);
                   query.first({
                               success: function(user) {
                                
                               // Adding buddy to the requesting user
                               var userBuddiesArray = user.get("Buddies");
                                
                               if (userBuddiesArray == null) {
                               userBuddiesArray = [currentUser];
                               } else {
                               userBuddiesArray.splice(0, 0, currentUser);
                               }
                                
                               user.set("Buddies", userBuddiesArray);
                                
                               var points = user.get("points");
                               points++;
                               user.set("points", points);
                                
                               user.save(null, {
                                         success: function(result) {
                                          
                                         Parse.Push.send({
                                                         channels: [username],
                                                         data: {
                                                         alert: currentUser + " " + "has just accepted your Buddy Request!"
                                                         }
                                                         }, {
                                                         success: function() {
                                                         // Push was successful
                                                         },
                                                         error: function(error) {
                                                         // Handle error
                                                         }
                                                         });
                                          
                                         //response.success("Success");
                                         },
                                         error: function(error) {
                                          
                                         }
                                         });
                                
                               },
                               error: function() {
                               //response.error("Failed");
                               }
                               });
                    
                   var requestedUserQuery = new Parse.Query(Parse.User);
                    
                   requestedUserQuery.equalTo("username", currentUser);
                   requestedUserQuery.first({
                                            success: function(user) {
                                             
                                            var userBuddiesArray = user.get("Buddies");
                                             
                                            if (userBuddiesArray == null) {
                                            userBuddiesArray = [username];
                                            } else {
                                            userBuddiesArray.splice(0, 0, username);
                                            }
                                             
                                            user.set("Buddies", userBuddiesArray);
                                             
                                            var userPendingBuddiesArray = user.get("pendingBuddies");
                                            var index = userPendingBuddiesArray.indexOf(username);
                                             
                                            userPendingBuddiesArray.splice(index, 1);
                                             
                                            user.set("pendingBuddies", userPendingBuddiesArray);
                                             
                                            var points = user.get("points");
                                            points++;
                                             
                                            user.set("points", points);
                                             
                                            user.save(null, {
                                                      success: function(result) {
                                                       
                                                      response.success("Success");
                                                      },
                                                      error: function(error) {
                                                       
                                                      }
                                                      });
                                             
                                            },
                                            error: function() {
                                            //response.error("Failed");
                                            }
                                             
                                            });
                    
});
 
Parse.Cloud.define("denyBuddy", function(request, response) {
                   
                   Parse.Cloud.useMasterKey();
                    
                   var username = request.params.username;
                   var currentUser = request.params.currentUser;
                    
                   var requestedUserQuery = new Parse.Query(Parse.User);
                    
                   requestedUserQuery.equalTo("username", currentUser);
                   requestedUserQuery.first({
                                            success: function(user) {
                                             
                                            var userPendingBuddiesArray = user.get("pendingBuddies");
                                             
                                            var index = userPendingBuddiesArray.indexOf(username);
                                             
                                            userPendingBuddiesArray.splice(index, 1);
                                             
                                            user.set("pendingBuddies", userPendingBuddiesArray);
                                             
                                            user.save(null, {
                                                      success: function(result) {
                                                       
                                                      response.success("Success");
                                                      },
                                                      error: function(error) {
                                                       
                                                      }
                                                      });
                                             
                                            },
                                            error: function() {
                                            response.error("Failed");
                                            }
                                             
                                            });
                    
});
 
Parse.Cloud.define("removeBuddy", function(request, response) {
                   Parse.Cloud.useMasterKey();
                    
                   var user1 = request.params.user1;
                   var user2 = request.params.user2;
                   var buddies = [];
                   var buddies2 = [];
                   var query = new Parse.Query(Parse.User);
                   var query2 = new Parse.Query(Parse.User);
                    
                   query.equalTo("username", user1);
                   query.first({
                               success: function(user) { 
                                
                               console.log(user);
                                
                               buddies = user.get("Buddies");
                                
                               var index = buddies.indexOf(user2);
                                
                               buddies.splice(index, 1);
                                
                               user.set("Buddies", buddies);
                                
                               var points = user.get("points");
                               points--;
                                
                               user.set("points", points);
                                
                               user.save(null, {
                                         success: function(result) {
                                          
                                         //response.success("Success");
                                         },
                                         error: function(error) {
                                          
                                         }
                                         });
                                
                               },
                               error: function() {
                               //response.error("Failed");
                               }
                               });
                    
                   query2.equalTo("username", user2);
                   query2.first({
                                success: function(user) { 
                                 
                                console.log(user);
                                 
                                buddies2 = user.get("Buddies");
                                 
                                var index = buddies2.indexOf(user1);
                                 
                                buddies2.splice(index, 1);
                                 
                                user.set("Buddies", buddies2);
                                 
                                var points = user.get("points");
                                points--;
                                 
                                user.set("points", points);
                                 
                                user.save(null, {
                                          success: function(result) {
                                           
                                          response.success("Success");
                                          },
                                          error: function(error) {
                                           
                                          }
                                          });
                                 
                                },
                                error: function() {
                                //response.error("Failed");
                                }
                                });

});
 
Parse.Cloud.define("searchUser", function(request, response) {

  var searchString = request.params.searchString;

  // Start query for users that start with searchString

  var query = new Parse.Query(Parse.User);

  query.startsWith("username", searchString);
  query.find({
  
  success: function(results) {
    
    // Return queried array of usernames.
    
  },
  
  error: function(error) {
  
  }

  });

});

Parse.Cloud.define("popPoints", function(request, response) {
  Parse.Cloud.useMasterKey();


  //This is the all-seeing function in Bubble. This function calculates the difficulty rating for the popularity system in Bubble. 
  //It is based on a Sigmoid-type function and based on the work of Sadaf Mackertich and Doug Woodrow.

  //Last updated: 12/9/2014

  //Parse Query for the number of points for the user
  var c = 1000;
  var currentUser = request.user;
  var maxDistance = request.params.bounds;
  var points = currentUser.get("points");
  var currentLocation = currentUser.get("location");
  var query = new Parse.Query(Parse.User);
  var queryNumber = new Parse.Query(Parse.User);
  query.withinMiles("location", currentLocation, maxDistance);
  query.count({
      success: function(count) {
        // The count request succeeded. Show the count
        //console.log("Number of People:" + count);
        var currentUserBuddies = currentUser.get("Buddies");
        var queryFriendsNearby = new Parse.Query(Parse.User);
        queryFriendsNearby.withinMiles("location", currentLocation, maxDistance);
        queryFriendsNearby.containedIn("username", currentUserBuddies);
        queryFriendsNearby.count({
          success: function(countFriends) {
              var popPoints = c*points/(count/countFriends + points);
              currentUser.set("popPoints", popPoints);
              currentUser.save();
              response.success("Success");
          },
          error: function(error) {
            // Failed to count friends
            response.error(error);
          }
        });
      },
      error: function(error) {
        // The request failed
        //console.log("Error finding the people around.");
        response.error(error);
      }
  });
      //Get the count of number of people around you
});


// IV. Sign-up Utilities
Parse.Cloud.define("addPoints", function(request, response) {
                   Parse.Cloud.useMasterKey();
                    
                   var username = request.params.username;
                   var query = new Parse.Query(Parse.User);
                    
                   query.equalTo("username", username);
                   query.first({
                               success: function(user) {
                                
                               console.log(user);
                                
                               user.set("points", 10);
                               user.set("Staff", false);
                                
                               user.save(null, {
                                         success: function(result) {
                                          
                                         response.success("Success");
                                         },
                                         error: function(error) {
                                          
                                         }
                                         });
                                
                               },
                               error: function() {
                               response.error("Failed");
                               }
                    });
                    
});

// V. Test Group Notifications
Parse.Cloud.job("MorningReminder", function(request, status) {
                Parse.Cloud.useMasterKey();
                 
                var morningMessages = ["Good Morning Bubbler! We hope you slept well. What are you doing today?", "Wakey wakey Bubbler! Sweet dreams last night? Tell us all about them. :)", "It's a beautiful morning Bubbler! What's on your agenda today? Tell us about it!"];
                 
                var alertString = morningMessages[(Math.floor((Math.random() * 10) + 1)%3)];
                 
                Parse.Push.send({
                                channels: [ "reminderGroup"],
                                data: {
                                alert: alertString
                                }
                                }, {
                                success: function() {
                                // Push was successful
                                status.success("Pushed reminder successfully.");
                                },
                                error: function(error) {
                                // Handle error
                                }
                                });                
                 
});
 
Parse.Cloud.job("AfternoonReminder", function(request, status) {
                Parse.Cloud.useMasterKey();
                 
                var afternoonMessages = ["Good Afternoon Bubbler! How is your day so far? Tell us about it!", "What's going on Bubbler? Tell us about your afternoon?", "The Bubble Team hopes your afternoon is going as well as ours is. What's going on around you?"];
                 
                var alertString = morningMessages[(Math.floor((Math.random() * 10) + 1)%3)];
                 
                Parse.Push.send({
                                channels: [ "reminderGroup"],
                                data: {
                                alert: alertString
                                }
                                }, {
                                success: function() {
                                // Push was successful
                                status.success("Pushed reminder successfully.");
                                },
                                error: function(error) {
                                // Handle error
                                }
                                });
            
});
 
Parse.Cloud.job("NightReminder", function(request, status) {
                Parse.Cloud.useMasterKey();
                 
                var morningMessages = ["Good Evening Bubbler! We hope your day was amazing. :) Tell us about your day!", "Phew! Today was a long day at Bubble. What's going on tonight? Party, sleep, school work? Tell us about it!", "Tonight is going to be awesome! Tell us what is going to make your night so awesome?"];
                 
                var alertString = morningMessages[(Math.floor((Math.random() * 10) + 1)%3)];
                 
                Parse.Push.send({
                                channels: [ "reminderGroup"],
                                data: {
                                alert: alertString
                                }
                                }, {
                                success: function() {
                                // Push was successful
                                status.success("Pushed reminder successfully.");
                                },
                                error: function(error) {
                                // Handle error
                                }
                                });
                 
});

Parse.Cloud.define("likePost", function(request, response) {

  Parse.Cloud.useMasterKey();

  var currentUsername = request.user.get("username");
  var currentUser = request.user;
  var objectId = request.params.objectId;
  var currentNumberOfLikes = 0;
  var post = Parse.Object.extend("Post");
  var postQuery = new Parse.Query(post);
  var LIKE_NOTIFICATION = "Like";
  var notification = Parse.Object.extend("Notifications");
  var notificationQuery = new Parse.Query(notification);
  var queriedNotification;
  var likers = [];
  var hasLikedBefore = [];
  var notificationObject;

  		// Get the post to like
        postQuery.equalTo("objectId", objectId);
        postQuery.find({

        success: function(result) {
           
            var queriedPost = result[0];
           
            currentNumberOfLikes = queriedPost.get("likes");
            currentNumberOfLikes++;

            var points = queriedPost.get("points");

            points++;

            queriedPost.set("points", points);

            likers = queriedPost.get("likers");

            if (likers === null || likers === undefined) {

            	likers = [currentUsername];

            } else {

            	likers.push(currentUsername);
            }

            queriedPost.set("likes", currentNumberOfLikes);
            queriedPost.set("likers", likers);
            
            hasLikedBefore = queriedPost.get("hasLikedBefore");
            
            if (hasLikedBefore === null || hasLikedBefore === undefined) {

                hasLikedBefore = [currentUsername];
                queriedPost.set("hasLikedBefore", hasLikedBefore);

            } else {

                var likedBefore = hasLikedBefore.indexOf(currentUsername);
                
                if (likedBefore == -1) {

                  hasLikedBefore.push(currentUsername);
                  queriedPost.set("hasLikedBefore", hasLikedBefore);

                }
                      
            }
           
            queriedPost.save(null, {
                                        success: function(savedPost) {
                                        	
                                        	Parse.Cloud.run("setNotification", {pObject : objectId, nType : "Like"}, {
											  success: function(result) {
											    response.success();
											  },
											  error: function(error) {
											  	response.error("Successfully saved post. Failed to save notification.");
											  }
											});

                                            },
                                            error: function(error) {
                                            	response.error(error);
                                            }

                                          });
                                         
             },
             error: function(error) {
            	console.log("FAILING");
              	response.error(error);
             }
        });

});

Parse.Cloud.define("unlikePost", function(request, response) {

  Parse.Cloud.useMasterKey();

  var currentUser = request.user;
  var objectId = request.params.objectId;
  var post = Parse.Object.extend("Post");
  var postQuery = new Parse.Query(post);
  var likers = [];

  postQuery.equalTo("objectId", objectId);
  postQuery.find({

    success: function(results) {

      post = results[0];
      
      var likes = post.get("likes");
      likes--;

      var points = post.get("points");

      points--;

      post.set("points", points);

      post.set("likes", likes);

      likers = post.get("likers"); 

      var index = likers.indexOf(currentUser.get("username"));

      if (index > -1) {
        likers.splice(index, 1);
      } 

      post.set("likers", likers);
      post.save({

        success: function(success) {
          response.success("Unliked complete");
        },
         error: function(error) {
          response.error(error);
         }

      });

    },
     error: function(error) {

      response.error(error);
     }

  });

});

Parse.Cloud.define("setNotification", function(request, response) {

	Parse.Cloud.useMasterKey();

	var notificationO = Parse.Object.extend("Notifications");
	var notificationQuery = new Parse.Query(notificationO);
	var p = Parse.Object.extend("Post");
	var postQuery = new Parse.Query(p);
	var post = request.params.pObject;
	var type = request.params.nType;
	var postObjectGlobal;

	postQuery.get(post, {

		success: function(postObject) {

   				postObjectGlobal = postObject;

				notificationQuery.equalTo("postObject", postObject);
				notificationQuery.equalTo("type", type);
				notificationQuery.first({

	       			 success: function(notificationObjects) {

	          			if (notificationObjects === null || notificationObjects === undefined) {

	          					// Keeps crashing right here... 
	          				var notification = new notificationO();
	          		
	          				notification.set("sendTo", postObjectGlobal.get("authorString"));
	          				notification.set("postObject", postObjectGlobal);
	          				notification.set("type", type);
	          		
	         				notification.save(null, {

	            				success: function(savedNotification) {
	             	 				response.success("Did save new notification");
	           					 },
	           					 error: function(error) {
	             					response.error(error);	
	           					 }

	          					});

	         			 } else {

	          				var notificationObject = notificationObjects[0];

	          				notificationObject.set("sendTo", queriedPost.get("authorString"));
	          				notificationObject.save(null, {

	           					 success: function(savedNotification) {
	           
	            					  response.success("Did save existing notification");

	           					 },
	           					 error: function(error) {
	            		 		 	console.log("Notification save failed.");
	           					    response.error(error);
	          					  }

	          				});

	         	 		}
	       			},
	      		 	error: function(error) {
	      		 		console.log("Notificaiton query Failed");
	       				response.error(error);
	       			}
	    		  });

 		},
  		 error: function(error) {
  				console.log("Get Post Failed");
   				response.error(error);
 	 	}		

	});

});

Parse.Cloud.define("pushNotification", function(request, response) {

	var channel = request.params.channel;
	var alert = request.params.alert;
	var sound = request.params.sound;
	var category = request.params.category;
	var requesting = request.params.requestingUser;
	var view = request.params.viewToPush;

 if (Object.prototype.toString.call( channel ) === '[object Array]') {
    
  Parse.Push.send({

      channels: channel,
      data: {
        alert: alert,
        badge: "Increment",
        sound: sound,
        category: category,
        requestingUser: requesting,
        viewToPush: view
      }
     }, 
      {
      success: function(success) {
        response.success("Successfully pushed");
      },
       error: function(error) {
        response.error(error);
     }
  });

  } else {

    Parse.Push.send({

      channels: [ channel ],
      data: {
        alert: alert,
        badge: "Increment",
        sound: sound,
        category: category,
        requestingUser: requesting,
        viewToPush: view
      }
     }, 
      {
      success: function(success) {
        response.success("Successfully pushed");
      },
       error: function(error) {
        response.error(error);
     }
    });

  }

});

Parse.Cloud.define("postStatus", function(request, response) {

		// Parameters
	var text = request.params.text;
	var data = request.params.file;
  	var file = new Parse.File("imageFile.jpg", data);
	var point = request.params.point;
	var locationName = request.params.locationName;
	var mentions = request.params.mentions;
	var currentUser = request.user;
	var userProfilePic = currentUser.get("ProfilePic");

	var postClass = Parse.Object.extend("Post");
	var post = new postClass();

	post.set("textContent", text);
	post.set("authorString", currentUser.getUsername());
	post.set("author", currentUser);
	post.set("likes", 0);
	post.set("profilePic", userProfilePic);
	post.set("points", currentUser.get("popPoints"));
	post.set("location", point);
	post.set("locationName", locationName);
	post.set("mentioned", mentions);

	post.save(null, {

        success: function(result) {
        	
			if (file === null || file === undefined) {

			} else {

				file.save().then(function() {

		   		 }, function(error) {

		   	 	});

			    post.set("image", file);

			    post.save(null, {

				    success: function(result) {
				      response.success("success");
				    }, 
				    error: function(error) {
				      response.success("success");
				    }
				});

			}

          	//response.success("success");
        }, 
        error: function(error) {
          	response.error(error);
        }

    });

});

