//
//  NowFeed.swift
//  Bubble
//
//  Created by Chayel Heinsen on 8/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class NowFeed: WKInterfaceController {

    @IBOutlet var table: WKInterfaceTable!
    var data = [AnyObject]()
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        WCSession.defaultSession().sendMessage(["NowFeedRequest" : 1], replyHandler: { [weak self] (userInfo) -> Void in
            
            if let weakSelf = self {
                weakSelf.data.removeAll(keepCapacity: false)
            }
            
            if let dict = (userInfo as [String : AnyObject])["response"] as? [AnyObject] {
                
                for postObj in dict {
                    let post = Post()
                    post.username = postObj.objectForKey("username") as! String
                    post.location = postObj.objectForKey("location") as! String
                    
                    if let text = postObj.objectForKey("text") as? String {
                        post.text = text
                    }
                    
                    if let photo = postObj.objectForKey("photo") as? String {
                        post.photoURL = NSURL(string: photo)
                    }
                    
                    if let weakSelf = self {
                        weakSelf.data.append(post)
                    }
                    
                }
                
                if let weakSelf = self {
                    weakSelf.reloadTable()
                }
                
            }
            
            }) { (error) -> Void in
                print("Error: \(error)")
        }
        
        /*
        WKInterfaceController.openParentApplication(["NowFeedRequest" : 1]) { [weak self] userInfo, error in
            print("User Info: \(userInfo)")
            print("Error: \(error)")
            
            if let weakSelf = self {
                weakSelf.data.removeAll(keepCapacity: false)
            }
            
            if let dict = (userInfo as? [String : AnyObject])?["response"] as? [AnyObject] {
                
                for postObj in dict {
                    let post = Post()
                    post.username = postObj.objectForKey("username") as! String
                    post.location = postObj.objectForKey("location") as! String
                    
                    if let text = postObj.objectForKey("text") as? String {
                        post.text = text
                    }
                    
                    if let photo = postObj.objectForKey("photo") as? String {
                        post.photoURL = NSURL(string: photo)
                    }
                    
                    if let weakSelf = self {
                        weakSelf.data.append(post)
                    }
                    
                }
                
                if let weakSelf = self {
                    weakSelf.reloadTable()
                }
                
            }
            
        }
        */

    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func reloadTable() {
        
        table.setNumberOfRows(data.count, withRowType: "Cell")
        
        for var index = 0; index < data.count; index++ {
            
            let post = data[index] as! Post
            
            if let cell = table.rowControllerAtIndex(index) as? Cell {
                cell.username.setText(post.username)
                cell.location.setText(post.location)
                cell.text.setText(post.text)
                
                if post.photoURL != nil {
                    cell.image.setHidden(true)
                } else {
                    cell.image.setHidden(true)
                    cell.containsImage.setHidden(true)
                }
            }
        }
        
    }

}
