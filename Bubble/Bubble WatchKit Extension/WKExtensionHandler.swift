//
//  WKExtensionHandler.swift
//  Bubble
//
//  Created by Chayel Heinsen on 9/19/15.
//  Copyright © 2015 Bubble Social App LLC. All rights reserved.
//

import Foundation
import WatchKit
import WatchConnectivity

@objc
class WKExtensionHandler : NSObject, WKExtensionDelegate, WCSessionDelegate {
    
    //MARK - WKExtensionDelegate
    
    func applicationDidFinishLaunching() {
        
    }
    
    func handleActionWithIdentifier(identifier: String?, forRemoteNotification remoteNotification: [NSObject : AnyObject]) {
        
    }
    
    func handleActionWithIdentifier(identifier: String?, forLocalNotification localNotification: UILocalNotification) {
        
    }
    
    func handleActionWithIdentifier(identifier: String?, forRemoteNotification remoteNotification: [NSObject : AnyObject], withResponseInfo responseInfo: [NSObject : AnyObject]) {
        
    }
    
    func handleActionWithIdentifier(identifier: String?, forLocalNotification localNotification: UILocalNotification, withResponseInfo responseInfo: [NSObject : AnyObject]) {
        
    }
    
    func handleUserActivity(userInfo: [NSObject : AnyObject]?) {
        
    }
    
    func didReceiveRemoteNotification(userInfo: [NSObject : AnyObject]) {
        
    }
    
    func didReceiveLocalNotification(notification: UILocalNotification) {
        
    }
    
}