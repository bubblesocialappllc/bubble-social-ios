//
//  InterfaceController.swift
//  Bubble WatchKit Extension
//
//  Created by Chayel Heinsen on 8/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

let sharedUserActivityType = "com.bubblesocial.Bubble.FromWatch"
let sharedIdentifierKey = "com.bubblesocial.Bubble"

class InterfaceController: WKInterfaceController, WCSessionDelegate {

    var watchSession : WCSession?
    @IBOutlet var logInText: WKInterfaceLabel!
    @IBOutlet var now: WKInterfaceButton!
    @IBOutlet var poppin: WKInterfaceButton!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        let userActivity = NSUserActivity(activityType: sharedUserActivityType)
        userActivity.becomeCurrent()
        updateUserActivity(sharedUserActivityType, userInfo: [sharedIdentifierKey : "open"], webpageURL: nil)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if WCSession.isSupported() {
            watchSession = WCSession.defaultSession()
            watchSession!.delegate = self
            watchSession!.activateSession()
        }
        
        /*
        if let currentUser = PFUser.currentUser() {
            logInText.setHidden(true)
            now.setHidden(false)
            poppin.setHidden(false)
        } else {
            logInText.setHidden(false)
            now.setHidden(true)
            poppin.setHidden(true)
        }
        */
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    /*
    @IBAction func post() {
        
        if PFUser.currentUser() != nil {
            self.presentTextInputControllerWithSuggestions(nil, allowedInputMode: .Plain, completion: { (results) -> Void in
                
                if let results = results {
                    WKInterfaceController.openParentApplication(["MakePost" : results], reply: nil)
                }
                
            })
        }
    }
    */
}
