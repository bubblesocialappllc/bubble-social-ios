//
//  Cell.swift
//  Bubble
//
//  Created by Chayel Heinsen on 8/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit
import WatchKit

class Cell: NSObject {
    
    @IBOutlet var username: WKInterfaceLabel!
    @IBOutlet var location: WKInterfaceLabel!
    @IBOutlet var image: WKInterfaceImage!
    @IBOutlet var containsImage: WKInterfaceLabel!
    @IBOutlet var text: WKInterfaceLabel!
    
}
