//
//  Post.swift
//  Bubble
//
//  Created by Chayel Heinsen on 8/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit

class Post: NSObject {
    var username: String!
    var text: String?
    var location: String!
    var photoURL: NSURL?
}
