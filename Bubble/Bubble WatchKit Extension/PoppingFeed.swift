//
//  PoppingFeed.swift
//  Bubble
//
//  Created by Chayel Heinsen on 8/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class PoppingFeed: WKInterfaceController {
    
    @IBOutlet var table: WKInterfaceTable!
    var data = [AnyObject]()
    var usePosts = true
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        self.runQuery()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func reloadTable() {
        
        if usePosts {
            table.setNumberOfRows(data.count, withRowType: "Cell")
            
            for var index = 0; index < data.count; index++ {
                
                let post = data[index] as! Post
                
                if let cell = table.rowControllerAtIndex(index) as? Cell {
                    cell.username.setText(post.username)
                    cell.location.setText(post.location)
                    
                    if let text = post.text {
                        cell.text.setText(text)
                    }
                    
                    if post.photoURL != nil {
//                        let request = NSURLRequest(URL: post.photoURL!)
//                        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
//                            (response, data, error) -> Void in
//                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                                cell.image.setImageData(data)
//                            })
//                        }
                        
                        cell.image.setHidden(true)
                        
                    } else {
                        cell.image.setHidden(true)
                        cell.containsImage.setHidden(true)
                    }
                }
            }
        } else {
            table.setNumberOfRows(data.count, withRowType: "People")
            
            for var index = 0; index < data.count; index++ {
                
                let person = data[index] as! Person
                
                if let cell = table.rowControllerAtIndex(index) as? People {
                    cell.username.setText(person.username)
                }
            }
        }
    }
    
    func runQuery() {
        
        if usePosts {
            
            WCSession.defaultSession().sendMessage(["PoppinPostsFeedRequest" : 1], replyHandler: { [weak self] (userInfo) -> Void in
                
                if let weakSelf = self {
                    weakSelf.data.removeAll(keepCapacity: false)
                }
                
                if let dict = (userInfo as [String : AnyObject])["response"] as? [AnyObject] {
                    
                    for postObj in dict {
                        let post = Post()
                        post.username = postObj.objectForKey("username") as! String
                        post.location = postObj.objectForKey("location") as! String
                        
                        if let text = postObj.objectForKey("text") as? String {
                            post.text = text
                        }
                        
                        if let photo = postObj.objectForKey("photo") as? String {
                            post.photoURL = NSURL(string: photo)
                        }
                        
                        if let weakSelf = self {
                            weakSelf.data.append(post)
                        }
                    }
                    
                    if let weakSelf = self {
                        weakSelf.reloadTable()
                    }
                }
                
                }) { (error) -> Void in
                    print("Error: \(error)")
            }

            
            /*
            WKInterfaceController.openParentApplication(["PoppinPostsFeedRequest" : 1]) { [weak self] userInfo, error in
                print("User Info: \(userInfo)")
                print("Error: \(error)")
                
                if let weakSelf = self {
                    weakSelf.data.removeAll(keepCapacity: false)
                }
                
                if let dict = (userInfo as? [String : AnyObject])?["response"] as? [AnyObject] {
                    
                    for postObj in dict {
                        let post = Post()
                        post.username = postObj.objectForKey("username") as! String
                        post.location = postObj.objectForKey("location") as! String
                        
                        if let text = postObj.objectForKey("text") as? String {
                            post.text = text
                        }
                        
                        if let photo = postObj.objectForKey("photo") as? String {
                            post.photoURL = NSURL(string: photo)
                        }
                        
                        if let weakSelf = self {
                            weakSelf.data.append(post)
                        }
                    }
                    
                    if let weakSelf = self {
                        weakSelf.reloadTable()
                    }
                }
            }
            */
        } else {
            
            WCSession.defaultSession().sendMessage(["PoppinPeopleFeedRequest" : 1], replyHandler: { [weak self] (userInfo) -> Void in
                
                if let weakSelf = self {
                    weakSelf.data.removeAll(keepCapacity: false)
                }
                
                if let dict = (userInfo as [String : AnyObject])["response"] as? [AnyObject] {
                    
                    for personObj in dict {
                        let person = Person()
                        person.username = personObj.objectForKey("username") as! String
                        
                        /*
                        if let photo = personObj["photo"] as? String {
                        person.profilePictureURL = NSURL(string: photo)
                        }
                        */
                        
                        if let weakSelf = self {
                            weakSelf.data.append(person)
                        }
                    }
                    
                    if let weakSelf = self {
                        weakSelf.reloadTable()
                    }
                }
                
                }) { (error) -> Void in
                    print("Error: \(error)")
            }

            /*
            WKInterfaceController.openParentApplication(["PoppinPeopleFeedRequest" : 1]) { [weak self] userInfo, error in
                print("User Info: \(userInfo)")
                print("Error: \(error)")
                
                if let weakSelf = self {
                    weakSelf.data.removeAll(keepCapacity: false)
                }
                
                if let dict = (userInfo as? [String : AnyObject])?["response"] as? [AnyObject] {
                    
                    for personObj in dict {
                        let person = Person()
                        person.username = personObj.objectForKey("username") as! String
                        
                        /*
                        if let photo = personObj["photo"] as? String {
                            person.profilePictureURL = NSURL(string: photo)
                        }
                        */
                        
                        if let weakSelf = self {
                            weakSelf.data.append(person)
                        }
                    }
                    
                    if let weakSelf = self {
                        weakSelf.reloadTable()
                    }
                }
            }
            */
        }
    }
    
    @IBAction func posts() {
        usePosts = true
        self.runQuery()
    }

    @IBAction func people() {
        usePosts = false
        self.runQuery()
    }
}
