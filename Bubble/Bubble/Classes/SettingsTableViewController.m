//
//  SettingsTableViewController.m
//  Bubble
//
//  Created by Doug Woodrow on 2/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "SettingsTableViewController.h"

@interface SettingsTableViewController ()

@property (strong, nonatomic) BreakOutToRefreshView *refresh;

@end

@implementation SettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = YES;
    
    self.refresh = [[BreakOutToRefreshView alloc] initWithScrollView:self.tableView];
    self.refresh.delegate = self;
    self.refresh.paddleColor = [UIColor bubblePink];
    self.refresh.ballColor = [UIColor darkGrayColor];
    self.refresh.blockColors = @[[UIColor flatGreenSeaColor], [UIColor flatNephritisColor], [UIColor flatEmeraldColor]];
    
    [self.tableView addSubview:self.refresh];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [self.refresh scrollViewDidScroll:scrollView];
    
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {

    [self.refresh scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    [self.refresh scrollViewWillBeginDragging:scrollView];
    
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellAnimation"]) {
        cell.alpha = 0;
        
        [UIView beginAnimations:@"fade" context:NULL];
        [UIView setAnimationDuration:0.8];
        cell.alpha = 1;
        [UIView commitAnimations];
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 6;
            break;
        case 2:
            return 4;
            break;
        case 3:
            return 2;
            break;
        case 4:
            return 2;
            break;
        default:
            return 0;
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0: { // Name
            break;
        }
        case 1: { // Share Bubble
            
            switch (indexPath.row) {
                case 0: { // Share Bubble with Friends
                    
                    [self shareBubble];
                    
                    break;
                }
                case 1: { // Rate Bubble
                    
                    [self rateBubble];
                    
                    break;
                }
                case 2: { // Follow Twitter
                    
                    [self followOnTwitter];
                    
                    break;
                }
                case 3: { // Like Facebook
                    
                    [self likeOnFacebook];
                    
                    break;
                }
                case 4: { // Follow Instagram
                    
                    [self followOnInstagram];
                    
                    break;
                }
                case 5: { // Message Us
                    
                    [self sendAMessage];
                    
                    break;
                }
                default:
                    break;
            }
            
            break;
        }
        case 2: { // Administration
            
            switch (indexPath.row) {
                case 0: { // Change Password
                    
                    [self changePassword];
                    
                    break;
                }
                case 1: { // Other Settings
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                    break;
                }
                case 2: { // Block Users
                    
                    break;
                }
                case 3: { // Log Out
                
                    [self logOut];
                    
                    break;
                }
                default:
                    break;
            
            }
            
            break;
        }
        case 3: { // Legal
            
            switch (indexPath.row) {
                case 0: { // TOU
                    
                    [self termsOfUse];
                    
                    break;
                }
                case 1: { // Acknowledgements
                    
                    [self acknowledgements];
                    
                    break;
                }
                default:
                    break;
                    
            }
            
            break;
            
        }
        case 4: { // Serious Stuff
            
            switch (indexPath.row) {
                case 0: { // Report Abuse
                    [self reportAbuse];
                    break;
                }
                case 1: { // Delete Account
                    [self deleteAccount];
                    break;
                }
                default:
                    break;
                    
            }
            
            break;
            
        }
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    switch (section) {
        case 0: {
            return @"This is where you decide how others see you on Bubble. Change your about me, profile pic, profile color, phone number and email.";
            break;
        }
        case 1: {
            return @"Woot woot! Like us and share Bubble with your friends and family!";
            break;
        }
        case 2: {
            return @"";
            break;
        }
        case 3: {
            return @"";
            break;
        }
        case 4: {
            
            NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
            
            //The year
            NSDateFormatter *formatter = [NSDateFormatter new];
            [formatter setDateFormat:@"yyyy"];
            NSString *year = [formatter stringFromDate:[NSDate date]];
            
            return [NSString stringWithFormat:@"Bubble Social | Version %@ (%@) | Copyright © %@", version, build, year];

            break;
        }
        default:
            return @"";
            break;
    }
    
}

#pragma mark - BreakOutToRefresh Delegate

- (void)refreshViewDidRefresh:(BreakOutToRefreshView *)refresh {
    [refresh endRefreshing];
}

#pragma mark - Helpers

- (void)shareBubble {
    
    NSString *string = @"Download Bubble Social in the App Store : http://bit.ly/16qNLnd";
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[string] applicationActivities:nil];
    
    [self.navigationController presentViewController:activityViewController animated:YES completion:nil];
    
    if ([activityViewController respondsToSelector:@selector(popoverPresentationController)]) {
        
        UIPopoverPresentationController *presentationController = [activityViewController popoverPresentationController];
        
        presentationController.sourceView = self.view;
    }
    
}

- (void)rateBubble {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://bit.ly/16qNLnd"]];
}

- (void)followOnTwitter {
    
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=bubblesocialapp"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/bubblesocialapp"]];
    }
    
}

- (void)likeOnFacebook {
    
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/504566749689430"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/bubblesocialapp"]];
    }
    
}

- (void)followOnInstagram {
    
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"instagram://user?username=bubblesocialapp"]]) {
         [KVNProgress showErrorWithStatus:@"Sorry. It looks like you don't have Instagram installed."];
    }
    
}

- (void)sendAMessage {
    [SupportKit showConversation];
}

- (void)changePassword {
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    alert.customViewColor = [CHColorHandler getUIColorFromNSString:[BSUser currentUser].color];
    
    UITextField *currentPassword = [alert addTextField:@"Current Password"];
    UITextField *newPassword = [alert addTextField:@"New Password"];
    
    [alert addButton:@"Change your password" actionBlock:^(void) {
        
        if ([currentPassword.text isEqualToString:[CHKeychain objectForKey:@"pwd"]] && newPassword.text.length > 0) {
            [[BSUser currentUser] setPassword:newPassword.text];
            [[BSUser currentUser] save];
        } else {
            [KVNProgress showErrorWithStatus:@"Your current password was incorrect or the new password field was empty"];
        }
        
    }];
    
    [alert showEdit:self.tabBarController title:@"Change your password" subTitle:nil closeButtonTitle:@"Cancel" duration:0.0f];
    
}

- (void)logOut {
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    alert.customViewColor = [CHColorHandler getUIColorFromNSString:[BSUser currentUser].color];

    [alert addButton:@"Log me out" actionBlock:^(void) {
        
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        
        if (currentInstallation.channels != nil) {
            [currentInstallation removeObject:[BSUser currentUser].username forKey:@"channels"];
            [currentInstallation saveEventually];
        }
        
        [BSUser logOut];
        [CHKeychain removeObjectForKey:@"pwd"];
        [PFQuery clearAllCachedResults];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LogOutUser" object:nil];
        
        [self.navigationController popViewControllerAnimated:YES];
        
        UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
        [tabBarController setSelectedIndex:FEED_TAB];
        
    }];
    
    [alert showCustom:self.tabBarController image:[IonIcons imageWithIcon:ion_arrow_graph_up_right size:20 color:[UIColor whiteColor]] color:[CHColorHandler getUIColorFromNSString:[BSUser currentUser].color] title:@"Are you sure?" subTitle:nil closeButtonTitle:@"Cancel" duration:0.0f];
    
}

- (void)termsOfUse {
    
    WebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    web.loadURL = @"http://bubblesocialapp.com/terms-of-use.php";
    web.title = @"Terms of Use & Privacy Policy";
    [self.navigationController pushViewController:web animated:YES];
    
}

- (void)acknowledgements {
    
    VTAcknowledgementsViewController *viewController = [VTAcknowledgementsViewController acknowledgementsViewController];
    viewController.headerText = NSLocalizedString(@"We love open source software!", nil); // optional
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (void)reportAbuse {
    [self sendAMessage]; // For right now we are using support kit to handle abuse reports.
}

- (void)deleteAccount {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Whoa" message:@"Are you sure you want to delete your account? We are going to miss you... 😭" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete Account" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:NO completion:nil];
        [self deleteAccountVerification];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[delete, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)deleteAccountVerification {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you really want to delete your account?" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete Account" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:NO completion:nil];
        [self actualDeleteAccount];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[delete, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)actualDeleteAccount {
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    alert.customViewColor = [CHColorHandler getUIColorFromNSString:[BSUser currentUser].color];
    
    UITextField *currentPassword = [alert addTextField:@"Current Password"];

    [alert addButton:@"Delete Acount Now" actionBlock:^(void) {
        
        if ([currentPassword.text isEqualToString:[CHKeychain objectForKey:@"pwd"]]) {
            
            [[BSUser currentUser] deleteInBackground];
            [BSUser logOut];
            [CHKeychain removeObjectForKey:@"pwd"];
            [PFQuery clearAllCachedResults];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LogOutUser" object:nil];
            
            [self.navigationController popViewControllerAnimated:YES];
            
            UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
            [tabBarController setSelectedIndex:FEED_TAB];
            
        } else {
            [KVNProgress showErrorWithStatus:@"Your current password was incorrect."];
        }
        
    }];
    
    [alert showEdit:self.tabBarController title:@"Enter your password" subTitle:@"to delete your account" closeButtonTitle:@"Cancel" duration:0.0f];
    
}

@end
