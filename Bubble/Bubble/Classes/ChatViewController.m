//
//  ChatViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 1/30/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "ChatViewController.h"

@interface ChatViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSTimer *timeout;

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.showLoadEarlierMessagesHeader = YES;
    
        // Set the senderId and display name so we know who is sending messages.
    self.senderId = self.appDelegate.currentUser.objectId;
    self.senderDisplayName = self.appDelegate.currentUser.username;
    
    
        // Check if the user wants avatars or not
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"my_avatar_preference"] == NO) {
        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"their_avatar_preference"] == NO) {
        self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    }
    
        // Set the avatars.
    PFFile *myAvatar = self.appDelegate.currentUser.profilePicture;
    [myAvatar getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
       
        self.avatars = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                       self.senderId : [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageWithData:data] diameter:kJSQMessagesCollectionViewAvatarSizeDefault],
                                                                       }];
        
        PFFile *theirAvatar = self.otherUser.profilePicture;
        [theirAvatar getDataInBackgroundWithBlock:^(NSData *theirData, NSError *error) {
           
            [self.avatars addEntriesFromDictionary:@{
                                                     self.otherUser.objectId : [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageWithData:theirData] diameter:kJSQMessagesCollectionViewAvatarSizeDefault],
                                                     }];
            [self.collectionView reloadData];
        }];
        
    }];
    
        // Create message bubble images objects.
        // We create bubble images one time and reuse them for good performance.
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    UIColor *myColor = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
    UIColor *theirColor = [CHColorHandler getUIColorFromNSString:self.otherUser.color];
    
    self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:myColor];
    self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:theirColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_close_round size:20 color:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(closePressed)];
    
    self.inputToolbar.tintColor = [UIColor bubblePink];
    
    self.messages = [NSMutableArray new];
    
    self.title = self.otherUser.username;
  
        // Find all of the messages with the parentNode.
    if (!self.isNewParent) {
        [self findMessages];
    }
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(findNewMessages) userInfo:nil repeats:YES];
    self.timeout = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(callTimeout) userInfo:nil repeats:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(findNewMessages) name:@"NewMessage" object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     */
     // self.collectionView.collectionViewLayout.springinessEnabled = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OnChat" object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    [self.timer invalidate];
    [self.timeout invalidate];
    [self callTimeout];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OffChat" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date {
    
    /**
     *  Sending a message. Your implementation of this method should do *at least* the following:
     *
     *  1. Play sound (optional)
     *  2. Add new id<JSQMessageData> object to your data source
     *  3. Call `finishSendingMessage`
     */
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                              senderDisplayName:senderDisplayName
                                              date:date
                                              text:text];
    
    [self.messages addObject:message];
    
    BSMessage *messageObj = [BSMessage object];
    
    messageObj.parent = self.parentNode;
    messageObj.sender = self.appDelegate.currentUser;
    messageObj.text = text;
    
    self.parentNode.lastMessageDate = message.date;
    self.parentNode.lastMessage = message.text;
    
    [self.parentNode saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
       
        [messageObj pinInBackgroundWithName:self.parentNode.objectId];
        [messageObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            [CloudCode pushToChannel:self.otherUser.username withAlert:[NSString stringWithFormat:@"%@: %@", self.senderDisplayName, text] andCategory:@"Message"];
            
        }];
        
        [self finishSendingMessageAnimated:YES];
        
    }];
}

- (void)didPressAccessoryButton:(UIButton *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *location = [UIAlertAction actionWithTitle:@"Send Current Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        JSQLocationMediaItem *locationMediaItem = [JSQLocationMediaItem new];
        [locationMediaItem setLocation:[[CLLocation alloc] initWithLatitude:self.appDelegate.currentLocation.latitude longitude:self.appDelegate.currentLocation.longitude] withCompletionHandler:^{
            
            [self.collectionView reloadData];
            
        }];
        
        JSQMessage *locationMessage = [JSQMessage messageWithSenderId:self.senderId displayName:self.senderDisplayName media:locationMediaItem];
        
        [self.messages addObject:locationMessage];
        
        BSMessage *messageObj = [BSMessage object];
        
        self.parentNode.lastMessageDate = locationMessage.date;
        self.parentNode.lastMessage = [NSString stringWithFormat:@"%@ shared their location", self.senderDisplayName];
        
        messageObj.parent = self.parentNode;
        messageObj.sender = self.appDelegate.currentUser;
        messageObj.location = self.appDelegate.currentLocation;
        [messageObj pinInBackgroundWithName:self.parentNode.objectId];
        [messageObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            [CloudCode pushToChannel:self.otherUser.username withAlert:[NSString stringWithFormat:@"%@: Shared their current location.", self.senderDisplayName] andCategory:@"Message"];
            
        }];
        
        [JSQSystemSoundPlayer jsq_playMessageSentSound];
        
        [self finishSendingMessageAnimated:YES];
        
    }];
    
    UIAlertAction *photo = [UIAlertAction actionWithTitle:@"Send Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       
        UIImagePickerController *picker = [UIImagePickerController new];
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[photo, location, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self.messages objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingBubbleImageData;
    }
    
    return self.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
        // Check if the user wants avatars or not
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"my_avatar_preference"] == NO && [message.senderId isEqualToString:self.senderId]) {
        
        return nil;
        
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"their_avatar_preference"] == NO && [message.senderId isEqualToString:[self.avatars objectForKey:self.otherUser.objectId]]) {
        
        return nil;
    }
    
    return [self.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            
            cell.textView.textColor = [UIColor whiteColor];
        
        } else {
            
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}

#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.messages objectAtIndex:indexPath.item];
    
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender {
    
    [self findPastMessages];
    
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessage *message = [[self.messages objectAtIndex:indexPath.row] copy];
    
    if (message.isMediaMessage) {
        
        id<JSQMessageMediaData> mediaData = message.media;
        
        if ([mediaData isKindOfClass:[JSQLocationMediaItem class]]) {
            
            JSQLocationMediaItem *locationItem = [((JSQLocationMediaItem *)mediaData) copy];
            
            CLLocationCoordinate2D location = locationItem.location.coordinate;
            
            INKMapsHandler *mapsHandler = [[INKMapsHandler alloc] init];
            // mapsHandler.useSystemDefault = YES;
            mapsHandler.center = CLLocationCoordinate2DMake(location.latitude, location.longitude);
            mapsHandler.zoom = 14;
            
            INKActivityPresenter *presenter = [mapsHandler directionsFrom:[NSString stringWithFormat:@"%f, %f", self.appDelegate.currentLocation.latitude, self.appDelegate.currentLocation.longitude] to:[NSString stringWithFormat:@"%f, %f", location.latitude, location.longitude]];
            [presenter presentModalActivitySheetFromViewController:self completion:nil];
            
            // This works but converting points to an address is pretty heavy and it takes a while.
            /*[BSLocation addressFromCurrentLocationWithBlock:^(NSString *address, NSError *error) {
                
                [BSLocation addressFromLocation:[PFGeoPoint geoPointWithLocation:locationItem.location] completion:^(NSString *otherAddress, NSError *error) {
                    
                    INKActivityPresenter *presenter = [mapsHandler directionsFrom:address to:otherAddress];
                    [presenter presentModalActivitySheetFromViewController:self completion:nil];
                    
                }];
                
            }];*/
            
        }
        
    }
    
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation {
    
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    // Convert image to data then to a file to save on Parse.
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    PFFile *file = [PFFile fileWithName:@"image.jpg" data:imageData];
    
    JSQPhotoMediaItem *photoMediaItem = [[JSQPhotoMediaItem alloc] initWithImage:image];
    
    JSQMessage *photoMessage = [JSQMessage messageWithSenderId:self.senderId displayName:self.senderDisplayName media:photoMediaItem];
    
    [self.messages addObject:photoMessage];
    
    BSMessage *messageObj = [BSMessage object];
    
    self.parentNode.lastMessageDate = photoMessage.date;
    self.parentNode.lastMessage = [NSString stringWithFormat:@"%@ shared a photo.", self.senderDisplayName];
    
    messageObj.parent = self.parentNode;
    messageObj.sender = self.appDelegate.currentUser;
    messageObj.photo = file;
    [messageObj pinInBackgroundWithName:self.parentNode.objectId];
    [messageObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        [CloudCode pushToChannel:self.otherUser.username withAlert:[NSString stringWithFormat:@"%@: Shared a photo.", self.senderDisplayName] andCategory:@"Message"];
        
    }];
    
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    [self finishSendingMessageAnimated:YES];
    
}

#pragma mark - Helpers

- (void)closePressed {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)findMessages {
    
    PFQuery *query = [BSMessage query];
    //[query whereKey:@"parent" equalTo:self.parentNode];
    //[query includeKey:@"sender"];
    [query fromPinWithName:self.parentNode.objectId];
    [query addDescendingOrder:@"createdAt"];
    [query setLimit:20];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
            // If for some reason the cache gets deleted or a user is on another device, we can load all of the messages.
        if (objects.count == 0) {
            [KVNProgress show];
            [self findNewMessages];
        } else {
            
            if (!error) {
                
                NSMutableArray *posts = [NSMutableArray new];
                
                for (BSMessage *obj in objects) {
                    
                    NSDateComponents *components = [[NSDateComponents alloc] init];
                    components.day = -1;
                    
                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDate *day = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];

                    if (obj.createdAt < day) {
                        [obj unpinWithName:self.parentNode.objectId];
                    } else {
                        [posts addObject:obj];
                    }
                }
                
                for (BSMessage *obj in posts) {
                    
                    BSUser *sender = obj.sender;
                    
                    JSQMessage *message;
                    
                    if (obj.text != nil) {
                        
                        message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username text:obj.text];
                        
                    } else {
                        
                        PFGeoPoint *point = obj.location;
                        
                        JSQLocationMediaItem *locationMediaItem = [JSQLocationMediaItem new];
                        [locationMediaItem setLocation:[[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude] withCompletionHandler:^{
                            
                            [self.collectionView reloadData];
                            
                        }];
                        
                        if (![sender.objectId isEqualToString:self.senderId]) {
                            locationMediaItem.appliesMediaViewMaskAsOutgoing = NO;
                        }
                        
                        message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username media:locationMediaItem];
                        
                    }
                    
                    //NSDate *createdAt = [obj createdAt];
                    
                    //message.date = createdAt;
                    
                    [self.messages insertObject:message atIndex:0];
                    
                }
                
                [self finishReceivingMessageAnimated:YES];
                [self scrollToBottomAnimated:YES];
                // We found everything from cache, now lets look on the network for anything new.
                [self findNewMessages];
            }
            
        }
    
    }];

}

- (void)findPastMessages {
    
    [KVNProgress show];
    
    PFQuery *query = [BSMessage query];
    [query whereKey:@"parent" equalTo:self.parentNode];
    [query includeKey:@"sender"];
    [query addDescendingOrder:@"createdAt"];
    [query setLimit:20];
    [query setSkip:self.messages.count + 1];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error) {
            
            if (objects.count != 20) {
                self.showLoadEarlierMessagesHeader = NO;
            }
            
            for (BSMessage *obj in objects) {
                
                BSUser *sender = obj.sender;
                
                JSQMessage *message;
                
                if (obj.text != nil) {
                    
                    message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username text:obj.text];
                    
                } else if (obj.location != nil) {
                    
                    PFGeoPoint *point = obj.location;
                    
                    JSQLocationMediaItem *locationMediaItem = [JSQLocationMediaItem new];
                    [locationMediaItem setLocation:[[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude] withCompletionHandler:^{
                        
                        [self.collectionView reloadData];
                        
                    }];
                    
                    if (![sender.objectId isEqualToString:self.senderId]) {
                        locationMediaItem.appliesMediaViewMaskAsOutgoing = NO;
                    }
                    
                    message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username media:locationMediaItem];
                    
                } else {
                    
                    PFFile *file = obj.photo;
                    JSQPhotoMediaItem *photoMediaItem = [JSQPhotoMediaItem new];

                    [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                        UIImage *image = [UIImage imageWithData:data];
                        photoMediaItem.image = image;
                        [self.collectionView reloadData];
                        
                    }];
                    
                    if (![sender.objectId isEqualToString:self.senderId]) {
                        photoMediaItem.appliesMediaViewMaskAsOutgoing = NO;
                    }
                    
                    message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username media:photoMediaItem];
                    
                }
                
                //NSDate *createdAt = [obj createdAt];
                
                //message.date = createdAt;
                
                [self.messages insertObject:message atIndex:0];
                
                [obj pinInBackgroundWithName:self.parentNode.objectId];
            }
            
            [KVNProgress dismiss];
            //[self finishReceivingMessageAnimated:YES];
            [self.collectionView reloadData];
            
        } else {
            [KVNProgress dismiss];
        }
        
    }];
    
}

- (void)findNewMessages {
    
        // If we have messages, we only need to find ones we don't have.
    if (self.messages.count > 0) {
        
        JSQMessage *m = [self.messages lastObject];
        
        NSDate *date = [m.date dateByAddingTimeInterval:0.5];
        
        PFQuery *query = [BSMessage query];
        [query whereKey:@"parent" equalTo:self.parentNode];
        [query whereKey:@"createdAt" greaterThan:date];
        [query includeKey:@"sender"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if (!error) {
                
                for (BSMessage *obj in objects) {
                    
                    BSUser *sender = obj.sender;
                    
                    JSQMessage *message;
                    
                    if (obj.text != nil) {
                        
                        message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username text:obj.text];
                   
                    } else if (obj.location != nil) {
                        
                        PFGeoPoint *point = obj.location;
                        
                        JSQLocationMediaItem *locationMediaItem = [JSQLocationMediaItem new];
                        [locationMediaItem setLocation:[[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude] withCompletionHandler:^{
                            
                            [self.collectionView reloadData];
                            
                        }];
                        
                        if (![sender.objectId isEqualToString:self.senderId]) {
                            locationMediaItem.appliesMediaViewMaskAsOutgoing = NO;
                        }
                        
                        message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username media:locationMediaItem];
                        
                    } else {
                        
                        PFFile *file = obj.photo;
                        JSQPhotoMediaItem *photoMediaItem = [JSQPhotoMediaItem new];
                        
                        [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                            UIImage *image = [UIImage imageWithData:data];
                            photoMediaItem.image = image;
                            [self.collectionView reloadData];
                            
                        }];
                        
                        if (![sender.objectId isEqualToString:self.senderId]) {
                            photoMediaItem.appliesMediaViewMaskAsOutgoing = NO;
                        }
                        
                        message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username media:photoMediaItem];
                        
                    }
                    
                    // NSDate *createdAt = [obj createdAt];
                    
                    //message.date = createdAt;
                    
                    [self.messages addObject:message];
                    
                    [obj pinInBackgroundWithName:self.parentNode.objectId];
                }
                
                JSQMessage *message = [self.messages lastObject];
                
                self.parentNode.lastMessageDate = message.date;
                self.parentNode.lastMessage = message.text;
                [self.parentNode saveInBackground];
                
                if (objects.count > 0) {
                    [self finishReceivingMessageAnimated:YES];
                    [self scrollToBottomAnimated:YES];
                }
            } else {
                [KVNProgress dismiss];
            }
            
        }];
        
    } else { // We find all of the messages.
        
        PFQuery *query = [BSMessage query];
        [query whereKey:@"parent" equalTo:self.parentNode];
        //[query whereKey:@"createdAt" greaterThan:date];
        [query includeKey:@"sender"];
        [query addDescendingOrder:@"createdAt"];
        [query setLimit:20];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if (!error) {
                
                if (objects.count != 20) {
                    self.showLoadEarlierMessagesHeader = YES;
                }
                
                for (BSMessage *obj in objects) {
                    
                    BSUser *sender = obj.sender;
                    
                    JSQMessage *message;
                    
                    if (obj.text != nil) {
                        
                        message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username text:obj.text];
                        
                    } else if (obj.location != nil) {
                        
                        PFGeoPoint *point = obj.location;
                        
                        JSQLocationMediaItem *locationMediaItem = [JSQLocationMediaItem new];
                        [locationMediaItem setLocation:[[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude] withCompletionHandler:^{
                            
                            [self.collectionView reloadData];
                            
                        }];
                        
                        if (![sender.objectId isEqualToString:self.senderId]) {
                            locationMediaItem.appliesMediaViewMaskAsOutgoing = NO;
                        }
                        
                        message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username media:locationMediaItem];
                        
                    } else {
                        
                        PFFile *file = obj.photo;
                        JSQPhotoMediaItem *photoMediaItem = [JSQPhotoMediaItem new];
                        
                        [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                            UIImage *image = [UIImage imageWithData:data];
                            photoMediaItem.image = image;
                            [self.collectionView reloadData];
                            
                        }];
                        
                        if (![sender.objectId isEqualToString:self.senderId]) {
                            photoMediaItem.appliesMediaViewMaskAsOutgoing = NO;
                        }
                        
                        message = [JSQMessage messageWithSenderId:sender.objectId displayName:sender.username media:photoMediaItem];
                        
                    }
                    
                    //NSDate *createdAt = [obj createdAt];
                    
                    //message.date = createdAt;
                    
                    [self.messages addObject:message];
                    
                    [obj pinInBackgroundWithName:self.parentNode.objectId];
                }
                
                [KVNProgress dismiss];
                [self finishReceivingMessageAnimated:YES];
                [self scrollToBottomAnimated:YES];
            } else {
                [KVNProgress dismiss];
            }
            
        }];
        
    }
    
}

- (void)callTimeout {
    
    [KVNProgress dismiss];
    
}

@end
