//
//  BSPhotoCollectionViewCell.m
//  Bubble
//
//  Created by Chayel Heinsen on 7/18/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSPhotoCollectionViewCell.h"

@implementation BSPhotoCollectionViewCell

- (void)awakeFromNib {
    if (!self.image) {
        self.image = [[PFImageView alloc] initWithFrame:self.contentView.frame];
        
        if (![self.contentView.subviews containsObject:self.image]) {
            [self.contentView addSubview:self.image];
        }
    }
}

@end
