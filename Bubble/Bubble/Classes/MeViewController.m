//
//  MeViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 1/25/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "MeViewController.h"
#import "CameraViewController.h"
#import "MainTableViewCell.h"

#define MILE 1609

@interface MeViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *postData;
@property (strong, nonatomic) PFQuery *refreshQuery;
@property (strong, nonatomic) SVPulsingAnnotationView *pulsingView;
@property (strong, nonatomic) NSMutableArray *buddies;
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) RangeView *pointsView;

@end

@implementation MeViewController {
    BOOL hasAddedBlur;
    BOOL reloadUser;
}

- (void)viewDidLayoutSubviews {
    
    if (!hasAddedBlur) {
        
        /*CGRect blurRect = CGRectMake(0, self.segment.frame.origin.y + 20, self.segment.frame.size.width, self.segment.frame.size.height);
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:blur];
        blurView.frame = blurRect;
        
        [self.mapView addSubview:blurView];*/
        
        hasAddedBlur = YES;
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainCell"];
    
    self.tableView.estimatedRowHeight = 50;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    //Setup for Location Sharing switch
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(toggleLocationServices:)];
    tapRecognizer.numberOfTapsRequired = 1;
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.postData = [NSMutableArray new];
    
    self.tableView.fetchDelegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.limit = 3;
    
    self.mapView.delegate = self;
    
    self.settings.image = [IonIcons imageWithIcon:ion_ios_settings size:25 color:[UIColor whiteColor]];
    
    self.pointsView = [[RangeView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    UIBarButtonItem *points = [[UIBarButtonItem alloc] initWithCustomView:self.pointsView];
    
    [self.navigationItem setRightBarButtonItem:points];
    
    [self.pointsView addGestureRecognizer:tapRecognizer];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, 125)];

    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubbleLogo"]];
    logo.contentMode = UIViewContentModeScaleAspectFill;
    
    //Set the size of the frame so that
    CGRect frame = CGRectMake([UIScreen mainScreen].applicationFrame.size.width/2, 0, 120, 30);
    logo.frame = frame;
    logo.frame = CGRectOffset(logo.frame, -logo.frame.size.width/2, 30);

    //Footer copyright
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, logo.frame.size.height+35, [[UIScreen mainScreen] applicationFrame].size.width, 20)];
    label.text = @"Bubble Social | Created with love in Orlando, FL";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0];
    
    [footer addSubview:logo];
    [footer addSubview:label];
    
    self.tableView.tableFooterView = footer;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetData) name:@"LogOutUser" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (reloadUser) {
        [[BSUser currentUser] fetchInBackground];
    }
    
    [self setUpUserInformation];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView refresh];
}

#pragma mark - UITableViewDelegate/DataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellAnimation"]) {
        cell.alpha = 0;
        
        [UIView beginAnimations:@"fade" context:nil];
        [UIView setAnimationDuration:0.8];
        cell.alpha = 1;
        [UIView commitAnimations];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section <= self.postData.count && self.postData.count != 0) {
        
        BSPost *post = [self.postData objectAtIndex:indexPath.section];
        
        MainTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MainCell"];
        
        //If the post has a photo
        if (post.photo) {
            
            cell.squareConstraint.active = YES;
            
            //We circumvent the Parse cache and directly access the NSCache to prevent photo flicker
            NSCache *cache = [NSCache new];
            UIImage *image = [cache objectForKey:post.photo.url];
            
            if (!image) {
                //[cell.image setImageWithString:post.authorUsername color:[UIColor bubblePink]];
                cell.image.file = post.photo;
                [cell.image loadInBackground];
            } else {
                cell.image.image = image;
            }
            
        } else {
            cell.image.image = [UIImage new];
            cell.squareConstraint.active = NO;
        }
        
        //The cell text
        cell.text.attributedText = [self attributedMessageFromMessage:post.text];
        
        //Adding the buttons to the footer
        //[cell.like setTarget:self andAction:@selector(likePost:)];
        [cell.comment setTarget:self andAction:@selector(commentPost:)];
        
        //Set the numbers based on the post
        cell.like.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likes];
        cell.comment.commentsCountLabel.text = [NSString stringWithFormat:@"%@", post.comments];
        
        NSMutableArray *likers = [NSMutableArray arrayWithArray:post.likers];
        
        if ([likers containsObject:self.appDelegate.currentUser.username]) {
            cell.like.backgroundColor = [UIColor bubblePink];
        } else {
            cell.like.backgroundColor = [UIColor lightGrayColor];
        }
        
        NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
        
        if ([commenters containsObject:self.appDelegate.currentUser.username]) {
            cell.comment.backgroundColor = [UIColor bubblePink];
        } else {
            cell.comment.backgroundColor = [UIColor lightGrayColor];
        }
        
        return cell;
        
    }
    
    // Just make sureNotificationsBlankCell that we always return something.
    MePageBlankCellTableViewCell *blankCell = [self.tableView dequeueReusableCellWithIdentifier:@"BlankCell"];;
    return blankCell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.postData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    header.backgroundColor = [UIColor whiteColor];
    
    BSPost *post = [self.postData objectAtIndex:section];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, header.frame.size.width, 4)];
    line.backgroundColor = [UIColor flatCloudsColor];
    
    PFImageView *profilePicture = [[PFImageView alloc] initWithFrame:CGRectMake(8, 8, 45, 45)];
    profilePicture.contentMode = UIViewContentModeScaleAspectFit;
    profilePicture.clipsToBounds = YES;
    profilePicture.layer.cornerRadius = profilePicture.frame.size.height / 2;
    
    UILabel *username = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 9, header.frame.size.width - 55, 25)];
    
    username.text = post.authorUsername;
    username.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    username.textColor = [UIColor darkGrayColor];
    
    UILabel *location = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 26, header.frame.size.width - 55, 25)];
    
    location.text = post.locationName;
    location.font = [UIFont systemFontOfSize:14];
    location.textColor = [UIColor lightGrayColor];
    
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(header.frame.size.width, 0, 0, 0)];
    time.text = [CHTime makeTimePassedFromDate:post.createdAt];
    time.font = [UIFont systemFontOfSize:14];
    time.textColor = [UIColor lightGrayColor];
    [time sizeToFit];
    time.frame = CGRectOffset(time.frame, -time.frame.size.width - 10, (header.frame.size.height / 2) - (time.frame.size.height / 2));
    
    profilePicture.file = post.profilePic;
    [profilePicture loadInBackground];
    
    [header addSubview:line];
    [header addSubview:profilePicture];
    [header addSubview:username];
    [header addSubview:location];
    [header addSubview:time];
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - CHPaginatedTableViewDelegate

- (void)tableView:(CHPaginatedTableView *)tableview fetchedItems:(NSArray *)objects {
    
    [self.postData addObjectsFromArray:objects];
    
    [self.tableView reloadData];
    
}

- (PFQuery *)queryForFetch {
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = -1;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *day = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
    
    self.refreshQuery = [BSPost query];
    [self.refreshQuery includeKey:@"author"];
    [self.refreshQuery orderByDescending:@"createdAt"];
    [self.refreshQuery whereKey:@"createdAt" lessThan:day];
    [self.refreshQuery whereKey:@"authorUsername" equalTo:self.appDelegate.currentUser.username];
    
    return self.refreshQuery;
    
}

- (void)pulledToRefresh {
    
    [self.postData removeAllObjects];
    
}

- (void)tableViewInitialized {
    
    UIColor *color = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
    self.tableView.refreshControl.backgroundColor = color;
    
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // Get the current size of the refresh controller
    CGRect refreshBounds = self.tableView.refreshControl.bounds;
    
    // Distance the table has been pulled >= 0
    CGFloat pullDistance = MAX(0.0, -self.tableView.refreshControl.frame.origin.y);
    
    // Half the width of the table
    //CGFloat midX = self.tableView.frame.size.width / 2.0;
    
    // Calculate the width and height of our graphics
    CGFloat bubbleHeight = self.tableView.indicatorView.bounds.size.height;
    CGFloat bubbleHeightHalf = bubbleHeight / 2.0;
    
    //CGFloat bubbleWidth = indicatorView.bounds.size.width;
    //CGFloat bubbleWidthHalf = bubbleWidth / 2.0;
    
    // Calculate the pull ratio, between 0.0-1.0
    //CGFloat pullRatio = MIN( MAX(pullDistance, 0.0), 100.0) / 100.0;
    
    // Set the Y coord of the graphics, based on pull distance
    CGFloat bubbleY = pullDistance / 2.0 - bubbleHeightHalf;
    
    // Calculate the X coord of the graphics
    CGFloat bubbleX = [UIScreen mainScreen].applicationFrame.size.width / 2;
    
    // Set the graphic's frames
    CGRect bubbleFrame = self.tableView.indicatorView.frame;
    bubbleFrame.origin.x = bubbleX;
    bubbleFrame.origin.y = bubbleY;
    
    self.tableView.indicatorView.frame = bubbleFrame;
    
    CGFloat xpoint = self.tableView.indicatorView.frame.size.width / 2;
    
    self.tableView.indicatorView.frame = CGRectOffset(self.tableView.indicatorView.frame, -xpoint, 0);
    
    // Set the enbubbleing view's frames
    refreshBounds.size.height = pullDistance;
    
    [self.tableView.refreshControl addSubview:self.tableView.indicatorView];
    
    [self.tableView.indicatorView startAnimating];
    
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    self.mapView.centerCoordinate = userLocation.location.coordinate;
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, (MILE * 15), (MILE * 15));
    
    [self.mapView setRegion:region animated:NO];
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    static NSString *identifier = @"currentLocation";
    self.pulsingView = (SVPulsingAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (self.pulsingView == nil) {
        self.pulsingView = [[SVPulsingAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        self.pulsingView.annotationColor = self.navigationController.navigationBar.barTintColor;
    }
    
    self.pulsingView.canShowCallout = YES;
    
    return self.pulsingView;
}

#pragma mark - Camera Delegate

- (void)cameraDismissedWithImage:(UIImage *)image {

    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    PFFile *file = [PFFile fileWithName:@"image.jpg" data:imageData];

    self.appDelegate.currentUser.profilePicture = file;
    [self.appDelegate.currentUser saveInBackground];

    self.profilePicture.image = image;

}

- (void)cameraDismissedWithCancel {

}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    // Upload image
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    PFFile *file = [PFFile fileWithName:@"profilePic.jpg" data:imageData];
    
    self.appDelegate.currentUser.profilePicture = file;
    [self.appDelegate.currentUser saveInBackground];
    
    self.profilePicture.image = image;

}

#pragma mark - IBActions

- (IBAction)segmentTapped:(id)sender {
    
    if (self.segment.selectedSegmentIndex == 0) {
        
        [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(removeSelectedSegment) userInfo:nil repeats:NO];
        
        // View Buddies
        [self performSegueWithIdentifier:@"showBuddiesSegue" sender:nil];
        
    } else {
        
        [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(removeSelectedSegment) userInfo:nil repeats:NO];
        
        // View Photos
        PhotosCollectionViewController *photos = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotosCollectionViewController"];
        photos.user = self.appDelegate.currentUser;
        
        [self.navigationController pushViewController:photos animated:YES];
    }
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([[segue destinationViewController] isKindOfClass:[ViewBuddiesViewController class]]) {
        
        ViewBuddiesViewController *buddies = [segue destinationViewController];
        buddies.user = self.appDelegate.currentUser;
        buddies.title = @"My Buddies";
    }
    
}


#pragma mark - Helpers

- (void)removeSelectedSegment {
    
    [self.segment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
}

- (void)setUpUserInformation {
    
    /*// This will appear as the title in the navigation bar
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.shadowColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    self.navigationItem.titleView = label;
    label.text = self.appDelegate.currentUser.username;
    [label sizeToFit];*/
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.navigationItem.title = self.appDelegate.currentUser.username;
    
    UIColor *color = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
    
    self.profilePicture.layer.borderWidth = 3;
    self.profilePicture.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profilePicture.clipsToBounds = YES;
    self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.height / 2;
    [self.profilePicture setImageWithString:self.appDelegate.currentUser.realName color:[UIColor bubblePink]];
    self.profilePicture.file = self.appDelegate.currentUser.profilePicture;
    [self.profilePicture loadInBackground];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeProfilePicture:)];
    [self.profilePicture setGestureRecognizers:@[tap]];
    
    NSString *pops = @"";
    
    if ([self.appDelegate.currentUser.posts integerValue] > 999999) {
        NSInteger v = [self.appDelegate.currentUser.posts integerValue] / 1000000;
        pops = [NSString stringWithFormat:@"%zdM", v];
    } else if ([self.appDelegate.currentUser.posts integerValue] > 9999) {
        NSInteger v = [self.appDelegate.currentUser.posts integerValue] / 1000;
        pops = [NSString stringWithFormat:@"%zdK", v];
    } else {
         pops = [NSString stringWithFormat:@"%zd", [self.appDelegate.currentUser.posts integerValue]];
    }

    self.name.text = self.appDelegate.currentUser.realName;
    self.subtitle.text = [NSString stringWithFormat:@"%@ Pops", pops];
    self.textView.text = self.appDelegate.currentUser.aboutMe;
    
    self.buddies = [NSMutableArray arrayWithArray:self.appDelegate.currentUser.buddies];
    self.photos = [NSMutableArray arrayWithArray:self.appDelegate.currentUser.photos];
    
    //Set segmented control titles
    [self.segment setTitle:[NSString stringWithFormat:@"Buddies • %zd", self.buddies.count] forSegmentAtIndex:0];
    [self.segment setTitle:[NSString stringWithFormat:@"Photos • %zd", self.photos.count] forSegmentAtIndex:1];
    
    self.navigationController.navigationBar.barTintColor = color;
    self.textView.tintColor = color; //Set the selectable color of links in About Me
    self.tableView.refreshControl.backgroundColor = color;
    self.aboutMe.textColor = color;
    self.nameBackground.backgroundColor = color;
 
    if([BSUser currentUser].locationHidden)
        [self.pointsView setLabelValue:[NSString stringWithFormat:@"Off"]];
    else
        [self.pointsView setLabelValue:[NSString stringWithFormat:@"On"]];
    self.pointsView.label.textColor = color;
    
    self.pastPopsBackground.backgroundColor = color;
    
}

- (void)changeProfilePicture:(UIGestureRecognizer *)tap {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Need to change your picture?" message:@"Take a selfie or choose from your library" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {

        [self dismissViewControllerAnimated:NO completion:nil];
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        CameraViewController *camera = [sb instantiateViewControllerWithIdentifier:@"CameraViewController"];
        
        camera.delegate = self;
        
        [self presentViewController:camera animated:YES completion:nil];
                             
    }];
    
    UIAlertAction *photos = [UIAlertAction actionWithTitle:@"Choose from Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        // Create image picker controller
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        // Set source to the camera
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        // Delegate is self
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        
        imagePicker.navigationBar.barTintColor = [CHColorHandler getUIColorFromNSString:[[BSUser currentUser] color]];
        imagePicker.navigationBar.tintColor = [UIColor whiteColor];
        
        [imagePicker.navigationBar setTitleTextAttributes:@{
                                                            NSForegroundColorAttributeName: [UIColor whiteColor]
                                                            }];
        
        // Show image picker
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alert addAction:camera];
    [alert addAction:photos];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)commentPost:(UIGestureRecognizer *)tap {
    
    CGPoint pressPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pressPoint];
    
    PFObject *object = [self.postData objectAtIndex:indexPath.section];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenComment" object:nil userInfo:@{ @"object" : object }];
    
}

- (NSAttributedString *)attributedMessageFromMessage:(NSString *)message {
    
    NSArray *messageWords = [message componentsSeparatedByString:@" "];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@""];
    
    for (NSString *word in messageWords) {
        
        NSDictionary *attributes;
        
        if ([word isEqualToString: @""]) {
            
        } else if ([word characterAtIndex:0] == '@') {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor bubblePink],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"username",
                           @"username" : [word substringFromIndex:1],
                           };
            
        } else if ([word characterAtIndex:0] == '#') {
            attributes = @{
                           NSForegroundColorAttributeName:[UIColor flatEmeraldColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"hashtag",
                           @"hashtag" : [word substringFromIndex:1],
                           };
            
        } else {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor darkGrayColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"normal",
                           };
        }
        
        NSAttributedString *subString = [[NSAttributedString alloc]
                                         initWithString:[NSString stringWithFormat:@"%@ ", word]
                                         attributes:attributes];
        
        [attributedMessage appendAttributedString:subString];
    }
    
    return attributedMessage;
}

- (void)messageTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:nil];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id wordTypes = [textView.attributedText attribute:@"wordType"
                                                  atIndex:characterIndex
                                           effectiveRange:&range];
        
        if ([wordTypes isEqualToString:@"username"]) {
            
            /*NSString *userName = [textView.attributedText attribute:@"username"
             atIndex:characterIndex
             effectiveRange:&range];*/
            
            //TODO: Handle username tap
            
        } else if ([wordTypes isEqualToString:@"hashtag"]) {
            
            NSString *hash = [textView.attributedText attribute:@"hashtag"
                                                        atIndex:characterIndex
                                                 effectiveRange:&range];
            hash = [hash lowercaseString];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenHashtag" object:nil userInfo:@{ @"hashtag" : hash }];
            
        } else {
            /*
             NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
             PFObject *obj = [self.postArray objectAtIndex:indexPath.section];
             commentObjectID = obj.objectId;
             [self performSegueWithIdentifier:@"Comments" sender:self];
             */
        }
    }
}

- (void)toggleLocationServices:(UITapGestureRecognizer *)recognizer {
    
    if ([BSUser currentUser].locationHidden) {
        [BSUser currentUser].locationHidden = NO;
        [self.pointsView setLabelValue:[NSString stringWithFormat:@"On"]];
    }
    else {
        [BSUser currentUser].locationHidden = YES;
        [self.pointsView setLabelValue:[NSString stringWithFormat:@"Off"]];
    }
    
    
}

- (void)resetData {
    reloadUser = YES;
}

@end
