//
//  VerifyPhoneNumberViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/30/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUpViewController.h"

@class Circle, SignUpViewController;
@interface VerifyPhoneNumberViewController : UIViewController <UITextFieldDelegate, SignUpDelegate>

@property (strong, nonatomic) NSString *phoneNumber;

@property (strong, nonatomic) IBOutlet UITextField *code;
@property (strong, nonatomic) IBOutlet Circle *firstCircle;
@property (strong, nonatomic) IBOutlet Circle *secondCircle;
@property (strong, nonatomic) IBOutlet Circle *thirdCircle;
@property (strong, nonatomic) IBOutlet Circle *fourthCircle;
@property (strong, nonatomic) IBOutlet UIButton *invisibleButton;
@property BOOL isSigningUp;

@end
