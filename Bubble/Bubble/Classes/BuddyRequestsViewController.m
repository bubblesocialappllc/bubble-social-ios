//
//  BuddyRequestsViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/23/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BuddyRequestsViewController.h"

@interface BuddyRequestsViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *pendingObjectIds;
@property (strong, nonatomic) NSMutableArray *pending;

@end

@implementation BuddyRequestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.pendingObjectIds = [NSMutableArray arrayWithArray:self.appDelegate.currentUser.pendingBuddies];
    
    PFQuery *query = [BSUser query];
    [query whereKey:@"objectId" containedIn:self.pendingObjectIds];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        self.pending = [NSMutableArray arrayWithArray:objects];
        [self.tableView reloadData];
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BuddyRequestTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"buddyRequestCell"];
    
    if (cell == nil) {
        cell = [[BuddyRequestTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"buddyRequestCell"];
    }
    
    BSUser *user = [self.pending objectAtIndex:indexPath.row];
    
    cell.profilePicture.clipsToBounds = YES;
    cell.profilePicture.layer.cornerRadius = cell.profilePicture.frame.size.height / 2;
    [cell.profilePicture setImageWithString:user.realName color:[UIColor bubblePink]];
    cell.profilePicture.file = user.profilePicture;
    [cell.profilePicture loadInBackground];
    
    cell.username.text = user.username;
    
    UITapGestureRecognizer *acceptTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(accept:)];
    UITapGestureRecognizer *denyTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deny:)];
    
    [cell.acceptView addGestureRecognizer:acceptTap];
    [cell.denyView addGestureRecognizer:denyTap];
    
    cell.acceptImageView.image = [IonIcons imageWithIcon:ion_checkmark_round size:20 color:[UIColor whiteColor]];
    cell.denyImageView.image = [IonIcons imageWithIcon:ion_close_round size:20 color:[UIColor whiteColor]];
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pending.count;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self openUserPageWithSelectedUser:[self.pending objectAtIndex:indexPath.row]];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (void)accept:(UITapGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    BuddyRequestTableViewCell *cell = (BuddyRequestTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    cell.acceptView.gestureRecognizers = nil;
    cell.denyView.gestureRecognizers = nil;
    
    BSUser *user = [self.pending objectAtIndex:indexPath.row];
    
    [CloudCode acceptBuddy:user completion:^(BOOL succeeded, NSError *error) {
        
        if (succeeded) {
            
            if (self.pending.count == 0) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }
        
    }];
    
    [self.appDelegate.currentUser.pendingBuddies removeObject:user.objectId];
    [self.pending removeObject:user];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (void)deny:(UITapGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    BSUser *user = [self.pending objectAtIndex:indexPath.row];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are you sure you want to reject?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Yes!" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [CloudCode denyBuddy:user completion:^(BOOL succeeded, NSError *error) {
           
            if (succeeded) {
                
                if (self.pending.count == 0) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                
            }
            
        }];
        
        [self.appDelegate.currentUser.pendingBuddies removeObject:user.objectId];
        [self.pending removeObject:user];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }];
    
    [alert addActions:@[yes, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (void)openUserPageWithSelectedUser:(BSUser *)selectedUser {
    
    UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
    user.user = selectedUser;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
    
    [self presentViewController:nc animated:YES completion:nil];
    
}

@end
