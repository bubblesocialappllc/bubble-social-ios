//
//  BuddyRequestsViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/23/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuddyRequestTableViewCell.h"
#import "UserViewController.h"

@interface BuddyRequestsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
