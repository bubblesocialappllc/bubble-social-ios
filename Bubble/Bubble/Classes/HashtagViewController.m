//
//  HashtagViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/3/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "HashtagViewController.h"

@interface HashtagViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *postData;
@property (strong, nonatomic) PFQuery *refreshQuery;

@end

@implementation HashtagViewController

- (void)viewDidLayoutSubviews {
    
    [self.navBar setFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.postData = [NSMutableArray new];
    
    self.tableView.fetchDelegate = self;
    
    self.done.image = [IonIcons imageWithIcon:ion_close_round size:20 color:[UIColor whiteColor]];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    self.navTitle.title = [NSString stringWithFormat:@"#%@", self.hashtag];
    self.title = self.navTitle.title;
    self.navigationItem.leftBarButtonItem = self.done;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.tableView refresh];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row <= self.postData.count) {
        
        BSPost *post = [self.postData objectAtIndex:indexPath.row];
        
        if (post.photo != nil && ![post.text isEqualToString:@""]) {
            
            TextImageTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextImageHashtag"];
            
            if (cell == nil) {
                
                cell = [[TextImageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextImageHashtag"];
                
            }
            
            cell.image.clipsToBounds = YES;
            cell.image.layer.cornerRadius = 20;
            cell.image.file = post.profilePic;
            [cell.image loadInBackground];
            
            cell.time.timeLabel.text = [CHTime makeTimePassedFromDate:[post createdAt]];
            
            [cell.postImage setImageWithString:post.authorUsername];
            cell.postImage.file = post.photo;
            [cell.postImage loadInBackground];
            
            cell.username.text = post.authorUsername;
            cell.location.text = post.locationName;
            
            UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageTapped:)];
            
            cell.textView.selectable = YES;
            cell.textView.attributedText = [self attributedMessageFromMessage:post.text];
            [cell.textView addGestureRecognizer:tapped];
            
            for (UIGestureRecognizer *recognizer in cell.textView.gestureRecognizers) {
                if ([recognizer isKindOfClass:[UILongPressGestureRecognizer class]]){
                    recognizer.enabled = NO;
                }
            }
            
            [cell.like setTarget:self andAction:@selector(likePost:)];
            [cell.comment setTarget:self andAction:@selector(commentPost:)];
            
            cell.like.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likes];
            cell.comment.commentsCountLabel.text = [NSString stringWithFormat:@"%@", post.comments];
            
            NSMutableArray *likers = [NSMutableArray arrayWithArray:post.likers];
            
            if ([likers containsObject:self.appDelegate.currentUser.username]) {
                cell.like.backgroundColor = [UIColor bubblePink];
            } else {
                cell.like.backgroundColor = [UIColor lightGrayColor];
            }
            
            NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
            
            if ([commenters containsObject:self.appDelegate.currentUser.username]) {
                cell.comment.backgroundColor = [UIColor bubblePink];
            } else {
                cell.comment.backgroundColor = [UIColor lightGrayColor];
            }
            
            UITapGestureRecognizer *cellTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCell:)];
            [cell addGestureRecognizer:cellTap];
            
            UITapGestureRecognizer *cellLikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(likePost:)];
            
            [cellTap setDelaysTouchesBegan: YES];
            [cellLikeTap setDelaysTouchesBegan: YES];
            
            cellTap.numberOfTapsRequired = 1;
            cellLikeTap.numberOfTapsRequired = 2;
            
            [cellTap requireGestureRecognizerToFail:cellLikeTap];
            
            [cell addGestureRecognizer:cellLikeTap];
            [cell addGestureRecognizer:cellTap];
            
            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellDidLongHold:)];
            [cell addGestureRecognizer:longPress];
            
            return cell;
            
        } else if (post.photo == nil && ![post.text isEqualToString:@""]) {
            
            TextTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextOnlyHashtag"];
            
            if (cell == nil) {
                
                cell = [[TextTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextOnlyHashtag"];
                
            }
            
            cell.image.clipsToBounds = YES;
            cell.image.layer.cornerRadius = 20;
            [cell.image setImageWithString:post.authorUsername];
            cell.image.file = post.profilePic;
            [cell.image loadInBackground];
            
            cell.time.timeLabel.text = [CHTime makeTimePassedFromDate:[post createdAt]];
            
            cell.username.text = post.authorUsername;
            cell.location.text = post.locationName;
            
            UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageTapped:)];
            
            cell.textView.selectable = YES;
            cell.textView.attributedText = [self attributedMessageFromMessage:post.text];
            [cell.textView addGestureRecognizer:tapped];
            
            for (UIGestureRecognizer *recognizer in cell.textView.gestureRecognizers) {
                if ([recognizer isKindOfClass:[UILongPressGestureRecognizer class]]){
                    recognizer.enabled = NO;
                }
            }
            
            [cell.like setTarget:self andAction:@selector(likePost:)];
            [cell.comment setTarget:self andAction:@selector(commentPost:)];
            
            cell.like.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likes];
            cell.comment.commentsCountLabel.text = [NSString stringWithFormat:@"%@", post.comments];
            
            NSMutableArray *likers = [NSMutableArray arrayWithArray:post.likers];
            
            if ([likers containsObject:self.appDelegate.currentUser.username]) {
                cell.like.backgroundColor = [UIColor bubblePink];
            } else {
                cell.like.backgroundColor = [UIColor lightGrayColor];
            }
            
            NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
            
            if ([commenters containsObject:self.appDelegate.currentUser.username]) {
                cell.comment.backgroundColor = [UIColor bubblePink];
            } else {
                cell.comment.backgroundColor = [UIColor lightGrayColor];
            }
            
            UITapGestureRecognizer *cellTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCell:)];
            [cell addGestureRecognizer:cellTap];
            
            UITapGestureRecognizer *cellLikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(likePost:)];
            
            [cellTap setDelaysTouchesBegan: YES];
            [cellLikeTap setDelaysTouchesBegan: YES];
            
            cellTap.numberOfTapsRequired = 1;
            cellLikeTap.numberOfTapsRequired = 2;
            
            [cellTap requireGestureRecognizerToFail:cellLikeTap];
            
            [cell addGestureRecognizer:cellLikeTap];
            [cell addGestureRecognizer:cellTap];
            
            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellDidLongHold:)];
            [cell addGestureRecognizer:longPress];
            
            return cell;
            
        }
        
    }
    
    // Just make sure that we always return something.
    UITableViewCell *nilCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"nilCell"];
    return nilCell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.postData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BSPost *post = [self.postData objectAtIndex:indexPath.row];
    
    if (post.photo != nil && [post.text isEqualToString:@""]) {
        
        return 400;
        
    } else if (post.photo != nil && ![post.text isEqualToString:@""]) {
        
        return 500;
        
    } else if (post.photo == nil && ![post.text isEqualToString:@""]) {
        
        return 190;
        
    }
    
    return 0;
}

#pragma mark - CHPaginatedTableViewDelegate

- (void)tableView:(CHPaginatedTableView *)tableview fetchedItems:(NSArray *)objects {
    
    [self.postData addObjectsFromArray:objects];
    
    [self.tableView reloadData];
    
}

- (PFQuery *)queryForFetch {
    
    self.refreshQuery = [BSPost query];
    [self.refreshQuery includeKey:@"author"];
    [self.refreshQuery orderByDescending:@"createdAt"];
    [self.refreshQuery whereKey:@"hashtag" equalTo:self.hashtag];
    
    return self.refreshQuery;
    
}

- (void)pulledToRefresh {
    
    [self.postData removeAllObjects];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.tableView scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

#pragma mark - IBActions

- (IBAction)done:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - Helpers

- (void)cellDidLongHold:(UILongPressGestureRecognizer *)hole {
    
    CGPoint point = [hole locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.row];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [self.postData removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        [post deleteInBackground];
        
    }];
    
    UIAlertAction *report = [UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        [self reportPost:post];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    if ([post.authorId isEqualToString:self.appDelegate.currentUser.objectId]) {
        [alert addActions:@[delete, cancel]];
    } else {
        [alert addActions:@[report, cancel]];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)tappedCell:(UITapGestureRecognizer *)tap {

    [self commentPost:tap];

}

- (void)refreshedLocation:(NSNotification *)notification {
    
    if (self.postData.count == 0) {
        [self.tableView refresh];
    }
    
}

- (void)likePost:(UIGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    TextTableViewCell *cell = (TextTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.row];
    
    BOOL shouldLike = NO;
    
    if (![post.authorUsername isEqualToString:self.appDelegate.currentUser.username]) {
        
        NSMutableArray *likers = [NSMutableArray new];
        
        [likers setArray:post.likers];
        
        for (NSString *liker in likers) {
            
            if ([liker isEqualToString:self.appDelegate.currentUser.username]) {
                
                shouldLike = NO;
                break;
            } else {
                shouldLike = YES;
            }
            
        }
        
        if (likers.count == 0) {
            shouldLike = YES;
        }
        
        if (shouldLike) {
            
            [self likePost:post label:cell.like.likesCountLabel];
            cell.like.backgroundColor = [UIColor bubblePink];
            [cell.like setEnabled:NO];
            
        } else {
            
            cell.like.backgroundColor = [UIColor lightGrayColor];
            [self unlikePost:post label:cell.like.likesCountLabel];
            [cell.like setEnabled:NO];
            
        }
        
    }
    
}

- (void)commentPost:(UIGestureRecognizer *)tap {
    
    CGPoint pressPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pressPoint];
    
    BSPost *object = [self.postData objectAtIndex:indexPath.row];
    
    CommentsViewController *comments = [CommentsViewController new];
    comments.post = object;
    comments.pushed = YES;
    
    [self.navigationController pushViewController:comments animated:YES];
    
}

- (void)likePost:(BSPost *)post label:(UILabel *)label {
    
    NSMutableArray *hasLikedBeforeCopy = [NSMutableArray arrayWithArray:post.hasLikedBefore];
    
    NSNumber *likes = post.likes;
    likes = [NSNumber numberWithInt:[likes intValue] + 1];
    
    label.text = [NSString stringWithFormat:@"%@", likes];
    
    NSMutableArray *likers = [NSMutableArray new];
    
    [likers setArray:post.likers];
    
    if (likers.count == 0 || likers == nil) {
        likers = [NSMutableArray arrayWithObject:self.appDelegate.currentUser.username];
    } else {
        
        BOOL hasLiked = [likers containsObject:self.appDelegate.currentUser.username];
        
        if (!hasLiked) {
            [likers addObject:self.appDelegate.currentUser.username];
        }
    }
    
    NSMutableArray *hasLikedBefore = [NSMutableArray arrayWithArray:post.hasLikedBefore];
    
    if (hasLikedBefore.count == 0 || hasLikedBefore == nil) {
        hasLikedBefore = [NSMutableArray arrayWithObject:self.appDelegate.currentUser.username];
    } else {
        
        BOOL hasLiked = [hasLikedBefore containsObject:self.appDelegate.currentUser.username];
        
        if (!hasLiked) {
            [hasLikedBefore addObject:self.appDelegate.currentUser.username];
        }
        
    }
    
    NSNumber *points = post.points;
    
    points = [NSNumber numberWithInt:[points intValue] + 1];
    
    post.likers = likers;
    post.hasLikedBefore = hasLikedBefore;
    post.likes = likes;
    post.points = points;
    
    [post saveEventually:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            
            if (![hasLikedBeforeCopy containsObject:self.appDelegate.currentUser.username]) {
                
                [CloudCode pushToChannel:post.authorUsername withAlert:[NSString stringWithFormat:@"%@ liked your post!", self.appDelegate.currentUser.username] andCategory:@"like" andViewToPush:[NSString stringWithFormat:@"bubblesocial://post/%@", post.objectId]];
                
            }
            
        }
        
    }];
    
}

- (void)unlikePost:(BSPost *)post label:(UILabel *)label {
    
    NSNumber *likes = post.likes;
    likes = [NSNumber numberWithInt:[likes intValue] - 1];
    
    label.text = [NSString stringWithFormat:@"%@", likes];
    
    NSMutableArray *likers = [NSMutableArray new];
    
    [likers setArray:post.likers];
    
    BOOL hasLiked = [likers containsObject:self.appDelegate.currentUser.username];
    
    if (hasLiked) {
        [likers removeObject:self.appDelegate.currentUser.username];
    }
    
    NSNumber *points = post.points;
    
    points = [NSNumber numberWithInt:[points intValue] - 1];
    
    post.likers = likers;
    post.likes = likes;
    post.points = points;
    
    [post saveEventually];
    
}

- (NSAttributedString *)attributedMessageFromMessage:(NSString *)message {
    
    NSArray *messageWords = [message componentsSeparatedByString:@" "];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@""];
    
    for (NSString *word in messageWords) {
        
        NSDictionary *attributes;
        
        if ([word isEqualToString: @""]) {
            
        } else if ([word characterAtIndex:0] == '@') {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor bubblePink],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"username",
                           @"username" : [word substringFromIndex:1],
                           };
            
        } else if ([word characterAtIndex:0] == '#') {
            attributes = @{
                           NSForegroundColorAttributeName:[UIColor flatEmeraldColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"hashtag",
                           @"hashtag" : [word substringFromIndex:1],
                           };
            
        } else {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor darkGrayColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"normal",
                           };
        }
        
        NSAttributedString *subString = [[NSAttributedString alloc]
                                         initWithString:[NSString stringWithFormat:@"%@ ", word]
                                         attributes:attributes];
        
        [attributedMessage appendAttributedString:subString];
    }
    
    return attributedMessage;
}

- (void)messageTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:nil];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id wordTypes = [textView.attributedText attribute:@"wordType"
                                                  atIndex:characterIndex
                                           effectiveRange:&range];
        
        if ([wordTypes isEqualToString:@"username"]) {
            
            /*NSString *userName = [textView.attributedText attribute:@"username"
             atIndex:characterIndex
             effectiveRange:&range];*/
            
            //TODO: Handle username tap
            
        } else if ([wordTypes isEqualToString:@"hashtag"]) {
            
            //TODO: Handle hashtag tap
            
        } else {
            
        }
    }
}

- (void)reportPost:(BSPost *)post {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"What's the problem?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *inappropriate = [UIAlertAction actionWithTitle:@"Inappropriate" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *spam = [UIAlertAction actionWithTitle:@"Spamming" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *other = [UIAlertAction actionWithTitle:@"Other" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[inappropriate, spam, other, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
