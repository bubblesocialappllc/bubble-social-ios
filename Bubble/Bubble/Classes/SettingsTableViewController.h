//
//  SettingsTableViewController.h
//  Bubble
//
//  Created by Doug Woodrow on 2/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"
@import VTAcknowledgementsViewController;

@class BreakOutToRefreshView;
@interface SettingsTableViewController : UITableViewController <BreakOutToRefreshDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
