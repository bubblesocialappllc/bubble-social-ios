//
//  ProfileSetupViewContoller.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileSetup2ViewContoller.h"
#import "CameraViewController.h"

@interface ProfileSetupViewContoller : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, CameraDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)addProfilePhoto:(id)sender;
- (IBAction)continueSetup:(id)sender;

@end
