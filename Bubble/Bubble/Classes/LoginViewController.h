//
//  LoginViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/14/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhoneNumberVerificationViewController.h"
#import "SignUpViewController.h"
#import "NewFacebookUserViewController.h"

@protocol LoginDelegate <NSObject>

/**
 *  Notifies the delegate that a user has logged in.
 *
 *  @param user The user that logged in.
 */
- (void)userDidLogin:(BSUser *)user;

@end

@class CHLoginTextField, SignUpViewController;
@interface LoginViewController : UIViewController <UITextFieldDelegate, SignUpDelegate, NewFacebookUserDelegate>

@property (strong, nonatomic) IBOutlet CHLoginTextField *usernameField;
@property (strong, nonatomic) IBOutlet CHLoginTextField *passwordField;
@property (strong, nonatomic) IBOutlet UIButton *facebookLogin;
@property (strong, nonatomic) IBOutlet UIButton *forgotPassword;
@property (weak, nonatomic) id<LoginDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *signUp;

- (IBAction)login:(id)sender;
- (IBAction)forgotPassword:(id)sender;
- (IBAction)signup:(id)sender;
- (IBAction)loginWithFacebook:(id)sender;

@end
