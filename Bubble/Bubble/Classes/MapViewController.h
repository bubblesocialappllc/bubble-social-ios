//
//  MapViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 1/12/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTableViewCell.h"
@import SVPulsingAnnotationView;
#import "MSAlertController+Additions.h"
#import "BSClusterMapView.h"
#import "BubbleAnnotation.h"

@interface MapViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, MONActivityIndicatorViewDelegate, RangeDelegate, TSClusterMapViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *weather;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet BSClusterMapView *mapView;
@property (strong, nonatomic) IBOutlet UILabel *degrees;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UIView *header;
@property (strong, nonatomic) UISegmentedControl *headerSegmentControl;

- (void)sortFeed:(UISegmentedControl *)sender;

@end
