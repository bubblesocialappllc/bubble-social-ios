//
//  PhotosCollectionViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/8/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "PhotosCollectionViewController.h"

@interface PhotosCollectionViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *photos;
@property (nonatomic) NSUInteger skip;
@property (assign, nonatomic, getter=isGrid) BOOL grid;

@end

@implementation PhotosCollectionViewController

static NSUInteger const limit = 1;
static NSString *const reuseIdentifier = @"PhotoCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    self.grid = YES;
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Register cell classes
    //[self.collectionView registerClass:[BSPhotoCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    self.collectionView.backgroundColor = [UIColor flatCloudsColor]; //[CHColorHandler getUIColorFromNSString:self.user.color];
    
    if ([self.user.username isEqualToString:self.appDelegate.currentUser.username]) {
        self.title = @"My Photos";
    } else {
        self.title = @"Photos";
    }
    
    UIBarButtonItem *filter = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_navicon size:30 color:[UIColor whiteColor]] style:UIBarButtonItemStyleDone target:self action:@selector(filter:)];
    filter.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = filter;
    
    self.photos = [NSMutableArray new];
    self.skip = 0; // DO NOT CHANGE THIS!!!!
    
    RFQuiltLayout *layout = (id)self.collectionView.collectionViewLayout;
    layout.delegate = self;
    layout.direction = UICollectionViewScrollDirectionVertical;
    
    layout.blockPixels = CGSizeMake([[UIScreen mainScreen] applicationFrame].size.width / 3, [[UIScreen mainScreen] applicationFrame].size.width / 3);
    
    [self queryForPhotos];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (self.user.objectId == self.appDelegate.currentUser.objectId) {
        return self.photos.count + 1;
    } else {
        return self.photos.count;
    }
   
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BSPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if (self.user.objectId == self.appDelegate.currentUser.objectId) {
        
        if (indexPath.row == 0) {
            cell.image.image = [IonIcons imageWithIcon:ion_android_add_circle size:cell.frame.size.width color:[UIColor whiteColor]];
            cell.backgroundColor = [CHColorHandler getUIColorFromNSString:self.user.color];

        } else {
            
            BSPhoto *photo = [self.photos objectAtIndex:indexPath.row - 1];
            
            [cell.image setImageWithString:self.user.realName color:[CHColorHandler getUIColorFromNSString:self.user.color]];
            
            cell.image.file = photo.photo;
            
            [cell.image loadInBackgroundWithProgress:^(int progress) {
                [cell.image updateImageDownloadProgress:progress];
                
                if (progress == 100) {
                    [cell.image reveal];
                }
            }];
            
        }
        
    } else {
        
        BSPhoto *photo = [self.photos objectAtIndex:indexPath.row];
        
        [cell.image setImageWithString:self.user.realName color:[CHColorHandler getUIColorFromNSString:self.user.color]];
        
        cell.image.file = photo.photo;
        
        [cell.image loadInBackgroundWithProgress:^(int progress) {
            [cell.image updateImageDownloadProgress:progress];
            
            if (progress == 100) {
                [cell.image reveal];
            }
        }];
        
    }
    
    UILongPressGestureRecognizer *hold = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellDidHold:)];
    [cell addGestureRecognizer:hold];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.user.objectId == self.appDelegate.currentUser.objectId) {
        
        if (indexPath.row == 0) {
            [self addPhoto];
        } else {
            
            BSPhotoViewHandler *handler = [[BSPhotoViewHandler alloc] initWithPhotos:self.photos];
            
            EBPhotoPagesController *photoPagesController = [[EBPhotoPagesController alloc] initWithDataSource:handler delegate:handler photoAtIndex:indexPath.row - 1];
            
            [self presentViewController:photoPagesController animated:YES completion:nil];
            
        }
        
    } else {
        
        BSPhotoViewHandler *handler = [[BSPhotoViewHandler alloc] initWithPhotos:self.photos];
        
        EBPhotoPagesController *photoPagesController = [[EBPhotoPagesController alloc] initWithDataSource:handler delegate:handler photoAtIndex:indexPath.row];
        
        [self presentViewController:photoPagesController animated:YES completion:nil];
        
    }
    
}

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

#pragma mark - RFQuiltLayoutDelegate

- (CGSize)blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    /*if (indexPath.row % 2 == 0)
     return CGSizeMake(2, 1);*/
    
    return CGSizeMake(1, 1);
}

- (UIEdgeInsets)insetsForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.grid) {
     return UIEdgeInsetsMake(1, 1, 1, 1);
    }
    
    return UIEdgeInsetsMake(4, 0, 4, 0);
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    // Upload image
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    PFFile *file = [PFFile fileWithName:@"image.jpg" data:imageData];
    
    BSPhoto *photo = [BSPhoto object];
    photo.user = self.user;
    photo.photo = file;
    photo.tags = [NSMutableArray new];
    photo.comments = [NSMutableArray new];
    
    [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (self.user.photos.count == 0) {
            self.user.photos = [NSMutableArray arrayWithObject:photo.objectId];
        } else {
            self.user.photos = [NSMutableArray arrayWithArray:[self.user.photos arrayByAddingObjectsFromArray:@[photo.objectId]]];
        }
        
        [self.user saveInBackground];
        
        // When we are done saving the photo, we insert it into our collection view to display it.
        [self.photos insertObject:photo atIndex:0];
        [self.collectionView reloadData];
        
    }];
    
}


#pragma mark - Camera Delegate

- (void)cameraDismissedWithImage:(UIImage *)image {
    
    // Upload image
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    PFFile *file = [PFFile fileWithName:@"image.jpg" data:imageData];
    
    BSPhoto *photo = [BSPhoto object];
    photo.user = self.user;
    photo.photo = file;
    photo.tags = [NSMutableArray new];
    photo.comments = [NSMutableArray new];
    
    [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (self.user.photos.count == 0) {
            self.user.photos = [NSMutableArray arrayWithObject:photo.objectId];
        } else {
            self.user.photos = [NSMutableArray arrayWithArray:[self.user.photos arrayByAddingObjectsFromArray:@[photo.objectId]]];
        }
        
        [self.user saveInBackground];
        
        // When we are done saving the photo, we insert it into our collection view to display it.
        [self.photos insertObject:photo atIndex:0];
        [self.collectionView reloadData];
        
    }];

}

- (void)cameraDismissedWithCancel {
    
}

#pragma mark - Helpers

- (void)cellDidHold:(UILongPressGestureRecognizer *)hold {
    CGPoint point = [hold locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:point];
    BSPhotoCollectionViewCell *cell = (BSPhotoCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Save photo" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *save = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (cell.image.image) {
            UIImageWriteToSavedPhotosAlbum(cell.image.image, nil, nil, nil);
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:save];
    [alert addAction:cancel];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

- (void)queryForPhotos {
    
    PFQuery *query = [BSPhoto query];
    [query whereKey:@"user" equalTo:self.user];
    [query addDescendingOrder:@"createdAt"];
    //query.limit = limit;
    //query.skip = self.skip;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
       
        [self.photos addObjectsFromArray:objects];
        
        if (objects.count == limit) {
            // There might be more objects in the table. Update the skip value.
            self.skip += limit;
            
            //[self queryForPhotos];
        }
        
        /*
        if (self.photos.count < 100) {
            RFQuiltLayout *layout = (id)self.collectionView.collectionViewLayout;
            layout.prelayoutEverything = YES;
        }
         */
        
        [self.collectionView reloadData];
        
    }];
    
}

- (void)addPhoto {
    
    MSAlertController *alert = [MSAlertController alertControllerWithTitle:nil message:@"Add a new photo!" preferredStyle:MSAlertControllerStyleActionSheet];
    
    MSAlertAction *camera = [MSAlertAction actionWithTitle:@"Camera" style:MSAlertActionStyleDefault handler:^(MSAlertAction *action) {
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        CameraViewController *camera = [sb instantiateViewControllerWithIdentifier:@"CameraViewController"];
        
        camera.delegate = self;
        
        [self presentViewController:camera animated:YES completion:nil];
        
    }];
    
    MSAlertAction *photos = [MSAlertAction actionWithTitle:@"Photos" style:MSAlertActionStyleDefault handler:^(MSAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        // Create image picker controller
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        // Set source to the camera
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        // Delegate is self
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        
        // Show image picker
        // [self presentViewController:imagePicker animated:YES];
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    
    MSAlertAction *cancel = [MSAlertAction actionWithTitle:@"Cancel" style:MSAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[camera, photos, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)filter:(id)sender {
    UIBarButtonItem *filter = (UIBarButtonItem *)sender;
    
    if (self.isGrid) {
        filter.image = [IonIcons imageWithIcon:ion_grid size:30 color:filter.tintColor];
        RFQuiltLayout *layout = (id)self.collectionView.collectionViewLayout;
        layout.blockPixels = CGSizeMake([[UIScreen mainScreen] applicationFrame].size.width / 1, [[UIScreen mainScreen] applicationFrame].size.width / 1);
        
        [self.collectionView.collectionViewLayout invalidateLayout];
        [self.collectionView setCollectionViewLayout:layout animated:YES];
        self.grid = NO;
    } else {
        filter.image = [IonIcons imageWithIcon:ion_navicon size:30 color:filter.tintColor];
        RFQuiltLayout *layout = (id)self.collectionView.collectionViewLayout;
        layout.blockPixels = CGSizeMake([[UIScreen mainScreen] applicationFrame].size.width / 3, [[UIScreen mainScreen] applicationFrame].size.width / 3);
        [self.collectionView.collectionViewLayout invalidateLayout];
        [self.collectionView setCollectionViewLayout:layout animated:YES];
        self.grid = YES;
    }
}

@end
