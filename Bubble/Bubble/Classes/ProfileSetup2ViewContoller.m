//
//  ProfileSetup2ViewContoller.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/16/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "ProfileSetup2ViewContoller.h"

@interface ProfileSetup2ViewContoller ()

@end

@implementation ProfileSetup2ViewContoller {
    CGRect origFrame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.aboutMe.delegate = self;
    self.interests.delegate = self;
    
    self.interests.tintColor = [UIColor bubblePink];
    self.interests.textColor = [UIColor bubblePink];
    self.interests.attributedText = [[NSAttributedString alloc] initWithString:@""];
    
    origFrame = self.view.frame;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextView Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame = CGRectOffset(self.view.frame, 0, -160);
        
    }];
    
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame = origFrame;
        
    }];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@" "]) {
        
        NSArray *components = [textView.text split];
        
        for (__strong NSString *str in components) {
            
            if (str.length > 0) {
                
                if (![[str substringToIndex:1] isEqualToString:@"#"]) {
                    
                    str = [str prependStringWithString:@"#"];
                    [self searchAndReplaceText:[str substringFromIndex:1] withText:str inTextView:textView];
                    
                }
            }
        }
        
    }
    
    return YES;
    
}

#pragma mark - IBActions

- (void)hideKeyboard {
    
    [self.aboutMe resignFirstResponder];
    [self.interests resignFirstResponder];
    
}

- (IBAction)continueSetup:(id)sender {
    
    if ([self.aboutMe hasText]) {
        
        NSArray *interests = [self.interests.text componentsSeparatedByString:@" "];
        
        BSUser *currentUser = [BSUser currentUser];
        
        currentUser.aboutMe = self.aboutMe.text;
        currentUser.tags = [NSMutableArray arrayWithArray:interests];
        
        [currentUser saveInBackground];
        
        ColorPickerViewController *color = [self.storyboard instantiateViewControllerWithIdentifier:@"ColorPickerViewController"];
        
        [self.navigationController pushViewController:color animated:YES];
        
    } else {
        
        [KVNProgress showErrorWithStatus:@"Please tell us something about you"];
        
    }
    
}

#pragma mark - Helpers

- (NSRegularExpression *)regularExpressionWithString:(NSString *)string {
    
    // Create a regular expression
    BOOL isCaseSensitive = NO;
    BOOL isWholeWords = YES;
    
    NSError *error = NULL;
    NSRegularExpressionOptions regexOptions = isCaseSensitive ? 0 : NSRegularExpressionCaseInsensitive;
    
    NSString *placeholder = isWholeWords ? @"\\b%@\\b" : @"%@";
    NSString *pattern = [NSString stringWithFormat:placeholder, string];
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:regexOptions error:&error];
   
    if (error) {
        NSLog(@"Couldn't create regex with given string and options");
    }
    
    return regex;
}

// Search for a searchString and replace it with the replacementString in the given text view with search options
- (void)searchAndReplaceText:(NSString *)searchString withText:(NSString *)replacementString inTextView:(UITextView *)textView {
    
    // Text before replacement
    NSString *beforeText = textView.text;
    
    // Create a range for it. We do the replacement on the whole
    // range of the text view, not only a portion of it.
    NSRange range = NSMakeRange(0, beforeText.length);
    
    // Call the convenient method to create a regex for us with the options we have
    NSRegularExpression *regex = [self regularExpressionWithString:searchString];
    
    // Call the NSRegularExpression method to do the replacement for us
    NSString *afterText = [regex stringByReplacingMatchesInString:beforeText options:0 range:range withTemplate:replacementString];
    NSMutableAttributedString *attributedAfterText = [[NSMutableAttributedString alloc] initWithString:afterText];
    
    // Update UI
    textView.attributedText = attributedAfterText;
}

// Search for a searchString in the given text view
- (void)searchText:(NSString *)searchString inTextView:(UITextView *)textView {
    
    // 1: Range of visible text
   // NSRange visibleRange = NSRangeFromString(textView.text);
    
    // 2: Get a mutable sub-range of attributed string of the text view that is visible
    NSMutableAttributedString *visibleAttributedText = textView.attributedText.mutableCopy;
    
    // Get the string of the attributed text
    //  NSString *visibleText = visibleAttributedText.string;
    
    // 3: Create a new range for the visible text. This is different
    // from visibleRange. VisibleRange is a portion of all textView that is visible, but
    // visibileTextRange is only for visibleText, so it starts at 0 and its length is
    // the length of visibleText
    NSRange visibleTextRange = NSMakeRange(0, textView.text.length);
    
    // 4: Call the convenient method to create a regex for us with the options we have
    NSRegularExpression *regex = [self regularExpressionWithString:searchString];
    
    // 5: Find matches
    NSArray *matches = [regex matchesInString:textView.attributedText.string options:NSMatchingReportProgress range:visibleTextRange];
    
    // 6: Iterate through the matches and change their color
    for (NSTextCheckingResult *match in matches) {
        
        UIColor *color = [CHColorHandler getUIColorFromNSString:[CHColorHandler getRandomColor]];
        
        NSRange matchRange = match.range;
        [visibleAttributedText addAttribute:NSForegroundColorAttributeName value:color range:matchRange];
    }
    
    // 7: Replace the range of the attributed string that we just highlighted
    // First, create a CFRange from the NSRange of the visible range
    CFRange visibleRange_CF = CFRangeMake(0, textView.attributedText.length);
    
    // Get a mutable copy of the attributed text of the text view
    NSMutableAttributedString *textViewAttributedString = textView.attributedText.mutableCopy;
    
    // Replace the visible range
    CFAttributedStringReplaceAttributedString((__bridge CFMutableAttributedStringRef)(textViewAttributedString), visibleRange_CF, (__bridge CFAttributedStringRef)(visibleAttributedText));
    
    // 8: Update UI
    textView.attributedText = textViewAttributedString;
}

@end
