//
//  AroundMeViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "AroundMeViewController.h"
#import "MainTableViewCell.h"
#import "SVPulsingAnnotationView.h"
#import "BubbleAnnotation.h"

static const NSInteger MILE = 1609;

@interface AroundMeViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *postData;
@property (strong, nonatomic) PFQuery *refreshQuery;
@property (strong, nonatomic) NSDate *lastUpdate;


    // Map
@property (strong, nonatomic) PFGeoPoint *usersLocation;
@property CGFloat range;
@property CGFloat rangeMeters;
@property (strong, nonatomic) MKCircle *circle; // Not used
@property (strong, nonatomic) SVPulsingAnnotationView *pulsingView;
@property (strong, nonatomic) BubbleAnnotation *userLocationAnnotation;
@property (strong, nonatomic) UITapGestureRecognizer *mapTap;
@property (strong, nonatomic) UIPinchGestureRecognizer *mapPinch;
@property (strong, nonatomic) MONActivityIndicatorView *indicatorView;
@property (strong, nonatomic) UIView *radiusCircle;
@property (strong, nonatomic) UILabel *radiusLabel;
@end

@implementation AroundMeViewController {

    CGRect mapFrameWhenClosed;
    BOOL mapIsOpen;
    PFGeoPoint *lastlocation;
    CGFloat lastScale;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainCell"];
    
    self.tableView.estimatedRowHeight = 50;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.postData = [NSMutableArray new];
    
    self.tableView.fetchDelegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    self.range = [CHKeychain doubleForKey:@"range"];

    self.rangeMeters = self.range / 0.00062137;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshedLocation:) name:@"RefreshedLocation" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(feedChanged:) name:@"ChangedFeed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self.tableView selector:@selector(refresh) name:@"SuccessfulPost" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(feedChanged:) name:@"bubbleMenuDidAppear" object:nil];

    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, 125)];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubbleLogo"]];
    logo.contentMode = UIViewContentModeScaleAspectFill;
    
    //Set the size of the frame so that
    CGRect frame = CGRectMake([UIScreen mainScreen].applicationFrame.size.width / 2, 0, 120, 30);
    logo.frame = frame;
    logo.frame = CGRectOffset(logo.frame, -logo.frame.size.width / 2, 30);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, logo.frame.size.height+35, [[UIScreen mainScreen] applicationFrame].size.width, 20)];
    label.text = @"Bubble Social | Created with love in Orlando, FL";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0];
    
    [footer addSubview:logo];
    [footer addSubview:label];
    
    self.tableView.tableFooterView = footer;

    // Set up map
    self.mapView = [[BSClusterMapView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.view.frame.size.height / 4)];
    self.usersLocation = self.appDelegate.currentLocation;
    [self.mapView setShowsUserLocation:YES];
    self.tableView.tableHeaderView = self.mapView;

    [self.mapView setScrollEnabled:NO];
    [self.mapView setPitchEnabled:NO];
    [self.mapView setZoomEnabled:NO];
    [self.mapView setRotateEnabled:NO];

    self.mapTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMapTap)];
    [self.mapView addGestureRecognizer:self.mapTap];
    [self.mapView setDelegate:self];

    // Pinch gesture recognizer
    self.mapPinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handleMapPinch:)];
    self.mapPinch.delegate = self;
    [self.mapView addGestureRecognizer:self.mapPinch];

    lastScale = 1;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];

    if ([BSUser currentUser]) {
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [[BSLocationManager sharedManager] updateLocation];
        });
        
    }

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mapFrameWhenClosed = self.mapView.frame;
    });

    self.pulsingView.annotationColor = self.navigationController.navigationBar.barTintColor;

    [self updateRange:[CHKeychain doubleForKey:@"range"]];

    if (!self.range || self.range == 0) {
        [CHKeychain setDouble:8.0 forKey:@"range"];
        self.range = 8.0;
    }

    if ([[self.mapView subviews] containsObject:self.radiusCircle]) {
        [self.radiusCircle removeFromSuperview];
    }

    // Add circle
    CGFloat height = self.mapView.frame.size.height - 10;
    CGFloat width  = self.tableView.frame.size.width;
    UIView *circle = [[UIView alloc] initWithFrame: CGRectMake((width / 2) - (height/2), 5, height, height)];
    circle.layer.cornerRadius = circle.frame.size.height / 2;
    circle.layer.borderColor = [[UIColor bubblePink] CGColor];
    circle.layer.borderWidth = .7;
    circle.backgroundColor = [[UIColor bubblePink] colorWithAlphaComponent:.1];
    self.radiusCircle = circle;

    [self.mapView addSubview: self.radiusCircle];

}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

#pragma mark - Gesture Recognizer Delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{

    [self refreshQuery];
    [self.tableView refresh];
    [self.tableView reloadData];
    
    [UIView animateWithDuration:.5
                          delay:0
         usingSpringWithDamping:.6
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.radiusCircle.transform = CGAffineTransformMakeScale(1, 1);
                     }
                     completion:^(BOOL finished) {

                     }];

}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        self.mapView.centerCoordinate = userLocation.location.coordinate;

    });

}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {

    MKAnnotationView *view;

    if ([annotation isKindOfClass:[BSClusterAnnotation class]]) {
        view = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([BSClusterAnnotation class])];

        if (!view) {
            view = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                reuseIdentifier:NSStringFromClass([BSClusterAnnotation class])];
            view.image = [IonIcons imageWithIcon:ion_ios_person size:10 color:[UIColor bubblePink]];
            view.canShowCallout = YES;
            view.centerOffset = CGPointMake(view.centerOffset.x, -view.frame.size.height/2);
        }

        return view;
    }

    static NSString *currentLocationIdentifier = @"currentLocation";
    /*self.pulsingView = (SVPulsingAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:currentLocationIdentifier];

    if (self.pulsingView == nil) {
        self.pulsingView = [[SVPulsingAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:currentLocationIdentifier];
        //self.pulsingView.annotationColor = self.navigationController.navigationBar.barTintColor;
        self.pulsingView.annotationColor = [UIColor bubblePink];
    }

    self.pulsingView.canShowCallout = YES;*/
    //self.pulsingView.pulseColor = [UIColor clearColor];
    //self.pulsingView.pulseColor = [UIColor bubblePink];

        //TODO: change this to another annotationview. If we dont want it to pulse, lets not use this one
    //self.pulsingView.pulseColor = [UIColor clearColor];


    MKAnnotationView *annotationView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:currentLocationIdentifier];


    if (annotationView == nil) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:currentLocationIdentifier];
        annotationView.image = [[BubbleAnnotation class] imageForAnnotationView];
    }

    CGRect labelFrame = CGRectMake(2.5, 2.5, annotationView.frame.size.width - 5, annotationView.frame.size.height - 5);

    if (self.radiusLabel == nil){
        self.radiusLabel = [[UILabel alloc] initWithFrame: labelFrame];
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.radiusLabel setText:[NSString stringWithFormat:@"%d", (int)self.range]];
        self.radiusLabel.textColor = [UIColor bubblePink];
        self.radiusLabel.adjustsFontSizeToFitWidth = YES;
        self.radiusLabel.font = [UIFont boldSystemFontOfSize:12];
        self.radiusLabel.textAlignment = NSTextAlignmentCenter;
        self.radiusLabel.alpha = 1;
    });

    [annotationView addSubview:self.radiusLabel];
    [annotationView bringSubviewToFront:self.radiusLabel];

    return annotationView;

}

- (MKAnnotationView *)mapView:(TSClusterMapView *)mapView viewForClusterAnnotation:(id<MKAnnotation>)annotation {

    BSClusteredAnnotation *view = (BSClusteredAnnotation *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([BSClusteredAnnotation class])];

    if (!view) {
        view = [[BSClusteredAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:NSStringFromClass([BSClusteredAnnotation class])];
    }

    return view;

}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {

    MKCircle *circleOverlay = (MKCircle *)overlay;
    MKCircleRenderer *circleView = [[MKCircleRenderer alloc] initWithCircle:circleOverlay];

    UIColor *color = [UIColor bubblePink];

    circleView.strokeColor = color;
    circleView.fillColor = [color colorWithAlphaComponent:0.1];
    circleView.lineWidth = 0.7;
    circleView.lineDashPattern = @[@3, @4];

    return circleView;
}


#pragma mark - UITableViewDelegate/DataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellAnimation"]) {
        cell.alpha = 0;
        
        [UIView beginAnimations:@"fade" context:nil];
        [UIView setAnimationDuration:0.8];
        cell.alpha = 1;
        [UIView commitAnimations];
    }
    
    if (indexPath.section == self.postData.count - 1) {
        [self.tableView paginate];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section <= self.postData.count && self.postData.count != 0) {
        
        BSPost *post = [self.postData objectAtIndex:indexPath.section];
        
        MainTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MainCell"];
        
        //If the post has a photo
        if (post.photo) {
            
            cell.squareConstraint.active = YES;
            
            //We circumvent the Parse cache and directly access the NSCache to prevent photo flicker
            NSCache *cache = [NSCache new];
            UIImage *image = [cache objectForKey:post.photo.url];
            
            if (!image) {
                //[cell.image setImageWithString:post.authorUsername color:[UIColor bubblePink]];
                cell.image.file = post.photo;
                [cell.image loadInBackground];
            } else {
                cell.image.image = image;
            }
            
        } else {
            cell.image.image = [UIImage new];
            cell.squareConstraint.active = NO;
        }
        
        //The cell text
        cell.text.attributedText = [self attributedMessageFromMessage:post.text];
        
        //Adding the buttons to the footer
        [cell.like setTarget:self andAction:@selector(likePost:)];
        [cell.comment setTarget:self andAction:@selector(commentPost:)];

        //Set the numbers based on the post
        cell.like.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likes];
        cell.comment.commentsCountLabel.text = [NSString stringWithFormat:@"%@", post.comments];
        
        NSMutableArray *likers = [NSMutableArray arrayWithArray:post.likers];
        
        if ([likers containsObject:self.appDelegate.currentUser.username]) {
            cell.like.backgroundColor = [UIColor bubblePink];
        } else {
            cell.like.backgroundColor = [UIColor lightGrayColor];
        }
        
        NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
        
        if ([commenters containsObject:self.appDelegate.currentUser.username]) {
            cell.comment.backgroundColor = [UIColor bubblePink];
        } else {
            cell.comment.backgroundColor = [UIColor lightGrayColor];
        }
        
        //Set up for cell clicks
        UITapGestureRecognizer *cellTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCell:)];
        UITapGestureRecognizer *cellLikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(likePost:)];
        
        [cellTap setDelaysTouchesBegan:YES];
        [cellLikeTap setDelaysTouchesBegan:YES];
        
        cellTap.numberOfTapsRequired = 1;
        cellLikeTap.numberOfTapsRequired = 2;
        
        [cellTap requireGestureRecognizerToFail:cellLikeTap];
        
        [cell addGestureRecognizer:cellLikeTap];
        [cell addGestureRecognizer:cellTap];
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellDidLongHold:)];
        [cell addGestureRecognizer:longPress];
        
        return cell;
        
    }
    
    // Just make sureNotificationsBlankCell that we always return something.
    MePageBlankCellTableViewCell *blankCell = [self.tableView dequeueReusableCellWithIdentifier:@"AroundMeBlankCell"];;
    return blankCell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.postData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    header.backgroundColor = [UIColor whiteColor];
    
    BSPost *post = [self.postData objectAtIndex:section];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, header.frame.size.width, 4)];
    line.backgroundColor = [UIColor flatCloudsColor];
    
    PFImageView *profilePicture = [[PFImageView alloc] initWithFrame:CGRectMake(8, 8, 45, 45)];
    profilePicture.contentMode = UIViewContentModeScaleAspectFit;
    profilePicture.clipsToBounds = YES;
    profilePicture.layer.cornerRadius = profilePicture.frame.size.height / 2;
    
    UILabel *username = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 9, header.frame.size.width - 55, 25)];
    
    username.text = post.authorUsername;
    username.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    username.textColor = [UIColor darkGrayColor];
    username.tag = 1;

    UILabel *location = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 26, header.frame.size.width - 55, 25)];
    
    location.text = post.locationName;
    location.font = [UIFont systemFontOfSize:14];
    location.textColor = [UIColor lightGrayColor];
    
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(header.frame.size.width, 0, 0, 0)];
    time.text = [CHTime makeTimePassedFromDate:post.createdAt];
    time.font = [UIFont systemFontOfSize:14];
    time.textColor = [UIColor lightGrayColor];
    [time sizeToFit];
    time.frame = CGRectOffset(time.frame, -time.frame.size.width - 10, (header.frame.size.height / 2) - (time.frame.size.height / 2));
    
    profilePicture.file = post.profilePic;
    [profilePicture loadInBackground];
    
    [header addSubview:line];
    [header addSubview:profilePicture];
    [header addSubview:username];
    [header addSubview:location];
    [header addSubview:time];
    [header setUserInteractionEnabled:YES];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapHeader:)];
    [header addGestureRecognizer:tap];

    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

#pragma mark - CHPaginatedTableViewDelegate

- (void)tableView:(CHPaginatedTableView *)tableview fetchedItems:(NSArray *)objects {
    
    NSInteger beforeCount = self.postData.count;
    
    for (BSPost *post in objects) {
        
        if (![self.postData containsObject:post]) {
            [self.postData addObject:post];
        }
    }
    
    NSInteger afterCount = self.postData.count;
    
    if (afterCount != beforeCount) {
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdAt"
                                                     ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        self.postData = [[self.postData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        
        [tableview reloadData];
    }
}

- (PFQuery *)queryForFetch {
    
    double range = [CHKeychain doubleForKey:@"range"];
    
    if (range > 10) {
        range = 10;
    }
    
    self.refreshQuery = [BSPost query];
    [self.refreshQuery includeKey:@"author"];
    
    /*
    //Only query for new posts
    if (self.lastUpdate != nil) {
         [self.refreshQuery whereKey:@"updatedAt" greaterThan:self.lastUpdate];
    }
     */
    
    if (self.appDelegate.currentLocation) {
        [self.refreshQuery whereKey:@"location" nearGeoPoint:self.appDelegate.currentLocation withinMiles:range]; //Queries the posts near you
    } else {
        [self.refreshQuery whereKey:@"location" nearGeoPoint:[PFGeoPoint geoPoint] withinMiles:range]; //Queries the posts near you
    }
    
    if (self.appDelegate.currentUser.blocked != nil) {
        [self.refreshQuery whereKey:@"authorId" notContainedIn:self.appDelegate.currentUser.blocked]; //Removes blocked users from the post array
    }
    
    [self.refreshQuery orderByDescending:@"createdAt"]; //Orders the posts by created at date
    
    //self.lastUpdate = [NSDate date]; //Sets the current time to the last updated variable for pulling only new posts next time we call the server
    
    return self.refreshQuery;
    
}

- (void)pulledToRefresh {
    
    //[self.postData removeAllObjects];
    
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // Get the current size of the refresh controller
    CGRect refreshBounds = self.tableView.refreshControl.bounds;
    
    // Distance the table has been pulled >= 0
    CGFloat pullDistance = MAX(0.0, -self.tableView.refreshControl.frame.origin.y);
    
    // Half the width of the table
    //CGFloat midX = self.tableView.frame.size.width / 2.0;
    
    // Calculate the width and height of our graphics
    CGFloat bubbleHeight = self.tableView.indicatorView.bounds.size.height;
    CGFloat bubbleHeightHalf = bubbleHeight / 2.0;
    
    //CGFloat bubbleWidth = indicatorView.bounds.size.width;
    //CGFloat bubbleWidthHalf = bubbleWidth / 2.0;
    
    // Calculate the pull ratio, between 0.0-1.0
    //CGFloat pullRatio = MIN( MAX(pullDistance, 0.0), 100.0) / 100.0;
    
    // Set the Y coord of the graphics, based on pull distance
    CGFloat bubbleY = pullDistance / 2.0 - bubbleHeightHalf;
    
    // Calculate the X coord of the graphics
    CGFloat bubbleX = [UIScreen mainScreen].applicationFrame.size.width / 2;
    
    // Set the graphic's frames
    CGRect bubbleFrame = self.tableView.indicatorView.frame;
    bubbleFrame.origin.x = bubbleX;
    bubbleFrame.origin.y = bubbleY;
    
    self.tableView.indicatorView.frame = bubbleFrame;
    
    CGFloat xpoint = self.tableView.indicatorView.frame.size.width / 2;
    
    self.tableView.indicatorView.frame = CGRectOffset(self.tableView.indicatorView.frame, -xpoint, 0);
    
    // Set the enbubbleing view's frames
    refreshBounds.size.height = pullDistance;
    
    [self.tableView.refreshControl addSubview:self.tableView.indicatorView];
    
    [self.tableView.indicatorView startAnimating];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.tableView scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

#pragma mark - Helpers

- (void)didTapHeader:(UITapGestureRecognizer *)gr {
    UILabel *label;

    for (UIView *v in gr.view.subviews) {
       
        if ([v isKindOfClass:[UILabel class]]) {
        
            if (v.tag == 1) {
                label = (UILabel *)v;
            }
        }
    }


    // Get username associated with this post
    NSString *username = label.text;
    BSUser *user;

    // Get user associated with username
    for (BSPost *post in self.postData) {
        
        if ([username isEqualToString:post.authorUsername]) {
            user = post.author;
        }
    }

    if (user) {
        
        if (![user.username isEqualToString:self.appDelegate.currentUser.username]) {

            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : user }];
            
        }
    }
}

/**
 *  Redraws the circle that shows the radius.
 *
 *  @param performRadiusUpdate YES if the radius is to be calculated and updated. NO otherwise
 */
- (void)drawRadiusCircle:(BOOL)performRadiusUpdate {


    if (performRadiusUpdate) {
        // Get region span
        MKCoordinateSpan span = self.mapView.region.span;

        // Get region center
        CLLocationCoordinate2D center = self.mapView.region.center;

        // Get latitude in meters
        CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:(center.latitude - span.latitudeDelta * 0.5) longitude:center.longitude];
        CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:(center.latitude + span.latitudeDelta * 0.5) longitude:center.longitude];

        int metersLatitude = [loc1 distanceFromLocation:loc2];

        if (metersLatitude / 2 > (10 / .00062137)){
            self.range = 10;
            self.rangeMeters = self.range / .00062137;
        } else {
            self.rangeMeters = metersLatitude / 2;
            self.range = self.rangeMeters * .00062137;
        }
        
        [CHKeychain setDouble:self.range forKey:@"range"];
        NSLog(@"Range changed to %lf", self.range);


    }

//    if (self.range < 1)
//        [self.rangeView setLabelValue:[NSString stringWithFormat:@"%.2lf", self.range]];
//    else
//        [self.rangeView setLabelValue:[NSString stringWithFormat:@"%i", (int)self.range]];

    MKUserLocation *userLocation = self.mapView.userLocation;

    NSMutableArray *overlays = [[self.mapView overlays] mutableCopy];

    dispatch_async(dispatch_get_main_queue(), ^{
//        if ([overlays containsObject:self.circle]) {
//
//            [overlays removObject:self.circle];
//            [self.mapView removeOverlay:self.circle];
//
//        }

        for (id<MKOverlay> object in [overlays mutableCopy]) {
            if ([object isKindOfClass:[MKCircle class]]) {
                [overlays removeObject:object];
                [self.mapView removeOverlay:object];
            }
        }

        self.circle = [MKCircle circleWithCenterCoordinate:userLocation.location.coordinate radius:self.rangeMeters];
        
        [self.mapView addOverlay:self.circle];
    });

}

- (void)setMapRegionAnimated: (BOOL)animated {

    MKUserLocation *userLocation = self.mapView.userLocation;

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, (self.rangeMeters * 2) + (MILE * (self.range / 5)), (self.rangeMeters * 2) + (MILE * (self.range / 5)));

    /*
     The idea behind the    + (MILE * (self.range / 5))    is that by doing this, the circle inside the region will
     have a seemingly constant distance to the top and bottom. Every time the region is calculated, the amount of extra
     space in the region is calculated based on the range itself, so its always proportionate.
     */

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mapView setRegion:region animated:animated];
    });
}

- (void)handleMapPinch:(UIPinchGestureRecognizer*)pinchRecognizer {

    if (lastScale - pinchRecognizer.scale > 0) {
            // Getting smaller -> Scale the circle as long as it leaves the range greater than 1/4 mile
        if (self.range * pinchRecognizer.scale > .25) {
            self.radiusCircle.transform = CGAffineTransformMakeScale(pinchRecognizer.scale, pinchRecognizer.scale);
            [self.radiusCircle setCenter:[self.mapView center]];
        }

    } else if (lastScale - pinchRecognizer.scale < 0){
            // Getting bigger -> Scale the map
        if (self.range * pinchRecognizer.scale < 10) {
            self.radiusCircle.transform = CGAffineTransformMakeScale(pinchRecognizer.scale, pinchRecognizer.scale);
            [self.radiusCircle setCenter:[self.mapView center]];
        }

    }

        // If pinch scale is positive, zoom the map out

    if (pinchRecognizer.state == UIGestureRecognizerStateEnded) {
        self.range = self.range * pinchRecognizer.scale;
        [self updateRange:self.range];
    }


    lastScale = pinchRecognizer.scale;

    [self.tableView refresh];
}

- (void) updateRangeLabel {

    NSString *labelText;

    if (self.range < 1) {
        labelText = @"3/4";
    }

    if (self.range < .75) {
        labelText = @"1/2";
    }

    if (self.range < .5) {
        labelText = @"1/4";
    }


    dispatch_async(dispatch_get_main_queue(), ^{
        if (labelText) {
            [self.radiusLabel setText: labelText];
        } else {
            [self.radiusLabel setText: [NSString stringWithFormat:@"%d", (int)self.range]];
        }

        [self.radiusLabel setNeedsDisplay];
    });


}

- (void) updateRange:(CGFloat)newRange {

        // Sanatize range

    self.range = newRange;

    if (self.range > 10) {
        self.range = 10;
    }

    if (self.range < .25) {
        self.range = .25;
    }

    self.rangeMeters = self.range / 0.00062137;

        // Set the range in the keychain
    [CHKeychain setDouble:self.range forKey:@"range"];

        // Change region of the map to fit the circle nicely
    [self setMapRegionAnimated:YES];

    [self updateRangeLabel];

        // If the range isnt the .25 or 10, the upper and lower bounds, then we should animate the circle out
    if (self.range != .25 && self.range != 10) {
        [UIView animateWithDuration:.3 animations:^{
            self.radiusCircle.transform = CGAffineTransformMakeScale(.01, .01);
        }];
    }

}

- (void) handleMapTap {

    // If the map is open, close it. Otherwise, open it
    mapIsOpen ? [self closeMap] : [self openMap];

}

- (void)openMap {

    self.mapView.userTrackingMode = MKUserTrackingModeNone;

    // Get new frame
    CGRect newMapFrame = CGRectMake(0, 0, self.tableView.frame.size.width, self.view.frame.size.height);
    UIView *tableHeaderView = self.tableView.tableHeaderView;

    CGFloat circleHeight = newMapFrame.size.height;
    CGFloat circleWidth  = self.tableView.frame.size.width;
    CGRect newCircleFrame = CGRectMake((circleWidth / 2) - (circleWidth - 10)/2, (circleHeight - (circleWidth-10))/2, circleWidth-10, circleWidth-10);

    [UIView animateWithDuration:0.2 animations:^{
        tableHeaderView.frame = newMapFrame;
        self.tableView.tableHeaderView = tableHeaderView;
        self.mapView.frame = newMapFrame;
        self.radiusCircle.center = self.mapView.center;
        self.radiusCircle.frame = newCircleFrame;
        self.radiusCircle.layer.cornerRadius = newCircleFrame.size.height /2;
    } completion:^(BOOL finished) {

        // Redraw the circle annotation & map region
        [self setMapRegionAnimated:YES];

        // Disable scrolling
        [self.tableView setScrollEnabled:NO];

        mapIsOpen = YES;
    }];


}

- (void)closeMap {

    self.mapView.userTrackingMode = MKUserTrackingModeFollow;

    // Get new frame
    CGRect newMapFrame = mapFrameWhenClosed;

    // Get reference to header
    UIView *tableHeaderView = self.tableView.tableHeaderView;

        // Circle Frame
    CGFloat circleHeight = newMapFrame.size.height - 10;
    CGFloat circleWidth  = self.tableView.frame.size.width;
    CGRect newCircleFrame = CGRectMake((circleWidth / 2) - (circleHeight/2), 5, circleHeight, circleHeight);

    [UIView animateWithDuration:0.2 animations:^{

        // Shrink the map
        self.mapView.frame = newMapFrame;
        self.radiusCircle.center = self.mapView.center;
        self.radiusCircle.frame = newCircleFrame;
        self.radiusCircle.layer.cornerRadius = self.radiusCircle.frame.size.height /2;

    } completion:^(BOOL finished) {

        // Shrink the header
        tableHeaderView.frame = newMapFrame;
        self.tableView.tableHeaderView = tableHeaderView;

        // Redraw the circle annotation & map region
        [self setMapRegionAnimated:YES];

        // Enable scrolling
        [self.tableView setScrollEnabled:YES];
        
        mapIsOpen = NO;;
    }];
    
}

- (void)cellDidLongHold:(UILongPressGestureRecognizer *)hole {

    CGPoint point = [hole locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    MainTableViewCell *cell = (MainTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        //There is a conflict here with deleting Pops and having the animation for Pops, we need to come back to this
        [self.postData removeObjectAtIndex:indexPath.section];
        [self.tableView reloadData];
        
        //[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        
        [post deleteInBackground];
        
    }];
    
    UIAlertAction *report = [UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        [self reportPost:post];
        
    }];
    
    UIAlertAction *save = [UIAlertAction actionWithTitle:@"Save Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (cell.image.image && post.photo) {
            UIImageWriteToSavedPhotosAlbum(cell.image.image, nil, nil, nil);
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    if ([post.authorId isEqualToString:self.appDelegate.currentUser.objectId]) {
        [alert addAction:delete];
        
        if (cell.image.image && post.photo) {
            [alert addAction:save];
        }
        
        [alert addAction:cancel];
        
    } else {
        [alert addAction:report];
        
        if (cell.image.image && post.photo) {
            [alert addAction:save];
        }
        
        [alert addAction:cancel];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)tappedCell:(UITapGestureRecognizer *)tap {
    
    CGPoint point = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];

    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    if (post.photo != nil) {
        [self commentPostWithImageViewer:tap];
    } else {
        [self commentPost:tap];
    }
    
}

- (void)feedChanged:(NSNotification *)notification {
    
    int page = 1;
    
    if ([notification.userInfo[@"page"] intValue] == page) {
        
        if (self.appDelegate.currentLocation != nil) {
            [self.tableView refresh];
        }
        
    }

    [self updateRange:[CHKeychain doubleForKey:@"range"]];

}

- (void)refreshedLocation:(NSNotification *)notification {

    if (lastlocation != nil && lastlocation == self.appDelegate.currentLocation) {
        return;
    }

    if (self.postData.count == 0) {
        [self.tableView refresh];
    }

    [self setMapRegionAnimated:YES];
    //[self drawRadiusCircle:NO];

    lastlocation = self.appDelegate.currentLocation;
}

- (void)openUser:(UIGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    BSUser *selectedUser = post.author;
    
    if (![selectedUser.username isEqualToString:self.appDelegate.currentUser.username]) {
        
         [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : selectedUser }];
        
        /*UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
        user.user = selectedUser;
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
        
        [self presentViewController:nc animated:YES completion:nil];*/
        
    }
    
}

- (void)likePost:(UIGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    TextTableViewCell *cell = (TextTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    BOOL shouldLike = NO;
    
    if (![post.authorUsername isEqualToString:self.appDelegate.currentUser.username]) {
        
        NSMutableArray *likers = [NSMutableArray new];
        
        [likers setArray:post.likers];
        
        for (NSString *liker in likers) {
            
            if ([liker isEqualToString:self.appDelegate.currentUser.username]) {
                
                shouldLike = NO;
                break;
            } else {
                shouldLike = YES;
            }
            
        }
        
        if (likers.count == 0) {
            shouldLike = YES;
        }
        
        if (shouldLike) {
            
            [self likePost:post label:cell.like.likesCountLabel];
            cell.like.backgroundColor = [UIColor bubblePink];
            [cell.like setEnabled:NO];
            
        } else {
            
            cell.like.backgroundColor = [UIColor lightGrayColor];
            [self unlikePost:post label:cell.like.likesCountLabel];
            [cell.like setEnabled:NO];
            
        }
        
    }
    
}

- (void)commentPost:(UIGestureRecognizer *)tap {
    
    CGPoint pressPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pressPoint];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenComment" object:nil userInfo:@{ @"object" : post }];
    
}

- (void)commentPostWithImageViewer:(UIGestureRecognizer *)tap {
    
    CGPoint pressPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pressPoint];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenCommentWithImageViewer" object:nil userInfo:@{ @"object" : post }];
}

- (void)likePost:(BSPost *)post label:(UILabel *)label {
    
    NSMutableArray *hasLikedBeforeCopy = [NSMutableArray arrayWithArray:post.hasLikedBefore];
    
    NSNumber *likes = post.likes;
    likes = [NSNumber numberWithInt:[likes intValue] + 1];
    
    label.text = [NSString stringWithFormat:@"%@", likes];
    
    NSMutableArray *likers = [NSMutableArray new];
    
    [likers setArray:post.likers];
    
    if (likers.count == 0 || likers == nil) {
        likers = [NSMutableArray arrayWithObject:self.appDelegate.currentUser.username];
    } else {
        
        BOOL hasLiked = [likers containsObject:self.appDelegate.currentUser.username];
        
        if (!hasLiked) {
            [likers addObject:self.appDelegate.currentUser.username];
        }
    }
    
    NSMutableArray *hasLikedBefore = [NSMutableArray arrayWithArray:post.hasLikedBefore];
    
    if (hasLikedBefore.count == 0 || hasLikedBefore == nil) {
        hasLikedBefore = [NSMutableArray arrayWithObject:self.appDelegate.currentUser.username];
    } else {
        
        BOOL hasLiked = [hasLikedBefore containsObject:self.appDelegate.currentUser.username];
        
        if (!hasLiked) {
            [hasLikedBefore addObject:self.appDelegate.currentUser.username];
        }
        
    }
    
    NSNumber *points = post.points;
    
    points = [NSNumber numberWithInt:[points intValue] + 1];
    
    post.likers = likers;
    post.hasLikedBefore = hasLikedBefore;
    post.likes = likes;
    post.points = points;
    
    [post saveEventually:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            
            if (![hasLikedBeforeCopy containsObject:self.appDelegate.currentUser.username]) {
               
                [CloudCode pushToChannel:post.authorUsername withAlert:[NSString stringWithFormat:@"%@ liked your post!", self.appDelegate.currentUser.username] andCategory:@"like" andViewToPush:[NSString stringWithFormat:@"bubblesocial://post/%@", post.objectId]];
                [CloudCode setNotification:post.objectId andType:[BSNotificationType BSLikeNotification]];
                
            }
            
        }
        
    }];
    
}

- (void)unlikePost:(BSPost *)post label:(UILabel *)label {
    
    NSNumber *likes = post.likes;
    likes = [NSNumber numberWithInt:[likes intValue] - 1];
    
    label.text = [NSString stringWithFormat:@"%@", likes];
    
    NSMutableArray *likers = [NSMutableArray new];
    
    [likers setArray:post.likers];
    
    BOOL hasLiked = [likers containsObject:self.appDelegate.currentUser.username];
    
    if (hasLiked) {
        [likers removeObject:self.appDelegate.currentUser.username];
    }
    
    NSNumber *points = post.points;
    
    points = [NSNumber numberWithInt:[points intValue] - 1];
    
    post.likers = likers;
    post.likes = likes;
    post.points = points;
    
    [post saveEventually];
    
}

- (NSAttributedString *)attributedMessageFromMessage:(NSString *)message {
    
    NSArray *messageWords = [message componentsSeparatedByString:@" "];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@""];
    
    for (NSString *word in messageWords) {
       
        NSDictionary *attributes;
        
        if ([word isEqualToString: @""]) {
            
        } else if ([word characterAtIndex:0] == '@') {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor bubblePink],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"username",
                           @"username" : [word substringFromIndex:1],
                           };
            
        } else if ([word characterAtIndex:0] == '#') {
            attributes = @{
                           NSForegroundColorAttributeName:[UIColor flatEmeraldColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"hashtag",
                           @"hashtag" : [word substringFromIndex:1],
                           };
            
        } else {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor darkGrayColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"normal",
                           };
        }
        
        NSAttributedString *subString = [[NSAttributedString alloc]
                                         initWithString:[NSString stringWithFormat:@"%@ ", word]
                                         attributes:attributes];
        
        [attributedMessage appendAttributedString:subString];
    }
    
    return attributedMessage;
}

- (void)messageTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:location
                                    inTextContainer:textView.textContainer
                                    fractionOfDistanceBetweenInsertionPoints:nil];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id wordTypes = [textView.attributedText attribute:@"wordType"
                                                atIndex:characterIndex
                                                effectiveRange:&range];
        
        if ([wordTypes isEqualToString:@"username"]) {
            
            NSString *userName = [textView.attributedText attribute:@"username"
                                                          atIndex:characterIndex
                                                          effectiveRange:&range];
            
            PFQuery *userQuery = [BSUser query];
            [userQuery whereKey:@"username" equalTo:userName];
            [userQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : object }];
                
            }];
            
        } else if ([wordTypes isEqualToString:@"hashtag"]) {
            
            NSString *hash = [textView.attributedText attribute:@"hashtag"
                                                      atIndex:characterIndex
                                                      effectiveRange:&range];
            hash = [hash lowercaseString];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenHashtag" object:nil userInfo:@{ @"hashtag" : hash }];
            
        } else {
            [self tappedCell:recognizer];
        }
    }
}

- (UIImage *)squareImageFromImage:(UIImage *)image {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    CGFloat screenWidth = [UIScreen mainScreen].applicationFrame.size.width;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = screenWidth / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = screenWidth / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(screenWidth, screenWidth);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)reportPost:(BSPost *)post {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"What's the problem?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *inappropriate = [UIAlertAction actionWithTitle:@"Inappropriate" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *spam = [UIAlertAction actionWithTitle:@"Spamming" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *other = [UIAlertAction actionWithTitle:@"Other" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[inappropriate, spam, other, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
