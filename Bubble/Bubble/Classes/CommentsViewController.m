//
//  CommentsViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/23/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "CommentsViewController.h"

#define kMinimumHeight 94.0
#define kAvatarSize 50

@interface CommentsViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *searchResult;
@property (strong, nonatomic) NSMutableArray *commentData;
@property (strong, nonatomic) NSIndexPath *editingIndex;

@property (nonatomic) NSUInteger limit; // Defaults to 5
@property (nonatomic) NSUInteger skip;

@end

@implementation CommentsViewController


#pragma mark - Initializer

- (id)init {
    self = [super initWithTableViewStyle:UITableViewStylePlain];

    if (self) {
        self.inverted = NO;
        self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.title = @"Comments";
        self.commentData = [NSMutableArray new];
        self.searchResult = [NSMutableArray new];
        [self.autoCompletionView registerNib:[UINib nibWithNibName:@"AutoCompleteTableViewCell" bundle:nil] forCellReuseIdentifier:@"AutoCompletionCell"];
        [self.tableView registerNib:[UINib nibWithNibName:@"CHTableViewCommentCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"CommentCell"];
        self.tableView.separatorColor = [UIColor clearColor];
        self.limit = 5;
        self.skip = 0;
    }
    
    return self;
}

/*
// Uncomment if you are using Storyboard.
// You don't need to call initWithCoder: anymore
+ (UITableViewStyle)tableViewStyleForCoder:(NSCoder *)decoder
{
    return UITableViewStylePlain;
}
*/

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    if (self.post != nil) {
        [self refreshTable];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - SLKTextViewController Events

- (void)didChangeKeyboardStatus:(SLKKeyboardStatus)status {
    // Notifies the view controller that the keyboard changed status.
    if (status == SLKKeyboardStatusDidShow) {
        [self.tableView scrollRectToVisible:CGRectMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height, self.tableView.bounds.size.width, self.tableView.bounds.size.height) animated:YES];
    }
}

- (void)textWillUpdate {
    // Notifies the view controller that the text will update.
    // Calling super does nothing
    
    [super textWillUpdate];
}

- (void)textDidUpdate:(BOOL)animated {
    // Notifies the view controller that the text did update.
    // Must call super
    
    [super textDidUpdate:animated];
}

- (BOOL)canPressRightButton {
    // Asks if the right button can be pressed
    return [super canPressRightButton];
}

- (void)didPressRightButton:(id)sender {
    // Notifies the view controller when the right button's action has been triggered, manually or by using the keyboard return key.
    // Must call super
    
    // This little trick validates any pending auto-correction or auto-spelling just after hitting the 'Send' button
    [self.textView refreshFirstResponder];
    
    [self sendComment:self.textView.text];
    
    /*NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
    UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
    
    // Fixes the cell from blinking (because of the transform, when using translucent cells)
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];*/
    
    [super didPressRightButton:sender];
}

/*
// Uncomment these methods for aditional events
- (void)didPressLeftButton:(id)sender {
    // Notifies the view controller when the left button's action has been triggered, manually.
 
    [super didPressLeftButton:sender];
}
 
- (id)keyForTextCaching {
    // Return any valid key object for enabling text caching while composing in the text view.
    // Calling super does nothing
}

- (void)didPasteMediaContent:(NSDictionary *)userInfo {
    // Notifies the view controller when a user did paste a media content inside of the text view
    // Calling super does nothing
}

- (void)willRequestUndo {
    // Notification about when a user did shake the device to undo the typed text
 
    [super willRequestUndo];
}
*/

#pragma mark - SLKTextViewController Edition

// Uncomment these methods to enable edit mode
- (void)didCommitTextEditing:(id)sender {
    // Notifies the view controller when tapped on the right "Accept" button for commiting the edited text

    BSComment *comment = [self.commentData objectAtIndex:self.editingIndex.row];
    NSString *message = [self.textView.text copy];
    
    [self.commentData removeObjectAtIndex:self.editingIndex.row];
    comment.comment = message;
    [self.commentData insertObject:comment atIndex:self.editingIndex.row];
    [self.tableView reloadData];
    [comment saveInBackground];
    
    self.editingIndex = nil;
    [super didCommitTextEditing:sender];
}

- (void)didCancelTextEditing:(id)sender {
    // Notifies the view controller when tapped on the left "Cancel" button
    self.editingIndex = nil;
    [super didCancelTextEditing:sender];
}

#pragma mark - SLKTextViewController Autocompletion

// Uncomment these methods to enable autocompletion mode
- (BOOL)canShowAutoCompletion {
    
    NSString *prefix = self.foundPrefix;
    NSString *word = self.foundWord;
    
    if ([prefix isEqualToString:@"@"]) {
        
        if (word.length > 2) {
            
            NSString *str = [word substringToIndex:1];
            word = [NSString stringWithFormat:@"%@%@",[str uppercaseString], [word substringFromIndex:1]];
            
            PFQuery *username = [BSUser query];
            [username whereKey:@"username" hasPrefix:[word lowercaseString]];
            PFQuery *realname = [BSUser query];
            [realname whereKey:@"realName" hasPrefix:word];
            
            PFQuery *full = [PFQuery orQueryWithSubqueries:@[username, realname]];
            [full findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
               
                [self.searchResult removeAllObjects];
                [self.searchResult addObjectsFromArray:objects];
                [self.autoCompletionView reloadData];
                
            }];
            
        }
    }
    
    return YES;
}

- (CGFloat)heightForAutoCompletionView {
    // Asks for the height of the autocompletion view
    CGFloat cellHeight = 34.0;
    return cellHeight * self.searchResult.count;
}

#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellAnimation"]) {
        cell.alpha = 0;
        
        [UIView beginAnimations:@"fade" context:NULL];
        [UIView setAnimationDuration:0.8];
        cell.alpha = 1;
        [UIView commitAnimations];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Returns the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Returns the number of rows in the section.
    if ([tableView isEqual:self.autoCompletionView]) {
        return self.searchResult.count;
    }
    
    return self.commentData.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    if ([tableView isEqual:self.autoCompletionView]) {
        // Configure the autocompletion cell...
        AutoCompleteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AutoCompletionCell" forIndexPath:indexPath];
        
        BSUser *user = self.searchResult[indexPath.row];
        
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:@""];
        
        NSMutableAttributedString *username = [[NSMutableAttributedString alloc] initWithString:user.username attributes:@{NSForegroundColorAttributeName : [CHColorHandler getUIColorFromNSString:user.color], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15]}];
        [text appendAttributedString:username];
        
        NSMutableAttributedString *name = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" • %@",user.realName] attributes:@{NSForegroundColorAttributeName : [UIColor lightGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Light" size:13]}];
        [text appendAttributedString:name];
        
        cell.username.attributedText = text;
        cell.profilePicture.clipsToBounds = YES;
        cell.profilePicture.layer.cornerRadius = 34/2;
        cell.profilePicture.file = user.profilePicture;
        [cell.profilePicture loadInBackground];
        
        // Cells must inherit the table view's transform
        // This is very important, since the main table view may be inverted
        cell.transform = self.autoCompletionView.transform;
        return cell;
        
    } else  {
        
            // Configure the message cell...
            CHTableViewCommentCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell" forIndexPath:indexPath];
            
            if (indexPath.row < self.commentData.count) {
            
                BSComment *obj = [self.commentData objectAtIndex:indexPath.row];
                
                cell.username.text = obj.commenter;
                [cell.username addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUser:)]];
                cell.username.userInteractionEnabled = YES;

                cell.label.text = obj.comment;
                cell.location.text = obj.locationName;
                
                if (obj.createdAt != nil) {
                    cell.time.timeLabel.text = [CHTime makeTimePassedFromDate:obj.createdAt];
                } else {
                    cell.time.timeLabel.text = [CHTime makeTimePassedFromDate:[NSDate date]];
                }
                
                PFFile *file = obj.profilePic;
                cell.profilePicture.clipsToBounds = YES;
                cell.profilePicture.layer.cornerRadius = 20;
                cell.profilePicture.file = file;
                [cell.profilePicture loadInBackground];
                
                UILongPressGestureRecognizer *hold = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellDidLongHold:)];
                [cell addGestureRecognizer:hold];
            }
            
            // Cells must inherit the table view's transform
            // This is very important, since the main table view may be inverted
            cell.transform = self.tableView.transform;
            return cell;
        }
    
}
- (void)openUser:(UIGestureRecognizer *)tap {

    NSString *username = ((UILabel*)tap.view).text;

    if ([username isEqualToString:self.appDelegate.currentUser.username]) {
        return;
    }

    //TODO: Try to find a better alternative to this.
    //Loading each user after they've already been clicked on is way too slow.
    //Maybe try to pull all the users for all comments after loading the view?

    PFQuery *userQuery = [BSUser query];
    [userQuery whereKey:@"username" equalTo:username];
    [userQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {

        //[[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : object }];

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        UserViewController *userVc = [storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];

         userVc.user = (BSUser*)object;

        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:userVc];

        [(UINavigationController*)self.parentViewController presentViewController:nav animated:YES completion:nil];

    }];

}
/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([tableView isEqual:self.autoCompletionView]) {
        return 34;
    }
    
    BSComment *message = self.commentData[indexPath.row];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    CGFloat width = CGRectGetWidth(tableView.frame)-kAvatarSize;
    width -= 25.0;
    
    CGRect titleBounds = [message.commenter boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    CGRect bodyBounds = [message.comment boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    if (message.comment.length == 0) {
        return 0.0;
    }
    
    CGFloat height = CGRectGetHeight(titleBounds);
    height += CGRectGetHeight(bodyBounds);
    height += 40.0;
    
    if (height < kMinimumHeight) {
        height = kMinimumHeight;
    }
    
    return height;
    
}
*/

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:self.tableView]) {
        
    }
    
    if ([tableView isEqual:self.autoCompletionView]) {
        BSUser *item = self.searchResult[indexPath.row];
        [self acceptAutoCompletionWithString:[item.username stringByAppendingString:@" "]];
    }
}

#pragma mark - Helpers

- (void)setupUI {
    
    // Sets the title to white
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    // Set the text input bar tint
    self.textInputbar.tintColor = self.navigationController.navigationBar.barTintColor;
    self.textView.placeholder = @"Your Comment Here..."; // Sets the placeholder
    self.textView.placeholderColor = [UIColor colorWithRed:0.397 green:0.279 blue:0.281 alpha:1.000];
    
    // Make the back button and add it to the navigation item
    if (!self.isPushed) {
        UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_arrow_back size:30 color:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
        self.navigationItem.leftBarButtonItem = back;
    }
    
    // Register for autocompletion
    [self registerPrefixesForAutoCompletion:@[@"@"]];
    
    // Set up the header
    CGFloat width = [[UIScreen mainScreen] applicationFrame].size.width;
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 320)];
    header.backgroundColor = self.navigationController.navigationBar.barTintColor;
    
    PFImageView *profilePic = [[PFImageView alloc] initWithFrame:CGRectMake(width / 2, 50, 100, 100)];
    profilePic.frame = CGRectOffset(profilePic.frame, -(profilePic.frame.size.width / 2), 0);
    profilePic.clipsToBounds = YES;
    profilePic.layer.cornerRadius = 50;
    profilePic.layer.borderWidth = 3;
    profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    profilePic.contentMode = UIViewContentModeScaleAspectFill;
    profilePic.file = self.post.author.profilePicture;
    [profilePic setImageWithString:self.post.authorUsername color:self.navigationController.navigationBar.barTintColor];
    [profilePic loadInBackground];
    
    UILabel *username = [[UILabel alloc] initWithFrame:CGRectMake(0, 155, width, 25)];
    username.font = [UIFont systemFontOfSize:24 weight:1.5];
    username.text = self.post.authorUsername;
    username.textColor = [UIColor whiteColor];
    username.textAlignment = NSTextAlignmentCenter;
    
    SLKTextView *textView = [[SLKTextView alloc] initWithFrame:CGRectMake(10, 175, width-20, 70)];
    textView.font = [UIFont boldSystemFontOfSize:18];
    textView.text = self.post.text;
    textView.selectable = NO;
    textView.textColor = [UIColor whiteColor];
    textView.backgroundColor = [UIColor clearColor];
    textView.textAlignment = NSTextAlignmentCenter;
    // Not adding this was causing a crash
    [textView addObserver:textView forKeyPath:NSStringFromSelector(@selector(contentSize)) options:NSKeyValueObservingOptionNew context:NULL];
    
    // Set up footer
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, header.frame.size.height-34, width, 34)];
    //footer.backgroundColor = [UIColor whiteColor];
    UILabel *footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 34)];
    footerLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:16];
    footerLabel.textAlignment = NSTextAlignmentCenter;
    footerLabel.textColor = self.navigationController.navigationBar.barTintColor;
    footerLabel.text = @"Load more comments...";
    
    [footer addSubview:footerLabel];
    
    UITapGestureRecognizer *footerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(refreshTable)];
    [footer addGestureRecognizer:footerTap];
    
    CGRect blurRect = CGRectMake(0, 0, width, 34);
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:blur];
    blurView.frame = blurRect;
     
    [footer addSubview:blurView];
    [footer bringSubviewToFront:footerLabel];
    
    [header addSubview:profilePic];
    [header addSubview:username];
    [header addSubview:textView];
    [header addSubview:footer];
    
    // Make the header right side up!
    header.transform = self.tableView.transform;
    
    // We add it to the footer because we are upside down!
    self.tableView.tableHeaderView = header;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.autoCompletionView.estimatedRowHeight = 34;
    self.autoCompletionView.rowHeight = UITableViewAutomaticDimension;
}

- (void)sendComment:(NSString *)commentText {
    
    commentText = [commentText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (![BSUser currentUser].locationHidden) {
        
        BSComment *comment = [BSComment object];
        comment.post = [BSPost objectWithoutDataWithObjectId:self.post.objectId];
        comment.commenter = [BSUser currentUser].username;
        comment.comment = commentText;
        
        [BSLocation cityNameWithCurrentLocationWithBlock:^(NSString *cityName, NSError *error) {
            comment.locationName = cityName;
            [comment saveInBackground];
        }];
        
        comment.profilePic = [BSUser currentUser].profilePicture;
        
        // Uses Grand Central Dispatch to process if the messages have arrived from Parse
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
            [self.commentData addObject:comment];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
            
            // Scroll the tableview down to see the new comment
            if (self.commentData.count > 0) {
                [self.tableView scrollRectToVisible:CGRectMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height, self.tableView.bounds.size.width, self.tableView.bounds.size.height) animated:YES];
            }
            
            [comment saveInBackgroundWithBlock:^(BOOL done, NSError *error){
                
                if (!error) {
                    
                    PFQuery *query = [BSPost query];
                    BSPost *postObj = (BSPost *)[query getObjectWithId:self.post.objectId];
                    
                    NSNumber *numOfComments = postObj.comments;
                    
                    if (numOfComments == nil) {
                        numOfComments = [NSNumber numberWithInt:0];
                    }
                    
                    NSNumber *upOne = [NSNumber numberWithInt:[numOfComments intValue] + 1];
                    postObj.comments = upOne;
                    
                    NSMutableArray *commenters = [NSMutableArray arrayWithArray:postObj.commenters];
                    
                    if (commenters == nil) {
                        
                        commenters = [NSMutableArray new];
                        [commenters addObject:[BSUser currentUser].username];
                        
                    } else {
                        
                        if (![commenters containsObject:[BSUser currentUser].username]) {
                            [commenters addObject:[BSUser currentUser].username];
                        }
                        
                    }
                    
                    postObj.commenters = commenters;
                    
                    [postObj saveInBackground];
                    
                    NSString *pushTo = postObj.authorUsername;
                   
                    if (![[BSUser currentUser].username isEqualToString:pushTo]) {
                        
                        [CloudCode pushToChannel:pushTo withAlert:[NSString stringWithFormat:@"%@ has commented on your post", [BSUser currentUser].username] andCategory:@"comment" andViewToPush:[NSString stringWithFormat:@"bubblesocial://post/%@", postObj.objectId]];
                        
                        [CloudCode setNotification:postObj.objectId andType:[BSNotificationType BSCommentNotification]];
                        
                    }
                    
                    NSMutableArray *mentioned = [NSMutableArray new];
                    NSString *foundMention = @"";
                    NSScanner *scanner = [NSScanner scannerWithString:commentText];
                    
                    while (scanner.scanLocation != commentText.length) {
                        [scanner scanUpToString:@"@" intoString:nil];
                        [scanner scanUpToString:@" " intoString:&foundMention];
                        
                        if (![foundMention isEqualToString:@""]) {
                            [mentioned addObject:foundMention];
                        }
                       
                        foundMention = nil;
                    }
                    
                    if (mentioned.count > 0) {
                        [CloudCode pushToChannels:mentioned withAlert:[NSString stringWithFormat:@"%@ has mentioned you in a comment", [BSUser currentUser].username] andCategory:@"mention" andViewToPush:[NSString stringWithFormat:@"bubblesocial://post/%@", postObj.objectId]];
                    }
                    
                }
                
            }];
            
        });
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Your Location is Hidden" message:@"To post a comment, you will need to unhide yourself first." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *comment = [UIAlertAction actionWithTitle:@"Unhide + Comment" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            self.appDelegate.currentUser.locationHidden = NO;
            
            [self.appDelegate.currentUser saveInBackgroundWithBlock:^(BOOL done, NSError *error) {
                
                [self sendComment:self.textView.text];
                
            }];
            
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addActions:@[comment, cancel]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }

}

- (void)closeView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)refreshTable {
    
    [KVNProgress show];
    
    NSTimer *messageResponseTimeout = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(messageResponseTimeoutRefresh) userInfo:nil repeats:NO];
    
    PFQuery *comments = [BSComment query];
    [comments whereKey:@"post" equalTo:self.post];
    comments.skip = self.skip;
    comments.limit = self.limit;
    [comments orderByDescending:@"createdAt"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [comments findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
            
            if (objects.count < self.limit) {
                self.skip += objects.count; // We got back less than our limit. Update skip so we don't query the same object
            } else if (objects.count == self.limit) {
                self.skip += self.limit; // There might be more objects in the table. Update the skip value.
            }
            
            for (BSComment *comment in objects) {
                [self.commentData insertObject:comment atIndex:0];
            }
            
            if (self.commentData.count > 0) {
                //self.blankImage.hidden = YES;
               // self.blankLabel.hidden = YES;
            }
            
            [self.tableView reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [KVNProgress dismiss];
                [messageResponseTimeout invalidate];
            });
            
        }];
        
    });
    
}

- (void)messageResponseTimeout {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [KVNProgress showErrorWithStatus:@"Sorry, comment didn't post. Is your internet connection okay?"];
    });
    
}

- (void)messageResponseTimeoutRefresh {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [KVNProgress showErrorWithStatus:@"This is taking a while. Is your internet connection okay?"];
    });
    
}

- (void)cellDidLongHold:(UILongPressGestureRecognizer *)hold {
    
    CGPoint point = [hold locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    
    CHTableViewCommentCellTableViewCell *cell = (CHTableViewCommentCellTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell.username.text isEqualToString:self.appDelegate.currentUser.username]) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *edit = [UIAlertAction actionWithTitle:@"Edit" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // Edit button was pressed
            
            self.editingIndex = indexPath;
            [self editText:cell.label.text];
            
        }];
        
        UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
            // Delete button was pressed
            BSComment *comment = [self.commentData objectAtIndex:indexPath.row];
            BSPost *post = comment.post;
            post.comments = [NSNumber numberWithInt:post.comments.intValue-1];
            [post saveEventually];
            [comment deleteInBackground];
            
            [self.commentData removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
            
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addActions:@[edit, delete, cancel]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

@end
