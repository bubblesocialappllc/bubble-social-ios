#import <AssetsLibrary/AssetsLibrary.h>
#import "RRSendMessageViewController.h"
#import "RRCustomScrollView.h"
@import ionicons;

@interface RRSendMessageViewController ()
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UIButton *buttonAddPhoto;
@property (nonatomic, strong) UILabel *numberLine;
@property (nonatomic, strong) NSMutableArray *photosThumbnailLibrairy;
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (nonatomic, strong) NSMutableArray *defaultSelectedPhotos;
@property (nonatomic, strong) UICollectionView *photosCollection;
@property (nonatomic, strong) RRCustomScrollView *selectedPhotosView;
@property (nonatomic, assign) BOOL state;
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (strong, nonatomic) UIButton *location;
@property (strong, nonatomic) FBShimmeringView *shimmer;
@property (strong, nonatomic) UILabel *loadingLabel;
@property (strong, nonatomic) UINavigationController *nc;
@property (strong, nonatomic) BSPlace *place;
@property (strong, nonatomic) NSString *currentCity;
@property (strong, nonatomic) UISwitch *saveImageSwitch;
@property (strong, nonatomic) UILabel *switchText;
@property (nonatomic, strong) void (^completion)(RRMessageModel *model, BOOL isCancel, BSPlace *place, BOOL shouldSavePhoto);

@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (nonatomic, strong) UIImageView *currentImage;

@property BOOL showingAutoComplete;

@end

# define CELL_PHOTO_IDENTIFIER      @"photoLibraryCell"
# define CELL_PREVIEW_IDENTIFIER    @"previewCell"
# define CLOSE_PHOTO_IMAGE          @"close"
# define ADD_PHOTO_IMAGE            @"camera"
# define LEFT_BUTTON                @"Cancel"
# define RIGHT_BUTTON               @"Post"
# define TITLE_CONTROLLER           @"New Post"
#define PostStatusLimit 170

@implementation RRSendMessageViewController

- (ALAssetsLibrary *) defaultAssetLibrairy {
    static ALAssetsLibrary *assetLibrairy;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        assetLibrairy = [[ALAssetsLibrary alloc] init];
    });
    return (assetLibrairy);
}

- (BOOL) shouldAutorotate {
    return (false);
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

#pragma mark Delegate

- (void)postMessage {
    
    if (![CLLocationManager locationServicesEnabled]) {
        
        UIAlertController *noLocationAlert = [UIAlertController alertControllerWithTitle:@"Looks like you turned off Location Services" message:@"Make sure you have Location Services turned on and share your location with us from Settings!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
            
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        
        [noLocationAlert addActions:@[settingsAction, cancel]];
        
        [self presentViewController:noLocationAlert animated:YES completion:nil];
        
    } else if (self.appDelegate.currentUser.locationHidden) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Your Location is Hidden" message:@"To post, you will need to unhide yourself first." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *unhideAction = [UIAlertAction actionWithTitle:@"Unhide + Post" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            self.appDelegate.currentUser.locationHidden = NO;
            [self.appDelegate.currentUser saveInBackground];
            
            RRMessageModel *modelMessage = [[RRMessageModel alloc] init];
            modelMessage.text = self.textView.text;
            
            if (self.selectedPhotos.count > 0) {
                UIImage *image = [self resizeImage:[self.selectedPhotos firstObject] width:320 height:320];
                modelMessage.photos = [@[image] mutableCopy];
            } else {
                modelMessage.photos = self.selectedPhotos;
            }
            
            if (self.completion != nil) {
                self.completion(modelMessage, false, self.place, self.saveImageSwitch.isOn);
            }
            
            if ([self.delegate respondsToSelector:@selector(getMessage:)]) {
                [self.delegate getMessage:modelMessage];
            }
            
        }];
        
        UIAlertAction *hideAction = [UIAlertAction actionWithTitle:@"Keep Hidden" style:UIAlertActionStyleDefault handler:nil];
        
        [alert addActions:@[unhideAction, hideAction]];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        self.textView.text = [self.textView.text removeWhiteSpacesFromString];
        
        if (self.textView.text.length > 0 || self.selectedPhotos.count > 0) {
            
            RRMessageModel *modelMessage = [[RRMessageModel alloc] init];
            modelMessage.text = self.textView.text;
           
            if (self.selectedPhotos.count > 0) {
                UIImage *image = [self resizeImage:[self.selectedPhotos firstObject] width:320 height:320];
                modelMessage.photos = [@[image] mutableCopy];
            } else {
                modelMessage.photos = self.selectedPhotos;
            }
            
            if (self.completion != nil && modelMessage.photos.count > 0) {
                self.completion(modelMessage, false, self.place, self.saveImageSwitch.isOn);
            } else if (self.completion != nil) {
                self.completion(modelMessage, false, self.place, false);
            }
            
            if ([self.delegate respondsToSelector:@selector(getMessage:)]) {
                [self.delegate getMessage:modelMessage];
            }
            
        } else {
            [self cancelMessage];
        }
        
    }
    
}

- (void) cancelMessage {
    
    if ([self.delegate respondsToSelector:@selector(messageCancel)]) {
        [self.delegate messageCancel];
    }
    
    if (self.completion != nil) {
        self.completion(nil, true, self.place, self.saveImageSwitch.isOn);
    }
}

#pragma mark UITextView delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self performSelector:@selector(setCursor:) withObject:textView afterDelay:0.01];
}

- (void)setCursor:(UITextView *)textView {
    textView.selectedRange = NSMakeRange(0, 0);
}

- (void)textViewDidChange:(UITextView *)textView {
    
    NSInteger limit = PostStatusLimit - textView.text.length;
    
    if (limit < 20) {
        [self.numberLine setTextColor:[UIColor redColor]];
    } else {
        [self.numberLine setTextColor:[UIColor whiteColor]];
    }
    
    if (limit < 0) {
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    } else {
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
    
    self.numberLine.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.textView.text.length];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    // We don't want new line characters
    if ([text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound) {
        return YES;
    }
    
    [textView resignFirstResponder];
    return NO;
}


#pragma mark UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (self.photosThumbnailLibrairy.count + 1);
}

- (void) collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        self.currentImage = ((UICollectionViewCellPhoto *)cell).photo;
    }
}

- (void) collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        self.currentImage = nil;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCellPhoto *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELL_PHOTO_IDENTIFIER forIndexPath:indexPath];
    
    if (indexPath.row > 0) {
        
        // Make sure we don't go out of bounds
        if (indexPath.row <= self.photosThumbnailLibrairy.count) {
        
            cell.photo.contentMode = UIViewContentModeScaleAspectFill;
            cell.photo.clipsToBounds = YES;
            
            PHAsset *asset = [self.photosThumbnailLibrairy objectAtIndex:indexPath.row - 1];
            
            // View dimensions are based on points, but we're requesting pixels from PHImageManager.
            NSInteger retinaMultiplier = [UIScreen mainScreen].scale;
            CGSize retinaSquare = CGSizeMake(50 * retinaMultiplier, 50 * retinaMultiplier);
            
            [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:retinaSquare contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage *image, NSDictionary *info) {
                
                cell.photo.image = image;
                
            }];
            
        }
        
        //cell.photo.image = [self.photosThumbnailLibrairy objectAtIndex:indexPath.row - 1];
    }
    
    [cell setNeedsDisplay];
    
    return cell;
}

- (void) addNewPhotoSelected:(UIImage *)photo withIndexPath:(NSIndexPath *)indexPath {
    
    CGPoint startPosition = (indexPath) ? [self.photosCollection cellForItemAtIndexPath:indexPath].frame.origin : CGPointZero;
    if (self.selectedPhotos.count == 0) {
        CGFloat positionY = self.textView.frame.origin.y + self.textView.frame.size.height / 2;
        CGFloat sizeHeigth = self.textView.frame.size.height / 2;
        
        
        self.photosCollection.userInteractionEnabled = NO;
        [UIView animateWithDuration:0.5 animations:^{
            self.textView.frame = CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y,
                                             self.textView.frame.size.width, self.textView.frame.size.height / 2);
        } completion:^(BOOL finished) {
            NSRange bottom = NSMakeRange(self.textView.text.length -1, 1);
            [self.textView scrollRangeToVisible:bottom];
        }];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.selectedPhotosView.frame = CGRectMake(self.textView.frame.origin.x, positionY,
                                                       self.textView.frame.size.width, sizeHeigth);
        } completion:^(BOOL finished) {
            [self addPhotoSelectedView:photo
                       initialPosition:startPosition];
            [self.selectedPhotos addObject:photo];
            self.photosCollection.userInteractionEnabled = YES;
        }];
    }
    else {
        [self addPhotoSelectedView:photo initialPosition:startPosition];
        [self.selectedPhotos addObject:photo];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  
    if (indexPath.row == 0) {
        [self postUIImagePickerController];
        return;
    }
    else if (self.numberPhoto != -1 && self.selectedPhotos.count >= self.numberPhoto) {
        return;
    }
    
    PHFetchResult *results = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
    
    [results enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
       
        PHAsset *thumbAsset = [self.photosThumbnailLibrairy objectAtIndex:indexPath.row - 1];
        PHAsset *asset = (PHAsset *)obj;
        
        if ([thumbAsset isEqual:asset]) {
            
            PHImageRequestOptions *options = [PHImageRequestOptions new];
            options.version = PHImageRequestOptionsVersionCurrent;
            options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            
            [[PHImageManager defaultManager] requestImageDataForAsset:obj options:nil resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                
                [self addNewPhotoSelected:[UIImage imageWithData:imageData] withIndexPath:indexPath];
                
            }];
            
        }
        
    }];
    
    //[self addNewPhotoSelected:[self.photosThumbnailLibrairy objectAtIndex:indexPath.row - 1] withIndexPath:indexPath];
    
}

# pragma mark UIPickerController

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:true completion:^{
        if (chosenImage)
            [self addNewPhotoSelected:chosenImage withIndexPath:nil];
        [self initAVFoundation];
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:true completion:^{
        [self initAVFoundation];
    }];
}

#pragma mark - Camera Delegate

- (void)cameraDismissedWithImage:(UIImage *)image {
    
    [self addNewPhotoSelected:image withIndexPath:nil];
    [self initAVFoundation];
    
}

- (void)cameraDismissedWithCancel {
    
    [self initAVFoundation];
    
}

- (void) postUIImagePickerController {
    [self.session stopRunning];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    CameraViewController *camera = [sb instantiateViewControllerWithIdentifier:@"CameraViewController"];
    
    camera.delegate = self;
    
    [self presentViewController:camera animated:YES completion:nil];
    
    /*UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];*/
}

# pragma mark AVFoundation

- (void) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer {
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    UIImage *image = [UIImage imageWithCGImage:quartzImage scale:1.0 orientation:UIImageOrientationRight];
    CGImageRelease(quartzImage);
    
    if (self.currentImage) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            self.currentImage.image = image;
        });
    }
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection {
    [self imageFromSampleBuffer:sampleBuffer];
}

- (AVCaptureDevice *)backCamera {
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == AVCaptureDevicePositionBack) {
            return (device);
        }
    }
    return (nil);
}

- (void) initAVFoundation {
    self.session = [[AVCaptureSession alloc] init];
    self.session.sessionPreset = AVCaptureSessionPresetHigh;
    self.captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    [self.captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    AVCaptureDevice *device = [self backCamera];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    if (!input) {
        NSLog(@"Error open input device");
        return ;
    }
    [self.session addInput:input];
    
    AVCaptureVideoDataOutput *captureOutput =[[AVCaptureVideoDataOutput alloc] init];
    
    captureOutput.alwaysDiscardsLateVideoFrames = YES;
    captureOutput.alwaysDiscardsLateVideoFrames = YES;
    dispatch_queue_t queue;
    queue = dispatch_queue_create("cameraQueue", NULL);
    [captureOutput setSampleBufferDelegate:self queue:queue];
    NSString* key = (NSString*)kCVPixelBufferPixelFormatTypeKey;
    NSNumber* value = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA];
    NSDictionary* videoSettings = [NSDictionary dictionaryWithObject:value forKey:key];
    [captureOutput setVideoSettings:videoSettings];
    
    [self.session addOutput:captureOutput];
    [self.session startRunning];
}

# pragma mark interface button

- (void) deletePhoto:(id)sender {
    NSInteger deletedPhoto = ((UIButton *)sender).tag;
    
    for (UIView *currentSubView in [self.selectedPhotosView subviews]) {
        if (currentSubView.tag > 0 && deletedPhoto == currentSubView.tag) {
            if ([currentSubView isKindOfClass:[UIImageView class]]) {
                [self.selectedPhotos removeObjectAtIndex:deletedPhoto - 1];
            }
            if ([currentSubView isKindOfClass:[UIImageView class]]) {
                [UIView animateWithDuration:0.3 animations:^{
                    currentSubView.frame = CGRectMake(currentSubView.frame.origin.x,
                                                      currentSubView.frame.origin.y + 50, 0, 0);
                } completion:^(BOOL finished) {
                    [currentSubView removeFromSuperview];
                }];
            }
            else {
                [currentSubView removeFromSuperview];
            }
        }
    }
    
    for (UIView *currentSubView in [self.selectedPhotosView subviews]) {
        if (currentSubView.tag > 0 && currentSubView.tag > deletedPhoto) {
            [UIView animateWithDuration:0.5 animations:^{
                currentSubView.tag -= 1;
                currentSubView.frame = CGRectMake(currentSubView.frame.origin.x - self.textView.frame.size.height,
                                                  currentSubView.frame.origin.y,
                                                  currentSubView.frame.size.width,
                                                  currentSubView.frame.size.height);
            }];
        }
    }
    self.selectedPhotosView.contentSize = CGSizeMake(self.selectedPhotosView.contentSize.width -
                                                     self.textView.frame.size.height,
                                                     self.selectedPhotosView.contentSize.height);
    if (self.selectedPhotos.count == 0) {
        self.switchText.hidden = YES;
        self.saveImageSwitch.hidden = YES;
        [UIView animateWithDuration:0.5 animations:^{
            self.textView.frame = CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y,
                                             self.textView.frame.size.width, self.textView.frame.size.height * 2);
        } completion:^(BOOL finished) {
            NSRange bottom = NSMakeRange(self.textView.text.length -1, 1);
            [self.textView scrollRangeToVisible:bottom];
            self.selectedPhotosView.frame = CGRectZero;
        }];
    }
}

- (void) addPhoto {
    
    if (self.state == true) {
        [self.view endEditing:YES];
        self.state = false;
        [self.view addSubview:self.photosCollection];
        
        if (self.photosThumbnailLibrairy.count != 0) {
            return ;
        }
        
        // View dimensions are based on points, but we're requesting pixels from PHImageManager.
        /*NSInteger retinaMultiplier = [UIScreen mainScreen].scale;
        CGSize retinaSquare = CGSizeMake(50 * retinaMultiplier, 50 * retinaMultiplier);*/
        
        PHFetchResult *results = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
        
        [results enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
           
            [self.photosThumbnailLibrairy addObject:obj];
            [self.photosCollection reloadData];
            
            /*[[PHImageManager defaultManager] requestImageForAsset:obj targetSize:retinaSquare contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage *image, NSDictionary *info) {
                
                [self.photosThumbnailLibrairy addObject:image];
                [self.photosCollection reloadData];
                
            }];*/
            
        }];
        
        /*ALAssetsLibrary *library = [self defaultAssetLibrairy];
        [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
            
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:^(ALAsset *alAsset,NSUInteger index, BOOL *innerStop) {
                
                if (alAsset) {
                    UIImage *currentThumbnail = [UIImage imageWithCGImage:[alAsset thumbnail]];
                    
                    
                }
            }];
        } failureBlock: ^(NSError *error) {
            NSLog(@"No groups photos");
        }];*/
    }
    else {
        [self.textView becomeFirstResponder];
        self.state = true;
    }
}

- (void) addPhotoSelectedView:(UIImage *)photo initialPosition:(CGPoint)position {
    CGFloat indexPositionX = self.textView.frame.size.height * self.selectedPhotos.count;
    
    UIImageView *photoView = [[UIImageView alloc] initWithFrame:CGRectMake(position.x, self.textView.frame.size.height,
                                                                           self.textView.frame.size.height - 10,
                                                                           self.textView.frame.size.height - 10)];
    
    UIButton *buttonClose = [[UIButton alloc] init];
    
    photoView.tag = self.selectedPhotos.count + 1;
    buttonClose.tag = self.selectedPhotos.count + 1;
    UIImageView *imgCloseButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    imgCloseButton.image = [IonIcons imageWithIcon:ion_close_circled size:20 color:[self inverseColorOfColor:[self averageColorOfImage:photo]]];
    
    [buttonClose addSubview:imgCloseButton];
    [buttonClose addTarget:self action:@selector(deletePhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    self.selectedPhotosView.contentSize = CGSizeMake(self.textView.frame.size.height +
                                                     self.selectedPhotosView.contentSize.width,
                                                     self.textView.frame.size.height);
    photoView.image = photo;
    photoView.contentMode = UIViewContentModeScaleAspectFit;
    [self.selectedPhotosView addSubview:photoView];
    
    [UIView animateWithDuration:0.5 animations:^{
        photoView.frame = CGRectMake(indexPositionX + 5, 5,
                                     self.textView.frame.size.height - 10,
                                     self.textView.frame.size.height - 10);
    } completion:^(BOOL finished) {
        buttonClose.frame = CGRectMake(photoView.frame.origin.x, 0, 25, 25);
        [self.selectedPhotosView addSubview:buttonClose];
    }];
    
    self.switchText.hidden = YES;
    self.saveImageSwitch.hidden = YES;
    
}

# pragma mark notification keyboard

- (void)notificationKeyboardUp:(NSNotification*)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    self.state = true;
    [UIView animateWithDuration:0.5 animations:^{
        self.textView.frame = CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y,
                                         self.textView.frame.size.width, (self.view.frame.size.height - 64) -
                                         keyboardFrameBeginRect.size.height - 40 - self.selectedPhotosView.frame.size.height);
        self.location.frame = CGRectMake(self.location.frame.origin.x, self.textView.frame.size.height - 75, self.location.frame.size.width, self.location.frame.size.height);
    }];
    self.buttonAddPhoto.frame = CGRectMake(self.buttonAddPhoto.frame.origin.x, self.view.frame.size.height -
                                           keyboardFrameBeginRect.size.height - 30, self.buttonAddPhoto.frame.size.width,
                                           self.buttonAddPhoto.frame.size.height);
    
    self.numberLine.frame = CGRectMake(self.numberLine.frame.origin.x, self.view.frame.size.height -
                                       keyboardFrameBeginRect.size.height - 30, self.numberLine.frame.size.width,
                                       self.numberLine.frame.size.height);
    
    CGRect frameForSwitch = self.buttonAddPhoto.frame;
    frameForSwitch = CGRectOffset(frameForSwitch, 35, 0);
    self.saveImageSwitch.frame = frameForSwitch;
    
    CGRect frameForSwitchText = self.switchText.frame;
    frameForSwitchText.origin.y = frameForSwitch.origin.y;
    self.switchText.frame = frameForSwitchText;
    
    self.photosCollection.frame = CGRectMake(0, self.view.frame.size.height - keyboardFrameBeginRect.size.height,
                                             self.view.frame.size.width, keyboardFrameBeginRect.size.height);
    
    if (self.defaultSelectedPhotos != nil) {
        self.textView.frame = CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y,
                                         self.textView.frame.size.width, self.textView.frame.size.height / 2);
        
        CGFloat positionY = self.textView.frame.origin.y + self.textView.frame.size.height;
        CGFloat sizeHeigth = self.textView.frame.size.height;
        self.selectedPhotosView.frame = CGRectMake(self.textView.frame.origin.x, positionY,
                                                   self.textView.frame.size.width, sizeHeigth);
        
        for (UIImage *currentPhoto in self.defaultSelectedPhotos) {
            [self addPhotoSelectedView:currentPhoto
                       initialPosition:CGRectMake(0, self.view.frame.size.height / 2, 0, 0).origin];
            [self.selectedPhotos addObject:currentPhoto];
        }
        self.defaultSelectedPhotos = nil;
    }
}

# pragma mark init interface

- (void) initPanelButton {
    
    UIColor *color = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
    
    self.buttonAddPhoto = [[UIButton alloc] initWithFrame:CGRectMake(10, -20, 30, 30)];
    UIImageView *imageButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    imageButton.contentMode = UIViewContentModeScaleAspectFit;
    imageButton.image = [IonIcons imageWithIcon:ion_camera size:30 color:[UIColor whiteColor]];
    [self.buttonAddPhoto addSubview:imageButton];
    [self.buttonAddPhoto addTarget:self action:@selector(addPhoto) forControlEvents:UIControlEventTouchUpInside];
    
    self.numberLine = [[UILabel alloc] initWithFrame:CGRectMake(10, - 20,
                                                                self.view.frame.size.width - 20, 20)];
    self.numberLine.textColor = [UIColor whiteColor];
    self.numberLine.textAlignment = NSTextAlignmentRight;
    
    self.numberLine.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.textView.text.length];
    
    self.saveImageSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(20, -20, 30, 30)];
    self.saveImageSwitch.onTintColor = color;
    self.saveImageSwitch.on = YES;
    
    self.switchText = [[UILabel alloc] initWithFrame:CGRectMake(100, -20, 200, 30)];
    self.switchText.text = @"Save photo to Me Page?";
    self.switchText.textColor = color;
    
    [self.view addSubview:self.numberLine];
    [self.view addSubview:self.buttonAddPhoto];
    [self.view addSubview:self.saveImageSwitch];
    [self.view addSubview:self.switchText];
    
    self.switchText.hidden = YES;
    self.saveImageSwitch.hidden = YES;
}

- (void) initPhotosCollection {
    UICollectionViewFlowLayout *layoutCollection = [[UICollectionViewFlowLayout alloc] init];
    
    layoutCollection.itemSize = CGSizeMake(self.view.frame.size.width / 4 - 2, self.view.frame.size.width / 4 - 2);
    layoutCollection.minimumLineSpacing = 2;
    layoutCollection.minimumInteritemSpacing = 2;
    layoutCollection.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.photosCollection = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layoutCollection];
    [self.photosCollection registerClass:[UICollectionViewCellPhoto class] forCellWithReuseIdentifier:CELL_PHOTO_IDENTIFIER];
    self.photosCollection.backgroundColor = [UIColor clearColor];
    self.photosCollection.delegate = self;
    self.photosCollection.dataSource = self;
}

- (void) initScrollSelectedPhotos {
    self.selectedPhotosView = [[RRCustomScrollView alloc] initWithFrame:CGRectZero];
    self.selectedPhotosView.canCancelContentTouches = YES;
    self.selectedPhotosView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.selectedPhotosView];
}

- (void) initTextView {
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(5, 64 + 5, self.view.frame.size.width - 10, 0)];
    
    self.textView.backgroundColor = [UIColor whiteColor];
    self.textView.font = [UIFont systemFontOfSize:18];
    self.textView.delegate = self;
    self.textView.keyboardType = UIKeyboardTypeTwitter;
    [self.view addSubview:self.textView];
    [self.textView becomeFirstResponder];
    
    self.location = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.location.tintColor = [UIColor darkGrayColor];
    self.location.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.location.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [self.location addTarget:self action:@selector(changeLocation) forControlEvents:UIControlEventTouchUpInside];
    self.location.frame = CGRectMake(10, self.textView.frame.size.height - 10, self.textView.frame.size.width, 50);
    [self.location setImage:[IonIcons imageWithIcon:ion_ios_location size:20 color:[UIColor darkGrayColor]] forState:UIControlStateNormal];
    [self.textView addSubview:self.location];
    
    CGRect shimmerFrame = self.location.frame;
    shimmerFrame = CGRectOffset(shimmerFrame, 20, 10);
    
    self.shimmer = [[FBShimmeringView alloc] initWithFrame:shimmerFrame];
    [self.location addSubview:self.shimmer];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeLocation)];
    
    self.loadingLabel = [[UILabel alloc] initWithFrame:self.shimmer.frame];
    self.loadingLabel.textColor = [UIColor lightGrayColor];
    self.loadingLabel.textAlignment = NSTextAlignmentLeft;
    self.loadingLabel.text = @"Bubbling...";
    self.loadingLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20];
    [self.shimmer addGestureRecognizer:tap];
    self.shimmer.contentView = self.loadingLabel;
    self.shimmer.shimmering = YES;
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:self.appDelegate.currentLocation.latitude longitude:self.appDelegate.currentLocation.longitude];
    
    __block NSString *locationName = @"";
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        for (CLPlacemark *placemark in placemarks) {
            
            locationName = [NSString stringWithFormat:@"%@", [placemark locality]];
            
            locationName = [locationName stringByAppendingString:[NSString stringWithFormat:@", %@", [placemark administrativeArea]]];
            
        }
        
        //shimmer.shimmering = NO;
        self.currentCity = locationName;
        self.loadingLabel.text = locationName;
        
    }];
    
}

- (void) initUI {
    self.place = [BSPlace new];
    self.state = true;
    self.numberPhoto = -1;
    self.view.backgroundColor = [UIColor colorWithWhite:0.847 alpha:1.000];
    
    self.selectedPhotos = [[NSMutableArray alloc] init];
    self.photosThumbnailLibrairy = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:RIGHT_BUTTON
                                                                    style:UIBarButtonItemStyleDone target:self action:@selector(postMessage)];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:LEFT_BUTTON
                                                                   style:UIBarButtonItemStyleDone target:self action:@selector(cancelMessage)];

    self.navigationItem.rightBarButtonItem = rightButton;
    self.navigationItem.leftBarButtonItem = leftButton;
    self.title = @"New Pop";
    
    [self initPhotosCollection];
    [self initScrollSelectedPhotos];
    [self initPanelButton];
    [self initTextView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationKeyboardUp:)
                                                 name:UIKeyboardDidShowNotification object:nil];
}

- (void) presentController:(UIViewController *)parentController blockCompletion:(void (^)(RRMessageModel *model, BOOL isCancel, BSPlace *place, BOOL shouldSavePhoto))completion {
    self.nc = [[UINavigationController alloc] initWithRootViewController:self];
    
    self.nc.navigationBar.tintColor = [UIColor whiteColor];
    self.nc.navigationBar.barTintColor = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
    self.nc.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    
    [parentController presentViewController:self.nc animated:true completion:nil];
    
    self.completion = completion;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self initAVFoundation];
}

# pragma mark constructor

- (instancetype) initWithMessage:(RRMessageModel *)message {
    self = [super init];
    
    if (self != nil) {
        [self initUI];
        self.textView.text = message.text;
        self.numberLine.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.textView.text.length];
        self.defaultSelectedPhotos = message.photos;
    }
    return (self);
}

- (instancetype) init {
    self = [super init];
    
    if (self != nil) {
        [self initUI];
    }
    return (self);
}

- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    self.selectedPhotos = nil;
    self.photosThumbnailLibrairy = nil;
}

- (UIColor *)averageColorOfImage:(UIImage *)image {
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), image.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    if(rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }
    else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}

- (UIColor *)inverseColorOfColor:(UIColor *)color {
    
    const CGFloat *componentColors = CGColorGetComponents(color.CGColor);
    
    UIColor *newColor = [[UIColor alloc] initWithRed:(1.0 - componentColors[0])
                                               green:(1.0 - componentColors[1])
                                                blue:(1.0 - componentColors[2])
                                               alpha:componentColors[3]];
    
    return newColor;
}

- (void)changeLocation {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LocationManagerViewController *view = [sb instantiateViewControllerWithIdentifier:@"LocationManagerViewController"];
    
    //The active label is passed to the next view to be modified for when this view is recalled, it shows the updated posting location
    view.label = self.loadingLabel;
    view.currentCity = self.currentCity;
    view.delegate = self;
    
    [self.nc pushViewController:view animated:YES];
    
}

- (void)locationManagerDidChoosePlace:(BSPlace *)place {
    self.place = place;
}

- (UIImage *)resizeImage:(UIImage *)image width:(CGFloat)width height:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    CGFloat widthRatio = newSize.width / image.size.width;
    CGFloat heightRatio = newSize.height / image.size.height;
    
    if (widthRatio > heightRatio) {
        newSize = CGSizeMake(image.size.width * heightRatio, image.size.height * heightRatio);
    } else {
        newSize = CGSizeMake(image.size.width * widthRatio, image.size.height * widthRatio);
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


@end