//
//  ColorPickerViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "ColorPickerViewController.h"

@interface ColorPickerViewController ()

@property (strong, nonatomic) NSMutableArray *colors;
@property (strong, nonatomic) UIColor *selectedColor;
@property (strong, nonatomic) AppDelegate *appDelegate;

@end

@implementation ColorPickerViewController

static NSString *const reuseIdentifier = @"ColorCell";
static NSInteger const NUMBLOCKS = 3;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.title = @"Pick a Profile Color";
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    self.colors = [NSMutableArray arrayWithArray:[CHColorHandler getColors]];
    [self.colors shuffle];
    
    RFQuiltLayout *layout = (id)[self.collectionView collectionViewLayout];
    layout.delegate = self;
    layout.direction = UICollectionViewScrollDirectionVertical;
    
    layout.blockPixels = CGSizeMake([[UIScreen mainScreen] applicationFrame].size.width / NUMBLOCKS, [[UIScreen mainScreen] applicationFrame].size.width / NUMBLOCKS);
    
    [self.collectionView reloadData];
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:@selector(done)];
    
    done.image = [IonIcons imageWithIcon:ion_checkmark size:35 color:[UIColor whiteColor]];
    
    if (self.appDelegate.currentUser.color != nil) {
        self.navigationController.navigationBar.barTintColor = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
    } else {
        self.navigationController.navigationBar.barTintColor = [UIColor bubblePink];
    }

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = done;
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if (self.selectedColor) {
     
        BSUser *currentUser = [BSUser currentUser];
        currentUser.color = [CHColorHandler getNSStringFromUIColor:self.selectedColor];
        
        [currentUser saveEventually];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionView Delegate/DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.colors.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    
    cell.backgroundColor = [self.colors objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    self.selectedColor = [self.colors objectAtIndex:indexPath.row];
    
    [self.navigationController.navigationBar setBarTintColor:self.selectedColor];
    
}

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

- (CGSize)blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath {
   
    /*if (indexPath.row % 2 == 0)
        return CGSizeMake(2, 1);*/
    
    return CGSizeMake(1, 1);
}

- (void)done {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserSetupProfile" object:nil];
    
    if ([self.parentViewController.childViewControllers containsObject:[EditProfileTableViewController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

@end
