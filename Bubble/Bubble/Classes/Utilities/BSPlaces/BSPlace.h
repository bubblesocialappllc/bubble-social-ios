//
//  BSPlace.h
//  Bubble
//
//  Created by Doug Woodrow on 3/1/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AFNetworking;
#import <Parse/Parse.h>

@interface BSPlace : NSObject

#pragma mark - Properties

/**
 *  The GeoPoint of the Place.
 */
@property (strong, nonatomic) PFGeoPoint *location;

/**
 *  The name of the Place.
 */
@property (strong, nonatomic) NSString *name;

/**
 *  An icon that can be displayed for the Place.
 */
@property (strong, nonatomic) NSString *iconURL;

/**
 *  The placeId that is unique to all Places.
 */
@property (strong, nonatomic) NSString *placeId;

/**
 *  The types of place the Place is.
 */
@property (strong, nonatomic) NSArray *types;

/**
 *  The vicinity of where the Place is. This could be an address
 */
@property (strong, nonatomic) NSString *vicinity;

/**
 *  A BOOL indicating if the Place is currently open.
 */
@property BOOL openNow;

#pragma mark - Methods

+ (BSPlace *)makePlaceWithName:(NSString *)name location:(CLLocation *)location placeId:(NSString *)placeId;

/**
 *  Finds places within 1 mile of the location that is passed in. Will only return up to 20 locations.
 *
 *  @param location The location to start the search from.
 *  @param block    The block to call with the found locations or an error.
 */
+ (void)findPlacesNearbyWithLocation:(CLLocation *)location completion:(void (^)(NSMutableArray *locationsNearby, NSError *error))block;

/**
 *  Finds places within 1 mile of the current location of the user. Will only return up to 20 locations.
 *
 *  @param block The block to call with the found locations or an error.
 */
+ (void)findPlacesNearbyWithCurrentLocationWithCompletion:(void (^)(NSMutableArray *locationsNearby, NSError *error))block;

/**
 *  Adds a place that Google doesn't already have. This is just for Bubble.
 *
 *  @param name     The name of the place.
 *  @param location The geopoint of the place.
 */
+ (void)addPlaceWithName:(NSString *)name andLocation:(CLLocation *)location completion:(void (^)(BSPlace *place, NSError *error))block;

/**
 *  Adds a place that Google doesn't already have at the current users location. THis is just for Bubble.
 *
 *  @param name The name of the place.
 */
+ (void)addPlaceAtCurrentLocationWithName:(NSString *)name completion:(void (^)(BSPlace *place, NSError *error))block;

@end
