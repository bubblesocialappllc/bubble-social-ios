//
//  BSPlace.m
//  Bubble
//
//  Created by Doug Woodrow on 3/1/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSPlace.h"

static NSString *const apiKey = @"AIzaSyBZbnRgIUBIeUhlyx-K9AS2dvqTCF9fGdQ";
static NSString *const baseURL = @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
static NSString *const baseAddURL = @"https://maps.googleapis.com/maps/api/place/add/json?key=AIzaSyBZbnRgIUBIeUhlyx-K9AS2dvqTCF9fGdQ";

static NSString *const types = @"accounting|airport|amusement_park|aquarium|art_gallery|atm|bakery|bank|bar|beauty_salon|bicycle_store|book_store|bowling_alley|bus_station|cafe|campground|car_dealer|car_rental|car_repair|car_wash|casino|cemetery|church|city_hall|clothing_store|convenience_store|courthouse|dentist|department_store|doctor|electrician|electronics_store|embassy|establishment|finance|fire_station|florist|food|funeral_home|furniture_store|gas_station|general_contractor|grocery_or_supermarket|gym|hair_care|hardware_store|health|hindu_temple|home_goods_store|hospital|insurance_agency|jewelry_store|laundry|lawyer|library|liquor_store|local_government_office|locksmith|lodging|meal_delivery|meal_takeaway|mosque|movie_rental|movie_theater|moving_company|museum|night_club|painter|park|parking|pet_store|pharmacy|physiotherapist|place_of_worship|plumber|police|post_office|real_estate_agency|restaurant|roofing_contractor|rv_park|school|shoe_store|shopping_mall|spa|stadium|storage|store|subway_station|synagogue|taxi_stand|train_station|travel_agency|university|veterinary_care|zoo";

@interface BSPlace ()

@property (strong, nonatomic) AppDelegate *appDelegate;

@end

@implementation BSPlace

/**
 *  Inits BSPlace.
 *
 *  @return A newly initilized BSPlace Object.
 */
- (id)init {
    
    if (self = [super init]) {
        self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    
    return self;
}


//NOTE ABOUT GOOGLE PLACES API RETURNS: Any given response will only return 20 places around you.

#pragma mark - Public Methods

+ (BSPlace *)makePlaceWithName:(NSString *)name location:(CLLocation *)location placeId:(NSString *)placeId {
    
    BSPlace *place = [BSPlace new];
    place.name = name;
    place.location = [PFGeoPoint geoPointWithLocation:location];
    place.placeId = placeId;
    place.iconURL = @"";
    place.types = @[@"other"];
    place.vicinity = @"";
    
    return place;
}

+ (void)findPlacesNearbyWithLocation:(CLLocation *)location completion:(void (^)(NSMutableArray *locationsNearby, NSError *error))block {
    
    PFGeoPoint *currentLocation = [PFGeoPoint geoPointWithLocation:location];
    
    [BSPlace findPlacesNearbyWithLocation:currentLocation block:^(NSDictionary *response, NSError *error) {
        
        if (!error) {
            
            NSMutableArray *array = [BSPlace formatData:response];
            
            block(array, nil);
        } else {
            block(nil, error);
            NSLog(@"%@", error);
        }
    }];

}

+ (void)findPlacesNearbyWithCurrentLocationWithCompletion:(void (^)(NSMutableArray *locationsNearby, NSError *error))block {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    PFGeoPoint *currentLocation = appDelegate.currentLocation;
    
    [BSPlace findPlacesNearbyWithLocation:currentLocation block:^(NSDictionary *response, NSError *error) {
       
        if (!error) {
            
            NSMutableArray *array = [BSPlace formatData:response];
            
            block(array, nil);
        } else {
            block(nil, error);
            NSLog(@"%@", error);
        }
    }];
    
}

+ (void)addPlaceWithName:(NSString *)name andLocation:(CLLocation *)location completion:(void (^)(BSPlace *place, NSError *error))block{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    PFGeoPoint *currentLocation = [PFGeoPoint geoPointWithLocation:location];
    
    NSDictionary *parameters = @{
                                 @"name" : name,
                                 @"location" : @{
                                         @"lat" : [NSNumber numberWithDouble:currentLocation.latitude],
                                         @"lng" : [NSNumber numberWithDouble:currentLocation.longitude]
                                         },
                                 @"types" : @[@"establishment"]
                                 };
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:baseAddURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        block([BSPlace makePlaceWithName:name location:location placeId:responseObject[@"place_id"]], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        block(nil, error);
    }];
    
}

+ (void)addPlaceAtCurrentLocationWithName:(NSString *)name completion:(void (^)(BSPlace *place, NSError *error))block {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:appDelegate.currentLocation.latitude longitude:appDelegate.currentLocation.longitude];
    
    [BSPlace addPlaceWithName:name andLocation:location completion:^(BSPlace *place, NSError *error) {
        
        block(place, error);
        
    }];
}


#pragma mark - Private Methods

/**
 *  Private method to get locations for a given location.
 *
 *  @param location The user's location as a CLLocation.
 *  @param block    Returns on finding locations nearby.
 */
+ (void)findPlacesNearbyWithLocation:(PFGeoPoint *)location block:(void (^)(NSDictionary *response, NSError *error))block {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *parameters = @{
                                  @"key" : apiKey,
                                  @"location" : [NSString stringWithFormat:@"%f,%f",location.latitude, location.longitude],
                                  @"rankby" : @"distance",
                                  @"type" : types,
                                 };
    
    __block NSDictionary *response;
    
    [manager GET:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        response = responseObject;
        block(response, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        block(nil, error);
        NSLog(@"%@", error.description);
    }];
    
}

/**
 *  Formats the JSON returned from findPlacesNearbyWithLocaiton:block: in BSPlace Objects.
 *
 *  @param data The data to format into BSPlace Objects.
 *
 *  @return An array of BSPlace Object.
 */
+ (NSMutableArray *)formatData:(NSDictionary *)data {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *array = [NSMutableArray new];
    
    NSArray *results = [data objectForKey:@"results"];
    
    [results each:^(id object) {
        
        BSPlace *place = [BSPlace new];
        
        NSNumber *lat = [[[object objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"];
        NSNumber *lng = [[[object objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"];
    
        place.name = [object objectForKey:@"name"];
        place.placeId = [object objectForKey:@"place_id"];
        place.iconURL = [object objectForKey:@"icon"];
        place.types = [object objectForKey:@"types"];
        place.vicinity = [object objectForKey:@"vicinity"];
        place.location = [PFGeoPoint geoPointWithLatitude:[lat doubleValue] longitude:[lng doubleValue]];
        
        CGFloat miles = [place.location distanceInMilesTo:appDelegate.currentLocation];
        
        if (miles < 1) {
            [array addObject:place];
        }
        
    }];
    
    return array;
    
}

@end
