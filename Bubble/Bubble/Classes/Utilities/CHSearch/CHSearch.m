//
//  CHSearch.m
//  Bubble
//
//  Created by Chayel Heinsen on 1/30/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "CHSearch.h"

@interface CHSearch ()

@property (readwrite, nonatomic) NSString *name;
@property (readwrite, nonatomic) NSString *realName;
@property (readwrite, nonatomic) BSUser *user;

@end

@implementation CHSearch

+ (NSMutableArray *)filteredUsernamesFromUsers:(NSArray *)users withSubstring:(NSString *)substring {
    
    NSMutableArray *filteredUsers = [NSMutableArray new];
    NSString *search = [substring stringByAppendingString:@"*"];
    
    if ([users.firstObject class] == [BSUser class]) {
        
        for (BSUser *user in users) {
            
            CHSearch *data = [CHSearch new];
            
            data.name = user.username;
            data.realName = user.realName;
            data.user = user;
            
            [filteredUsers addObject:data];
        }
        
    } else {
        
        for (NSString *user in users) {
            
            CHSearch *data = [CHSearch new];
            
            data.name = user;
            
            [filteredUsers addObject:data];
        }
        
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name LIKE[c] %@", search];
    
    [filteredUsers filterUsingPredicate:predicate];
    
    return filteredUsers;
    
}

@end
