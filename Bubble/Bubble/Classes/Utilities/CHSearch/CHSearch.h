//
//  CHSearch.h
//  Bubble
//
//  Created by Chayel Heinsen on 1/30/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CHSearch : NSObject

@property (readonly, nonatomic) NSString *name;
@property (readonly, nonatomic) NSString *realName;
@property (readonly, nonatomic) BSUser *user;
/**
 *  Filters the users with the given substring.
 *
 *  @param users     The PFUser objects or NSString objects to filter through.
 *  @param substring The string to filter with.
 *
 *  @return An array of results.
 */
+ (NSMutableArray *)filteredUsernamesFromUsers:(NSArray *)users withSubstring:(NSString *)substring;


@end
