//
//  BSConnect.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/7/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MultipeerConnectivity;

@protocol BSConnectDelegate <NSObject>

/**
 *  Notifies the delegate that we have received data from a peer.
 *
 *  @param session The session that was used to connect peers.
 *  @param data    The data that we received.
 *  @param peerID  The peerID of the user who sent us the data. This should be their username.
 */
- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID;

/**
 *  Notifies the delegate when the peer changes state. This will let you know when a user has accepted an invitation and the devices are connected.
 *
 *  @param session The session that was used to connect peers.
 *  @param peerID  The peerID of the user who sent us the resource. This should be their username.
 *  @param state   The state of the session. Look at MCSessionState for states.
 */
- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state;

/**
 *  Askes the delegate if we should connect to a receiving invitation.
 *
 *  @param advertiser   The advertiser object that was invited to join the session.
 *  @param peerID       The peerID of the user who sent us the invitation. This should be their username.
 *  @param context      An arbitrary piece of data received from the nearby peer. This can be used to provide further information to the user about the nature of the invitation.
 *
 *  @return Return YES if we should connect to the peer, otherwise NO;
 */
- (BOOL)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID withContext:(NSData *)context;

@optional

/**
 *  Notifies the delegate that we have started receiving a resource from a peer. Resources are usually images, videos or documents.
 *
 *  @param session      The session that was used to connect peers.
 *  @param resourceName The name of the resource we are receiving.
 *  @param peerID       The peerID of the user who sent us the resource. This should be thier username.
 *  @param progress     The current progress.
 */
- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress;

/**
 *  Notifies the delegate that we have finished recieving a resource from a peer. Resources are usually imagse, videos or documents.
 *
 *  @param session      The session that was used to connect peers.
 *  @param resourceName The name of the resource we received.
 *  @param peerID       The peerID of the user who sent us the resource. This should be their username.
 *  @param localURL     The url of the resource where you can find the resource on device.
 *  @param error        An error is on occured, otherwise nil.
 */
- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error;

/**
 *  Notifies the delegate that we recieved a stream.
 *
 *  @param session    The session that was used to connect peers.
 *  @param stream     The stream. Streaming music for example.
 *  @param streamName The name of the stream.
 *  @param peerID     The peerID of the user who sent us the stream. This should be their username.
 */
- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID;

/**
 *  Notifies the delegate when we found the peer we where looking for.
 *
 *  @param browser The browser that found the peer.
 *  @param peerID  The peerID of the user that we were looking for.
 */
- (void)browser:(MCNearbyServiceBrowser *)browser foundSearchedPeer:(MCPeerID *)peerID;

/**
 *  Notifies the delegate when we have found a peer;
 *
 *  @param browser The browser that found the peer.
 *  @param peerID  The peerID of the user that was found.
 *  @param info    The info dictionary advertised by the discovered peer. For more information on the contents of this dictionary, see the documentation for initWithPeer:discoveryInfo:serviceType: in MCNearbyServiceAdvertiser Class Reference.
 */
- (void)browser:(MCNearbyServiceBrowser *)browser foundPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary *)info;

/**
 *  Notifies the delegate when we have lost a peer.
 *
 *  @param browser The browser that found the peer.
 *  @param peerID  The peerID of the user that was lost.
 */
- (void)browser:(MCNearbyServiceBrowser *)browser lostPeer:(MCPeerID *)peerID;

@end

@interface BSConnect : NSObject <MCNearbyServiceAdvertiserDelegate, MCNearbyServiceBrowserDelegate, MCSessionDelegate>

#pragma mark - Properties

@property (weak, nonatomic) id<BSConnectDelegate> delegate;

/**
 *  The current users peerID. This should be their username.
 */
@property (strong, nonatomic) MCPeerID *peerID;

/**
 *  The service name to run on. Ex: bubble-service
 */
@property (strong, nonatomic) NSString *service;

/**
 *  The advertiser that notifies availability of the local peer and handles invitations from nearby peers.
 */
@property (strong, nonatomic) MCNearbyServiceAdvertiser *advertiser;

/**
 *  The browser that looks for other devices that are broadcasting.
 */
@property (strong, nonatomic) MCNearbyServiceBrowser *browser;

/**
 *  The session facilitates communication among all peers in a multipeer session.
 */
@property (strong, nonatomic) MCSession *session;

#pragma mark - Public Methods

/**
 *  Access the BSConnect instance. You should call the following on your App Delegate.
 *  BSConnect *connect = [BSConnect sharedConnect];
 *  [connect setDelegate:self];
 *  [connect setPeerID:peerID service:service];
 *
 *  @return The BSConnect Instance.
 */
+ (instancetype)sharedConnect;

/**
 *  Initializes a BSConnect object.
 *
 *  @param peerID  The peerID to set the current user to. This should be the users' username.
 *  @param service The service to bradcast on. Must be 1–15 characters long. Can contain only ASCII lowercase letters, numbers, and hyphens.
 *
 *  @return A newly Initialized BSConnect object.
 */
/*- (id)initWithPeerID:(MCPeerID *)peerID service:(NSString *)service;*/

/**
 *  Starts advertising the peer to other users.
 */
- (void)startAdvertising;

/**
 *  Stops advertising the peer to other users.
 */
- (void)stopAdvertising;

/**
 *  Starts searching for a peerID.
 *
 *  @param peerID The peerID to search for.
 */
- (void)startBrowsingForPeerID:(MCPeerID *)peerID;

/**
 *  Stops searching. This will automatically be called when the peerID is found.
 */
- (void)stopBrowsing;

/**
 *  Set the peerID and the session of BSConnect.
 *
 *  @param peerID  The current users peerID. This should be their username.
 *  @param service The service to bradcast on. Must be 1–15 characters long. Can contain only ASCII lowercase letters, numbers, and hyphens.
 */
- (void)setPeerID:(MCPeerID *)peerID service:(NSString *)service;

@end
