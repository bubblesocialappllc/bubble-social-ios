//
//  BSConnect.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/7/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSConnect.h"

BOOL usingSharedConnect = NO;

@interface BSConnect ()

@property (strong, nonatomic) MCPeerID *browsingForPeerID;

@end

@implementation BSConnect

- (id)init {
    self = [super init];
   
    if (self) {
        NSAssert(usingSharedConnect, @"BSConnect should contains a shared instance. Please use that. [BSConnect sharedConnect]");
    }
    
    return self;
}

- (id)initWithPeerID:(MCPeerID *)peerID service:(NSString *)service {
    
    if (self = [super init]) {
        
        self.peerID = peerID;
        self.session = [[MCSession alloc] initWithPeer:self.peerID securityIdentity:nil encryptionPreference:MCEncryptionOptional];
        self.session.delegate = self;
        
        self.advertiser = [[MCNearbyServiceAdvertiser alloc] initWithPeer:self.peerID discoveryInfo:nil serviceType:service];
        self.advertiser.delegate = self;
        
        self.browser = [[MCNearbyServiceBrowser alloc] initWithPeer:self.peerID serviceType:service];
        self.browser.delegate = self;
        
    }
    
    return self;
    
}

#pragma mark - Public Methods

+ (instancetype)sharedConnect {
    
    // Structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // Initialize sharedObject as nil (first call only)
    __strong static BSConnect *sharedConnect = nil;
    
    // Executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        usingSharedConnect = YES;
        sharedConnect = [[self alloc] initWithPeerID:[[MCPeerID alloc] initWithDisplayName:@"init"] service:@"init"];
        usingSharedConnect = NO;
    });
    
    // Returns the same object each time
    return sharedConnect;
    
}

- (void)setPeerID:(MCPeerID *)peerID service:(NSString *)service {
    
    self.peerID = peerID;
    self.service = service;
    
    self.session = [[MCSession alloc] initWithPeer:self.peerID securityIdentity:nil encryptionPreference:MCEncryptionOptional];
    self.session.delegate = self;
    
    self.advertiser = [[MCNearbyServiceAdvertiser alloc] initWithPeer:self.peerID discoveryInfo:nil serviceType:service];
    self.advertiser.delegate = self;
    
    self.browser = [[MCNearbyServiceBrowser alloc] initWithPeer:self.peerID serviceType:service];
    self.browser.delegate = self;
    
}

- (void)startAdvertising {
    
    [self.advertiser startAdvertisingPeer];
    
}

- (void)stopAdvertising {
    
    [self.advertiser stopAdvertisingPeer];
    
}

- (void)startBrowsingForPeerID:(MCPeerID *)peerID {
    
    self.browsingForPeerID = peerID;
    [self.browser startBrowsingForPeers];
    
}

- (void)stopBrowsing {
    
    [self.browser stopBrowsingForPeers];
    
}

#pragma mark - MCSession Delegate

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state {
    
    if ([self.delegate respondsToSelector:@selector(session:peer:didChangeState:)]) {
        
        [self.delegate session:session peer:peerID didChangeState:state];
        
    } else {
        
        [NSException exceptionWithName:@"BSConnectDelegate couldn't call session:peer:didChangeState:" reason:@"session:peer:didChangeState: is required" userInfo:nil];
        
    }
    
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID  {
    
    if ([self.delegate respondsToSelector:@selector(session:didReceiveData:fromPeer:)]) {
        
        [self.delegate session:session didReceiveData:data fromPeer:peerID];
        
    } else {
        
        [NSException exceptionWithName:@"BSConnectDelegate couldn't call session:didReceiveData:fromPeer" reason:@"session:didReceiveData:fromPeer is required" userInfo:nil];
        
    }
    
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress {
    
    if ([self.delegate respondsToSelector:@selector(session:didStartReceivingResourceWithName:fromPeer:withProgress:)]) {
        
        [self.delegate session:session didStartReceivingResourceWithName:resourceName fromPeer:peerID withProgress:progress];
        
    }
    
}


- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error {
    
    if ([self.delegate respondsToSelector:@selector(session:didFinishReceivingResourceWithName:fromPeer:atURL:withError:)]) {
        
        [self.delegate session:session didFinishReceivingResourceWithName:resourceName fromPeer:peerID atURL:localURL withError:error];
        
    }
    
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID {
    
    if ([self.delegate respondsToSelector:@selector(session:didReceiveStream:withName:fromPeer:)]) {
        
        [self.delegate session:session didReceiveStream:stream withName:streamName fromPeer:peerID];
        
    }
    
}

#pragma mark - MCNearbyServiceBrowser Delegate

- (void)browser:(MCNearbyServiceBrowser *)browser foundPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary *)info {
    
    if ([self.delegate respondsToSelector:@selector(browser:foundPeer:withDiscoveryInfo:)]) {
        
        [self.delegate browser:browser foundPeer:peerID withDiscoveryInfo:info];
        
    }
    
    if ([peerID.displayName isEqualToString:self.browsingForPeerID.displayName]) {
        
        if ([self.delegate respondsToSelector:@selector(browser:foundSearchedPeer:)]) {
            
            [self.delegate browser:browser foundSearchedPeer:self.browsingForPeerID];
            
        }
        
        [self stopBrowsing];
        
    }
    
}

- (void)browser:(MCNearbyServiceBrowser *)browser lostPeer:(MCPeerID *)peerID {
    
    if ([self.delegate respondsToSelector:@selector(browser:lostPeer:)]) {
        
        [self.delegate browser:browser lostPeer:peerID];
        
    }
    
}

#pragma mark - MCNearbyServiceAdvertiser Delegate

- (void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID withContext:(NSData *)context invitationHandler:(void (^)(BOOL, MCSession *))invitationHandler {
    
    if ([self.delegate respondsToSelector:@selector(advertiser:didReceiveInvitationFromPeer:withContext:invitationHandler:)]) {
        
        BOOL connect = [self.delegate advertiser:advertiser didReceiveInvitationFromPeer:peerID withContext:context];
        
        invitationHandler(connect, (connect ? self.session : nil));
        
    } else {
        
        [NSException exceptionWithName:@"BSConnectDelegate couldn't call advertiser:didReceiveInvitationFromPeer:withContext:invitationHandler:" reason:@"advertiser:didReceiveInvitationFromPeer:withContext:invitationHandler: is required" userInfo:nil];
        
    }
    
}

@end
