//
//  BSAppCall.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/4/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSScheme.h"
@import Parse;

@interface BSAppCall : NSObject

/**
 *  This should be called in application:openURL:sourceApplication:annotation: This will open the page specified by the url. Please reference THIS CLASS to url prototype.
 *
 *  @param url               The URL that was asked to open.
 *  @param sourceApplication The application that requested the URL to be opened.
 *
 *  @return YES if the URL was meant for Bubble Social, otherwise NO.
 */
+ (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication;

@end
