//
//  BSAppCall.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/4/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSAppCall.h"
#import "CHColorHandler.h"

 /**
  *
  * This is the url prototype.
  *
  * @"bubblesocial://parseclassname/?param1=this&param2=that"
  *
  * Example: bubblesocial://post/?objectId=gDJWx6pbVx
  *
  */

@implementation BSAppCall

#pragma mark - Public

+ (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication {
    
    if (![[url scheme] isEqualToString:@"bubblesocial"]) {
        return NO;
    }

    if (url.absoluteString.length < 18) {
        return NO;
    }
    
    NSDictionary *urlDict = [BSScheme convertRESTToURL:url.absoluteString];
    
    if (urlDict == nil) {
        return NO;
    }
    
    NSDictionary *query = [self parseQueryString:urlDict[@"query"]];
    
    if ([urlDict[@"path"] isEqualToString:@"post"]) {
        [self openPostWithQuery:query];
    } else if ([urlDict[@"path"] isEqualToString:@"view"]) {
        
        if ([query[@"view"] isEqualToString:@"notificationfeed"]) {
            [self changeTabWithName:@"notificationfeed"];
        } else if ([query[@"view"] isEqualToString:@"aroundfeed"]) {
            [self changeTabWithName:@"aroundme"];
        } else if ([query[@"view"] isEqualToString:@"buddiesfeed"]) {
            [self changeTabWithName:@"buddiesfeed"];
        } else if ([query[@"view"] isEqualToString:@"messages"]) {
            [self changeTabWithName:@"messages"];
        } else if ([query[@"view"] isEqualToString:@"map"]) {
            [self changeTabWithName:@"map"];
        } else if ([query[@"view"] isEqualToString:@"me"]) {
            [self changeTabWithName:@"me"];
        } else if ([query[@"view"] isEqualToString:@"search"]) {
            [self changeTabWithName:@"search"];
        } else if ([query[@"view"] isEqualToString:@"createpost"]) {
            [self openStatusWithQuery:query];
        } else if ([query[@"view"] isEqualToString:@"range"]) {
            [self openRangeWithQuery:query];
        } else if ([query[@"view"] isEqualToString:@"user"]) {
            [self openUserWithQuery:query];
        } else if ([query[@"view"] isEqualToString:@"buddyrequest"]) {
            [self openBuddyRequestWithQuery:query];
        }
        
    }
    
    return YES;
}

#pragma mark - Private

/**
 *  Parses the query from the URL.
 *
 *  @param query The query to parse.
 *
 *  @return A dictionary containing the keys and values.
 */
+ (NSDictionary *)parseQueryString:(NSString *)query {
 
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:6];
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    
    for (NSString *pair in pairs) {
        
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        
        NSString *key = [[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString *val = [[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [dict setObject:val forKey:key];
    }
    
    return dict;
}

/**
 *  Presents the view controller modally.
 *
 *  @param viewController The view controller to present.
 *  @param query          The dictionary that was returned from parsing the query.
 */
+ (void)showViewController:(UIViewController *)viewController withQuery:(NSDictionary *)query {
    
    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    [tabBarController presentViewController:viewController animated:YES completion:nil];
    
}

+ (void)openPostWithQuery:(NSDictionary *)query {
    
    PFQuery *postQuery = [BSPost query];
    [postQuery getObjectInBackgroundWithId:[query objectForKey:@"objectId"] block:^(PFObject *object, NSError *error) {
        
        BSPost *post = (BSPost *)object;
        
        if (post.photo) {
            
            PFQuery *photoQ = [BSPhoto query];
            [photoQ whereKey:@"post" equalTo:post.objectId];
            [photoQ getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
               
                BSPhotoViewHandler *handler = [[BSPhotoViewHandler alloc] initWithPhoto:(BSPhoto *)object];
                
                EBPhotoPagesController *controller = [[EBPhotoPagesController alloc] initWithDataSource:handler delegate:handler];
                
                [self showViewController:controller withQuery:query];
                                                      
            }];
            
        } else {
        
            CommentsViewController *post = [CommentsViewController new];
            post.post = (BSPost *)object;
            
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:post];
            nc.navigationBar.barTintColor = [CHColorHandler getUIColorFromNSString:post.post.author.color];
            
            [self showViewController:nc withQuery:query];
            
        }
    }];
    
}

+ (void)changeTabWithName:(NSString *)name {
    
    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    if ([name isEqualToString:@"notificationfeed"]) {
        [tabBarController setSelectedIndex:FEED_TAB];
    } else if ([name isEqualToString:@"aroundfeed"]) {
        [tabBarController setSelectedIndex:FEED_TAB];
    } else if ([name isEqualToString:@"buddiesfeed"]) {
        [tabBarController setSelectedIndex:FEED_TAB];
    } else if ([name isEqualToString:@"messages"]) {
        [tabBarController setSelectedIndex:MESSAGES_TAB];
    } else if ([name isEqualToString:@"map"]) {
        [tabBarController setSelectedIndex:MAP_TAB];
    } else if ([name isEqualToString:@"search"]) {
        [tabBarController setSelectedIndex:SEARCH_TAB];
    } else if ([name isEqualToString:@"me"]) {
        [tabBarController setSelectedIndex:ME_TAB];
    }
    
}

+ (void)openUserWithQuery:(NSDictionary *)query {
    
    PFQuery *userQuery = [BSUser query];
    [userQuery getObjectInBackgroundWithId:query[@"objectId"] block:^(PFObject *object, NSError *error) {
        
        UserViewController *user = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"UserViewController"];
        
        user.user = (BSUser *)object;
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
        
        [self showViewController:nc withQuery:query];
        
    }];
    
}

+ (void)openStatusWithQuery:(NSDictionary *)query {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenPost" object:nil];
    
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    RRSendMessageViewController *sendMessage = [RRSendMessageViewController new];
    [sendMessage presentController:tabBarController blockCompletion:^(RRMessageModel *model, BOOL isCancel, BSPlace *place) {
        
        if (!isCancel) {
            
            NSData *data = [NSData new];
            
            if (model.photos.count > 0) {
                
                data = UIImageJPEGRepresentation(model.photos.firstObject, 0.4);
                
            }
            
            [CloudCode postStatus:model.text file:data location:appDelegate.currentLocation pointOfInterest:place];
            
        }
        
        [tabBarController dismissViewControllerAnimated:YES completion:nil];

    }];
     */
    
}

+ (void)openBuddyRequestWithQuery:(NSDictionary *)query {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    ViewBuddiesViewController *buddies = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ViewBuddiesViewController"];
    
    buddies.user = appDelegate.currentUser;
    buddies.title = @"My Buddies";
    buddies.isModal = YES;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:buddies];
    nc.navigationBar.barTintColor = [CHColorHandler getUIColorFromNSString:appDelegate.currentUser[@"Color"]];
                                     
    [self showViewController:nc withQuery:query];
    
}

+ (void)openRangeWithQuery:(NSDictionary *)query {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenPopularity" object:nil];
}

@end
