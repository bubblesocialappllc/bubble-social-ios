//
//  BSMessage.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

@import Parse;
#import "BSUser.h"
#import "BSMessageHead.h"

@interface BSMessage : PFObject <PFSubclassing>

@property (strong, nonatomic) BSMessageHead *parent;
@property (strong, nonatomic) BSUser *sender;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) PFFile *photo;
@property (strong, nonatomic) PFGeoPoint *location;

+ (NSString *)parseClassName;

@end
