//
//  BSMessage.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSMessage.h"

@implementation BSMessage

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic parent, sender, text, photo, location;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"Message";
}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

@end