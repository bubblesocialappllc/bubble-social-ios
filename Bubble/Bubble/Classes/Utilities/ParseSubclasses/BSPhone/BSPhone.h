//
//  BSPhone.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>

@interface BSPhone : PFObject <PFSubclassing>

@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSNumber *verificationCode;
@property (nonatomic) BOOL verified;

+ (NSString *)parseClassName;

@end
