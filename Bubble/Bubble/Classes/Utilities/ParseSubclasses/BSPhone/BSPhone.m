//
//  BSPhone.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSPhone.h"

@implementation BSPhone

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic phoneNumber, verified, verificationCode;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"Phone";
}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

@end
