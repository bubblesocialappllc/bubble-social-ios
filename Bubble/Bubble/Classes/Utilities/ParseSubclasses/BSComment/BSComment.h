//
//  BSComment.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
#import "BSPost.h"
@import EBPhotoPages_BubbleSocial;

@interface BSComment : PFObject <PFSubclassing, EBPhotoCommentProtocol>

@property (strong, nonatomic) NSString *comment;
@property (strong, nonatomic) NSString *commenter;
@property (strong, nonatomic) NSString *locationName;
@property (strong, nonatomic) PFFile *photo;
@property (strong, nonatomic) BSPost *post;
@property (strong, nonatomic) PFFile *profilePic;

+ (NSString *)parseClassName;

@end
