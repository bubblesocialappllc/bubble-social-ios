//
//  BSComment.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSComment.h"

@implementation BSComment

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic comment, commenter, locationName, photo, profilePic, post;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"Comment";
}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

#pragma mark - EBPhotoTagProtocol

- (NSString *)commentText {
    return self.comment;
}

- (NSDate *)postDate {
    return self.createdAt;
}

- (NSString *)authorName {
    return self.commenter;
}

- (UIImage *)authorAvatar {
    return [self loadFile];
}

#pragma mark - Helpers

- (UIImage *)loadFile {
    
    __strong __block UIImage *image;
    
    PFFile *file = self.profilePic;
    
    [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
       
        image = [UIImage imageWithData:data];
        
    }];
    
    return image;
}

@end
