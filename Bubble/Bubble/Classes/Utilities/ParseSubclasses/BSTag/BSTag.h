//
//  BSTag.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/20/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>

@interface BSTag : PFObject <PFSubclassing, EBPhotoTagProtocol>

/**
 *  The text of the tag.
 */
@property (strong, nonatomic) NSString *text;

/**
 *  CGPoint as a string - {x,y} To use this please use CGPointFromString(@"{x,y}"). To convert back to an NSString, use NSStringFromCGPoint(point)
 */
@property (strong, nonatomic) NSString *point;

+ (NSString *)parseClassName;
+ (NSString *)tagPointWithCGPoint:(CGPoint)point;

@end
