//
//  BSTag.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/20/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSTag.h"

@implementation BSTag

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic text, point;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"Tag";
}

+ (NSString *)tagPointWithCGPoint:(CGPoint)point {
    return NSStringFromCGPoint(point);
}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

#pragma mark - EBPhotoTagProtocol

- (CGPoint)normalizedPosition {
    return CGPointFromString(self.point);
}

- (NSString *)tagText {
    return self.text;
}

@end
