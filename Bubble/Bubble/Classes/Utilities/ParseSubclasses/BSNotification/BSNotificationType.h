//
//  BSNotificationType.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSNotificationType : NSString

+ (BSNotificationType *)BSLikeNotification;
+ (BSNotificationType *)BSCommentNotification;
+ (BSNotificationType *)BSBuddyRequestNotification;
+ (BSNotificationType *)BSResponseNotification;
+ (BSNotificationType *)BSMentionNotification;
+ (BSNotificationType *)BSMentionedInCommentNotification;

@end
