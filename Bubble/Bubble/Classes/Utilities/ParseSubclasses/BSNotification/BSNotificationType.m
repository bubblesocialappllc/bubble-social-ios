//
//  BSNotificationType.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSNotificationType.h"

@implementation BSNotificationType

+ (BSNotificationType *)BSNotificationTypeWithString:(NSString *)string {
    return (BSNotificationType *)string;
}

+ (BSNotificationType *)BSLikeNotification {
    return [BSNotificationType BSNotificationTypeWithString:@"Like"];
}

+ (BSNotificationType *)BSCommentNotification {
    return [BSNotificationType BSNotificationTypeWithString:@"Comment"];
}

+ (BSNotificationType *)BSBuddyRequestNotification {
    return [BSNotificationType BSNotificationTypeWithString:@"BuddyRequest"];
}

+ (BSNotificationType *)BSResponseNotification {
    return [BSNotificationType BSNotificationTypeWithString:@"Response"];
}

+ (BSNotificationType *)BSMentionNotification {
    return [BSNotificationType BSNotificationTypeWithString:@"Mention"];
}

+ (BSNotificationType *)BSMentionedInCommentNotification {
    return [BSNotificationType BSNotificationTypeWithString:@"MentionInComment"];
}

@end
