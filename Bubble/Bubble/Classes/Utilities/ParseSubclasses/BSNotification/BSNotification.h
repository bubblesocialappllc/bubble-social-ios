//
//  BSNotification.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
#import "BSNotificationType.h"
#import "BSPost.h"

@interface BSNotification : PFObject <PFSubclassing>

@property (strong, nonatomic) BSPost *post;
@property (strong, nonatomic) NSString *query;
@property (strong, nonatomic) NSString *sendTo;
@property (strong, nonatomic) NSString *sentFrom;
@property (strong, nonatomic) BSNotificationType *type;

+ (NSString *)parseClassName;

@end
