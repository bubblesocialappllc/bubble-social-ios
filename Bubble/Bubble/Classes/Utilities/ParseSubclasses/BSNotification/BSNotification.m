//
//  BSNotification.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSNotification.h"

@implementation BSNotification

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic post, query, sendTo, sentFrom, type;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"Notification";
}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

@end
