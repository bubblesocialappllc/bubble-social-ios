//
//  BSPhoto.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
#import "BSUser.h"

@interface BSPhoto : PFObject <PFSubclassing>

@property (strong, nonatomic) BSUser *user;
@property (strong, nonatomic) PFFile *photo;
@property (strong, nonatomic) NSString *post;
@property (strong, nonatomic) NSMutableArray *tags;
@property (strong, nonatomic) NSMutableArray *comments;

+ (NSString *)parseClassName;

@end
