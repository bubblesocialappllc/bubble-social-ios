//
//  BSPhoto.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSPhoto.h"

@implementation BSPhoto

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic user, photo, post, tags, comments;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"Photo";
}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

//#pragma mark - FICEntity
//
//- (NSString *)UUID {
//    CFUUIDBytes UUIDBytes = FICUUIDBytesFromMD5HashOfString(self.user.objectId);
//    NSString *UUID = FICStringWithUUIDBytes(UUIDBytes);
//    return UUID;
//}
//
//- (NSString *)sourceImageUUID {
//    CFUUIDBytes sourceImageUUIDBytes = FICUUIDBytesFromMD5HashOfString(self.photo.url);
//    NSString *sourceImageUUID = FICStringWithUUIDBytes(sourceImageUUIDBytes);
//    return sourceImageUUID;
//}
//
//- (NSURL *)sourceImageURLWithFormatName:(NSString *)formatName {
//    return [NSURL URLWithString:self.photo.url];
//}
//
//- (FICEntityImageDrawingBlock)drawingBlockForImage:(UIImage *)image withFormatName:(NSString *)formatName {
//    
//    FICEntityImageDrawingBlock drawingBlock = ^(CGContextRef context, CGSize contextSize) {
//        CGRect contextBounds = CGRectZero;
//        contextBounds.size = contextSize;
//        CGContextClearRect(context, contextBounds);
//        
//        /*// Clip medium thumbnails so they have rounded corners
//         if ([formatName isEqualToString:XXImageFormatNameUserThumbnailMedium]) {
//         UIBezierPath clippingPath = [self _clippingPath];
//         [clippingPath addClip];
//         }*/
//        
//        UIGraphicsPushContext(context);
//        [image drawInRect:contextBounds];
//        UIGraphicsPopContext();
//    };
//    
//    return drawingBlock;
//}

@end
