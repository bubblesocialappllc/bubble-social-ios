//
//  BSPost.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/26/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
#import "BSUser.h"

@interface BSPost : PFObject <PFSubclassing>

@property (strong, nonatomic) NSString *authorId;
@property (strong, nonatomic) BSUser *author;
@property (strong, nonatomic) NSString *authorUsername;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *hashtag;
@property (strong, nonatomic) PFFile *photo;
@property (strong, nonatomic) NSArray *commenters;
@property (strong, nonatomic) NSNumber *comments;
@property (strong, nonatomic) NSArray *likers;
@property (strong, nonatomic) NSArray *hasLikedBefore;
@property (strong, nonatomic) NSNumber *likes;
@property (strong, nonatomic) PFGeoPoint *location;
@property (strong, nonatomic) NSString *locationName;
@property (strong, nonatomic) NSNumber *points;
@property (strong, nonatomic) PFFile *profilePic;
@property (strong, nonatomic) NSArray *reporters;
@property (strong, nonatomic) NSArray *mentioned;

+ (NSString *)parseClassName;

@end
