//
//  BSPost.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/26/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSPost.h"

@implementation BSPost

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic authorId, author, authorUsername, text, hashtag, photo, commenters, comments, likers, hasLikedBefore, likes, location, locationName, points, profilePic, reporters, mentioned;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"Post";
}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

@end
