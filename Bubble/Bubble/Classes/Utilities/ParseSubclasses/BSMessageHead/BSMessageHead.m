//
//  BSMessageHead.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSMessageHead.h"

@implementation BSMessageHead

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic firstUser, firstUserUsername, secondUser, secondUserUsername, lastMessage, lastMessageDate;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"MessageHead";
}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

@end