//
//  BSMessageHead.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

@import Parse;
#import "BSUser.h"

@interface BSMessageHead : PFObject <PFSubclassing>

@property (strong, nonatomic) BSUser *firstUser;
@property (strong, nonatomic) NSString *firstUserUsername;
@property (strong, nonatomic) BSUser *secondUser;
@property (strong, nonatomic) NSString *secondUserUsername;
@property (strong, nonatomic) NSString *lastMessage;
@property (strong, nonatomic) NSDate *lastMessageDate;

+ (NSString *)parseClassName;

@end
