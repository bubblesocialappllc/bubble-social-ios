//
//  BSReport.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/22/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSReport.h"

@implementation BSReport

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic reporter, reason, post, photo;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"Reporting";
}

+ (void)reportPost:(BSPost *)post withReason:(NSString *)reason {
    
    
    
}

+ (void)reportPhoto:(BSPhoto *)photo withReason:(NSString *)reason {
    
    BSReport *report = [BSReport object];
    report.photo = photo;
    report.reason = reason;
    report.reporter = [BSUser currentUser];
    
    if (photo.post != nil) {
        report.post = [BSPost objectWithoutDataWithObjectId:photo.post];
    }
    
    [report saveEventually];
    
}

#pragma mark - Instance Methods

- (id)reported {
    
    if (self.post != nil) {
        return self.post;
    } else {
        return self.photo;
    }

}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

@end
