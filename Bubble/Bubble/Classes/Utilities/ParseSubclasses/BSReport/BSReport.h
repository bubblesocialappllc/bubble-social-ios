//
//  BSReport.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/22/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

@import Parse;

@interface BSReport : PFObject <PFSubclassing>

/**
 *  The user that reported.
 */
@property (strong, nonatomic) BSUser *reporter;

/**
 *  The reason for the report.
 */
@property (strong, nonatomic) NSString *reason;

/**
 *  The post that was reported.
 */
@property (strong, nonatomic) BSPost *post;

/**
 *  The photo that was reported.
 */
@property (strong, nonatomic) BSPhoto *photo;

+ (NSString *)parseClassName;

/**
 *  Reports a post. If the post contains a photo, please use reportPhoto:withReason: as it will report the post if one is contained.
 *
 *  @param post   The post object to report.
 *  @param reason The reason for the report.
 */
+ (void)reportPost:(BSPost *)post withReason:(NSString *)reason;

/**
 *  Reports a photo. This will also report a post if the photo contains a post.
 *
 *  @param photo  The photo object to report.
 *  @param reason The reason for the report.
 */
+ (void)reportPhoto:(BSPhoto *)photo withReason:(NSString *)reason;

/**
 *  Use this to get the reported object on a BSReport.
 *
 *  @return The reported object. Valid types are: BSPost, BSPhoto
 */
- (id)reported;

@end
