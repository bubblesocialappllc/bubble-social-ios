//
//  BSLocation.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>

/**
 *  The block that will return the city name as Orlando, Fl or an error if one occurs.
 *
 *  @param cityName The city name. Ex: Orlando, FL
 *  @param error    An error if one occurs.
 */
typedef void(^CityNameFromLocationBlock)(NSString *cityName, NSError *error);

/**
 *  The block hat will return the address or an error if one occurs.
 *
 *  @param address The address for the location.
 *  @param error   An error if one occurs.
 */
typedef void(^AddressFromLocationBlock)(NSString *address, NSError *error);

@interface BSLocation : PFObject <PFSubclassing>

@property (strong, nonatomic) PFGeoPoint *geoPoint;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *count;
@property (strong, nonatomic) NSArray *posts;
@property (strong, nonatomic) NSString *placeId;

+ (NSString *)parseClassName;

/**
 *  Gets the city name for the location.
 *
 *  @param point The point to get the city name from.
 *  @param block The block that will return the city name as Orlando, Fl or an error if one occurs.
 */
+ (void)cityNameWithLocation:(PFGeoPoint *)point completion:(CityNameFromLocationBlock)block;

/**
 *  Gets the city name for the current location.
 *
 *  @param block The block that will return the city name as Orlando, Fl or an error if one occurs.
 */
+ (void)cityNameWithCurrentLocationWithBlock:(CityNameFromLocationBlock)block;

/**
 *  Gets the address for the location.
 *
 *  @param point The point to get the address from.
 *  @param completion The block that will return the address or an error if one occurs.
 */
+ (void)addressFromLocation:(PFGeoPoint *)point completion:(AddressFromLocationBlock)block;

/**
 *  Gets the address for the current location.
 *
 *  @param block The block that will return the address or an error if one occurs.
 */
+ (void)addressFromCurrentLocationWithBlock:(AddressFromLocationBlock)block;

@end
