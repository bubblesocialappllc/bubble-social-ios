//
//  BSLocation.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSLocation.h"

@implementation BSLocation

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic geoPoint, name, count, posts, placeId;

#pragma mark - Public
#pragma mark - Class Methods

+ (NSString *)parseClassName {
    return @"Location";
}

+ (void)cityNameWithLocation:(PFGeoPoint *)point completion:(CityNameFromLocationBlock)block {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude];
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        NSString *locationName = [NSString string];
        
        for (CLPlacemark *placemark in placemarks) {
            
            locationName = [NSString stringWithFormat:@"%@", [placemark locality]];
            
            locationName = [locationName stringByAppendingString:[NSString stringWithFormat:@", %@", [placemark administrativeArea]]];
            
        }
        
        block(locationName, error);
        
    }];
    
}

+ (void)cityNameWithCurrentLocationWithBlock:(CityNameFromLocationBlock)block {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [BSLocation cityNameWithLocation:appDelegate.currentLocation completion:^(NSString *cityName, NSError *error) {
        block(cityName, error);
    }];
    
}

+ (void)addressFromLocation:(PFGeoPoint *)point completion:(AddressFromLocationBlock)block {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude];
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        CLPlacemark *placemark = [placemarks firstObject];
        
        NSString *address = ABCreateStringWithAddressDictionary([placemark addressDictionary], NO);
        
        block(address, error);
        
    }];
    
}

+ (void)addressFromCurrentLocationWithBlock:(AddressFromLocationBlock)block {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [BSLocation addressFromLocation:appDelegate.currentLocation completion:^(NSString *address, NSError *error) {
        block(address, error);
    }];
}

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

@end
