//
//  BSUser.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/1/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSUser.h"

@implementation BSUser

// We set all of our properties as dynamic to tell PFObject that these exist and are valid.
@dynamic staff, beta, realName, pendingBuddies, buddies, color, photos, profilePicture, gender, location, locationHidden, points, popPoints, phone, birthday, blocked, tags, aboutMe, notifications, posts;

#pragma mark - Private
#pragma mark - Class Methods

+ (void)load {
    [self registerSubclass];
}

@end