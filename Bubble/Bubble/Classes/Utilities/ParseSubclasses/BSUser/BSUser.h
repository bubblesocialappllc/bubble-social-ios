//
//  BSUser.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/1/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
#import "BSPhone.h"

@interface BSUser : PFUser <PFSubclassing>

@property (nonatomic) BOOL staff;
@property (nonatomic) BOOL beta;
@property (strong, nonatomic) NSString *realName;
@property (strong, nonatomic) NSString *aboutMe;
@property (strong, nonatomic) NSMutableArray *pendingBuddies;
@property (strong, nonatomic) NSMutableArray *buddies;
@property (strong, nonatomic) NSString *color;
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) PFFile *profilePicture;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) PFGeoPoint *location;
@property (nonatomic) BOOL locationHidden;
@property (strong, nonatomic) NSNumber *points;
@property (strong, nonatomic) NSNumber *popPoints;
@property (strong, nonatomic) BSPhone *phone;
@property (strong, nonatomic) NSDate *birthday;
@property (strong, nonatomic) NSMutableArray *blocked;
@property (strong, nonatomic) NSMutableArray *tags;
@property (strong, nonatomic) NSDictionary *notifications;
@property (strong, nonatomic) NSNumber *posts;

@end
