//
//  NSMutableArray+Additions.h
//  Pods
//
//  Created by Chayel Heinsen on 1/12/15.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (NSMutableArrayAdditions)

- (void)shuffle;

@end
