//
//  NSMutableArray+Additions.m
//  Pods
//
//  Created by Chayel Heinsen on 1/12/15.
//
//

#import "NSMutableArray+Additions.h"

@implementation NSMutableArray (NSMutableArrayAdditions)

- (void)shuffle {
    
    NSUInteger count = [self count];
    
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((unsigned int)remainingCount);
        [self exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
    
}

@end
