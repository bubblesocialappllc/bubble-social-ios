//
//  UIColor+Bubble.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHHelpers.h"

@interface UIColor(Bubble)

#pragma mark - Helpers

/**
 *  A UIColor from HEX.
 *
 *  @param hexString The HEX string. Can contain # if you want.
 *
 *  @return A new UIColor from the HEX.
 */
+ (UIColor *)colorWithHex:(NSString *)hexString;

#pragma mark - Colors

+ (UIColor *)bubblePink;


@end
