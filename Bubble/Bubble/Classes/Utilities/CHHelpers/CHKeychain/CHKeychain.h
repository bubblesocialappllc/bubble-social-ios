//
//  CHKeychain.h
//  Chayel Heinsen API
//
//  Created by Chayel Heinsen on 7/25/14.
//  Copyright © 2014 Chayel Heinsen. All rights reserved.
//

#import "CHHelpers.h"

@import Foundation;
@import Security;

@interface CHKeychain : NSObject

/**
 *  The service to init the Keychain with.
 */
@property (strong, nonatomic) NSString *service CHHELPERS_DEPRECATED("This isn't used anymore, please use the class methods. +setObject:value ForKey:key");

/**
 *  The group to init the Keychain with.
 */
@property (strong, nonatomic) NSString *group CHHELPERS_DEPRECATED("This isn't used anymore, please use the class methods. +setObject:value ForKey:key");

/**
 *  Saves an object into the Keychain.
 *
 *  @param value The object to save.
 *  @param key   The key indentifing the value.
 *
 *  @return YES if saved successfully, otherwise NO.
 */
+ (BOOL)setObject:(id)value forKey:(NSString *)key;

/**
 *  Removes an object from the Keychain.
 *
 *  @param key The key indentifing the value.
 *
 *  @return YES if deleted successfully, otherwise NO if object was not found or an error occured.
 */
+ (BOOL)removeObjectForKey:(NSString *)key;

/**
 @abstract Loads a given value from the Keychain
 @param key The key identifying the value you want to load.
 @return The value identified by key or nil if it doesn't exist.
 */
/**
 *  Gets an object from the Keychain.
 *
 *  @param key The key identifying the object you want.
 *
 *  @return The object indentified by the key or nil if the object could't be found.
 */
+ (id)objectForKey:(NSString *)key;

+ (BOOL)setDouble:(double)value forKey:(NSString *)key;
+ (double)doubleForKey:(NSString *)key;

/**
 *  Initializes a keychain object with the service and group.
 *
 *  @param service The name of the service to use. This should be a defined in your Prefix-Header.
 *  @param group   The group to use if you wish to use keychain information in different apps. (OPTIONAL)
 *
 *  @return A newly initialized keychain object
 */
- (id)initWithService:(NSString *)service withGroup:(NSString *)group CHHELPERS_DEPRECATED("This isn't used anymore, please use the class methods. +setObject:value ForKey:key");

/**
 *  Inserts a new object into the keychain.
 *
 *  @param key  The key to use for the object.
 *  @param data The data to add to the keychain.
 *
 *  @return YES if succesfully added, otherwise NO.
 */
- (BOOL)insertObjectForKey:(NSString *)key withData:(NSData *)data CHHELPERS_DEPRECATED("Use +setObject:value ForKey:key");

/**
 *  Updates an existing object in the keychain.
 *
 *  @param key  The key for the object.
 *  @param data The new data to replace the old data.
 *
 *  @return YES if succesfully updated, otherwise NO if unsuccesful or not found.
 */
- (BOOL)updateObjectForKey:(NSString *)key withData:(NSData *)data CHHELPERS_DEPRECATED("Use +setObject:value ForKey:key");

/**
 *  Removes an existing object from the keychain.
 *
 *  @param key The key for the object.
 *
 *  @return YES if succesfully added, otherwise NO if unsuccesful or not found.
 */
- (BOOL)removeObjectForKey:(NSString *)key CHHELPERS_DEPRECATED("Use +removeObjectForKey:key");

/**
 *  Returns an object in the keychain.
 *
 *  @param key The key for the object.
 *
 *  @return The object in the keychain.
 */
- (NSData *)objectForKey:(NSString *)key CHHELPERS_DEPRECATED("Use +objectForKey:key");

@end