//
//  Keychain.m
//  Chayel Heinsen API
//
//  Created by Chayel Heinsen on 7/25/14.
//  Copyright © 2014 Chayel Heinsen. All rights reserved.
//

#import "CHKeychain.h"

#define CHECK_OSSTATUS_ERROR(x) (x == noErr) ? YES : NO

@interface CHKeychain ()

/**
 *  Queries the Keychain.
 *
 *  @param key The key to query for.
 *
 *  @return The Keychain.
 */
+ (NSMutableDictionary *)getKeychainQuery:(NSString *)key;

/**
 *  Prepares the Keychain.
 *
 *  @param key The key to post to the Keychain.
 *
 *  @return The Keychain.
 */
- (NSMutableDictionary*)prepareDict:(NSString *)key CHHELPERS_DEPRECATED("This isn't used anymore, please use the class methods. +setObject:value ForKey:key");

@end

@implementation CHKeychain

#pragma mark - Public Methods

+ (BOOL)setObject:(id)value forKey:(NSString *)key {

    NSMutableDictionary *keychainQuery = [self getKeychainQuery:key];
    
        // Delete any previous value with this key
        // We could use SecItemUpdate but its unnecesarily more complicated
    [self removeObjectForKey:key];
    
    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:value] forKey:(__bridge id)kSecValueData];
    
    OSStatus result = SecItemAdd((__bridge CFDictionaryRef)keychainQuery, NULL);
    
    return CHECK_OSSTATUS_ERROR(result);
}

+ (BOOL)removeObjectForKey:(NSString *)key {
    
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:key];
    
    OSStatus result = SecItemDelete((__bridge CFDictionaryRef)keychainQuery);
    
    return CHECK_OSSTATUS_ERROR(result);
}

+ (id)objectForKey:(NSString *)key {
    
    id value = nil;
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:key];
    CFDataRef keyData = NULL;
    
    [keychainQuery setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    [keychainQuery setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    
    if (SecItemCopyMatching((__bridge CFDictionaryRef)keychainQuery, (CFTypeRef *)&keyData) == noErr) {
        
        @try {
            value = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge NSData *)keyData];
        }
        @catch (NSException *e) {
            NSLog(@"Unarchive of %@ failed: %@", key, e);
            value = nil;
        }
        @finally {}
    }
    
    if (keyData) {
        CFRelease(keyData);
    }
    
    return value;
}

+ (BOOL)setDouble:(double)value forKey:(NSString *)key; {
    return [CHKeychain setObject:[NSNumber numberWithDouble:value] forKey:key];
}

+ (double)doubleForKey:(NSString *)key {
    return [[CHKeychain objectForKey:key] doubleValue];
}

- (id)initWithService:(NSString *)service withGroup:(NSString *)group {
    
    self = [super init];
    
    if (self) {
        
        self.service = [NSString stringWithString:service];
        
        if (group)
            self.group = [NSString stringWithString:group];
    }
    
    return  self;
}

- (BOOL)insertObjectForKey:(NSString *)key withData:(NSData *)data {
    
    NSMutableDictionary *dict = [self prepareDict:key];
    [dict setObject:data forKey:(__bridge id)kSecValueData];
    
    OSStatus status = SecItemAdd((__bridge CFDictionaryRef)dict, NULL);
    
    if (errSecSuccess != status) {
        NSLog(@"Unable add item with key %@ error:%d",key, (int)status);
    }
    
    return (errSecSuccess == status);
}

- (BOOL)updateObjectForKey:(NSString *)key withData:(NSData *)data {
    
    NSMutableDictionary *dictKey = [self prepareDict:key];
    
    NSMutableDictionary *dictUpdate = [[NSMutableDictionary alloc] init];
    [dictUpdate setObject:data forKey:(__bridge id)kSecValueData];
    
    OSStatus status = SecItemUpdate((__bridge CFDictionaryRef)dictKey, (__bridge CFDictionaryRef)dictUpdate);
    
    if (errSecSuccess != status) {
        NSLog(@"Unable add update with key %@ error:%d",key, (int)status);
    }
    
    return (errSecSuccess == status);
    
}

- (BOOL)removeObjectForKey:(NSString *)key {
    
    NSMutableDictionary *dict = [self prepareDict:key];
    OSStatus status = SecItemDelete((__bridge CFDictionaryRef)dict);
    
    if (status != errSecSuccess) {
        NSLog(@"Unable to remove item for key %@ with error:%d",key, (int)status);
        return NO;
    }
    
    return  YES;
}

- (NSData *)objectForKey:(NSString *)key {
    
    NSMutableDictionary *dict = [self prepareDict:key];
    [dict setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    [dict setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    
    CFTypeRef result = nil;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)dict,&result);
    
    if (status != errSecSuccess) {
        NSLog(@"Unable to fetch item for key %@ with error:%d",key, (int)status);
        return nil;
    }
    
    return (__bridge NSData *)result;
}

#pragma mark - Private Methods

+ (NSMutableDictionary *)getKeychainQuery:(NSString *)key {
    
    return [@{(__bridge id)kSecClass            : (__bridge id)kSecClassGenericPassword,
              (__bridge id)kSecAttrService      : key,
              (__bridge id)kSecAttrAccount      : key,
              (__bridge id)kSecAttrAccessible   : (__bridge id)kSecAttrAccessibleAfterFirstUnlock
              } mutableCopy];
}

- (NSMutableDictionary*)prepareDict:(NSString *)key {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    
    NSData *encodedKey = [key dataUsingEncoding:NSUTF8StringEncoding];
    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrGeneric];
    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrAccount];
    [dict setObject:self.service forKey:(__bridge id)kSecAttrService];
    [dict setObject:(__bridge id)kSecAttrAccessibleAlwaysThisDeviceOnly forKey:(__bridge id)kSecAttrAccessible];
    
        // This is for sharing data across apps
    if (self.group != nil)
        [dict setObject:self.group forKey:(__bridge id)kSecAttrAccessGroup];
    
    return  dict;
    
}

@end
