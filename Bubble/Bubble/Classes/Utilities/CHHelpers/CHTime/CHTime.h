//
//  CHTime.h
//  Bubble
//
//  Created by Chayel Heinsen on 11/9/14.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CHHelpers.h"

@interface CHTime : NSObject

/**
*  Creates a string that show how much time has passed since the inputed time
*
*  @param date The date to get passed time from
*
*  @return Passed time
*/
+ (NSString *)makeTimePassedFromDate:(NSDate *)date;

@end
