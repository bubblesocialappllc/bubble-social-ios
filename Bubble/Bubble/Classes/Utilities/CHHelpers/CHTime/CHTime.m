//
//  CHTime.m
//  Bubble
//
//  Created by Chayel Heinsen on 11/9/14.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

#import "ChTime.h"

@implementation CHTime

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

+ (NSString *)makeTimePassedFromDate:(NSDate *)date {
    
    NSCalendar *c = [NSCalendar currentCalendar];
    
    NSDate *created = date;
    NSDate *now = [NSDate date];
    
    NSDateComponents *components;
    
    if ([CHDevice deviceVersion] >= 8.0) {
        components = [c components:(NSCalendarUnitYear|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond) fromDate:created toDate:now options:0];
    } else {
        components = [c components:(NSYearCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:created toDate:now options:0];
    }
    
    if (components.year > 0) {
        
        return [NSString stringWithFormat:@"%zdy", components.year];
        
    } else if (components.day > 0) {
            
        return [NSString stringWithFormat:@"%zdd", components.day];
    
    } else if (components.hour > 0) {
        
        return [NSString stringWithFormat:@"%zdh", components.hour];
        
    } else if (components.minute > 0) {
        
        return [NSString stringWithFormat:@"%zdm", components.minute];
        
    } else if (components.second > 30) {
        
       return [NSString stringWithFormat:@"%zds", components.second];
        
    } else {
        return @"Just Now";
    }
    
}

#pragma GCC diagnostic pop

@end
