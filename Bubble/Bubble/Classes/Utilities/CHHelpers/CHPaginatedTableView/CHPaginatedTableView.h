//
//  CHPaginatedTableView.h
//  Pods
//
//  Created by Chayel Heinsen on 12/19/14.
//
//

#import <UIKit/UIKit.h>
#import "CHHelpers.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
@import MONActivityIndicatorView;

@class CHPaginatedTableView;

@protocol CHPaginatedTableViewDelegate <NSObject>

/**
 *  This is called when CHPaginatedTableView has fetched the objects. Use this method to add the items to your tableview items array and reload the table.
 *
 *  @param tableview The tableview
 *  @param objects   The objects received from the query.
 */
- (void)tableView:(CHPaginatedTableView *)tableview fetchedItems:(NSArray *)objects;

/**
 *  Return the query you want to run.
 *
 *  @return THe query to run.
 */
- (PFQuery *)queryForFetch;

/**
 *  Notifies the delegate that the user pulled to refresh. You should use this to clear out your items array so you dont see duplicates.
 */
- (void)pulledToRefresh;

@optional

/**
 *  Notifies the delegate that the tableview has finished initializing.
 */
- (void)tableViewInitialized;

@end

@interface CHPaginatedTableView : UITableView <MONActivityIndicatorViewDelegate>

/**
 *  The refresh control.
 */
@property (strong, nonatomic) UIRefreshControl *refreshControl;

/**
 *  The balls view.
 */
@property (strong, nonatomic) MONActivityIndicatorView *indicatorView;

/**
 *  Sets the number of items per page. Defaults to 20.
 */
@property (nonatomic) NSInteger limit;

/**
 *  The delegate object.
 */
@property (weak, nonatomic) id<CHPaginatedTableViewDelegate> fetchDelegate;

/**
 *  Refreshes the feed.
 */
- (void)refresh;

/**
 *  This must be called on scrollViewDidEndDragging:willDecelerate
 *
 *  @param scrollView The scrollview.
 *  @param decelerate YES if will decelerate, otherwise NO.
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

/**
 *  Call this to load the next amount of cells.
 */
- (void)paginate;

@end

@interface UIScrollView (CHHelpersScrollView)

- (CGFloat)distanceToBottom;

@end
