//
//  CHPaginatedTableView.m
//  Pods
//
//  Created by Chayel Heinsen on 12/19/14.
//
//

#import "CHPaginatedTableView.h"

static NSInteger DefaultLimit = 20;

@interface CHPaginatedTableView()

/**
 *  The amount of items to skip. This is for pagination. Defaults to 0.
 */
@property (nonatomic) NSInteger skip;

@property (strong, nonatomic) NSMutableArray *fetchedObjects;

@end

@implementation CHPaginatedTableView

#pragma mark - Init

- (id)init {
    
    self = [super init];
   
    if (self) {
        
        self.limit = DefaultLimit;
        self.skip = 0;
        self.fetchedObjects = [NSMutableArray new];
        [self setupRefreshControl];
        
        if ([self.fetchDelegate respondsToSelector:@selector(tableViewInitialized)]) {
            [self.fetchDelegate tableViewInitialized];
        }
        
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.limit = DefaultLimit;
        self.skip = 0;
        self.fetchedObjects = [NSMutableArray new];
        [self setupRefreshControl];
        
        if ([self.fetchDelegate respondsToSelector:@selector(tableViewInitialized)]) {
            [self.fetchDelegate tableViewInitialized];
        }
    
    }
    
    return self;
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    /*
    CGFloat yVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView].y;
   
    if (yVelocity < 0) {
        // When we scroll far enough, we should automatically start to fetch for new objects.
        if ([scrollView distanceToBottom] < 250) {
            [self fetch];
        }
    } else {
        
    }
    */
    
    /*CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    CGFloat y = offset.y + bounds.size.height - inset.bottom;
    CGFloat h = size.height;
    
    CGFloat reloadDistance = 50;
   
    if (y > h + reloadDistance) {
        
        [self fetch];
        
    }*/
}

#pragma mark - MONActivityIndicatorViewDelegate

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
      circleBackgroundColorAtIndex:(NSUInteger)index {
    
    return [UIColor whiteColor];
}

#pragma mark - Helpers

#pragma mark -- Private

- (void)fetch {
    
    if (self.refreshControl == nil) {
        [self setupRefreshControl];
    }
    
    if (self.fetchedObjects == nil) {
        self.fetchedObjects = [NSMutableArray new];
    }
    
    if (self.limit <= 0) {
        self.limit = DefaultLimit;
    }
    
    [self.fetchedObjects removeAllObjects];
    
    if ([self.fetchDelegate respondsToSelector:@selector(queryForFetch)]) {
        
        PFQuery *query = [self.fetchDelegate queryForFetch];
        [query setLimit:self.limit];
        [query setSkip:self.skip];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if (!error) {
                
                [self.fetchedObjects setArray:objects];
                
                if (objects.count < self.limit) {
                    self.skip += objects.count; // We got back less than our limit. Update skip so we don't query the same object
                } else if (objects.count == self.limit) {
                    self.skip += self.limit; // There might be more objects in the table. Update the skip value.
                }
                
                if ([self.fetchDelegate respondsToSelector:@selector(tableView:fetchedItems:)]) {
                    
                    [self.fetchDelegate tableView:self fetchedItems:self.fetchedObjects];
                    
                } else {
                    
                    NSLog(@"fetchDelegate couldn't respond to tableview:fetchedItems: That means you haven't implemented it!");
                    
                }
                
            } else {
                
                [BSQueryErrorHandler handleError:error];
                
            }
            
        }];
        
    } else {
        
        NSLog(@"fetchDelegate couldn't respond to queryForFetch. That means you haven't implemented it!");
        
    }
    
    [self.indicatorView stopAnimating];
    [self.indicatorView removeFromSuperview];
    
    [self.refreshControl endRefreshing];
    
}

- (void)setupRefreshControl {
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@""]];
    [self.refreshControl setBackgroundColor:[UIColor bubblePink]];
    [self.refreshControl setTintColor:[UIColor clearColor]];
    
    self.indicatorView = [[MONActivityIndicatorView alloc] init];
    self.indicatorView.delegate = self;
    self.indicatorView.numberOfCircles = 5;
    self.indicatorView.radius = 10;
    self.indicatorView.internalSpacing = 5;
    self.indicatorView.duration = 0.8;
    self.indicatorView.delay = 0.2;
    self.indicatorView.center = self.refreshControl.center;
    self.indicatorView.clipsToBounds = YES;
    
    [self addSubview:self.refreshControl];
    
}

#pragma mark -- Public

- (void)refresh {
    
    self.skip = 0;
    
    if ([self.fetchDelegate respondsToSelector:@selector(pulledToRefresh)]) {
        [self.fetchDelegate pulledToRefresh];
    }
    
    [self fetch];
    
}

- (void)paginate {
    [self fetch];
}

@end

@implementation UIScrollView (CHHelpersScrollView)

- (CGFloat)distanceToBottom {
    
    CGPoint contentOffset = self.contentOffset;
    CGSize contentSize = self.contentSize;
    CGRect bounds = self.bounds;
    return fabs(contentOffset.y - (contentSize.height - bounds.size.height));
    
}

@end
