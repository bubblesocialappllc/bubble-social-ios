//
//  CHHelpers.h
//  Pods
//
//  Created by Chayel Heinsen on 12/12/14.
//
//

#ifndef Pods_CHHelpers_h
#define Pods_CHHelpers_h

    // This file will import all of the files included in CHHelpers

#define CHHELPERS_DEPRECATED(x) __attribute((deprecated(x)))

#import "CHDevice.h"
#import "CHKeychain.h"
#import "NSString+Additions.h"
#import "NSMutableArray+Additions.h"
#import "CHTime.h"
#import "UIColor+Bubble.h"
#import "CHPaginatedTableView.h"

#endif
