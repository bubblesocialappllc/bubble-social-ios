//
//  CHDevice.h
//  Bubble
//
//  Created by Chayel Heinsen on 10/6/14.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>
#import <sys/utsname.h>
#import "CHHelpers.h"

@interface CHDevice : NSObject

/**
 *  The type of device.
 */
typedef NS_ENUM(NSInteger, DeviceType){
    /**
     *  iPhone 4
     */
    iPhone4 = 3,
    /**
     *  iPhone 4S
     */
    iPhone4S = 4,
    /**
     *  iPhone 5
     */
    iPhone5 = 5,
    /**
     *  iPhone 5C
     */
    iPhone5C = 5,
    /**
     *  iPhone 5S
     */
    iPhone5S = 6,
    /**
     *  iPhone 6
     */
    iPhone6 = 7,
    /**
     *  iPhone 6+
     */
    iPhone6Plus = 8,
    /**
     *  Simulator
     */
    Simulator = 0
};

/**
 *  The height of the device.
 */
typedef NS_ENUM(NSInteger, DeviceSize){
    /**
     *  3.5 inches
     */
    iPhone35inch = 1,
    /**
     *  4 inches
     */
    iPhone4inch = 2,
    /**
     *  4.7 inches
     */
    iPhone47inch = 3,
    /**
     *  5.5 inches
     */
    iPhone55inch = 4
};

/**
 *  Returns the Device Type.
 *
 *  @return The type of device in use.
 */
+ (DeviceType)deviceType;

/**
 *  Returns the Device Size.
 *
 *  @return The size of the device in use.
 */
+ (DeviceSize)deviceSize;

/**
 *  Returns the Device Name.
 *
 *  @return The name of the device in use.
 */
+ (NSString *)deviceName;

/**
 *  Returns the Device iOS Version Number.
 *
 *  @return The iOS Version Number.
 */
+ (CGFloat)deviceVersion;

/*
 
 //Check for device model
 if ([Device deviceType] == iPhone6)
 
    NSLog(@"You got the new iPhone 6. Nice!");
 
 else if ([Device deviceType] == iPhone6Plus)
 
    NSLog(@"You got the iPhone 6 Plus. Bigger is better!");
 
 
 // Check for device screen size
 if ([Device deviceSize] == iPhone4inch)
 
    NSLog(@"Your screen is 4 inches");
 
    //Get device machine name.
    NSLog(@"%@",[Device deviceName]);
    
    //e.g: Outputs 'iPhone6,2'

 */

@end
