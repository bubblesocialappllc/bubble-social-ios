//
//  BSClusterMapView.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/18/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

@import TSClusterMapView;
#import "BSClusterAnnotation.h"
#import "BSClusteredAnnotation.h"

@interface BSClusterMapView : TSClusterMapView <MKMapViewDelegate, TSClusterMapViewDelegate>



@end
