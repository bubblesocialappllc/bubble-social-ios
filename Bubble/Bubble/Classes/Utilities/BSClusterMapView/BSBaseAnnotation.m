//
//  BSBaseAnnotation.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/18/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSBaseAnnotation.h"

@implementation BSBaseAnnotation

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    NSDictionary * coordinateDictionary = [dictionary objectForKey:@"coordinates"];
    
    return [self initWithCoordinates:CLLocationCoordinate2DMake([[coordinateDictionary objectForKey:@"latitude"] doubleValue], [[coordinateDictionary objectForKey:@"longitude"] doubleValue]) title:[dictionary objectForKey:@"name"] subtitle:nil];
}

- (id)initWithCoordinates:(CLLocationCoordinate2D)location title:(NSString *)title subtitle:(NSString *)subtitle {
    
    self = [super init];
    
    if (self != nil) {
        _coordinate = location;
        _title = title;
        _subtitle = subtitle;
    }
    
    return self;
}

@end
