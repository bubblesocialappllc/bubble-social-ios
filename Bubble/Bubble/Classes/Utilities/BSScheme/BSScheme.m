//
//  BSScheme.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/5/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSScheme.h"

@implementation BSScheme

+ (NSDictionary *)convertRESTToURL:(NSString *)rest {
    
    NSString *urlStr = @"bubblesocial://", *path = @"", *pathception = @"", *param = @"", *query;
    
    if ([rest hasSuffix:@"/"]) {
        rest = [rest substringToIndex:rest.length - 1];
    }
    
    NSScanner *scanner = [NSScanner scannerWithString:rest];
    [scanner setScanLocation:15];
    [scanner scanUpToString:@"/" intoString:&path];
    
    if (scanner.scanLocation != rest.length) {
        [scanner setScanLocation:scanner.scanLocation + 1];
    } else {
        NSLog(@"The request was invalid. Make sure you made the right request.");
        return nil;
    }
    
    path = [path lowercaseString];
    
    if ([path isEqualToString:@"post"]) {
        [scanner scanUpToString:@"" intoString:&param];
    } else if ([path isEqualToString:@"view"]) {
        [scanner scanUpToString:@"/" intoString:&pathception];
        
        if (scanner.scanLocation != rest.length) {
            [scanner setScanLocation:scanner.scanLocation + 1];
        }
        
    }
    
    if ([pathception isEqualToString:@"user"]) {
        [scanner scanUpToString:@"" intoString:&param];
    }
    
    if ([path isEqualToString:@"post"]) {
        urlStr = [urlStr stringByAppendingString:[NSString stringWithFormat:@"%@/?objectId=%@", path, param]];
    } else if ([path isEqualToString:@"view"]) {
        
        if ([pathception isEqualToString:@"user"]) {
            urlStr = [urlStr stringByAppendingString:[NSString stringWithFormat:@"%@/?view=%@&objectId=%@", path, pathception, param]];
        } else {
            urlStr = [urlStr stringByAppendingString:[NSString stringWithFormat:@"%@/?view=%@", path, pathception]];
        }
        
    }
    
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSScanner *scan = [NSScanner scannerWithString:urlStr];
    [scan scanUpToString:@"?" intoString:nil];
    [scan setScanLocation:scan.scanLocation + 1];
    [scan scanUpToString:@"" intoString:&query];
    
    return @{
             @"url" : url,
             @"path" : path,
             @"query" : query,
             };
}

@end