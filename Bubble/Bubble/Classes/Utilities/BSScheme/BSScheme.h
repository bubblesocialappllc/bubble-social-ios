//
//  BSScheme.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/5/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSScheme : NSObject

/**
 *  Converts the REST API to a query that we can parse easily.
 *
 *  @param rest The rest string.
 *
 *  @return A dictionary containing the new full url, path and the query.
 */
+ (NSDictionary *)convertRESTToURL:(NSString *)rest;

@end
