//
//  MSAlertController+Additions.h
//  Bubble
//
//  Created by Chayel Heinsen on 1/12/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "MSAlertController.h"

@interface MSAlertController (BSUtilities)

- (void)addActions:(NSArray *)actions;

@end

@interface UIAlertController (BSUtilities)

- (void)addActions:(NSArray *)actions;

@end