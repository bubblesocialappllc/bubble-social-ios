//
//  NSMutableArray+BSUtilities.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (BSUtilities)

- (void)shuffle;

@end
