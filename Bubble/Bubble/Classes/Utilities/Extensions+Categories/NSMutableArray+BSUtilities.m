//
//  NSMutableArray+BSUtilities.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "NSMutableArray+BSUtilities.h"

@implementation NSMutableArray (BSUtilities)

- (void)shuffle {
    
    NSUInteger count = [self count];
    
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t)remainingCount);
        [self exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
}

@end
