//
//  PFImageView+BSUtilities.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/24/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <ParseUI/ParseUI.h>

@interface PFImageView (BSUtilities)

- (void)loadInBackgroundWithProgress:(void (^)(int progress))progress;
- (void)loadInBackgroundAndCropToSize:(CGSize)size completion:(void (^)())completion;

@end
