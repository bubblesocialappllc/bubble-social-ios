//
//  PFImageView+BSUtilities.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/24/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "PFImageView+BSUtilities.h"

@implementation PFImageView (BSUtilities)

- (void)loadInBackgroundWithProgress:(void (^)(int progress))progress {
    
    [self.file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        self.image = [UIImage imageWithData:data];
    } progressBlock:^(int percentDone) {
        progress(percentDone);
    }];
    
}

- (void)loadInBackgroundAndCropToSize:(CGSize)size completion:(void (^)())completion {
    
    [self.file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        self.image = [self resizeImage:[UIImage imageWithData:data] width:size.width height:size.height];
        //self.image = [self cropImage:[UIImage imageWithData:data] toRect:CGRectMake(0, 0, size.width, size.height)];
        completion();
    }];
}

- (UIImage *)cropImage:(UIImage *)imageToCrop toRect:(CGRect)rect {
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (UIImage *)resizeImage:(UIImage *)image width:(CGFloat)width height:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    CGFloat widthRatio = newSize.width / image.size.width;
    CGFloat heightRatio = newSize.height / image.size.height;
    
    if (widthRatio > heightRatio) {
        newSize = CGSizeMake(image.size.width * heightRatio, image.size.height * heightRatio);
    } else {
        newSize = CGSizeMake(image.size.width * widthRatio, image.size.height * widthRatio);
    }
    
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
