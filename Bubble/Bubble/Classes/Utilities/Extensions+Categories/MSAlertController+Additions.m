//
//  MSAlertController+Additions.m
//  Bubble
//
//  Created by Chayel Heinsen on 1/12/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "MSAlertController+Additions.h"

@implementation MSAlertController (BSUtilities)

- (void)addActions:(NSArray *)actions {
    
    for (MSAlertAction *action in actions) {
        
        [self addAction:action];
        
    }
    
}

@end

@implementation UIAlertController (BSUtilities)

- (void)addActions:(NSArray *)actions {
    
    for (UIAlertAction *action in actions) {
        
        [self addAction:action];
        
    }
    
}

@end

