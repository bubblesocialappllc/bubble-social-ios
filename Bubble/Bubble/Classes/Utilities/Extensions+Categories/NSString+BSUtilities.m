//
//  NSString+BSUtilities.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/25/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "NSString+BSUtilities.h"

@implementation NSString (BSUtilities)

- (NSString *)prependStringWithString:(NSString *)string {
    
    return [NSString stringWithFormat:@"%@%@", string, self];
    
}

@end
