//
//  NSString+BSUtilities.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/25/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (BSUtilities)

- (NSString *)prependStringWithString:(NSString *)string;

@end
