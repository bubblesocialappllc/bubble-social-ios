//
//  BSLocationManager.m
//  Bubble
//
//  Created by Chayel Heinsen  on 5/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSLocationManager.h"
@import AFNetworking;

#define BSLOCATIONMANAGER_METERS_TO_FEET(meters) ((meters) * (1 / 0.3048))
#define BSLOCATIONMANAGER_METERS_TO_MILES(meters) (meters * 0.00062137)

@interface BSLocationManager ()

/**
 *  The CLLocationManager.
 */
@property (strong, nonatomic) CLLocationManager *manager;
@property (copy, nonatomic) void(^determineStateBlock)(CLRegionState state, CLRegion *region);
@property (copy, nonatomic) CLRegion *determiningStateRegion;

@end

@implementation BSLocationManager

#pragma mark - Overides

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        self.manager = [CLLocationManager new];
        self.manager.delegate = self;
        // The minimum distance (measured in meters) a device must move horizontally before an update event is generated.
        self.manager.distanceFilter = 5;
        self.authorizationStatus = (BSLocationAuthorizationStatus)[CLLocationManager authorizationStatus];
    }
    
    return self;
}

- (void)setDistanceFilter:(CGFloat)distanceFilter {
    _distanceFilter = distanceFilter;
    self.manager.distanceFilter = distanceFilter;
}

- (NSInteger)floor {
    return self.currentLocation.floor.level;
}

- (CLLocationDistance)altitude {
    return self.currentLocation.altitude;
}

- (NSDate *)timestamp {
    return self.currentLocation.timestamp;
}

- (CLLocationDirection)course {
    return self.currentLocation.course;
}

- (CLLocationSpeed)speed {
    return self.currentLocation.speed;
}

- (BOOL)isRegionMonitoringAvailable {
    return [CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]];
}

- (NSSet *)monitoredRegions {
    return self.manager.monitoredRegions;
}

- (CLLocationDistance)maximumRegionMonitoringDistance {
    return self.manager.maximumRegionMonitoringDistance;
}

- (NSInteger)countMonitoredRegions {
    return self.manager.monitoredRegions.count;
}

#pragma mark - Instance Methods

- (void)updateLocation {
    
    // We first check to see if we have permission to use location.
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        
        // If we don't have permission, we check the Info.plist to check which usage desciption key is available.
        BOOL alwaysUsageKeyExists = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"] != nil;
        BOOL whenInUseUsageKeyExists = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"] != nil;
        
        // If neither keys are availble, we throw an error.
        if (alwaysUsageKeyExists == NO && whenInUseUsageKeyExists == NO) {
            
            if (self.errorBlock) {
                NSError *error = [NSError errorWithDomain:@"BSLocationManagerDomain" code:9000
                                                 userInfo:@{NSLocalizedFailureReasonErrorKey : NSLocalizedString(@"A description key is missing. Please enter either NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription into your Info.plist", nil)}];
                self.errorBlock(error);
            }
        } else if (alwaysUsageKeyExists) {
            [self.manager requestAlwaysAuthorization]; // If the Info.plist contains both keys, we will prioritize Always.
        } else {
            [self.manager requestWhenInUseAuthorization]; // Request for when in use.
        }
        
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways ||
               [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        // We have permission to use location!
        [self.manager startUpdatingLocation];
        //[self.manager startMonitoringVisits];
    }
    
}

- (void)stopUpdatingLocation {
    [self.manager stopUpdatingLocation];
}

- (CLLocationDistance)distanceFromLocation:(CLLocation *)location {
    return [self.currentLocation distanceFromLocation:location];
}

- (CGFloat)distanceFromLocationInMiles:(CLLocation *)location {
    CLLocationDistance distance = [self.currentLocation distanceFromLocation:location];
    return BSLOCATIONMANAGER_METERS_TO_MILES(distance);
}

- (CGFloat)distanceFromLocationInFeet:(CLLocation *)location {
    CLLocationDistance distance = [self.currentLocation distanceFromLocation:location];
    return BSLOCATIONMANAGER_METERS_TO_FEET(distance);
}

- (void)registerRegionForMonitoring:(CLCircularRegion *)region {
    [self.manager startMonitoringForRegion:region];
}

- (void)registerLocationForMonitoring:(CLLocation *)location
                               radius:(CLLocationDistance)radius
                           identifier:(NSString *)identifier {
    
    /*
     *  If the radius is too large, registration fails automatically,
     *  so clamp the radius to the max value.
     */
    if (radius > self.manager.maximumRegionMonitoringDistance) {
        radius = self.manager.maximumRegionMonitoringDistance;
    }
    
    // Create the region
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:location.coordinate radius:radius identifier:identifier];
    [self registerRegionForMonitoring:region];
}

- (void)stopMonitoringRegion:(CLRegion *)region {
    [self.manager stopMonitoringForRegion:region];
}

- (void)stopMonitoringAllRegions {
    NSSet *regions = self.manager.monitoredRegions;
    
    for (CLRegion *region in regions) {
        [self.manager stopMonitoringForRegion:region];
    }
}

- (void)registerBeaconRegionWithUUID:(NSUUID *)proximityUUID identifier:(NSString *)identifier {
    // Create the beacon region to be monitored.
    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID identifier:identifier];
    
    // Register the beacon region with the location manager.
    [self.manager startMonitoringForRegion:beaconRegion];
}

- (void)startRangingBeaconsInRegion:(CLBeaconRegion *)region {
    
    if ([CLLocationManager isRangingAvailable]) {
        [self.manager startRangingBeaconsInRegion:region];
    }
}

- (void)stopRangingBeaconsInRegion:(CLBeaconRegion * __nonnull)region {
    [self.manager stopRangingBeaconsInRegion:region];
}

- (void)requestStateForRegion:(CLRegion *)region completion:(void(^)(CLRegionState state, CLRegion *region))block {
    self.determiningStateRegion = region;
    self.determineStateBlock = block;
    [self.manager requestStateForRegion:region];
}

#pragma mark - Class Methods

+ (instancetype)sharedManager {
    
    static BSLocationManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [self new];
    });
    
    return sharedManager;
}

+ (BOOL)locationServicesEnabled {
    return [CLLocationManager locationServicesEnabled];
}

+ (void)reverseGeocodeFromZipCode:(NSString *)zipCode completion:(void(^)(id response, NSError *error))block {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *dict = @{
                           @"address" : @"",
                           @"components" : [NSString stringWithFormat:@"postal_code:%@", zipCode],
                           };
    
    [manager GET:@"https://maps.googleapis.com/maps/api/geocode/json" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        
        if (block) {
            block(responseObject, nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        if (block) {
            block(nil, error);
        }
    }];
}

#pragma mark - CLLocationManager

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    if (locations.count > 0) {
        self.currentLocation = [locations lastObject];
        
        if (self.didUpdateLocationBlock) {
            CLLocation *location = [locations lastObject];
            self.didUpdateLocationBlock(location);
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if (self.errorBlock) {
        self.errorBlock(error);
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    self.authorizationStatus = (BSLocationAuthorizationStatus)status;
    
    // This will be called after the permission request alert shows. If we are granted permission, we start updating the location.
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            break;
        }
        case kCLAuthorizationStatusRestricted: {
            break;
        }
        case kCLAuthorizationStatusDenied: {
            break;
        }
        case kCLAuthorizationStatusAuthorizedAlways: {
            [self.manager startUpdatingLocation];
            break;
        }
        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            [self.manager startUpdatingLocation];
            break;
        }
        default: {
            break;
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BSLocationManagerDidChangeLocationAuthStatus" object:nil userInfo:@{@"status" : [NSNumber numberWithInt:status]}];
}

#pragma mark - CLVisits

- (void)locationManager:(CLLocationManager *)manager didVisit:(CLVisit *)visit {
    
    if ([visit.departureDate isEqualToDate:[NSDate distantFuture]]) {
        // User has arrive but not left the location
    } else {
        // The visit is complete
    }
}

#pragma mark - Region Monitoring

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    
    if (self.regionUpdateBlock) {
        self.regionUpdateBlock(region, YES);
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    
    if (self.regionUpdateBlock) {
        self.regionUpdateBlock(region, NO);
    }
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    
    if (self.determineStateBlock && [self.determiningStateRegion.identifier isEqualToString:region.identifier]) {
        self.determineStateBlock(state, region);
        
        switch (state) {
            case CLRegionStateUnknown:
                break;
            case CLRegionStateInside:
                
                if (self.regionUpdateBlock) {
                    self.regionUpdateBlock(region, YES);
                }
                break;
            case CLRegionStateOutside:
                
                if (self.regionUpdateBlock) {
                    self.regionUpdateBlock(region, NO);
                }
                break;
            default:
                break;
        }
        
        self.determineStateBlock = nil;
        self.determiningStateRegion = nil;
    }
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    NSLog(@"Location Monitoring Error: %@", error);
    
    if (self.errorBlock) {
        self.errorBlock(error);
    }
}

#pragma mark - Beacon Monitoring

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    
    if (beacons.count > 0 && self.didRangeBeaconsBlock) {
        
        NSMutableArray *beaconsToReturn = [NSMutableArray new];
        NSMutableArray *unknowns = [NSMutableArray new];
        
        for (CLBeacon *beacon in beacons) {
            
            if (beacon.proximity == CLProximityUnknown) {
                [unknowns addObject:beacon];
            } else {
                [beaconsToReturn addObject:beacon];
            }
        }
        
        [beaconsToReturn addObjectsFromArray:unknowns];
        
        self.didRangeBeaconsBlock(beaconsToReturn, region);
    }
}

@end
