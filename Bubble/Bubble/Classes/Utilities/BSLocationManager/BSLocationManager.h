//
//  BSLocationManager.h
//  Bubble
//
//  Created by Chayel Heinsen  on 5/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

@import Foundation;
@import UIKit;
@import CoreLocation;

#pragma mark - TypeDefs

typedef void(^BSLocationManagerDidUpdateLocationBlock)(CLLocation  * __nonnull location);
typedef void(^BSLocationManagerErrorBlock)(NSError * __nonnull error);
typedef void(^BSLocationManagerRegionUpdateBlock)(CLRegion * __nonnull region, BOOL didEnter);
typedef void(^BSLocationManagerDidRangeBeaconsBlock)(NSArray * __nonnull beacons, CLBeaconRegion * __nonnull region);

/**
 *  Represents the current authorization state of the application.
 */
typedef NS_ENUM(NSInteger, BSLocationAuthorizationStatus) {
    // User has not yet made a choice with regards to this application
    BSLocationAuthorizationStatusNotDetermined = 0,
    
    // This application is not authorized to use location services.  Due
    // to active restrictions on location services, the user cannot change
    // this status, and may not have personally denied authorization.
    BSLocationAuthorizationStatusRestricted,
    
    // User has explicitly denied authorization for this application, or
    // location services are disabled in Settings.
    BSLocationAuthorizationStatusDenied,
    
    // User has granted authorization to use their location at any time,
    // including monitoring for regions, visits, or significant location changes.
    BSLocationAuthorizationStatusAuthorizedAlways NS_ENUM_AVAILABLE_IOS(8_0),
    
    // User has granted authorization to use their location only when your app
    // is visible to them (it will be made visible to them if you continue to
    // receive location updates while in the background).  Authorization to use
    // launch APIs has not been granted.
    BSLocationAuthorizationStatusAuthorizedWhenInUse NS_ENUM_AVAILABLE_IOS(8_0),
    
    // This value is deprecated, but was equivalent to the new -Always value.
    BSLocationAuthorizationStatusAuthorized NS_ENUM_DEPRECATED_IOS(2_0, 8_0, "Use BSLocationAuthorizationStatusAuthorizedAlways") = BSLocationAuthorizationStatusAuthorizedAlways
};

#pragma mark - Constants


#pragma mark - BSLocationManager

@interface BSLocationManager : NSObject <CLLocationManagerDelegate>

#pragma mark - Properties

/**
 *  This block will be called when the current location updates.
 */
@property (strong, nonatomic, nullable) BSLocationManagerDidUpdateLocationBlock didUpdateLocationBlock;

/**
 *  This block will be called when an error is thrown.
 */
@property (strong, nonatomic, nullable) BSLocationManagerErrorBlock errorBlock;

/**
 *  This block will be called when a user enters/exits a region that is being monitored. This could be a CLCircularRegion or a CLBeaconRegion.
 *
 *  @warning The system doesn’t report boundary crossings until the boundary plus a system-defined cushion distance is exceeded.
 *  This cushion value prevents the system from generating numerous entered and exited events in quick succession
 *  while the user is traveling close the edge of the boundary.
 */
@property (strong, nonatomic, nullable) BSLocationManagerRegionUpdateBlock regionUpdateBlock;

/**
 *  This block will be called when beacons were ranged.
 */
@property (strong, nonatomic, nullable) BSLocationManagerDidRangeBeaconsBlock didRangeBeaconsBlock;

/**
 *  The current location of the device.
 */
@property (strong, nonatomic, nullable) CLLocation *currentLocation;

/**
 *  The time at which the currentLocation was determined.
 */
@property(copy, nonatomic, readonly, nullable) NSDate *timestamp;

/**
 *  The logical floor of the building in which the user is located. The ground floor of a building is represented by 0, NOT 1.
 *
 *  @warning If floor information is not available, this will be nil.
 */
@property (assign, nonatomic, readonly) NSInteger floor;

/**
 *  The altitude measured in meters. Positive values indicate altitudes above sea level. Negative values indicate altitudes below sea level.
 */
@property (assign, nonatomic, readonly) CLLocationDistance altitude;

/**
 *  The direction the device is traveling. Course values are measured in degrees starting at due north and
 *  continuing clockwise around the compass. Thus, north is 0 degrees, east is 90 degrees, south is 180 degrees, and so on.
 *  Course values may not be available on all devices. A negative value indicates that the direction is invalid.
 */
@property (assign, nonatomic, readonly) CLLocationDirection course;

/**
 *  The instantaneous speed of the device in meters per second in the direction of its current heading.
 *  A negative value indicates an invalid speed. Because the actual speed can change many times between the
 *  delivery of subsequent location events, you should use this property for informational purposes only.
 */
@property (assign, nonatomic, readonly) CLLocationSpeed speed;

/**
 *  The minimum distance (measured in meters) a device must move horizontally before an update event is generated.
 *  Pass in kCLDistanceFilterNone to be notified of all movements.
 */
@property (assign, nonatomic) CGFloat distanceFilter;

/**
 *  Represents the current authorization state of the application.
 */
@property (assign, nonatomic) BSLocationAuthorizationStatus authorizationStatus;

/**
 *  YES if region monitoring is available, otherwise NO.
 */
@property (assign, nonatomic, readonly, getter=isRegionMonitoringAvailable) BOOL regionMonitoringAvailable;

/**
 *  A set of CLCirularRegion's that are currently being monitored.
 */
@property (strong, nonatomic, readonly, nullable) NSSet *monitoredRegions;

/**
 *  The maximun region monitoring distance.
 */
@property (assign, nonatomic, readonly) CLLocationDistance maximumRegionMonitoringDistance;

/**
 *  The current number of regions being moitored.
 */
@property (assign, nonatomic, readonly) NSInteger countMonitoredRegions;

#pragma mark - Instance Methods

/**
 *  Calling this will ask the GPS to update. If we don't have location permission yet, this will ask for it.
 */
- (void)updateLocation;

/**
 *   This will stop location updates. If you want to start location updates again, just call updateLocation.
 */
- (void)stopUpdatingLocation;

/**
 *  Returns the distance (in meters) from the receiver’s location to the specified location.
 *
 *  This method measures the distance between the two locations by tracing a line between them that follows the
 *  curvature of the Earth. The resulting arc is a smooth curve and does not take into account specific altitude
 *  changes between the two locations.
 *
 *  @param location The other location.
 *
 *  @return The distance (in meters) between the two locations.
 */
- (CLLocationDistance)distanceFromLocation:(CLLocation * __nonnull)location;

/**
 *  Returns the distance (in miles) from the receiver’s location to the specified location.
 *
 *  This method measures the distance between the two locations by tracing a line between them that follows the
 *  curvature of the Earth. The resulting arc is a smooth curve and does not take into account specific altitude
 *  changes between the two locations.
 *
 *  @param location The other location.
 *
 *  @return The distance (in miles) between the two locations.
 */
- (CGFloat)distanceFromLocationInMiles:(CLLocation * __nonnull)location;

/**
 *  Returns the distance (in feet) from the receiver’s location to the specified location.
 *
 *  This method measures the distance between the two locations by tracing a line between them that follows the
 *  curvature of the Earth. The resulting arc is a smooth curve and does not take into account specific altitude
 *  changes between the two locations.
 *
 *  @param location The other location.
 *
 *  @return The distance (in feet) between the two locations.
 */
- (CGFloat)distanceFromLocationInFeet:(CLLocation * __nonnull)location;

/**
 *  Registers a region for monitoring. If the region is too large (you can use [SPLocationManager sharedManager].maximumRegionMonitoringDistance to check)
 *  this will automatically fail registration. If you want to make sure the region will always register,
 *  use registerLocationForMonitoring:radius:identier which will automatically check the radius and set it too the maximum
 *  distance if it's already too large.
 *
 *  Monitoring will start immediately!
 *  Notifications will only be given when the user crosses the region boundary. If you need to check if the user
 *  is inside a region already, you can call requestStateForRegion:
 *
 *  @warning  Core Location limits the number of regions that may be simultaneously monitored by a single app to 20.
 *  To work around this limit, consider registering only those regions in the user’s immediate vicinity.
 *  As the user’s location changes, you can remove regions that are now farther way and add regions coming up on the
 *  user’s path. If you attempt to register a region and space is unavailable, the request is ignored.
 *
 *  @param region The region to monitor.
 */
- (void)registerRegionForMonitoring:(CLCircularRegion * __nonnull)region;

/**
 *  Creates a region and registers it for monitoring.
 *
 *  Monitoring will start immediately!
 *  Notifications will only be given when the user crosses the region boundary. If you need to check if the user
 *  is inside a region already, you can call requestStateForRegion:completion:
 *
 *  @warning  Core Location limits the number of regions that may be simultaneously monitored by a single app to 20.
 *  To work around this limit, consider registering only those regions in the user’s immediate vicinity.
 *  As the user’s location changes, you can remove regions that are now farther way and add regions coming up on the
 *  user’s path. If you attempt to register a region and space is unavailable, the request is ignored.
 *
 *  @param location   The center of the region.
 *  @param radius     The radius of the region in meters.
 *  @param identifier The identifier of the region.
 */
- (void)registerLocationForMonitoring:(CLLocation * __nonnull)location radius:(CLLocationDistance)radius identifier:(NSString * __nonnull)identifier;

/**
 *  Stops monitoring a region if we are currently monitoring it.
 *
 *  @param region The region to stop monitoring.
 */
- (void)stopMonitoringRegion:(CLRegion * __nonnull)region;

/**
 *  Stops monitoring all regions.
 */
- (void)stopMonitoringAllRegions;

/**
 *  Registers an iBeacon to monitor.
 *
 *  Monitoring will start immediately!
 *  Notifications will only be given when the user crosses the region boundary. If you need to check if the user
 *  is inside a region already, you can call requestStateForRegion:completion:
 *
 *  @warning  Core Location limits the number of regions that may be simultaneously monitored by a single app to 20.
 *  To work around this limit, consider registering only those regions in the user’s immediate vicinity.
 *  As the user’s location changes, you can remove regions that are now farther way and add regions coming up on the
 *  user’s path. If you attempt to register a region and space is unavailable, the request is ignored.
 *
 *  @param proximityUUID The unique UUID of the iBeacon.
 *  @param identifier    The identifier for this region.
 */
- (void)registerBeaconRegionWithUUID:(NSUUID * __nonnull)proximityUUID identifier:(NSString * __nonnull)identifier;

/**
 *  Starts searching for beacons in a region. You should only call this when a user has entered a region.
 *  The best place to call this is in the regionUpdateBlock when the region is a CLBeaconRegion and didEnter is YES.
 *
 *  @param region The region to start ranging.
 */
- (void)startRangingBeaconsInRegion:(CLBeaconRegion * __nonnull)region;

/**
 *  Stops the delivery of notifications for the specified beacon region.
 *  The best place to call this is in the regionUpdateBlock when the region is a CLBeaconRegion and didEnter is NO.
 *
 *  @param region The region that identifies the beacons. The object you specify need not be the exact same
 *  object that you registered but the beacon attributes should be the same.
 */
- (void)stopRangingBeaconsInRegion:(CLBeaconRegion * __nonnull)region;

/**
 *  Gets the current state of a region.
 *
 *  @warning Because this is being run asynchronously, you should wait for the block to be called before requesting for
 *  another state. If you call this mutiple times before the block is called, you will only receive a callback for the last
 *  request!
 *
 *  @param region The region.
 *  @param block  The block that will be called when the state is determined. This block will only be called ONCE!
 *  If you need to determine the state for another region, you must call requestStateForRegion:completion: again.
 */
- (void)requestStateForRegion:(CLRegion * __nonnull)region completion:(void(^ __nonnull)(CLRegionState state, CLRegion * __nonnull region))block;

#pragma mark - Class Methods

/**
 *  The shared manager
 */
+ (nonnull instancetype)sharedManager;

/**
 *  Checks to see if the user has location services enabled.
 *
 *  @return YES if location services are enabled, otherwise NO.
 */
+ (BOOL)locationServicesEnabled;

/**
 *  Gets location information from the zip code.
 *
 *  @param zipCode The zip code to reverse geocode.
 *  @param block   The block that will be called with the response or an error if one occurs.
 */
+ (void)reverseGeocodeFromZipCode:(NSString * __nonnull)zipCode completion:(void(^ __nullable)(id __nullable response, NSError * __nullable error))block;

@end
