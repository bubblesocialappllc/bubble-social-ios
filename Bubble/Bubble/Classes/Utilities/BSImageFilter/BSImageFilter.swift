//
//  BSImageFilter.swift
//  Bubble
//
//  Created by Chayel Heinsen on 3/30/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit

@objc class BSImageFilter: NSObject {
    
    /**
    The type of filter to use for the image.
    
    - BSSepiaFilter:    A CISepiaTone Filter.
    - BSVignetteFilter: A CIVignette Filter
    */
    enum BSFilter {
        case BSSepiaFilter
        case BSVignetteFilter
    }
    
    //MARK: - Private Properties
    private var context: CIContext!
    private var image: CIImage!
    private var currentFilter: CIFilter!
    
    //MARK: - Public Properties
    private(set) var filter: BSFilter!
    /// The output image.
    var processedImage: UIImage!
    var intensity: Float = 0.0 {
        didSet {
            applyIntensity(intensity)
        }
    }
    
    var radius: Float = 1.0 {
        didSet {
            applyRadius(radius)
        }
    }
    
    init(filter: BSFilter, image: UIImage) {
        self.context = CIContext(options: nil)
        self.image = CIImage(image: image)
        self.filter = filter
        
        switch (filter) {
        case (BSFilter.BSSepiaFilter):
            self.currentFilter = CIFilter(name: "CISepiaTone")
            self.currentFilter.setValue(self.image, forKey: kCIInputImageKey)
        case (BSFilter.BSVignetteFilter):
            self.currentFilter = CIFilter(name: "CIVignette")
            self.currentFilter.setValue(self.image, forKey: kCIInputImageKey)
        }
        
       self.intensity = 0.5
    }
    
    private func applyIntensity(intensity: Float) {
        let inputKeys = currentFilter.inputKeys
        
        if inputKeys.contains(kCIInputIntensityKey) { currentFilter.setValue(intensity, forKey: kCIInputIntensityKey) }
        //if contains(inputKeys, kCIInputRadiusKey) { currentFilter.setValue(intensity * 200, forKey: kCIInputRadiusKey) }
        //if contains(inputKeys, kCIInputScaleKey) { currentFilter.setValue(intensity * 10, forKey: kCIInputScaleKey) }
        //if contains(inputKeys, kCIInputCenterKey) { currentFilter.setValue(CIVector(x: currentImage.size.width / 2, y: currentImage.size height / 2), forKey: kCIInputCenterKey) }
        
        let cgImage = context.createCGImage(currentFilter.outputImage!, fromRect: currentFilter.outputImage!.extent)
        let processedImage = UIImage(CGImage: cgImage)
        self.processedImage = processedImage
    }
    
    private func applyRadius(radius: Float) {
        let inputKeys = currentFilter.inputKeys
    
        if inputKeys.contains(kCIInputRadiusKey) { currentFilter.setValue(intensity * 200, forKey: kCIInputRadiusKey) }
        
        let cgImage = context.createCGImage(currentFilter.outputImage!, fromRect: currentFilter.outputImage!.extent)
        let processedImage = UIImage(CGImage: cgImage)
        self.processedImage = processedImage
    }
    
}
