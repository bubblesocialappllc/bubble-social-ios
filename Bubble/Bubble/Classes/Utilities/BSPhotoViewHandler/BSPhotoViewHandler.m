//
//  BSPhotoViewHandler.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/21/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSPhotoViewHandler.h"

@interface BSPhotoViewHandler ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) UIViewController *viewController;

@end

@implementation BSPhotoViewHandler

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.photos = [NSMutableArray new];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didOpenViewController:) name:@"DidOpenPhotoViewer" object:nil];
        
    }
    
    return self;
}

- (instancetype)initWithPhotos:(NSArray *)photos {
    
    self = [super init];
    
    if (self) {
        
        self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.photos = [NSMutableArray arrayWithArray:photos];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didOpenViewController:) name:@"DidOpenPhotoViewer" object:nil];
        
    }
    
    return self;
    
}

- (instancetype)initWithPhoto:(BSPhoto *)photo {
    
    self = [super init];
    
    if (self) {
        
        self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.photos = [NSMutableArray arrayWithArray:@[photo]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didOpenViewController:) name:@"DidOpenPhotoViewer" object:nil];
        
    }
    
    return self;
    
}

#pragma mark - EBPhotoPages Datasource

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController
    shouldExpectPhotoAtIndex:(NSInteger)index {
    
    if (index < self.photos.count){
        return YES;
    }
    
    return NO;
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
                imageAtIndex:(NSInteger)index
           completionHandler:(void (^)(UIImage *))handler {
    
    BSPhoto *photo = [self.photos objectAtIndex:index];
    
    [photo.photo getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        handler([UIImage imageWithData:data]);
    }];
    
}

- (void)photoPagesController:(EBPhotoPagesController *)controller attributedCaptionForPhotoAtIndex:(NSInteger)index completionHandler:(void (^)(NSAttributedString *))handler {
    
    handler(nil);
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
      captionForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSString *))handler {
    
    BSPhoto *photo = [self.photos objectAtIndex:index];
    
    if (photo.post != nil) {
        
        PFQuery *query = [BSPost query];
        [query getObjectInBackgroundWithId:photo.post block:^(PFObject *object, NSError *error) {
            
            BSPost *post = (BSPost *)object;
            
            handler(post.text);
            
        }];
        
    } else {
        handler(nil);
    }
    
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
     metaDataForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSDictionary *))handler {
    
    handler(nil);
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
         tagsForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSArray *))handler {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        BSPhoto *photo = [self.photos objectAtIndex:index];
        
        PFQuery *query = [BSTag query];
        [query whereKey:@"objectId" containedIn:photo.tags];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            handler(objects);
        }];
        
    });
    
}


- (void)photoPagesController:(EBPhotoPagesController *)controller
     commentsForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSArray *))handler {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        BSPhoto *photo = [self.photos objectAtIndex:index];
        
        PFQuery *query = [BSComment query];
        [query whereKey:@"objectId" containedIn:photo.comments];
        [query orderByAscending:@"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            handler(objects);
        }];
    });
    
}

- (void)photoPagesController:(EBPhotoPagesController *)controller numberOfCommentsForPhotoAtIndex:(NSInteger)index completionHandler:(void (^)(NSInteger))handler {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        BSPhoto *photo = [self.photos objectAtIndex:index];
        
        PFQuery *query = [BSComment query];
        [query whereKey:@"objectId" containedIn:photo.comments];
        [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
            handler(number);
        }];
        
    });
    
}

#pragma mark - User Permissions

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowTaggingForPhotoAtIndex:(NSInteger)index {
    
    return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)controller
 shouldAllowDeleteForComment:(id<EBPhotoCommentProtocol>)comment
             forPhotoAtIndex:(NSInteger)index {
    
    return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowCommentingForPhotoAtIndex:(NSInteger)index {
    
    return YES;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowActivitiesForPhotoAtIndex:(NSInteger)index {
    
    return YES;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowMiscActionsForPhotoAtIndex:(NSInteger)index {
    
    return YES;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowDeleteForPhotoAtIndex:(NSInteger)index {
    
    BSPhoto *photo = [self.photos objectAtIndex:index];
    
    if ([photo.user.objectId isEqualToString:self.appDelegate.currentUser.objectId]) {
        return YES;
    }
    
    return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController
     shouldAllowDeleteForTag:(EBTagPopover *)tagPopover
              inPhotoAtIndex:(NSInteger)index {
    
    return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController
    shouldAllowEditingForTag:(EBTagPopover *)tagPopover
              inPhotoAtIndex:(NSInteger)index {
    
    return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowReportForPhotoAtIndex:(NSInteger)index {
    
    return YES;
}

#pragma mark - EBPPhotoPagesDelegate

- (void)photoPagesControllerDidDismiss:(EBPhotoPagesController *)photoPagesController {
    
}

- (void)photoPagesController:(EBPhotoPagesController *)photoPagesController
       didReportPhotoAtIndex:(NSInteger)index{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"What's the problem?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *inappropriate = [UIAlertAction actionWithTitle:@"Inappropriate" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPhoto:[self.photos objectAtIndex:index] withReason:action.title];
    }];
    
    UIAlertAction *spam = [UIAlertAction actionWithTitle:@"Spamming" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPhoto:[self.photos objectAtIndex:index] withReason:action.title];
    }];
    
    UIAlertAction *other = [UIAlertAction actionWithTitle:@"Other" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPhoto:[self.photos objectAtIndex:index] withReason:action.title];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[inappropriate, spam, other, cancel]];
    
    [self.viewController presentViewController:alert animated:YES completion:nil];
    
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
            didDeleteComment:(id<EBPhotoCommentProtocol>)deletedComment
             forPhotoAtIndex:(NSInteger)index {
    
    
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
         didDeleteTagPopover:(EBTagPopover *)tagPopover
              inPhotoAtIndex:(NSInteger)index {
    
    
}

- (void)photoPagesController:(EBPhotoPagesController *)photoPagesController
       didDeletePhotoAtIndex:(NSInteger)index {
    
    BSPhoto *photo = [self.photos objectAtIndex:index];
    
    [self.appDelegate.currentUser.photos removeObject:photo.objectId];
    [self.appDelegate.currentUser saveInBackground];
    
    [self.photos removeObjectAtIndex:index];
    
    [photo deleteInBackground];

    [self setPhotos:self.photos];
    
}

- (void)photoPagesController:(EBPhotoPagesController *)photoPagesController
         didAddNewTagAtPoint:(CGPoint)tagLocation
                    withText:(NSString *)tagText
             forPhotoAtIndex:(NSInteger)index
                     tagInfo:(NSDictionary *)tagInfo {
    
    
    BSPhoto *photo = [self.photos objectAtIndex:index];
    
    BSTag *tag = [BSTag object];
    tag.text = tagText;
    tag.point = [BSTag tagPointWithCGPoint:tagLocation];
    
    [tag saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (photo.tags.count > 0) {
            [photo.tags addObject:tag.objectId];
        } else {
            photo.tags = [NSMutableArray arrayWithArray:@[tag.objectId]];
        }
        
        [photo saveInBackground];
        
    }];
    
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
              didPostComment:(NSString *)comment
             forPhotoAtIndex:(NSInteger)index {
    
    [BSLocation cityNameWithCurrentLocationWithBlock:^(NSString *cityName, NSError *error) {
        
        BSPhoto *photo = [self.photos objectAtIndex:index];
        BSComment *com = [BSComment object];
        com.comment = comment;
        com.commenter = self.appDelegate.currentUser.username;
        com.profilePic = self.appDelegate.currentUser.profilePicture;
        com.locationName = cityName;
        
        if (photo.post != nil) {
            
            PFQuery *query = [BSPost query];
            [query includeKey:@"author"];
            [query getObjectInBackgroundWithId:photo.post block:^(PFObject *object, NSError *error) {
                
                BSPost *post = (BSPost *)object;
                
                NSNumber *numOfComments = post.comments;
                
                if (numOfComments == nil) {
                    numOfComments = [NSNumber numberWithInt:0];
                }
                
                NSNumber *upOne = [NSNumber numberWithInt:[numOfComments intValue] + 1];
                post.comments = upOne;
                
                NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
                
                if (commenters == nil) {
                    
                    commenters = [NSMutableArray new];
                    [commenters addObject:[BSUser currentUser].username];
                    
                } else {
                    
                    if (![commenters containsObject:[BSUser currentUser].username]) {
                        [commenters addObject:[BSUser currentUser].username];
                    }
                    
                }
                
                post.commenters = commenters;
                com.post = post;
                
                [com saveInBackground];
                
                [CloudCode pushToChannel:post.author.username withAlert:[NSString stringWithFormat:@"%@ has commented on your post", [BSUser currentUser].username] andCategory:@"comment" andViewToPush:[NSString stringWithFormat:@"bubblesocial://post/%@", post.objectId]];
                
                [CloudCode setNotification:post.objectId andType:[BSNotificationType BSCommentNotification]];
                
            }];
            
        }
        
        [com saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if (photo.comments.count > 0) {
                [photo.comments addObject:com.objectId];
            } else {
                photo.comments = [NSMutableArray arrayWithArray:@[com.objectId]];
            }
            
            [photo saveInBackground];
            
        }];
        
        EBPhotoViewController *photoController = [controller currentPhotoViewController];
        NSMutableArray *comments = [NSMutableArray arrayWithArray:photoController.comments];
        [comments addObject:com];
        
        [controller setComments:comments forPhotoAtIndex:index];
        [photoController.commentsView.tableView reloadData];
        
    }];
    
}

#pragma mark - Helpers

- (void)didOpenViewController:(NSNotification *)notification {
    self.viewController = [notification.userInfo objectForKey:@"viewController"];
}


@end
