//
//  BSPhotoViewHandler.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/21/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
@import EBPhotoPages_BubbleSocial;

@interface BSPhotoViewHandler : NSObject <EBPhotoPagesDataSource, EBPhotoPagesDelegate>

/**
 *  Inits a handler with photos.
 *
 *  @param photos An array of BSPhoto objects.
 *
 *  @return A newly initialized handler.
 */
- (instancetype)initWithPhotos:(NSArray *)photos;

/**
 *  Inits a hander with a photo.
 *
 *  @param photo A BSPhoto objects.
 *
 *  @return A newly initialized handler.
 */
- (instancetype)initWithPhoto:(BSPhoto *)photo;

/**
 *  An array of BSPhoto objects.
 */
@property (strong, nonatomic) NSMutableArray *photos;

@end