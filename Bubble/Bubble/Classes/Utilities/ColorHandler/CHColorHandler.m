//
//  CHColorHandler.m
//  Bubble
//
//  Created by Chayel Heinsen on 4/28/14.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

#import "CHColorHandler.h"
#define NumberOfColors 21

@implementation CHColorHandler

+ (UIColor *)getUIColorFromNSString:(NSString *)string {
    
    UIColor *color = nil;
    
    if ([string isEqualToString:@"flatPomegranateColor"]) {
        color = [UIColor flatPomegranateColor];
        
    } else if ([string isEqualToString:@"flatEmeraldColor"]) {
        color = [UIColor flatEmeraldColor];
        
    } else if ([string isEqualToString:@"flatBelizeHoleColor"]) {
        color = [UIColor flatBelizeHoleColor];
        
    } else if ([string isEqualToString:@"flatAlizarinColor"]) {
        color = [UIColor flatAlizarinColor];
        
    } else if ([string isEqualToString:@"flatOrangeColor"]) {
        color = [UIColor flatOrangeColor];
        
    } else if ([string isEqualToString:@"flatWisteriaColor"]) {
        color = [UIColor flatWisteriaColor];
        
    } else if ([string isEqualToString:@"flatTurquoiseColor"]) {
        color = [UIColor flatTurquoiseColor];
        
    } else if ([string isEqualToString:@"flatPeterRiverColor"]) {
        color = [UIColor flatPeterRiverColor];
        
    } else if ([string isEqualToString:@"flatAmethystColor"]) {
        color = [UIColor flatAmethystColor];
        
    } else if ([string isEqualToString:@"flatWetAsphaltColor"]) {
        color = [UIColor flatWetAsphaltColor];
        
    } else if ([string isEqualToString:@"flatGreenSeaColor"]) {
        color = [UIColor flatGreenSeaColor];
        
    } else if ([string isEqualToString:@"flatNephritisColor"]) {
        color = [UIColor flatNephritisColor];
        
    } else if ([string isEqualToString:@"flatMidnightBlueColor"]) {
        color = [UIColor flatMidnightBlueColor];
        
    } else if ([string isEqualToString:@"flatSunFlowerColor"]) {
        color = [UIColor flatSunFlowerColor];
        
    } else if ([string isEqualToString:@"flatCarrotColor"]) {
        color = [UIColor flatCarrotColor];
        
    } else if ([string isEqualToString:@"flatCloudsColor"]) {
        color = [UIColor flatCloudsColor];
        
    } else if ([string isEqualToString:@"flatConcreteColor"]) {
        color = [UIColor flatConcreteColor];
        
    } else if ([string isEqualToString:@"flatPumpkinColor"]) {
        color = [UIColor flatPumpkinColor];
        
    } else if ([string isEqualToString:@"flatSilverColor"]) {
        color = [UIColor flatSilverColor];
        
    } else if ([string isEqualToString:@"flatAsbestosColor"]) {
        color = [UIColor flatAsbestosColor];
        
    } else if ([string isEqualToString:@"bubblePink"]) {
       color = [UIColor bubblePink];
    }
    
    return color;
    
}

+ (NSString *)getRandomColor {
    
    //UIColor *color = nil;
    NSString *string = nil;
    
    unsigned int num = (arc4random() % NumberOfColors) + 1;
    
    switch (num) {
        case 1:
            //color = [UIColor flatTurquoiseColor];
            string = @"flatTurquoiseColor";
            break;
        case 2:
            //color = [UIColor flatEmeraldColor];
            string = @"flatEmeraldColor";
            break;
        case 3:
            //color = [UIColor flatPeterRiverColor];
            string = @"latPeterRiverColor";
            break;
        case 4:
            //color = [UIColor flatAmethystColor];
            string = @"flatAmethystColor";
            break;
        case 5:
            //color = [UIColor flatWetAsphaltColor];
            string = @"flatWetAsphaltColor";
            break;
        case 6:
            //color = [UIColor flatGreenSeaColor];
            string = @"flatGreenSeaColor";
            break;
        case 7:
            //color = [UIColor flatNephritisColor];
            string = @"flatNephritisColor";
            break;
        case 8:
            //color = [UIColor flatBelizeHoleColor];
            string = @"flatBelizeHoleColor";
            break;
        case 9:
            //color = [UIColor flatWisteriaColor];
            string = @"flatWisteriaColor";
            break;
        case 10:
            //color = [UIColor flatMidnightBlueColor];
            string = @"flatMidnightBlueColor";
            break;
        case 11:
            //color = [UIColor flatSunFlowerColor];
            string = @"flatSunFlowerColor";
            break;
        case 12:
            //color = [UIColor flatCarrotColor];
            string = @"flatCarrotColor";
            break;
        case 13:
            //color = [UIColor flatAlizarinColor];
            string = @"flatAlizarinColor";
            break;
        case 14:
            //color = [UIColor flatCloudsColor];
            string = @"flatCloudsColor";
            break;
        case 15:
            //color = [UIColor flatConcreteColor];
            string = @"flatConcreteColor";
            break;
        case 16:
            //color = [UIColor flatOrangeColor];
            string = @"flatOrangeColor";
            break;
        case 17:
            //color = [UIColor flatPumpkinColor];
            string = @"flatPumpkinColor";
            break;
        case 18:
            //color = [UIColor flatPomegranateColor];
            string = @"flatPomegranateColor";
            break;
        case 19:
            //color = [UIColor flatSilverColor];
            string = @"flatSilverColor";
            break;
        case 20:
            //color = [UIColor flatAsbestosColor];
            string = @"flatAsbestosColor";
            break;
        case 21:
             //color = [UIColor bubblePink];
             string = @"bubblePink";
            break;
            
        default:
             //color = [UIColor bubblePink];
            break;
    }
    
    return string;
    
}

+ (NSArray *)getColors {
   
    NSArray *colors = @[[UIColor flatTurquoiseColor], [UIColor flatEmeraldColor], [UIColor flatPeterRiverColor], [UIColor flatAmethystColor], [UIColor flatWetAsphaltColor], [UIColor flatGreenSeaColor], [UIColor flatNephritisColor], [UIColor flatBelizeHoleColor], [UIColor flatWisteriaColor], [UIColor flatMidnightBlueColor], [UIColor flatSunFlowerColor], [UIColor flatCarrotColor], [UIColor flatAlizarinColor], [UIColor flatConcreteColor], [UIColor flatOrangeColor], [UIColor flatPumpkinColor], [UIColor flatPomegranateColor], [UIColor flatAsbestosColor], [UIColor bubblePink]];
    
    return colors;
    
}

+ (NSString *)getNSStringFromUIColor:(UIColor *)color {
    
    NSString *string = nil;
    
    if ([color isEqual: [UIColor flatTurquoiseColor]]) {
        string = @"flatTurquoiseColor";
        
    } else if ([color isEqual: [UIColor flatEmeraldColor]]) {
        string = @"flatEmeraldColor";
        
    } else if ([color isEqual: [UIColor flatPeterRiverColor]]) {
        string = @"flatPeterRiverColor";
        
    } else if ([color isEqual: [UIColor flatAmethystColor]]) {
        string = @"flatAmethystColor";
        
    } else if ([color isEqual: [UIColor flatWetAsphaltColor]]) {
        string = @"flatWetAsphaltColor";
        
    } else if ([color isEqual: [UIColor flatGreenSeaColor]]) {
        string = @"flatGreenSeaColor";
        
    } else if ([color isEqual: [UIColor flatNephritisColor]]) {
        string = @"flatNephritisColor";
        
    } else if ([color isEqual: [UIColor flatBelizeHoleColor]]) {
        string = @"flatBelizeHoleColor";
        
    } else if ([color isEqual: [UIColor flatWisteriaColor]]) {
        string = @"flatWisteriaColor";
        
    } else if ([color isEqual: [UIColor flatMidnightBlueColor]]) {
        string = @"flatMidnightBlueColor";
        
    } else if ([color isEqual: [UIColor flatSunFlowerColor]]) {
        string = @"flatSunFlowerColor";
        
    } else if ([color isEqual: [UIColor flatCarrotColor]]) {
        string = @"flatCarrotColor";
        
    } else if ([color isEqual: [UIColor flatAlizarinColor]]) {
        string = @"flatAlizarinColor";
        
    } else if ([color isEqual: [UIColor flatConcreteColor]]) {
        string = @"flatConcreteColor";
        
    } else if ([color isEqual: [UIColor flatOrangeColor]]) {
        string = @"flatOrangeColor";
        
    } else if ([color isEqual: [UIColor flatPumpkinColor]]) {
        string = @"flatPumpkinColor";
        
    } else if ([color isEqual: [UIColor flatPomegranateColor]]) {
        string = @"flatPomegranateColor";
        
    } else if ([color isEqual: [UIColor flatAsbestosColor]]) {
        string = @"flatAsbestosColor";
        
    } else if ([color isEqual: [UIColor bubblePink]]) {
        string = @"bubblePink";
    }
    
    return string;
}

- (NSString *)getRandomColor
{
    //UIColor *color = nil;
    NSString *string = nil;
    
    unsigned int num = (arc4random() % NumberOfColors) + 1;
    
    switch (num) {
        case 1:
            //color = [UIColor flatTurquoiseColor];
            string = @"flatTurquoiseColor";
            break;
        case 2:
            //color = [UIColor flatEmeraldColor];
            string = @"flatEmeraldColor";
            break;
        case 3:
            //color = [UIColor flatPeterRiverColor];
            string = @"latPeterRiverColor";
            break;
        case 4:
            //color = [UIColor flatAmethystColor];
            string = @"flatAmethystColor";
            break;
        case 5:
            //color = [UIColor flatWetAsphaltColor];
            string = @"flatWetAsphaltColor";
            break;
        case 6:
            //color = [UIColor flatGreenSeaColor];
            string = @"flatGreenSeaColor";
            break;
        case 7:
            //color = [UIColor flatNephritisColor];
            string = @"flatNephritisColor";
            break;
        case 8:
            //color = [UIColor flatBelizeHoleColor];
            string = @"flatBelizeHoleColor";
            break;
        case 9:
            //color = [UIColor flatWisteriaColor];
            string = @"flatWisteriaColor";
            break;
        case 10:
            //color = [UIColor flatMidnightBlueColor];
            string = @"flatMidnightBlueColor";
            break;
        case 11:
            //color = [UIColor flatSunFlowerColor];
            string = @"flatSunFlowerColor";
            break;
        case 12:
            //color = [UIColor flatCarrotColor];
            string = @"flatCarrotColor";
            break;
        case 13:
            //color = [UIColor flatAlizarinColor];
            string = @"flatAlizarinColor";
            break;
        case 14:
            //color = [UIColor flatCloudsColor];
            string = @"flatCloudsColor";
            break;
        case 15:
            //color = [UIColor flatConcreteColor];
            string = @"flatConcreteColor";
            break;
        case 16:
            //color = [UIColor flatOrangeColor];
            string = @"flatOrangeColor";
            break;
        case 17:
            //color = [UIColor flatPumpkinColor];
            string = @"flatPumpkinColor";
            break;
        case 18:
            //color = [UIColor flatPomegranateColor];
            string = @"flatPomegranateColor";
            break;
        case 19:
            //color = [UIColor flatSilverColor];
            string = @"flatSilverColor";
            break;
        case 20:
            //color = [UIColor flatAsbestosColor];
            string = @"flatAsbestosColor";
            break;
        case 21:
            //color = [UIColor bubblePink];
            string = @"bubblePink";
            break;
            
        default:
            //color = [UIColor bubblePink];
            break;
    }
    
    return string;

}

- (UIColor *)getUIColorFromNSString:(NSString *)string
{
    UIColor *color = nil;
    
    if ([string isEqualToString:@"flatPomegranateColor"]) {
        color = [UIColor flatPomegranateColor];
        
    } else if ([string isEqualToString:@"flatEmeraldColor"]) {
        color = [UIColor flatEmeraldColor];
        
    } else if ([string isEqualToString:@"flatBelizeHoleColor"]) {
        color = [UIColor flatBelizeHoleColor];
        
    } else if ([string isEqualToString:@"flatAlizarinColor"]) {
        color = [UIColor flatAlizarinColor];
        
    } else if ([string isEqualToString:@"flatOrangeColor"]) {
        color = [UIColor flatOrangeColor];
        
    } else if ([string isEqualToString:@"flatWisteriaColor"]) {
        color = [UIColor flatWisteriaColor];
        
    } else if ([string isEqualToString:@"flatTurquoiseColor"]) {
        color = [UIColor flatTurquoiseColor];
        
    } else if ([string isEqualToString:@"flatPeterRiverColor"]) {
        color = [UIColor flatPeterRiverColor];
        
    } else if ([string isEqualToString:@"flatAmethystColor"]) {
        color = [UIColor flatAmethystColor];
        
    } else if ([string isEqualToString:@"flatWetAsphaltColor"]) {
        color = [UIColor flatWetAsphaltColor];
        
    } else if ([string isEqualToString:@"flatGreenSeaColor"]) {
        color = [UIColor flatGreenSeaColor];
        
    } else if ([string isEqualToString:@"flatNephritisColor"]) {
        color = [UIColor flatNephritisColor];
        
    } else if ([string isEqualToString:@"flatMidnightBlueColor"]) {
        color = [UIColor flatMidnightBlueColor];
        
    } else if ([string isEqualToString:@"flatSunFlowerColor"]) {
        color = [UIColor flatSunFlowerColor];
        
    } else if ([string isEqualToString:@"flatCarrotColor"]) {
        color = [UIColor flatCarrotColor];
        
    } else if ([string isEqualToString:@"flatCloudsColor"]) {
        color = [UIColor flatCloudsColor];
        
    } else if ([string isEqualToString:@"flatConcreteColor"]) {
        color = [UIColor flatConcreteColor];
        
    } else if ([string isEqualToString:@"flatPumpkinColor"]) {
        color = [UIColor flatPumpkinColor];
        
    } else if ([string isEqualToString:@"flatSilverColor"]) {
        color = [UIColor flatSilverColor];
        
    } else if ([string isEqualToString:@"flatAsbestosColor"]) {
        color = [UIColor flatAsbestosColor];
        
    } else if ([string isEqualToString:@"bubblePink"]) {
        color = [UIColor bubblePink];
    }
    
    return color;
}

@end
