//
//  CHColorHandler.h
//  Bubble
//
//  Created by Chayel Heinsen on 4/28/14.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+extensions.h"

@interface CHColorHandler : NSObject

/**
 *  Gets the UIColor that is the same name as the string.
 *
 *  @param string The name of the color.
 *
 *  @return The UIColor that is the same name as the string.
 */
+ (UIColor *)getUIColorFromNSString:(NSString *)string;

/**
 *  Generates a random UIColor.
 *
 *  @return Returns a NSString with name to pass to getUIColorFromNSString.
 */
+ (NSString *)getRandomColor;

/**
 *  Gets the available colors for use.
 *
 *  @return An array of UIColor objects available for use.
 */
+ (NSArray *)getColors;

/**
 *  Gets the UIColor name.
 *
 *  @param color The UIColor that you want to get the name of.
 *
 *  @return An NSString of the UIColor name.
 */
+ (NSString *)getNSStringFromUIColor:(UIColor *)color;

/**
 *  Gets the UIColor that is the same name as the string.
 *
 *  @param string The name of the color.
 *
 *  @return The UIColor that is the same name as the string.
 */
- (UIColor *)getUIColorFromNSString:(NSString *)string;

/**
 *  Generates a random UIColor.
 *
 *  @return Returns a NSString with name to pass to getUIColorFromNSString.
 */
- (NSString *)getRandomColor;

@end
