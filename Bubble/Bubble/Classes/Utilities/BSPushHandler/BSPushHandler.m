//
//  BSPushHandler.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/26/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSPushHandler.h"

@implementation BSPushHandler

+ (void)handlePush:(NSDictionary *)userInfo application:(UIApplication *)application {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *category = userInfo[@"aps"][@"category"];
    
    NSString *title, *text;
    
    NSScanner *scan = [NSScanner scannerWithString:userInfo[@"aps"][@"alert"]];
    [scan scanUpToString:@" " intoString:&title];
    [scan scanUpToString:@"" intoString:&text];
    
    if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground) {
        
        // Opened from a push notification when the app was in the background
        if ([category isEqualToString:@"Message"]) {
            
            if (!appDelegate.isInChat) {
                
                BSAlertView *alert = [[BSAlertView alloc] initWithStyle:BSAlertStyleInfo title:title message:text icon:ion_ios_email iconSize:15 target:self action:@selector(openMessage:)];
                alert.userInfo = userInfo;
                
                [BSPushHandler openMessage:alert];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewMessage" object:nil userInfo:nil];
            
        } else {
            
            BSAlertView *alert = [[BSAlertView alloc] initWithStyle:BSAlertStyleInfo title:title message:text target:self action:@selector(openNotification:)];
            alert.userInfo = userInfo;
            
            [BSPushHandler openNotification:alert];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewNotification" object:nil userInfo:userInfo];
            
        }
        
    } else { // App is already active
        
        if ([category isEqualToString:@"Message"]) {
            
            if (!appDelegate.isInChat) {
                
                BSAlertView *alert = [[BSAlertView alloc] initWithStyle:BSAlertStyleInfo title:title message:text icon:ion_ios_email iconSize:15 target:self action:@selector(openMessage:)];
                alert.userInfo = userInfo;
                //[alert show];
                
                [BSPushHandler openNotification:alert];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewMessage" object:nil userInfo:nil];
            
        } else {
            
            BSAlertView *alert = [[BSAlertView alloc] initWithStyle:BSAlertStyleInfo title:title message:text target:self action:@selector(openNotification:)];
            alert.userInfo = userInfo;
            //[alert show];
            
            [BSPushHandler openNotification:alert];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewNotification" object:nil userInfo:userInfo];
            
        } 
        
    }
    
}

#pragma mark - Selectors

+ (void)openMessage:(BSAlertView *)sender {
    [BSPushHandler openMessageThreadWithUser:[[sender.title removeWhiteSpacesFromString] removeSubString:@":"]];
}

+ (void)openNotification:(BSAlertView *)sender {    
    NSDictionary *userInfo = sender.userInfo;
}

#pragma mark - Helpers for selectors

+ (void)openMessageThreadWithUser:(NSString *)username {
    
    PFQuery *query = [BSUser query];
    [query whereKey:@"username" equalTo:username];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        [self findCachedMessagesBetweenUsers:@[[BSUser currentUser], object]];
    }];
    
}

/**
 *  Finds cached messages between users.
 *
 *  @param users An array of two PFUser objects. Ex: @[currentUser, otherUser];
 */
+ (void)findCachedMessagesBetweenUsers:(NSArray *)users {
    
    __block BSUser *user1 = [users firstObject];
    __block BSUser *user2 = [users objectAtIndex:1];
    
    // Search the cache for a head that contains both the current user and the user that we are currently looking at.
    PFQuery *query = [BSMessageHead query];
    [query whereKey:@"firstUserUsername" equalTo:user1.username];
    [query whereKey:@"secondUserUsername" equalTo:user2.username];
    [query fromPinWithName:@"MessageHeads"];
    
    PFQuery *query2 = [BSMessageHead query];
    [query2 whereKey:@"firstUserUsername" equalTo:user2.username];
    [query2 whereKey:@"secondUserUsername" equalTo:user1.username];
    [query2 fromPinWithName:@"MessageHeads"];
    
    PFQuery *full = [PFQuery orQueryWithSubqueries:@[query, query2]];
    //[full fromPinWithName:@"MessageHeads"];
    [full findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        // There isn't anything cached, so lets look on the server.
        if (objects.count == 0) {
            [self findServerMessagesBetweenUsers:@[user1, user2]];
        } else { // We found a head, lets open it.
            [self openMessageThreadWithUser:user2 andParentNode:[objects firstObject]];
        }
        
    }];
    
}

/**
 *  Finds server messages between users.
 *
 *  @param users An array of two PFUser objects. Ex: @[currentUser, otherUser];
 */
+ (void)findServerMessagesBetweenUsers:(NSArray *)users {
    
    BSUser *user1 = [users firstObject];
    BSUser *user2 = [users objectAtIndex:1];
    
    // Search the cache for a head that contains both the current user and the user that we are currently looking at.
    PFQuery *query = [BSMessageHead query];
    [query whereKey:@"firstUserUsername" equalTo:user1.username];
    [query whereKey:@"secondUserUsername" equalTo:user2.username];
    
    PFQuery *query2 = [BSMessageHead query];
    [query2 whereKey:@"firstUserUsername" equalTo:user2.username];
    [query2 whereKey:@"secondUserUsername" equalTo:user1.username];
    
    PFQuery *full = [PFQuery orQueryWithSubqueries:@[query, query2]];
    [full findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        // There isn't anything on the server, lets create one.
        if (objects.count == 0) {
            [self createMessageThreadWithUser:[users objectAtIndex:1]];
        } else {
            [self openMessageThreadWithUser:[users objectAtIndex:1] andParentNode:[objects firstObject]];
        }
        
    }];
    
}

/**
 *  Creates a new message thread with the given user.
 *
 *  @param user The user to create a message thread with.
 */
+ (void)createMessageThreadWithUser:(BSUser *)user {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    BSMessageHead *parentNode = [BSMessageHead object];
    [parentNode setObject:appDelegate.currentUser forKey:@"firstUser"];
    [parentNode setObject:appDelegate.currentUser.username forKey:@"firstUserUsername"];
    [parentNode setObject:user forKey:@"secondUser"];
    [parentNode setObject:user.username forKey:@"secondUserUsername"];
    
    ChatViewController *chat = [ChatViewController messagesViewController];
    chat.otherUser = user;
    chat.parentNode = parentNode;
    chat.isNewParent = YES;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:chat];
     nc.navigationBar.barTintColor = [UIColor bubblePink];
     nc.navigationBar.tintColor = [UIColor whiteColor];
     nc.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
     
    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    [tabBarController presentViewController:nc animated:YES completion:nil];
    
}

+ (void)openMessageThreadWithUser:(BSUser *)user andParentNode:(BSMessageHead *)parentNode {
    
    ChatViewController *chat = [ChatViewController messagesViewController];
    chat.otherUser = user;
    chat.parentNode = parentNode;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:chat];
    nc.navigationBar.barTintColor = [UIColor bubblePink];
    nc.navigationBar.tintColor = [UIColor whiteColor];
    nc.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    
    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    [tabBarController presentViewController:nc animated:YES completion:nil];
    
}

@end
