//
//  BSPushHandler.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/26/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

@import Foundation;
@import UIKit;
#import "BSAlertView.h"

@interface BSPushHandler : NSObject

/**
 *  Handles remote push notifications. Call this from @code - (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler @endcode
 *
 *  @param userInfo The userInfo from the above method.
 *  @param application The application. This is used to determine if we are already open or not.
 */
+ (void)handlePush:(NSDictionary *)userInfo application:(UIApplication *)application;

@end
