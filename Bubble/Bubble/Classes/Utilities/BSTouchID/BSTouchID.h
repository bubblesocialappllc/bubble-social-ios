//
//  BSTouchID.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/18/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

@import Foundation;
@import LocalAuthentication;

/**
 * The result from showTouchIDAuthenticationWithReason:withCompletion:
 */
typedef NS_ENUM(NSUInteger, BSTouchIDResult) {
    
    /**
     *  The user has successfully authenticated.
     */
    BSTouchIDResultSuccess = 0,
    
    /**
     *  Some error has occured.
     */
    BSTouchIDResultError,
    
    /**
     *  Authentication was not successful because the user failed to provide valid credentials.
     */
    BSTouchIDResultAuthenticationFailed,
    
    /**
     *  Authentication was canceled by the user—for example, the user tapped Cancel in the dialog.
     */
    BSTouchIDResultUserCancel,
    
    /**
     *  Authentication was canceled because the user tapped the fallback button (Enter Password).
     */
    BSTouchIDResultUserFallback,
    
    /**
     *  Authentication was canceled by system—for example, if another application came to foreground while the authentication dialog was up.
     */
    BSTouchIDResultSystemCancel,
    
    /**
     *  Authentication could not start because the passcode is not set on the device.
     */
    BSTouchIDResultPasscodeNotSet,
    
    /**
     *  Authentication could not start because Touch ID is not available on the device.
     */
    BSTouchIDResultNotAvailable,
    
    /**
     *  Authentication could not start because Touch ID has no enrolled fingers.
     */
    BSTouchIDResultNotEnrolled
    
};

/**
 *  The block that is called when we have finished with authentication, either a success or an error.
 *
 *  @param result The BSTouchIDResult of the authentication. Check BSTouchIDResult for valid results.
 */
typedef void(^BSTouchIDCompletion)(BSTouchIDResult result);

/**
 *  BSTouchID is an easy way to authenticate using TouchID.
 */
NS_CLASS_AVAILABLE_IOS(8_0) @interface BSTouchID : NSObject

/**
 *  Shows the TouchID alert.
 *
 *  @param reason Text to show in the alert.
 *  @param block  The completion handler. It will return a TouchID result as a BSTouchIDResult.
 */
+ (void)showTouchIDAuthenticationWithReason:(NSString *)reason withCompletion:(BSTouchIDCompletion)block;

@end
