//
//  BSTouchID.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/18/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSTouchID.h"

@implementation BSTouchID

+ (void)showTouchIDAuthenticationWithReason:(NSString *)reason withCompletion:(BSTouchIDCompletion)block {
    
    LAContext *context = [[LAContext alloc] init];
    
    NSError *error = nil;
    
    // Check if we have TouchID and the user has it enabled.
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        
        // If we do have TouchID, then we can bring up the alert box.
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:reason
                 reply:^(BOOL success, NSError *error) {
            
            if (success) {
                
                block(BSTouchIDResultSuccess);
                
            } else {
                
                switch(error.code) { // Errors
                    case LAErrorAuthenticationFailed:
                        block(BSTouchIDResultAuthenticationFailed);
                        break;
                    case LAErrorUserCancel:
                        block(BSTouchIDResultUserCancel);
                        break;
                    case LAErrorUserFallback:
                        block(BSTouchIDResultUserFallback);
                        break;
                    case LAErrorSystemCancel:
                        block(BSTouchIDResultSystemCancel);
                        break;
                    default:
                        block(BSTouchIDResultError);
                        break;
                }
            }
        }];
        
    } else { // If we don't have TouchID or the user hasn't enabled it, then we send back an error.
        
        switch(error.code) {
            case LAErrorPasscodeNotSet:
                block(BSTouchIDResultPasscodeNotSet);
                break;
            case LAErrorTouchIDNotAvailable:
                block(BSTouchIDResultNotAvailable);
                break;
            case LAErrorTouchIDNotEnrolled:
                block(BSTouchIDResultNotEnrolled);
                break;
            default:
                block(BSTouchIDResultError);
                break;
        }
    }
    
}

@end
