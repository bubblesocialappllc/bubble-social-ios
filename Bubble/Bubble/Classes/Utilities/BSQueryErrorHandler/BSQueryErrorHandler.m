//
//  BSQueryErrorHandler.m
//  Bubble
//
//  Created by Chayel Heinsen on 3/27/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSQueryErrorHandler.h"

@implementation BSQueryErrorHandler

+ (void)handleError:(NSError *)error {
    
    if (![error.domain isEqualToString:PFParseErrorDomain]) {
        return;
    }
    
    switch (error.code) {
        case kPFErrorInvalidSessionToken: {
            [self handleInvalidSessionTokenError];
            break;
        }
            // Other Parse API Errors that you want to explicitly handle.
    }
}

+ (void)handleInvalidSessionTokenError {
    [BSUser logOut];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LogOutUser" object:nil];
}

@end
