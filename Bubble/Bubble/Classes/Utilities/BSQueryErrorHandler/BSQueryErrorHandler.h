//
//  BSQueryErrorHandler.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/27/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface BSQueryErrorHandler : NSObject

+ (void)handleError:(NSError *)error;

@end
