//
//  CloudCode.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/30/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "BSUser.h"

typedef void(^CloudCodeCompletion)(BOOL succeeded, NSError *error);

@interface CloudCode : NSObject

#pragma mark - Push

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channel The name of the channel to push to. Usually the username of the user. Ex: @username or username
 *  @param alert   The alert to send.
 */
+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert;

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channel The name of the channel to push to. Usually the username of the user. Ex: @username or username
 *  @param alert   The alert to send.
 *  @param block   The block to call when the push is done.
 */
+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert completion:(CloudCodeCompletion)block;

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channel The name of the channel to push to. Usually the username of the user. Ex: @username or username
 *  @param alert   The alert to send.
 *  @param category The category for interactive notifications.
 */
+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert andCategory:(NSString *)category;

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channel The name of the channel to push to. Usually the username of the user. Ex: @username or username
 *  @param alert   The alert to send.
 *  @param category The category for interactive notifications.
 *  @param block   The block to call when the push is done.
 */
+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert andCategory:(NSString *)category completion:(CloudCodeCompletion)block;

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channels The names of the channel to push to. Usually the usernames of the user. Ex: @username or username
 *  @param alert   The alert to send.
 */
+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert;

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channels The names of the channel to push to. Usually the usernames of the user. Ex: @username or username
 *  @param alert   The alert to send.
 *  @param block   The block to call when the push is done.
 */
+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert completion:(CloudCodeCompletion)block;

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channels The names of the channel to push to. Usually the usernames of the user. Ex: @username or username
 *  @param alert   The alert to send.
 *  @param category The category for interactive notifications.
 */
+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert andCategory:(NSString *)category;

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channel  The name of the channel to push to. Usually the usernames of the user. Ex: @username or username
 *  @param alert    The alert to send.
 *  @param category The category for interactive notifications.
 *  @param view     The view to open. Should be a query. bubblesocial://post/objectId
 */
+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert andCategory:(NSString *)category andViewToPush:(NSString *)view;

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channels The names of the channel to push to. Usually the usernames of the user. Ex: @username or username
 *  @param alert    The alert to send.
 *  @param category The category for interactive notifications.
 *  @param view     The view to open. Should be a query. bubblesocial://post/objectId
 */
+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert andCategory:(NSString *)category andViewToPush:(NSString *)view;

/**
 *  Calls a cloud method that will make a push notification in the background.
 *
 *  @param channels The names of the channel to push to. Usually the usernames of the user. Ex: @username or username
 *  @param alert   The alert to send.
 *  @param category The category for interactive notifications.
 *  @param block   The block to call when the push is done.
 */
+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert andCategory:(NSString *)category completion:(CloudCodeCompletion)block;

#pragma mark - Bubble Notification Feed

/**
 *  Sets a notification when the post is liked, commented, added buddy, and someone responded
 *
 *  @param objectId The object that contains the post's pointer
 *  @param type     The type of post that is being processed
 */
+ (void)setNotification:(NSString *)objectId andType:(NSString *)type;

#pragma mark - Post

/**
 *  Likes the post.
 *
 *  @param post The post object to like.
 */
+ (void)likePost:(PFObject *)post;

/**
 *  Likes the post.
 *
 *  @param post  The post object to like.
 *  @param block The block to call when the like is done.
 */
+ (void)likePost:(PFObject *)post completion:(CloudCodeCompletion)block;


/**
 *  Unlikes the post.
 *
 *  @param post The post object to unlike.
 */
+ (void)unlikePost:(PFObject *)post;

/**
 *  Unlikes the post.
 *
 *  @param post  The post object to unlike.
 *  @param block The block to call when the unlike is done.
 */
+ (void)unlikePost:(PFObject *)post completion:(CloudCodeCompletion)block;

/**
 *  Posts a status.
 *
 *  @param text The text to post.
 *  @param file The file to add with the post. If you don't need a file, pass nil.
 *  @param point The geopoint of the post.
 */
+ (void)postStatus:(NSString *)text file:(NSData *)file location:(PFGeoPoint *)point pointOfInterest:(BSPlace *)pointOfInterest shouldSavePhotoToUser:(BOOL)savePhoto;

/**
 *  Posts a status.
 *
 *  @param text  The text to post.
 *  @param file The file to add with the post. If you don't need a file, pass nil.
 *  @param point The geopoint of the post.
 *  @param block The block to call when we have saved the post.
 */
+ (void)postStatus:(NSString *)text file:(NSData *)file location:(PFGeoPoint *)point pointOfInterest:(BSPlace *)pointOfInterest shouldSavePhotoToUser:(BOOL)savePhoto completion:(CloudCodeCompletion)block;

#pragma mark - Buddies

/**
 *  This will request the user to be a buddy.
 *
 *  @param buddy The user you would like to request as a buddy.
 */
+ (void)addPendingBuddy:(BSUser *)buddy;

/**
 *  This will request the user to be a buddy.
 *
 *  @param buddy The user you would like to request as a buddy.
 *  @param block The block that will be called when finished.
 */
+ (void)addPendingBuddy:(BSUser *)buddy completion:(CloudCodeCompletion)block;

/**
 *  This will remove a buddy the current user has.
 *
 *  @param buddy The user you want to remove.
 */
+ (void)removeBuddy:(BSUser *)buddy;

/**
 *  This will remove a buddy the current use has.
 *
 *  @param buddy The user you want to remove.
 *  @param block The block that will be called when finished.
 */
+ (void)removeBuddy:(BSUser *)buddy completion:(CloudCodeCompletion)block;

/**
 *  This will accept a buddy that is a pending buddy.
 *
 *  @param buddy The user you would like accept.
 */
+ (void)acceptBuddy:(BSUser *)buddy;


/**
 *  This will accept a buddy that is a pending buddy.
 *
 *  @param buddy The user you would like to accept.
 *  @param block The block that will be called when finished.
 */
+ (void)acceptBuddy:(BSUser *)buddy completion:(CloudCodeCompletion)block;

/**
 *  This will deny a buddy that is a pending buddy.
 *
 *  @param buddy The user you would like to deny.
 */
+ (void)denyBuddy:(BSUser *)buddy;

/**
 *  This will deny a buddy that is a pending buddy.
 *
 *  @param buddy The user you would like to deny.
 *  @param block The block that will be called when finished.
 */
+ (void)denyBuddy:(BSUser *)buddy completion:(CloudCodeCompletion)block;

#pragma mark - Points

/**
 *  Sets the current users populartity points.
 */
+ (void)setPopularityPoints;

/**
 *  Sets the current users populartity points.
 *
 *  @param block The block to call when we have set points.
 */
+ (void)setPopularityPointsWithCompletion:(CloudCodeCompletion)block;

#pragma mark - Phone Verification

/**
 *  Sends a text message with a verification code to a user.
 *
 *  @param number The phone number to send a message to. Ex: 4075550123
 */
+ (void)verifyPhoneNumber:(NSString *)number;

/**
 *  Sends a text message with a verification code to a user.
 *
 *  @param number The phone number to send a message to. Ex: 4075550123
 *  @param block  The block that will be called when finished.
 */
+ (void)verifyPhoneNumber:(NSString *)number completion:(CloudCodeCompletion)block;

@end
