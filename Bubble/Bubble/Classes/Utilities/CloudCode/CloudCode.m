//
//  CloudCode.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/30/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "CloudCode.h"

@implementation CloudCode

#pragma mark - Push

+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert {
    
    if ([channel hasPrefix:@"@"]) {
        channel = [channel removeSubString:@"@"];
    }
    
    [CloudCode pushToChannels:@[channel] withAlert:alert andCategory:nil completion:nil];
    
    /*[PFCloud callFunctionInBackground:@"pushNotification"
                       withParameters:@{@"channel" : channel, @"alert" : alert, @"category" : @"", @"sound" : @"Pop.aiff", @"requestingUser" : @"", @"viewToPush" : @""}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error) {
                                        NSLog(@"%@", result);
                                    } else {
                                        NSLog(@"%@", error.localizedDescription);
                                    }
                                }];*/
    
}

+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert completion:(CloudCodeCompletion)block {
    
    if ([channel hasPrefix:@"@"]) {
        channel = [channel removeSubString:@"@"];
    }
    
    [CloudCode pushToChannels:@[channel] withAlert:alert andCategory:nil completion:^(BOOL succeeded, NSError *error) {
        block(succeeded, error);
    }];
    
    /*[PFCloud callFunctionInBackground:@"pushNotification"
                       withParameters:@{@"channel" : channel, @"alert" : alert, @"category" : @"", @"sound" : @"Pop.aiff", @"requestingUser" : @"", @"viewToPush" : @""}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error) {
                                        
                                        block(YES, nil);
                                        
                                    } else {
                                        
                                        block(NO, error);
                                        
                                    }
                                    
                                }];*/
    
}

+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert andCategory:(NSString *)category {
    
    /*NSString *requestingUser = @"";
    NSString *viewToPush = @"";
    
        // We automatically plug in the needed items if we are on a certain category
    if ([category isEqualToString:PENDING_BUDDIES]) {
        
        requestingUser = [BSUser currentUser].username;
        viewToPush = BUDDYREQUEST;
        
    }*/
    
    if ([channel hasPrefix:@"@"]) {
        channel = [channel removeSubString:@"@"];
    }
    
    [CloudCode pushToChannels:@[channel] withAlert:alert andCategory:category completion:nil];
    
    /*[PFCloud callFunctionInBackground:@"pushNotification"
                       withParameters:@{@"channel" : channel, @"alert" : alert, @"category" : category, @"sound" : @"Pop.aiff", @"requestingUser" : requestingUser, @"viewToPush" : viewToPush}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error) {
                                        NSLog(@"%@", result);
                                    } else {
                                        NSLog(@"%@", error.localizedDescription);
                                    }
                                }];*/
    
}

+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert andCategory:(NSString *)category completion:(CloudCodeCompletion)block {
    
    if ([channel hasPrefix:@"@"]) {
        channel = [channel removeSubString:@"@"];
    }
    
    [CloudCode pushToChannels:@[channel] withAlert:alert andCategory:nil completion:^(BOOL succeeded, NSError *error) {
        block(succeeded, error);
    }];
    
    /*NSString *requestingUser = @"";
    NSString *viewToPush = @"";
    
        // We automatically plug in the needed items if we are on a certain category
    if ([category isEqualToString:PENDING_BUDDIES]) {
        
        requestingUser = [BSUser currentUser].username;
        viewToPush = BUDDYREQUEST;
        
    }
    
    [PFCloud callFunctionInBackground:@"pushNotification"
                       withParameters:@{@"channel" : channel, @"alert" : alert, @"category" : category, @"sound" : @"Pop.aiff", @"requestingUser" : requestingUser, @"viewToPush" : viewToPush}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error) {
                                        
                                        block(YES, nil);
                                        
                                    } else {
                                        
                                        block(NO, error);
                                        
                                    }
                                    
                                }];*/
    
}

+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert {
    
    NSMutableArray *channel = [NSMutableArray new];
    
    for (__strong NSString *object in channels) {
        
        if ([object hasPrefix:@"@"]) {
            object = [object removeSubString:@"@"];
            [channel addObject:object];
        }
        
    }
    
    [CloudCode pushToChannels:channel withAlert:alert andCategory:nil completion:nil];
    
    /*[PFCloud callFunctionInBackground:@"pushNotification"
                       withParameters:@{@"channel" : channels, @"alert" : alert, @"category" : @"", @"sound" : @"Pop.aiff", @"requestingUser" : @"", @"viewToPush" : @""}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error) {
                                        NSLog(@"%@", result);
                                    } else {
                                        NSLog(@"%@", error.localizedDescription);
                                    }
                                }];*/
    
}

+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert completion:(CloudCodeCompletion)block {
    
    NSMutableArray *channel = [NSMutableArray new];
    
    for (__strong NSString *object in channels) {
        
        if ([object hasPrefix:@"@"]) {
            object = [object removeSubString:@"@"];
            [channel addObject:object];
        }
        
    }
    
    [CloudCode pushToChannels:channel withAlert:alert andCategory:nil completion:^(BOOL succeeded, NSError *error) {
        block(succeeded, error);
    }];
    
    /*[PFCloud callFunctionInBackground:@"pushNotification"
                       withParameters:@{@"channel" : channels, @"alert" : alert, @"category" : @"", @"sound" : @"Pop.aiff", @"requestingUser" : @"", @"viewToPush" : @""}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error) {
                                        
                                        block(YES, nil);
                                        
                                    } else {
                                        
                                        block(NO, error);
                                        
                                    }
                                    
                                }];*/
    
}

+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert andCategory:(NSString *)category {
    
    NSMutableArray *channel = [NSMutableArray new];
    
    for (__strong NSString *object in channels) {
        
        if ([object hasPrefix:@"@"]) {
            object = [object removeSubString:@"@"];
            [channel addObject:object];
        }
        
    }
    
    [CloudCode pushToChannels:channel withAlert:alert andCategory:category completion:nil];
    
    /*NSString *requestingUser = @"";
    NSString *viewToPush = @"";
    
        // We automatically plug in the needed items if we are on a certain category
    if ([category isEqualToString:PENDING_BUDDIES]) {
        
        requestingUser = [BSUser currentUser].username;
        viewToPush = BUDDYREQUEST;
        
    }
    
    [PFCloud callFunctionInBackground:@"pushNotification"
                       withParameters:@{@"channel" : channels, @"alert" : alert, @"category" : category, @"sound" : @"Pop.aiff", @"requestingUser" : requestingUser, @"viewToPush" : viewToPush}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error) {
                                        NSLog(@"%@", result);
                                    } else {
                                        NSLog(@"%@", error.localizedDescription);
                                    }
                                }];*/
    
}

+ (void)pushToChannel:(NSString *)channel withAlert:(NSString *)alert andCategory:(NSString *)category andViewToPush:(NSString *)view {
    [CloudCode pushToChannels:@[channel] withAlert:alert andCategory:category andViewToPush:view];
}

+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert andCategory:(NSString *)category andViewToPush:(NSString *)view {
    
    NSString *requestingUser = @"";
    NSString *viewToPush = @"";
    
    if (category == nil) {
        category = @"";
    }
    
    // We automatically plug in the needed items if we are on a certain category
    if ([category isEqualToString:PENDING_BUDDIES]) {
        requestingUser = [BSUser currentUser].username;
        viewToPush = BUDDYREQUEST;
    } else {
        viewToPush = view;
    }
    
    if (viewToPush == nil) {
        viewToPush = @"";
    }
    
    [PFCloud callFunctionInBackground:@"pushNotification"
                       withParameters:@{@"channel" : channels, @"alert" : alert, @"category" : category, @"sound" : @"Pop.aiff", @"requestingUser" : requestingUser, @"viewToPush" : viewToPush}
                                block:nil];
    
}

+ (void)pushToChannels:(NSArray *)channels withAlert:(NSString *)alert andCategory:(NSString *)category completion:(CloudCodeCompletion)block {
    
    NSString *requestingUser = @"";
    NSString *viewToPush = @"";
    
    if (category == nil) {
        category = @"";
    }
    
        // We automatically plug in the needed items if we are on a certain category
    if ([category isEqualToString:PENDING_BUDDIES]) {
        requestingUser = [BSUser currentUser].username;
        viewToPush = BUDDYREQUEST;
    }
    
    [PFCloud callFunctionInBackground:@"pushNotification"
                       withParameters:@{@"channel" : channels, @"alert" : alert, @"category" : category, @"sound" : @"Pop.aiff", @"requestingUser" : requestingUser, @"viewToPush" : viewToPush}
                                block:nil];
    
}

#pragma mark - Bubble Notification Feed

+ (void)setNotification:(NSString *)objectId andType:(NSString *)type {
    [PFCloud callFunctionInBackground:@"setNotification"
                       withParameters:@{@"pObject" : objectId, @"nType" : type}
                                block:nil];
}

#pragma mark - Post

+ (void)likePost:(PFObject *)post {
    
    [PFCloud callFunctionInBackground:@"likePost"
                       withParameters:@{@"objectId" : post.objectId}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error)
                                        NSLog(@"%@", result);
                                    else
                                        NSLog(@"%@", error.localizedDescription);
                                    
                                }];
    
    
}

+ (void)likePost:(PFObject *)post completion:(CloudCodeCompletion)block {
    
    
    [PFCloud callFunctionInBackground:@"likePost"
                       withParameters:@{@"objectId" : post.objectId}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error)
                                        block(YES, nil);
                                    else
                                        block(NO, error);
                                    
                                }];
    
    
}

+ (void)unlikePost:(PFObject *)post {
    
    [PFCloud callFunctionInBackground:@"unlikePost"
                       withParameters:@{@"objectId" : post.objectId}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error)
                                        NSLog(@"%@", result);
                                    else
                                        NSLog(@"%@", error.localizedDescription);
                                    
                                }];
    
}

+ (void)unlikePost:(PFObject *)post completion:(CloudCodeCompletion)block {
    
    [PFCloud callFunctionInBackground:@"unlikePost"
                       withParameters:@{@"objectId" : post.objectId}
                                block:^(NSString *result, NSError *error) {
                                    
                                    if (!error)
                                        block(YES, nil);
                                    else
                                        block(NO, error);
                                    
                                }];
    
}

+ (void)postStatus:(NSString *)text file:(NSData *)file location:(PFGeoPoint *)point pointOfInterest:(BSPlace *)pointOfInterest shouldSavePhotoToUser:(BOOL)savePhoto {
    
    [CloudCode setPopularityPoints];
    
        // Make sure we don't pass a nil object.
    if (file == nil) {
        file = [NSData data];
    }
    
    text = [text removeWhiteSpacesFromString];
    
    NSScanner *scanner = [NSScanner scannerWithString:text];
    NSString *foundUsername, *foundUsernameSecond, *foundUsernameThird, *pushTo, *pushToSecond, *pushToThird;
    
    [scanner scanUpToString:@"@" intoString:nil];
    [scanner scanUpToString:@" " intoString:&foundUsername];
    [scanner scanUpToString:@"@" intoString:nil];
    [scanner scanUpToString:@" " intoString:&foundUsernameSecond];
    [scanner scanUpToString:@"@" intoString:nil];
    [scanner scanUpToString:@" " intoString:&foundUsernameThird];
    
    pushTo = [foundUsername substringFromIndex:1];
    pushToSecond = [foundUsernameSecond substringFromIndex:1];
    pushToThird = [foundUsernameThird substringFromIndex:1];
    
    NSMutableArray *mentioned = [NSMutableArray new];
    
    if (pushTo != nil) {
        [mentioned addObject:pushTo];
    }
    
    if (pushToSecond != nil) {
        [mentioned addObject:pushToSecond];
    }
    
    if (pushToThird != nil) {
        [mentioned addObject:pushToThird];
    }
    
    if (mentioned != nil) {
        mentioned = [NSMutableArray new];
    }
    
    [BSLocation cityNameWithLocation:point completion:^(NSString *cityName, NSError *error) {
       
        if (pointOfInterest.name == nil || pointOfInterest.location == nil || pointOfInterest.placeId == nil) {
            pointOfInterest.name = @"";
            pointOfInterest.placeId = @"";
            pointOfInterest.location = [PFGeoPoint geoPointWithLatitude:0 longitude:0];
        }
        
        [PFCloud callFunctionInBackground:@"postStatus" withParameters:@{
                                                                         @"shouldSavePhoto" : [NSNumber numberWithBool:savePhoto],
                                                                         @"text" : text,
                                                                         @"file": file,
                                                                         @"point" : point,
                                                                         @"locationName" : cityName,
                                                                         @"mentions" : mentioned,
                                                                         @"pointOfInterest" : pointOfInterest.placeId,
                                                                         @"pointOfInterestName" : pointOfInterest.name,
                                                                         @"pointOfInterestGeoPoint" : pointOfInterest.location                                                                }
                                    block:^(NSString *result, NSError *error) {
                                        
                                        if (!error)
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"SuccessfulPost" object:nil];
                                        else {
                                            NSLog(@"%@", error.localizedDescription);
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"SuccessfulPost" object:nil];
                                        }
                                    }];

        
    }];
    
    /*CLLocation *location = [[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude];
    
    __block NSString *locationName = [NSString string];
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        for (CLPlacemark *placemark in placemarks) {
            
            locationName = [NSString stringWithFormat:@"%@", [placemark locality]];
            
            locationName = [locationName stringByAppendingString:[NSString stringWithFormat:@", %@", [placemark administrativeArea]]];
            
        }
        
        if (pointOfInterest.name == nil || pointOfInterest.location == nil || pointOfInterest.placeId == nil) {
            pointOfInterest.name = @"";
            pointOfInterest.placeId = @"";
            pointOfInterest.location = [PFGeoPoint geoPointWithLatitude:0 longitude:0];
        }
        
        [PFCloud callFunctionInBackground:@"postStatus" withParameters:@{
                                                                         @"text" : text,
                                                                         @"file": file,
                                                                         @"point" : point,
                                                                         @"locationName" : locationName,
                                                                         @"mentions" : mentioned,
                                                                         @"pointOfInterest" : pointOfInterest.placeId,
                                                                         @"pointOfInterestName" : pointOfInterest.name,
                                                                         @"pointOfInterestGeoPoint" : pointOfInterest.location                                                                }
                                    block:^(NSString *result, NSError *error) {
                                        
                                        if (!error)
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"SuccessfulPost" object:nil];
                                        else {
                                            NSLog(@"%@", error.localizedDescription);
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"SuccessfulPost" object:nil];
                                        }
        }];
        
    }];*/
    
}

+ (void)postStatus:(NSString *)text file:(NSData *)file location:(PFGeoPoint *)point pointOfInterest:(BSPlace *)pointOfInterest shouldSavePhotoToUser:(BOOL)savePhoto completion:(CloudCodeCompletion)block {
    
    [CloudCode setPopularityPoints];
    
        // Make sure we don't pass a nil object.
    if (file == nil) {
        file = [NSData data];
    }
    
    text = [text removeWhiteSpacesFromString];
    
        // From here
    NSScanner *scanner = [NSScanner scannerWithString:text];
    NSString *foundUsername, *foundUsernameSecond, *foundUsernameThird, *pushTo, *pushToSecond, *pushToThird;
    
    [scanner scanUpToString:@"@" intoString:nil];
    [scanner scanUpToString:@" " intoString:&foundUsername];
    [scanner scanUpToString:@"@" intoString:nil];
    [scanner scanUpToString:@" " intoString:&foundUsernameSecond];
    [scanner scanUpToString:@"@" intoString:nil];
    [scanner scanUpToString:@" " intoString:&foundUsernameThird];
    
    pushTo = [foundUsername substringFromIndex:1];
    pushToSecond = [foundUsernameSecond substringFromIndex:1];
    pushToThird = [foundUsernameThird substringFromIndex:1];
    
    NSMutableArray *mentioned = [NSMutableArray new];
    
    if (pushTo != nil) {
        [mentioned addObject:pushTo];
    }
    
    if (pushToSecond != nil) {
        [mentioned addObject:pushToSecond];
    }
    
    if (pushToThird != nil) {
        [mentioned addObject:pushToThird];
    }
    
    if (mentioned != nil) {
        mentioned = [NSMutableArray new];
    }
    
        // To here
        // Needs to be slightly rewritten. The variable names don't make sense.
    
    [BSLocation cityNameWithLocation:point completion:^(NSString *cityName, NSError *error) {
        
        if (pointOfInterest.name == nil || pointOfInterest.location == nil || pointOfInterest.placeId == nil) {
            pointOfInterest.name = @"";
            pointOfInterest.placeId = @"";
            pointOfInterest.location = [PFGeoPoint geoPointWithLatitude:0 longitude:0];
        }
        
        [PFCloud callFunctionInBackground:@"postStatus" withParameters:@{
                                                                         @"shouldSavePhoto" : [NSNumber numberWithBool:savePhoto],
                                                                         @"text" : text,
                                                                         @"file": file,
                                                                         @"point" : point,
                                                                         @"locationName" : cityName,
                                                                         @"mentions" : mentioned,
                                                                         @"pointOfInterest" : pointOfInterest.placeId,
                                                                         @"pointOfInterestName" : pointOfInterest.name,
                                                                         @"pointOfInterestGeoPoint" : pointOfInterest.location                                                                }
                                    block:^(NSString *result, NSError *error) {
                                        
                                        if (!error) {
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"SuccessfulPost" object:nil];
                                            block(YES, nil);
                                        } else {
                                            NSLog(@"%@", error.localizedDescription);
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"SuccessfulPost" object:nil];
                                            block(NO, nil);
                                        }
                                    }];
        
        
    }];
    
    /*CLLocation *location = [[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude];
    
    __block NSString *locationName = [NSString string];
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        for (CLPlacemark *placemark in placemarks) {
            
            locationName = [NSString stringWithFormat:@"%@", [placemark locality]];
            
            locationName = [locationName stringByAppendingString:[NSString stringWithFormat:@", %@", [placemark administrativeArea]]];
            
        }
        
        if (pointOfInterest.name == nil || pointOfInterest.location == nil || pointOfInterest.placeId == nil) {
            pointOfInterest.name = @"";
            pointOfInterest.placeId = @"";
            pointOfInterest.location = [PFGeoPoint geoPointWithLatitude:0 longitude:0];
        }
        
        [PFCloud callFunctionInBackground:@"postStatus" withParameters:@{
                                                                        @"text" : text,
                                                                        @"file": file,
                                                                        @"point" : point,
                                                                        @"locationName" : locationName,
                                                                        @"mentions" : mentioned,
                                                                        @"pointOfInterest" : pointOfInterest.placeId,
                                                                        @"pointOfInterestName" : pointOfInterest.name,
                                                                        @"pointOfInterestGeoPoint" : pointOfInterest.location
                                                                        }
                                    block:^(NSString *result, NSError *error) {
                                        
                                        if (!error)
                                            block(YES, nil);
                                        else
                                            block(NO, error);
                                        
        }];
        
    }];*/
    
}

#pragma mark - Buddies

+ (void)addPendingBuddy:(BSUser *)buddy {
    
    [PFCloud callFunctionInBackground:@"setPendingBuddy" withParameters:@{@"username" : buddy.username, @"requestingUser" : [BSUser currentUser].username, @"client" : @"iOS"} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            NSLog(@"%@", result);
        } else {
            NSLog(@"%@", error.localizedDescription);
        }
        
    }];
    
}

+ (void)addPendingBuddy:(BSUser *)buddy completion:(CloudCodeCompletion)block {
    
    [PFCloud callFunctionInBackground:@"setPendingBuddy" withParameters:@{@"username" : buddy.username, @"requestingUser" : [BSUser currentUser].username, @"client" : @"iOS"} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            
            block(YES, nil);
            
        } else {
            
            block(NO, error);
        }
        
    }];
    
}

+ (void)removeBuddy:(BSUser *)buddy {
    
    [PFCloud callFunctionInBackground:@"removeBuddy" withParameters:@{@"userToRemove" : buddy.username, @"client" : @"iOS"} block:^(NSString *result, NSError *error) {
        
        [[BSUser currentUser] fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {}];
        
        if (!error) {
            NSLog(@"%@", result);
        } else {
            NSLog(@"%@", error.localizedDescription);
        }
        
    }];
    
}

+ (void)removeBuddy:(BSUser *)buddy completion:(CloudCodeCompletion)block {
    
    [PFCloud callFunctionInBackground:@"removeBuddy" withParameters:@{@"userToRemove" : buddy.username, @"client" : @"iOS"} block:^(NSString *result, NSError *error) {
        
        [[BSUser currentUser] fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {}];
        
        if (!error) {
            
            block(YES, nil);
            
        } else {
            
            block(NO, error);
        }
        
    }];
    
}

+ (void)acceptBuddy:(BSUser *)buddy {
    
    [PFCloud callFunctionInBackground:@"acceptBuddy" withParameters:@{@"username" : buddy.username, @"currentUser" : [BSUser currentUser].username} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            NSLog(@"%@", result);
        } else {
            NSLog(@"%@", error.localizedDescription);
        }
        
    }];
    
}

+ (void)acceptBuddy:(BSUser *)buddy completion:(CloudCodeCompletion)block {
    
    [PFCloud callFunctionInBackground:@"acceptBuddy" withParameters:@{@"username" : buddy.username, @"currentUser" : [BSUser currentUser].username} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            
            block(YES, nil);
            
        } else {
            
            block(NO, error);
        }
        
    }];
    
}

+ (void)denyBuddy:(BSUser *)buddy {
    
    [PFCloud callFunctionInBackground:@"denyuddy" withParameters:@{@"username" : buddy.username, @"currentUser" : [BSUser currentUser].username} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            NSLog(@"%@", result);
        } else {
            NSLog(@"%@", error.localizedDescription);
        }
        
    }];
    
}

+ (void)denyBuddy:(BSUser *)buddy completion:(CloudCodeCompletion)block {
    
    [PFCloud callFunctionInBackground:@"denyBuddy" withParameters:@{@"username" : buddy.username, @"currentUser" : [BSUser currentUser].username} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            
            block(YES, nil);
            
        } else {
            
            block(NO, error);
        }
        
    }];
    
}

#pragma mark - Points

+ (void)setPopularityPoints {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    CGFloat range = [defaults doubleForKey:@"range"];
    
    NSNumber *bounds = [NSNumber numberWithFloat:range];
    
    [PFCloud callFunctionInBackground:@"popPoints" withParameters:@{@"bounds" : bounds} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            NSLog(@"%@", result);
        } else {
            NSLog(@"%@", error.localizedDescription);
        }
        
    }];
    
}

+ (void)setPopularityPointsWithCompletion:(CloudCodeCompletion)block {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    CGFloat range = [defaults doubleForKey:@"range"];
    
    NSNumber *bounds = [NSNumber numberWithFloat:range];
    
    [PFCloud callFunctionInBackground:@"popPoints" withParameters:@{@"bounds" : bounds} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            block(YES, nil);
        } else {
            block(NO, error);
        }
        
    }];
    
}

#pragma mark - Phone Verification

+ (void)verifyPhoneNumber:(NSString *)number {
    
    [PFCloud callFunctionInBackground:@"phoneVerification" withParameters:@{@"phoneNumber" : number} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            NSLog(@"%@", result);
        } else {
            NSLog(@"%@", error.localizedDescription);
        }

        
    }];
    
}

+ (void)verifyPhoneNumber:(NSString *)number completion:(CloudCodeCompletion)block {
    
    [PFCloud callFunctionInBackground:@"phoneVerification" withParameters:@{@"phoneNumber" : number} block:^(NSString *result, NSError *error) {
        
        if (!error) {
            block(YES, nil);
        } else {
            block(NO, error);
        }
        
        
    }];
    
}

@end
