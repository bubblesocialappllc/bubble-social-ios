//
//  MeViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 1/25/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewBuddiesViewController.h"
#import "MePageBlankCellTableViewCell.h"
#import "PhotosCollectionViewController.h"
@import SVPulsingAnnotationView;
@import MSAlertController;

@interface MeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CHPaginatedTableViewDelegate, MKMapViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CameraDelegate>

@property (strong, nonatomic) IBOutlet CHPaginatedTableView *tableView;
@property (strong, nonatomic) IBOutlet PFImageView *profilePicture;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *subtitle;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UILabel *aboutMe;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segment;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *settings;
@property (strong, nonatomic) IBOutlet UIView *nameBackground;
@property (strong, nonatomic) IBOutlet UIView *pastPopsBackground;

- (IBAction)segmentTapped:(id)sender;

@end
