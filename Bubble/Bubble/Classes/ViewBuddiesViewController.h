//
//  ViewBuddiesViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/21/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HasPendingBuddiesTableViewCell.h"
#import "UserTableViewCell.h"
#import "UserViewController.h"

@interface ViewBuddiesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) BSUser *user;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property BOOL isModal;

@end
