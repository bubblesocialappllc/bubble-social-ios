//
//  MapViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 1/12/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "MapViewController.h"
#import "MainTableViewCell.h"

static const NSInteger MILE = 1609;

@interface MapViewController ()

@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) PFGeoPoint *usersLocation;
@property (strong, nonatomic) MONActivityIndicatorView *indicatorView;
@property (strong, nonatomic) SVPulsingAnnotationView *pulsingView;
@property (strong, nonatomic) BubbleAnnotation *userLocationAnnotation;
@property (strong, nonatomic) MKCircle *circle;
@property (strong, nonatomic) BSUser *currentUser;
@property (strong, nonatomic) RangeView *rangeView;
@property (strong, nonatomic) UIBarButtonItem *rangeButton;
@property (strong, nonatomic) NSMutableArray *postData;
@property (strong, nonatomic) NSMutableArray *clusterAnnotations;

@property CFTimeInterval lastWeatherFetch;
@property CGFloat range;
@property CGFloat rangeMeters;
@property BOOL hasShownHiddenError;
@property UIView *radiusCircle;
@property (strong, nonatomic) UIPinchGestureRecognizer *mapPinch;
@property (strong, nonatomic) UITapGestureRecognizer *mapTap;
@property (strong, nonatomic) UILabel *rangeLabel;

@end

@implementation MapViewController {
    
    CGRect tableViewOrig;
    CGRect mapFrameWhenClosed;
    BOOL mapIsOpen;
    CGFloat lastScale;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.degrees setHidden:YES];
    
    self.currentUser = [BSUser currentUser];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainCell"];
    
    self.tableView.estimatedRowHeight = 50;
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.users = [NSMutableArray new];
    self.postData = [NSMutableArray new];
    
    self.usersLocation = self.appDelegate.currentLocation;

    self.lastWeatherFetch = 0;
    
    self.range = [CHKeychain doubleForKey:@"range"];

    self.rangeMeters = self.range / 0.00062137;
    
    self.rangeView = [[RangeView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    UITapGestureRecognizer *rangeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openPopularity)];
    
    [self.rangeView addGestureRecognizer:rangeTap];
    
    self.rangeButton = [[UIBarButtonItem alloc] initWithCustomView:self.rangeView];
    
    [self.rangeView setLabelValue:[NSString stringWithFormat:@"%@", [BSUser currentUser].points]];

    [self.navigationItem setLeftBarButtonItem:self.rangeButton];
    
    [self setupRefreshControlWithColor:[UIColor bubblePink]];
    
    //[self.tableView setContentInset:UIEdgeInsetsMake(self.mapView.frame.size.height-20,0,0,0)];
    //[self.tableView setScrollIndicatorInsets:self.tableView.contentInset];

//    [self refreshTable];

        // Set up map
    self.mapTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMapTap)];
    [self.mapView addGestureRecognizer:self.mapTap];
    [self.mapView setDelegate:self];

        // Pinch gesture recognizer
    self.mapPinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handleMapPinch:)];
    self.mapPinch.delegate = self;
    [self.mapView addGestureRecognizer:self.mapPinch];

    lastScale = 1;

    [self.mapView addGestureRecognizer:self.mapTap];
    [self.mapView setShowsUserLocation:YES];
    [self.mapView setScrollEnabled:NO];
    [self.mapView setPitchEnabled:NO];
    [self.mapView setZoomEnabled:NO];
    [self.mapView setRotateEnabled:NO];
    [self.tableView setAutoresizesSubviews:NO];

        // Set up segmented control
    self.headerSegmentControl = [UISegmentedControl new];
    [self.headerSegmentControl insertSegmentWithTitle:@"People" atIndex:0 animated:NO];
    [self.headerSegmentControl insertSegmentWithTitle:@"Pops" atIndex:1 animated:NO];
    self.headerSegmentControl.tintColor = [UIColor bubblePink];
    [self.headerSegmentControl setFrame: CGRectMake(0, 0, self.tableView.frame.size.width, 30)];
    self.headerSegmentControl.contentMode = UIViewContentModeCenter;
    self.headerSegmentControl.layer.cornerRadius = 0;
    self.headerSegmentControl.backgroundColor = [UIColor whiteColor];
    [self.headerSegmentControl setSelectedSegmentIndex:0];
    [self.headerSegmentControl addTarget:self action:@selector(sortFeed:) forControlEvents:UIControlEventValueChanged];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];

    [self getCurrentWeather];

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mapFrameWhenClosed = self.tableView.tableHeaderView.frame;
    });

    [self setupRefreshControlWithColor:self.navigationController.navigationBar.barTintColor];
    //self.pulsingView.annotationColor = self.navigationController.navigationBar.barTintColor;

    [self updateRange:[CHKeychain doubleForKey:@"range"]];

    if (!self.range || self.range == 0) {
        [CHKeychain setDouble:8.0 forKey:@"range"];
        self.range = 8.0;
    }


    if ([[self.mapView subviews] containsObject:self.radiusCircle]) {
        [self.radiusCircle removeFromSuperview];
    }
    
    // Add circle
    CGFloat height = self.mapView.frame.size.height - 10;
    CGFloat width  = self.tableView.frame.size.width;
    UIView *circle = [[UIView alloc] initWithFrame: CGRectMake((width / 2) - (height/2), 5, height, height)];
    circle.layer.cornerRadius = circle.frame.size.height / 2;
    circle.layer.borderColor = [[UIColor bubblePink] CGColor];
    circle.layer.borderWidth = .7;
    circle.backgroundColor = [[UIColor bubblePink] colorWithAlphaComponent:.1];
    self.radiusCircle = circle;

    [self.mapView addSubview: self.radiusCircle];

    if ([CHKeychain objectForKey:@"mostCurrentRank"]) {
        [self.rangeView setLabelValue:[NSString stringWithFormat:@"%@", [CHKeychain objectForKey:@"mostCurrentRank"]]];
    } else {
        [self rank];
    }
    
//    [self setMapRegionAnimated:YES];
    //[self drawRadiusCircle: NO];

    [self refreshTable];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Gesture Recognizer Delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - UIScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    
//        // Get the current size of the refresh controller
//    CGRect refreshBounds = self.refreshControl.bounds;
//    
//        // Distance the table has been pulled >= 0
//    CGFloat pullDistance = MAX(0.0, -self.refreshControl.frame.origin.y);
//    
//        // Half the width of the table
//        //CGFloat midX = self.tableView.frame.size.width / 2.0;
//    
//        // Calculate the width and height of our graphics
//    CGFloat bubbleHeight = self.indicatorView.bounds.size.height;
//    CGFloat bubbleHeightHalf = bubbleHeight / 2.0;
//    
//        //CGFloat bubbleWidth = indicatorView.bounds.size.width;
//        //CGFloat bubbleWidthHalf = bubbleWidth / 2.0;
//    
//        // Calculate the pull ratio, between 0.0-1.0
//        //CGFloat pullRatio = MIN( MAX(pullDistance, 0.0), 100.0) / 100.0;
//    
//        // Set the Y coord of the graphics, based on pull distance
//    CGFloat bubbleY = pullDistance / 2.0 - bubbleHeightHalf;
//    
//        // Calculate the X coord of the graphics
//    CGFloat bubbleX = self.view.frame.size.width / 2;
//    
//        // Set the graphic's frames
//    CGRect bubbleFrame = self.indicatorView.frame;
//    bubbleFrame.origin.x = bubbleX;
//    bubbleFrame.origin.y = bubbleY;
//    
//    self.indicatorView.frame = bubbleFrame;
//    
//    CGFloat xpoint = self.indicatorView.frame.size.width / 2;
//    
//    self.indicatorView.frame = CGRectOffset(self.indicatorView.frame, -xpoint, 0);
//    
//        // Set the enbubbleing view's frames
//    refreshBounds.size.height = pullDistance;
//    
//    [self.refreshControl addSubview:self.indicatorView];
//    
//    [self.indicatorView startAnimating];
//    
//        // Adjust the header's frame to keep it pinned to the top of the scroll view
//    CGRect headerFrame = self.headerSegmentControl.frame;
//    CGFloat yOffset = scrollView.contentOffset.y;
//   
//    headerFrame.origin.y = MAX(0, yOffset);
//    self.headerSegmentControl.frame = headerFrame;
//    
//        // If the user is pulling down on the top of the scroll view, adjust the scroll indicator
//    CGFloat height = CGRectGetHeight(headerFrame);
//    
//    if (yOffset< 0) {
//        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(ABS(yOffset) + height, 0, 0, 0);
//    }

}

#pragma mark - UITableViewDelegate/DataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellAnimation"]) {
        cell.alpha = 0;
        
        [UIView beginAnimations:@"fade" context:NULL];
        [UIView setAnimationDuration:0.8];
        cell.alpha = 1;
        [UIView commitAnimations];
    }
    
    /*
    if (self.headerSegmentControl.selectedSegmentIndex == 1) {
        
        if (indexPath.section == self.postData.count) {
            [self.tableView paginate];
        }
    }
     */
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (self.headerSegmentControl.selectedSegmentIndex) {
        case 0: {
            
            if (indexPath.row <= self.users.count) {
                
                UserTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"mapUserCell"];
                
                if (cell == nil) {
                    
                    cell = [[UserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mapUserCell"];
                    
                }
                
                BSUser *user = [self.users objectAtIndex:indexPath.row];
                
                cell.profilePicture.clipsToBounds = YES;
                cell.profilePicture.layer.cornerRadius = 20;
                [cell.profilePicture setImageWithString:user.realName];
                cell.profilePicture.file = user.profilePicture;
                [cell.profilePicture loadInBackground];
                
                cell.username.text = user.username;
                
                return cell;
            }

            break;
        }
        case 1: {
            
            if (indexPath.section <= self.postData.count && self.postData.count != 0) {
                
                BSPost *post = [self.postData objectAtIndex:indexPath.section];
                
                MainTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MainCell"];
                
                //If the post has a photo
                if (post.photo) {
                    
                    cell.squareConstraint.active = YES;
                    
                    //We circumvent the Parse cache and directly access the NSCache to prevent photo flicker
                    NSCache *cache = [NSCache new];
                    UIImage *image = [cache objectForKey:post.photo.url];
                    
                    if (!image) {
                        //[cell.image setImageWithString:post.authorUsername color:[UIColor bubblePink]];
                        cell.image.file = post.photo;
                        [cell.image loadInBackground];
                    } else {
                        cell.image.image = image;
                    }
                    
                } else {
                    cell.image.image = [UIImage new];
                    cell.squareConstraint.active = NO;
                }
                
                //The cell text
                cell.text.attributedText = [self attributedMessageFromMessage:post.text];
                
                //Adding the buttons to the footer
                [cell.like setTarget:self andAction:@selector(likePost:)];
                [cell.comment setTarget:self andAction:@selector(commentPost:)];
                
                //Set the numbers based on the post
                cell.like.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likes];
                cell.comment.commentsCountLabel.text = [NSString stringWithFormat:@"%@", post.comments];
                
                NSMutableArray *likers = [NSMutableArray arrayWithArray:post.likers];
                
                if ([likers containsObject:self.appDelegate.currentUser.username]) {
                    cell.like.backgroundColor = [UIColor bubblePink];
                } else {
                    cell.like.backgroundColor = [UIColor lightGrayColor];
                }
                
                NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
                
                if ([commenters containsObject:self.appDelegate.currentUser.username]) {
                    cell.comment.backgroundColor = [UIColor bubblePink];
                } else {
                    cell.comment.backgroundColor = [UIColor lightGrayColor];
                }
                
                //Set up for cell clicks
                UITapGestureRecognizer *cellTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCell:)];
                UITapGestureRecognizer *cellLikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(likePost:)];
                
                [cellTap setDelaysTouchesBegan:YES];
                [cellLikeTap setDelaysTouchesBegan:YES];
                
                cellTap.numberOfTapsRequired = 1;
                cellLikeTap.numberOfTapsRequired = 2;
                
                [cellTap requireGestureRecognizerToFail:cellLikeTap];
                
                [cell addGestureRecognizer:cellLikeTap];
                [cell addGestureRecognizer:cellTap];
                
                UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellDidLongHold:)];
                [cell addGestureRecognizer:longPress];
                
                return cell;
                
            }
            break;
        }
        default:
            break;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (self.headerSegmentControl.selectedSegmentIndex) {
        case 0: {
            return self.users.count;
            break;
        }
        case 1: {
            return 1;
            break;
        }
        default:
            return self.users.count;
            break;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    switch (self.headerSegmentControl.selectedSegmentIndex) {
        case 0: {
            return 1;
            break;
        }
        case 1: {
            return self.postData.count;
            break;
        }
        default:
            return 1;
            break;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    switch (self.headerSegmentControl.selectedSegmentIndex) {
        case 0: {
            
            BSUser *selectedUser = [self.users objectAtIndex:indexPath.row];
            
            if (![selectedUser.username isEqualToString:self.appDelegate.currentUser.username]) {
                
                UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
                user.user = selectedUser;
                
                UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
                
                [self presentViewController:nc animated:YES completion:nil];
                
            }
            
            break;
        }
        case 1: {
            
            /*BSPost *selectedUser = [self.postData objectAtIndex:indexPath.row];
            
            if (![selectedUser.username isEqualToString:self.appDelegate.currentUser.username]) {
                
                UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
                user.user = selectedUser;
                
                UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
                
                [self presentViewController:nc animated:YES completion:nil];
                
            }*/
            
            break;
        }
        default:
            
            break;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (self.headerSegmentControl.selectedSegmentIndex == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        view.backgroundColor = [UIColor whiteColor];
        [self.headerSegmentControl setFrame: CGRectMake(0, 0, tableView.frame.size.width, 30)];
        [view addSubview:self.headerSegmentControl];
        
        return view;
    }
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    header.backgroundColor = [UIColor whiteColor];
    
    if (section == 0) {
        header.frame = CGRectMake(0, 0, self.view.frame.size.width, 90);
        [self.headerSegmentControl setFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        [header addSubview:self.headerSegmentControl];
        
        BSPost *post = [self.postData objectAtIndex:section];
        
        //UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, header.frame.size.width, 4)];
        //line.backgroundColor = [UIColor flatCloudsColor];
        
        PFImageView *profilePicture = [[PFImageView alloc] initWithFrame:CGRectMake(8, 38, 45, 45)];
        profilePicture.contentMode = UIViewContentModeScaleAspectFit;
        profilePicture.clipsToBounds = YES;
        profilePicture.layer.cornerRadius = profilePicture.frame.size.height / 2;
        
        UILabel *username = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 39, header.frame.size.width - 55, 25)];
        
        username.text = post.authorUsername;
        username.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
        username.textColor = [UIColor darkGrayColor];
        username.tag = 1;

        UILabel *location = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 56, header.frame.size.width - 55, 25)];
        
        location.text = post.locationName;
        location.font = [UIFont systemFontOfSize:14];
        location.textColor = [UIColor lightGrayColor];
        
        UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(header.frame.size.width, 15, 0, 0)];
        time.text = [CHTime makeTimePassedFromDate:post.createdAt];
        time.font = [UIFont systemFontOfSize:14];
        time.textColor = [UIColor lightGrayColor];
        [time sizeToFit];
        time.frame = CGRectOffset(time.frame, -time.frame.size.width - 10, (header.frame.size.height / 2) - (time.frame.size.height / 2));
        
        profilePicture.file = post.profilePic;
        [profilePicture loadInBackground];

        UITapGestureRecognizer *headerGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapHeader:)];
        [headerGr setDelegate:self];
        [header addGestureRecognizer:headerGr];

        //[header addSubview:line];
        [header addSubview:profilePicture];
        [header addSubview:username];
        [header addSubview:location];
        [header addSubview:time];

        [header setUserInteractionEnabled:YES];
        return header;
    }
    
    BSPost *post = [self.postData objectAtIndex:section];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, header.frame.size.width, 4)];
    line.backgroundColor = [UIColor flatCloudsColor];
    
    PFImageView *profilePicture = [[PFImageView alloc] initWithFrame:CGRectMake(8, 8, 45, 45)];
    profilePicture.contentMode = UIViewContentModeScaleAspectFit;
    profilePicture.clipsToBounds = YES;
    profilePicture.layer.cornerRadius = profilePicture.frame.size.height / 2;
    
    UILabel *username = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 9, header.frame.size.width - 55, 25)];
    
    username.text = post.authorUsername;
    username.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    username.textColor = [UIColor darkGrayColor];
    username.tag = 1;
    
    UILabel *location = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 26, header.frame.size.width - 55, 25)];
    
    location.text = post.locationName;
    location.font = [UIFont systemFontOfSize:14];
    location.textColor = [UIColor lightGrayColor];
    
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(header.frame.size.width, 0, 0, 0)];
    time.text = [CHTime makeTimePassedFromDate:post.createdAt];
    time.font = [UIFont systemFontOfSize:14];
    time.textColor = [UIColor lightGrayColor];
    [time sizeToFit];
    time.frame = CGRectOffset(time.frame, -time.frame.size.width - 10, (header.frame.size.height / 2) - (time.frame.size.height / 2));
    
    profilePicture.file = post.profilePic;
    [profilePicture loadInBackground];
    
    [header addSubview:line];
    [header addSubview:profilePicture];
    [header addSubview:username];
    [header addSubview:location];
    [header addSubview:time];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapHeader:)];
    [header addGestureRecognizer:tap];
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (self.headerSegmentControl.selectedSegmentIndex == 0) {
        return 30;
    }
    
    if (section == 0) {
        return 90;
    }
    
    return 60;
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
    
        self.mapView.centerCoordinate = userLocation.location.coordinate;
        
    });
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    MKAnnotationView *view;
    
    if ([annotation isKindOfClass:[BSClusterAnnotation class]]) {
        view = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([BSClusterAnnotation class])];
        
        if (!view) {
            view = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                reuseIdentifier:NSStringFromClass([BSClusterAnnotation class])];
            view.image = [IonIcons imageWithIcon:ion_ios_person size:10 color:[UIColor bubblePink]];
            view.canShowCallout = YES;
            view.centerOffset = CGPointMake(view.centerOffset.x, -view.frame.size.height/2);
        }
        
        return view;
    }
    
    static NSString *currentLocationIdentifier = @"currentLocation";

    MKAnnotationView *annotationView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:currentLocationIdentifier];


    if (annotationView == nil) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:currentLocationIdentifier];
        annotationView.image = [[BubbleAnnotation class] imageForAnnotationView];
    }

    CGRect labelFrame = CGRectMake(2.5, 2.5, annotationView.frame.size.width - 5, annotationView.frame.size.height - 5);

    if (self.rangeLabel == nil){
        self.rangeLabel = [[UILabel alloc] initWithFrame: labelFrame];
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.rangeLabel setText:[NSString stringWithFormat:@"%d", (int)self.range]];
        self.rangeLabel.textColor = [UIColor bubblePink];
        self.rangeLabel.adjustsFontSizeToFitWidth = YES;
        self.rangeLabel.font = [UIFont boldSystemFontOfSize:12];
        self.rangeLabel.textAlignment = NSTextAlignmentCenter;
        self.rangeLabel.alpha = 1;
    });

    [annotationView addSubview:self.rangeLabel];
    [annotationView bringSubviewToFront:self.rangeLabel];

    return annotationView;
    
}

- (MKAnnotationView *)mapView:(TSClusterMapView *)mapView viewForClusterAnnotation:(id<MKAnnotation>)annotation {
    
    BSClusteredAnnotation *view = (BSClusteredAnnotation *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([BSClusteredAnnotation class])];
  
    if (!view) {
        view = [[BSClusteredAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:NSStringFromClass([BSClusteredAnnotation class])];
    }
    
    return view;
    
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {

        // THIS ISNT USED ANYMORE

    MKCircle *circleOverlay = (MKCircle *)overlay;
    MKCircleRenderer *circleView = [[MKCircleRenderer alloc] initWithCircle:circleOverlay];
    
    UIColor *color = self.navigationController.navigationBar.barTintColor;
    
    circleView.strokeColor = color;
    circleView.fillColor = [color colorWithAlphaComponent:0.3];
    circleView.lineWidth = 0.7;
    circleView.lineDashPattern = @[@3, @4];

    return circleView;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{

    [self refreshTable];

    [UIView animateWithDuration:.5
                          delay:0
         usingSpringWithDamping:.6
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.radiusCircle.transform = CGAffineTransformMakeScale(1, 1);
                     }
                     completion:^(BOOL finished) {

                     }];
    
}
#pragma mark - Range Delegate

- (double)getCurrentRange {
    
    return self.range;
    
}

- (void) updateRange:(CGFloat)newRange {

    // Sanatize range

    self.range = newRange;

    if (self.range > 10) {
        self.range = 10;
    }

    if (self.range < .25) {
        self.range = .25;
    }

    self.rangeMeters = self.range / 0.00062137;

    // Set the range in the keychain
    [CHKeychain setDouble:self.range forKey:@"range"];

    // Change region of the map to fit the circle nicely
    [self setMapRegionAnimated:YES];

    [self updateRangeLabel];

    // If the range isnt the .25 or 10, the upper and lower bounds, then we should animate the circle out
    if (self.range != .25 && self.range != 10) {
        [UIView animateWithDuration:.3 animations:^{
            self.radiusCircle.transform = CGAffineTransformMakeScale(.01, .01);
        }];
    }
    
    
}

- (void)changeRangeViewDismissed:(double)newRange {

    //TODO: FIGURE SOMETHING OUT WITH THIS METHOD
//    
//    self.range = newRange;
//    
//    [CHKeychain setDouble:newRange forKey:@"range"];
//    
//    [self.rangeView setLabelValue:[NSString stringWithFormat:@"%@", [BSUser currentUser].points]];
//    
//    self.rangeMeters = self.range / 0.00062137;
//    
//    //[self reloadMap];
//    [self refreshTable];
}

#pragma mark - MONActivityIndicatorViewDelegate

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
      circleBackgroundColorAtIndex:(NSUInteger)index {
    
    return [UIColor whiteColor];
}

#pragma mark - IBActions

- (void)sortFeed:(UISegmentedControl *)sender {

    switch (sender.selectedSegmentIndex) {
            // Sort by popularity points
        case 0: {
            
            NSSortDescriptor *pointsDescriptor = [[NSSortDescriptor alloc] initWithKey:@"popPoints" ascending:NO];
            NSArray *descriptors = [NSArray arrayWithObjects:pointsDescriptor, nil];
            
            [self.users setArray:[self.users sortedArrayUsingDescriptors:descriptors]];
            
            self.tableView.rowHeight = 50;
            
            [self.tableView reloadData];
            
            break;
        }
            // Sort by postData
        case 1: {
            
            self.tableView.rowHeight = UITableViewAutomaticDimension;
            
            PFQuery *postQuery = [BSPost query];
            
            NSDateComponents *components = [NSDateComponents new];
            components.day = -1;
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDate *day = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
            
            [postQuery includeKey:@"author"];
            [postQuery whereKey:@"createdAt" greaterThan:day];
            [postQuery whereKey:@"location" nearGeoPoint:self.appDelegate.currentLocation withinMiles:[CHKeychain doubleForKey:@"range"]];
            
            if (self.currentUser.blocked != nil) {
                [postQuery whereKey:@"authorId" notContainedIn:self.currentUser.blocked];
            }
            
            [postQuery orderByDescending:@"points"];
            [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                
                self.postData = [objects mutableCopy];
                [self.tableView reloadData];
            }];
            
            break;
        }
    }
}

#pragma mark - Helpers

- (void)didTapHeader:(UITapGestureRecognizer *)gr {
    UILabel *label;

    for (UIView *v in gr.view.subviews) {
       
        if ([v isKindOfClass:[UILabel class]]) {
        
            if (v.tag == 1) {
                label = (UILabel *)v;
            }
        }
    }

    // Get username associated with this post
    NSString *username = label.text;
    BSUser *user;

    // Get user associated with username
    for (BSPost *post in self.postData) {
     
        if ([username isEqualToString:post.authorUsername]) {
            user = post.author;
        }
    }

    if (user) {

        if (![user.username isEqualToString:self.appDelegate.currentUser.username]) {

            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : user }];
            
        }
    }
}

- (void)setupRefreshControlWithColor:(UIColor *)color {
    
    [self.refreshControl removeFromSuperview];
    
    self.refreshControl = nil;
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@""]];
    [self.refreshControl setBackgroundColor:color];
    [self.refreshControl setTintColor:[UIColor clearColor]];
    
    self.indicatorView = [[MONActivityIndicatorView alloc] init];
    self.indicatorView.delegate = self;
    self.indicatorView.numberOfCircles = 5;
    self.indicatorView.radius = 10;
    self.indicatorView.internalSpacing = 5;
    self.indicatorView.duration = 0.8;
    self.indicatorView.delay = 0.2;
    self.indicatorView.center = self.refreshControl.center;
    self.indicatorView.clipsToBounds = YES;
    
    [self.tableView addSubview:self.refreshControl];
}

- (void)refreshTable {
    
    if (![CLLocationManager locationServicesEnabled]) {
        
        if ([CHDevice deviceVersion] >= 8.0) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Looks like you turned off Location Services" message:@"Make sure you have Location Services turned on and share your location with us from Settings!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
                
            }];
            
            UIAlertAction *leaveAloneAction = [UIAlertAction actionWithTitle:@"Leave me alone right now!" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                self.appDelegate.stopShowingLocationError = YES;
                
            }];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Alright" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                
            }];
            
            [alert addActions:@[settingsAction, leaveAloneAction, cancel]];
            
            /*UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Looks like you turned off Location Services"
                                  message:@"Make sure you have Location Services turned on and share your location with us from Settings!"
                                  delegate:self
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:@"Settings", @"Leave me alone right now!", nil];*/
            
            if (!self.appDelegate.stopShowingLocationError) {
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        } else {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Looks like you turned off Location Services" message:@"Make sure you have Location Services turned on and share your location with us from Settings! Just go to Settings > Privacy > Location" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *leaveAloneAction = [UIAlertAction actionWithTitle:@"Leave me alone right now!" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                self.appDelegate.stopShowingLocationError = YES;
                
            }];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Alright" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                
            }];
            
            [alert addActions:@[leaveAloneAction, cancel]];

            
            /*UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Looks like you turned off Location Services"
                                  message:@"Make sure you have Location Services turned on and share your location with us from Settings! Just go to Settings > Privacy > Location"
                                  delegate:self
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:@"Leave me alone right now!", nil];*/
            
            if (!self.appDelegate.stopShowingLocationError) {
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        
        [self.indicatorView stopAnimating];
        [self.indicatorView removeFromSuperview];
        
        [self.refreshControl endRefreshing];
        
    } else {
        
        if (self.appDelegate.currentLocation != nil) {
            
            if (self.headerSegmentControl.selectedSegmentIndex == 0) {
                PFQuery *userQuery = [BSUser query];
                
                [userQuery whereKey:@"location" nearGeoPoint:self.appDelegate.currentLocation withinMiles:self.range];
                [userQuery whereKey:@"locationHidden" equalTo:[NSNumber numberWithBool:NO]];
                [userQuery setLimit:100];
                [userQuery orderByDescending:@"popPoints"];
                
                [userQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    
                    if (!error) {
                        
                        self.users = [objects mutableCopy];
                        //self.postData = [NSMutableArray new];
                        
                        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(reloadData) userInfo:nil repeats:NO];
                        
                    }
                }];
            } else {
                PFQuery *postQuery = [BSPost query];
                
                NSDateComponents *components = [NSDateComponents new];
                components.day = -1;
                
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDate *day = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
                
                [postQuery includeKey:@"author"];
                [postQuery whereKey:@"createdAt" greaterThan:day];
                [postQuery whereKey:@"location" nearGeoPoint:self.appDelegate.currentLocation withinMiles:[CHKeychain doubleForKey:@"range"]];
                
                if (self.currentUser.blocked != nil) {
                    [postQuery whereKey:@"authorId" notContainedIn:self.currentUser.blocked];
                }
                
                [postQuery orderByDescending:@"points"];
                [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                
                    self.postData = [objects mutableCopy];
                    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(reloadData) userInfo:nil repeats:NO];
                }];
                
            }
            
            [self setMapRegionAnimated:YES];
            
        } else {
            
            [KVNProgress showErrorWithStatus:@"Whoops! I don't seem to have your location yet"];
            
        }
        
    }
}

- (void)reloadMap {

        // THIS ISNT USED ANYMORE

    // Get region span
    MKCoordinateSpan span = self.mapView.region.span;

    // Get region center
    CLLocationCoordinate2D center = self.mapView.region.center;

    // Get latitude in meters
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:(center.latitude - span.latitudeDelta * 0.5) longitude:center.longitude];
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:(center.latitude + span.latitudeDelta * 0.5) longitude:center.longitude];

    int metersLatitude = [loc1 distanceFromLocation:loc2];

    if (metersLatitude / 2 > (10 / .00062137)){
        self.range = 10;
        self.rangeMeters = self.range / .00062137;
    } else {
        self.rangeMeters = metersLatitude / 2;
        self.range = self.rangeMeters * .00062137;
    }

    [CHKeychain setDouble:self.range forKey:@"range"];

//    if (self.range < 1)
//        [self.rangeView setLabelValue:[NSString stringWithFormat:@"%.2lf", self.range]];
//    else
//        [self.rangeView setLabelValue:[NSString stringWithFormat:@"%i", (int)self.range]];

    MKUserLocation *userLocation = self.mapView.userLocation;

    NSMutableArray *overlays = [[self.mapView overlays] mutableCopy];


    dispatch_async(dispatch_get_main_queue(), ^{
        if ([overlays containsObject:self.circle]) {

            [overlays removeObject:self.circle];
            [self.mapView removeOverlay:self.circle];

        }

        self.circle = [MKCircle circleWithCenterCoordinate:userLocation.location.coordinate radius:self.rangeMeters];

        [self.mapView addOverlay:self.circle];
    });
}

- (void)reloadData {
    
    if (!self.currentUser.locationHidden) {
        
        [self.tableView setAlpha:1.0];
        [self sortFeed:self.headerSegmentControl];
        
    } else {
        
        [self.tableView setAlpha:0.0];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Your Location is Hidden" message:@"To view other users in your area you will need to unhide yourself first." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *unhideAction = [UIAlertAction actionWithTitle:@"Unhide + Show" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self.currentUser setObject:[NSNumber numberWithBool:NO] forKey:@"locationHidden"];
            
            [self.currentUser saveInBackground];
            
            self.hasShownHiddenError = NO;
            
            [self.tableView setAlpha:1.0];
            [self.tableView reloadData];
            
        }];
        
        UIAlertAction *hideAction = [UIAlertAction actionWithTitle:@"Keep Hidden" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            self.hasShownHiddenError = NO;
            
        }];
        
        [alert addActions:@[unhideAction, hideAction]];

        
        /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your Location is Hidden"
                                                        message:@"To view other users in your area you will need to unhide yourself first."
                                                       delegate:self
                                              cancelButtonTitle:@"Keep Hidden"
                                              otherButtonTitles:@"Unhide + Show", nil];*/
        if (!self.hasShownHiddenError) {
            
            self.hasShownHiddenError = YES;
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    
    [self.indicatorView stopAnimating];
    [self.indicatorView removeFromSuperview];
    
    [self.refreshControl endRefreshing];
}

- (void)setMapRegionAnimated: (BOOL)animated {

    MKUserLocation *userLocation = self.mapView.userLocation;

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, (self.rangeMeters * 2) + (MILE * (self.range / 5)), (self.rangeMeters * 2) + (MILE * (self.range / 5)));

    /*
     The idea behind the    + (MILE * (self.range / 5))    is that by doing this, the circle inside the region will
     have a seemingly constant distance to the top and bottom. Every time the region is calculated, the amount of extra
     space in the region is calculated based on the range itself, so its always proportionate.
     */

    [self.mapView setRegion:region animated:animated];
}

- (void)handleMapPinch:(UIPinchGestureRecognizer*)pinchRecognizer {

    if (lastScale - pinchRecognizer.scale > 0) {
        // Getting smaller -> Scale the circle as long as it leaves the range greater than 1/4 mile
        if (self.range * pinchRecognizer.scale > .25) {
            self.radiusCircle.transform = CGAffineTransformMakeScale(pinchRecognizer.scale, pinchRecognizer.scale);
            [self.radiusCircle setCenter:[self.mapView center]];
        }

    } else if (lastScale - pinchRecognizer.scale < 0){
        // Getting bigger -> Scale the circle as long as it leaves the range less than 10 miles
        if (self.range * pinchRecognizer.scale < 10) {
            self.radiusCircle.transform = CGAffineTransformMakeScale(pinchRecognizer.scale, pinchRecognizer.scale);
            [self.radiusCircle setCenter:[self.mapView center]];
        }

    }

    if (pinchRecognizer.state == UIGestureRecognizerStateEnded) {
        self.range = self.range * pinchRecognizer.scale;
        [self updateRange:self.range];
    }
    
    
    lastScale = pinchRecognizer.scale;
}

- (void) updateRangeLabel {

    NSString *labelText;

    if (self.range < 1) {
        labelText = @"3/4";
    }

    if (self.range < .75) {
        labelText = @"1/2";
    }

    if (self.range < .5) {
        labelText = @"1/4";
    }


    dispatch_async(dispatch_get_main_queue(), ^{
        if (labelText) {
            [self.rangeLabel setText: labelText];
        } else {
            [self.rangeLabel setText: [NSString stringWithFormat:@"%d", (int)self.range]];
        }

        [self.rangeLabel setNeedsDisplay];
    });
    
    
}

- (void)openPopularity {
    
    PopularityViewController *popularityVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PopularityViewController"];
    popularityVC.delegate = self;
    
    [self presentViewController:popularityVC animated:YES completion:nil];
    
}

- (void)handleMapTap {

        // If the map is animating, then do nothing
    //TODO: Write this

        // If the map is open, close it. Otherwise, open it
    mapIsOpen ? [self closeMap] : [self openMap];

}

- (void)rank {
    NSNumber *numberWithDouble = [NSNumber numberWithDouble:[CHKeychain doubleForKey:@"range"]];
    
    [PFCloud callFunctionInBackground:@"rank"
                       withParameters:@{
                                        @"bounds" : numberWithDouble
                                        }
                                block:^(id object, NSError *error) {
                                    [CHKeychain setObject:object forKey:@"mostCurrentRank"];
                                    
                                    [self.rangeView setLabelValue:[NSString stringWithFormat:@"%@", [CHKeychain objectForKey:@"mostCurrentRank"]]];
                                }];
}

- (void)openMap {

    self.mapView.userTrackingMode = MKUserTrackingModeNone;

    // Get new frame
    CGRect newMapFrame = CGRectMake(0, 0, self.tableView.frame.size.width, self.view.frame.size.height);
    UIView *tableHeaderView = self.tableView.tableHeaderView;

    CGFloat circleHeight = newMapFrame.size.height;
    CGFloat circleWidth  = self.tableView.frame.size.width;
    CGRect newCircleFrame = CGRectMake((circleWidth / 2) - (circleWidth - 10)/2, (circleHeight - (circleWidth-10))/2, circleWidth-10, circleWidth-10);

    [UIView animateWithDuration:0.2 animations:^{
        tableHeaderView.frame = newMapFrame;
        self.tableView.tableHeaderView = tableHeaderView;
        self.mapView.frame = newMapFrame;
        self.radiusCircle.center = self.mapView.center;
        self.radiusCircle.frame = newCircleFrame;
        self.radiusCircle.layer.cornerRadius = newCircleFrame.size.height /2;
    } completion:^(BOOL finished) {

        // Redraw the circle annotation & map region
        [self setMapRegionAnimated:YES];

        // Disable scrolling
        [self.tableView setScrollEnabled:NO];

        mapIsOpen = YES;
    }];
    
    
}
- (void)closeMap {

    self.mapView.userTrackingMode = MKUserTrackingModeFollow;

    // Get new frame
    CGRect newMapFrame = mapFrameWhenClosed;

    // Get reference to header
    UIView *tableHeaderView = self.tableView.tableHeaderView;

    // Circle Frame
    CGFloat circleHeight = newMapFrame.size.height - 10;
    CGFloat circleWidth  = self.tableView.frame.size.width;
    CGRect newCircleFrame = CGRectMake((circleWidth / 2) - (circleHeight/2), 5, circleHeight, circleHeight);

    [UIView animateWithDuration:0.2 animations:^{

        // Shrink the map
        self.mapView.frame = newMapFrame;
        self.radiusCircle.center = self.mapView.center;
        self.radiusCircle.frame = newCircleFrame;
        self.radiusCircle.layer.cornerRadius = self.radiusCircle.frame.size.height /2;

    } completion:^(BOOL finished) {

        // Shrink the header
        tableHeaderView.frame = newMapFrame;
        self.tableView.tableHeaderView = tableHeaderView;

        // Redraw the circle annotation & map region
        [self setMapRegionAnimated:YES];

        // Enable scrolling
        [self.tableView setScrollEnabled:YES];
        
        mapIsOpen = NO;;
    }];
    
}

- (void)getCurrentWeather {
    
    CFTimeInterval elapsedTime = CACurrentMediaTime() - self.lastWeatherFetch;
    
    if (self.lastWeatherFetch == 0 || elapsedTime > 600) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSString *url = @"http://api.openweathermap.org/data/2.5/weather?";
        
        url = [url stringByAppendingString:[NSString stringWithFormat:@"lat=%f&lon=%f&units=imperial", self.appDelegate.currentLocation.latitude, self.appDelegate.currentLocation.longitude]];
        
        [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            self.lastWeatherFetch = CACurrentMediaTime();
            
            // NSLog(@"JSON: %@", responseObject);
            
            NSString *tempStr = responseObject[@"main"][@"temp"];
            NSInteger temp = [tempStr integerValue];
            self.degrees.text = [NSString stringWithFormat:@"%zd°F", temp];
            self.degrees.clipsToBounds = YES;
            self.degrees.layer.cornerRadius = 25;
            self.degrees.alpha = 0.0;
            [self.degrees setHidden:NO];
            
            [UIView animateWithDuration:0.5 animations:^{
               
                self.degrees.alpha = 1.0;
                
            }];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
        
    }
    
}

- (void)startClustering {
    
    [self parseTestJSON];
    
}

- (void)parseTestJSON {
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        
        NSData *JSONData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"BSClusterTest" ofType:@"json"]];
        
        for (NSDictionary *annotationDictionary in [NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:nil]) {
            
            BSClusterAnnotation *annotation = [[BSClusterAnnotation alloc] initWithDictionary:annotationDictionary];
            [self.clusterAnnotations addObject:annotation];
            [self.mapView addClusteredAnnotation:annotation];
        }
        
    }];
    
}

- (void)openUser:(UIGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    BSUser *selectedUser = post.author;
    
    if (![selectedUser.username isEqualToString:self.appDelegate.currentUser.username]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : selectedUser }];
        
        /*UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
         user.user = selectedUser;
         
         UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
         
         [self presentViewController:nc animated:YES completion:nil];*/
        
    }
    
}

- (void)likePost:(UIGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    TextTableViewCell *cell = (TextTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    BOOL shouldLike = NO;
    
    if (![post.authorUsername isEqualToString:self.appDelegate.currentUser.username]) {
        
        NSMutableArray *likers = [NSMutableArray new];
        
        [likers setArray:post.likers];
        
        for (NSString *liker in likers) {
            
            if ([liker isEqualToString:self.appDelegate.currentUser.username]) {
                
                shouldLike = NO;
                break;
            } else {
                shouldLike = YES;
            }
            
        }
        
        if (likers.count == 0) {
            shouldLike = YES;
        }
        
        if (shouldLike) {
            
            [self likePost:post label:cell.like.likesCountLabel];
            cell.like.backgroundColor = [UIColor bubblePink];
            [cell.like setEnabled:NO];
            
        } else {
            
            cell.like.backgroundColor = [UIColor lightGrayColor];
            [self unlikePost:post label:cell.like.likesCountLabel];
            [cell.like setEnabled:NO];
            
        }
        
    }
    
}

- (void)commentPost:(UIGestureRecognizer *)tap {
    
    CGPoint pressPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pressPoint];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenComment" object:nil userInfo:@{ @"object" : post }];
    
}

- (void)commentPostWithImageViewer:(UIGestureRecognizer *)tap {
    
    CGPoint pressPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pressPoint];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenCommentWithImageViewer" object:nil userInfo:@{ @"object" : post }];
    
}

- (void)likePost:(BSPost *)post label:(UILabel *)label {
    
    NSMutableArray *hasLikedBeforeCopy = [NSMutableArray arrayWithArray:post.hasLikedBefore];
    
    NSNumber *likes = post.likes;
    likes = [NSNumber numberWithInt:[likes intValue] + 1];
    
    label.text = [NSString stringWithFormat:@"%@", likes];
    
    NSMutableArray *likers = [NSMutableArray new];
    
    [likers setArray:post.likers];
    
    if (likers.count == 0 || likers == nil) {
        likers = [NSMutableArray arrayWithObject:self.appDelegate.currentUser.username];
    } else {
        
        BOOL hasLiked = [likers containsObject:self.appDelegate.currentUser.username];
        
        if (!hasLiked) {
            [likers addObject:self.appDelegate.currentUser.username];
        }
    }
    
    NSMutableArray *hasLikedBefore = [NSMutableArray arrayWithArray:post.hasLikedBefore];
    
    if (hasLikedBefore.count == 0 || hasLikedBefore == nil) {
        hasLikedBefore = [NSMutableArray arrayWithObject:self.appDelegate.currentUser.username];
    } else {
        
        BOOL hasLiked = [hasLikedBefore containsObject:self.appDelegate.currentUser.username];
        
        if (!hasLiked) {
            [hasLikedBefore addObject:self.appDelegate.currentUser.username];
        }
        
    }
    
    NSNumber *points = post.points;
    
    points = [NSNumber numberWithInt:[points intValue] + 1];
    
    post.likers = likers;
    post.hasLikedBefore = hasLikedBefore;
    post.likes = likes;
    post.points = points;
    
    [post saveEventually:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            
            if (![hasLikedBeforeCopy containsObject:self.appDelegate.currentUser.username]) {
                
                [CloudCode pushToChannel:post.authorUsername withAlert:[NSString stringWithFormat:@"%@ liked your post!", self.appDelegate.currentUser.username] andCategory:@"like" andViewToPush:[NSString stringWithFormat:@"bubblesocial://post/%@", post.objectId]];
                [CloudCode setNotification:post.objectId andType:[BSNotificationType BSLikeNotification]];
                
            }
            
        }
        
    }];
    
}

- (void)unlikePost:(BSPost *)post label:(UILabel *)label {
    
    NSNumber *likes = post.likes;
    likes = [NSNumber numberWithInt:[likes intValue] - 1];
    
    label.text = [NSString stringWithFormat:@"%@", likes];
    
    NSMutableArray *likers = [NSMutableArray new];
    
    [likers setArray:post.likers];
    
    BOOL hasLiked = [likers containsObject:self.appDelegate.currentUser.username];
    
    if (hasLiked) {
        [likers removeObject:self.appDelegate.currentUser.username];
    }
    
    NSNumber *points = post.points;
    
    points = [NSNumber numberWithInt:[points intValue] - 1];
    
    post.likers = likers;
    post.likes = likes;
    post.points = points;
    
    [post saveEventually];
    
}

- (NSAttributedString *)attributedMessageFromMessage:(NSString *)message {
    
    NSArray *messageWords = [message componentsSeparatedByString:@" "];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@""];
    
    for (NSString *word in messageWords) {
        
        NSDictionary *attributes;
        
        if ([word isEqualToString: @""]) {
            
        } else if ([word characterAtIndex:0] == '@') {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor bubblePink],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"username",
                           @"username" : [word substringFromIndex:1],
                           };
            
        } else if ([word characterAtIndex:0] == '#') {
            attributes = @{
                           NSForegroundColorAttributeName:[UIColor flatEmeraldColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"hashtag",
                           @"hashtag" : [word substringFromIndex:1],
                           };
            
        } else {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor darkGrayColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"normal",
                           };
        }
        
        NSAttributedString *subString = [[NSAttributedString alloc]
                                         initWithString:[NSString stringWithFormat:@"%@ ", word]
                                         attributes:attributes];
        
        [attributedMessage appendAttributedString:subString];
    }
    
    return attributedMessage;
}

- (void)messageTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:location
                                                      inTextContainer:textView.textContainer
                             fractionOfDistanceBetweenInsertionPoints:nil];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id wordTypes = [textView.attributedText attribute:@"wordType"
                                                  atIndex:characterIndex
                                           effectiveRange:&range];
        
        if ([wordTypes isEqualToString:@"username"]) {
            
            NSString *userName = [textView.attributedText attribute:@"username"
                                                            atIndex:characterIndex
                                                     effectiveRange:&range];
            
            PFQuery *userQuery = [BSUser query];
            [userQuery whereKey:@"username" equalTo:userName];
            [userQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : object }];
                
            }];
            
        } else if ([wordTypes isEqualToString:@"hashtag"]) {
            
            NSString *hash = [textView.attributedText attribute:@"hashtag"
                                                        atIndex:characterIndex
                                                 effectiveRange:&range];
            hash = [hash lowercaseString];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenHashtag" object:nil userInfo:@{ @"hashtag" : hash }];
            
        } else {
            [self tappedCell:recognizer];
        }
    }
}

- (void)reportPost:(BSPost *)post {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"What's the problem?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *inappropriate = [UIAlertAction actionWithTitle:@"Inappropriate" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *spam = [UIAlertAction actionWithTitle:@"Spamming" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *other = [UIAlertAction actionWithTitle:@"Other" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[inappropriate, spam, other, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)tappedCell:(UITapGestureRecognizer *)tap {
    
    CGPoint point = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    if (post.photo != nil) {
        [self commentPostWithImageViewer:tap];
    } else {
        [self commentPost:tap];
    }
    
}

- (void)cellDidLongHold:(UILongPressGestureRecognizer *)hole {
    
    CGPoint point = [hole locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    MainTableViewCell *cell = (MainTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        //There is a conflict here with deleting Pops and having the animation for Pops, we need to come back to this
        [self.postData removeObjectAtIndex:indexPath.section];
        [self.tableView reloadData];
        
        //[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        
        [post deleteInBackground];
        
    }];
    
    UIAlertAction *report = [UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        [self reportPost:post];
        
    }];
    
    UIAlertAction *save = [UIAlertAction actionWithTitle:@"Save Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (cell.image.image && post.photo) {
            UIImageWriteToSavedPhotosAlbum(cell.image.image, nil, nil, nil);
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    if ([post.authorId isEqualToString:self.appDelegate.currentUser.objectId]) {
        [alert addAction:delete];
        
        if (cell.image.image && post.photo) {
            [alert addAction:save];
        }
        
        [alert addAction:cancel];
        
    } else {
        [alert addAction:report];
        
        if (cell.image.image && post.photo) {
            [alert addAction:save];
        }
        
        [alert addAction:cancel];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
