//
//  SearchViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchTableViewCell.h"

@interface SearchViewController : UIViewController <UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *emptySearch;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
@property (strong, nonatomic) IBOutlet UISearchDisplayController *searchController;
#pragma GCC diagnostic pop


@end
