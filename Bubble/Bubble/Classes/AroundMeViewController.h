//
//  AroundMeViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HashtagViewController.h"
#import "MePageBlankCellTableViewCell.h"
#import "BSClusterMapView.h"

@interface AroundMeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CHPaginatedTableViewDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet CHPaginatedTableView *tableView;
@property (strong, nonatomic) BSClusterMapView *mapView;

@end
