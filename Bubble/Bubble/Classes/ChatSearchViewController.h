//
//  ChatSearchViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 1/30/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Parse;

@protocol ChatSearchDelegate <NSObject>

- (void)chosenUser:(BSUser *)user;

@end

@interface ChatSearchViewController : UIViewController <UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource, UITableViewDelegate>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
@property (strong, nonatomic) IBOutlet UISearchDisplayController *searchBarController;
#pragma GCC diagnostic pop

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) id<ChatSearchDelegate> delegate;

@end
