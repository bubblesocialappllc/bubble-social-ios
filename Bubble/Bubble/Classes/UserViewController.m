//
//  UserViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/19/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "UserViewController.h"

@interface UserViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *postData;
@property (strong, nonatomic) PFQuery *refreshQuery;
@property (strong, nonatomic) NSMutableArray *buddies;
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) RangeView *pointsView;

@end

@implementation UserViewController  {
    BOOL hasAddedBlur;
}

- (void)viewDidLayoutSubviews {
    
    if (!hasAddedBlur) {
        
        CGRect blurRect = CGRectMake(0, self.segment.frame.origin.y, self.segment.frame.size.width, self.segment.frame.size.height);
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:blur];
        blurView.frame = blurRect;
        
        [self.headerBackground addSubview:blurView];
        
        hasAddedBlur = YES;
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.postData = [NSMutableArray new];
    
    self.pointsView = [[RangeView alloc] initWithFrame:CGRectMake(self.profilePicture.frame.origin.x, self.profilePicture.frame.origin.y + self.profilePicture.frame.size.height - 30, 30, 30)];
    [self.header addSubview:self.pointsView];
    
    /*self.tableView.fetchDelegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.limit = 3;*/
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_person_stalker size:30 color:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(showOptions)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_close_round size:30 color:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(closeViewController)];
    
    [self.appDelegate.currentUser fetchInBackground];
    [self.user fetchInBackground];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpUserInformation];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView refresh];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row <= self.postData.count && self.postData.count != 0) {
        
        BSPost *post = [self.postData objectAtIndex:indexPath.row];
        
        if (post.photo != nil && [post.text isEqualToString:@""]) {
            
            ImageTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ImageOnlyMe"];
            
            if (cell == nil) {
                
                cell = [[ImageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ImageOnlyMe"];
                
            }
            
            cell.image.clipsToBounds = YES;
            cell.image.layer.cornerRadius = 20;
            cell.image.file = self.appDelegate.currentUser.profilePicture;
            [cell.image loadInBackground];
            
            NSDate *date = [post createdAt];
            // Add 24 hours to the date becuase these posts are created 24 hours after they
            // were actually posted because there are all of the backups, which populate after 24 hours.
            date = [date dateByAddingTimeInterval:24 * 60 * 60];
            
            cell.time.timeLabel.text = [CHTime makeTimePassedFromDate:date];
            
            [cell.postImage setImageWithString:post.authorUsername];
            cell.postImage.file = post.photo;
            [cell.postImage startLoaderWithTintColor:[UIColor bubblePink]];
            [cell.postImage loadInBackgroundWithProgress:^(int progress) {
                [cell.postImage updateImageDownloadProgress:progress];
                
                if (progress == 100) {
                    [cell.postImage reveal];
                }
            }];
            
            cell.username.text = post.authorUsername;
            cell.location.text = post.locationName;
            
            //[cell.like setTarget:self andAction:@selector(likePost:)];
            //[cell.comment setTarget:self andAction:@selector(commentPost:)];
            
            cell.like.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likes];
            
            NSMutableArray *likers = [NSMutableArray arrayWithArray:post.likers];
            
            if ([likers containsObject:self.appDelegate.currentUser.username]) {
                cell.like.backgroundColor = [UIColor bubblePink];
            } else {
                cell.like.backgroundColor = [UIColor lightGrayColor];
            }
            
            NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
            
            if ([commenters containsObject:self.appDelegate.currentUser.username]) {
                cell.comment.backgroundColor = [UIColor bubblePink];
            } else {
                cell.comment.backgroundColor = [UIColor lightGrayColor];
            }
            
            return cell;
            
        } else if (post.photo != nil && ![post.text isEqualToString:@""]) {
            
            TextImageTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextImageMe"];
            
            if (cell == nil) {
                
                cell = [[TextImageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextImageMe"];
                
            }
            
            cell.image.clipsToBounds = YES;
            cell.image.layer.cornerRadius = 20;
            cell.image.file = self.appDelegate.currentUser.profilePicture;
            [cell.image loadInBackground];
            
            NSDate *date = [post createdAt];
            // Add 24 hours to the date becuase these posts are created 24 hours after they
            // were actually posted because there are all of the backups, which populate after 24 hours.
            date = [date dateByAddingTimeInterval:24 * 60 * 60];
            
            cell.time.timeLabel.text = [CHTime makeTimePassedFromDate:date];
            
            [cell.postImage setImageWithString:post.authorUsername];
            cell.postImage.file = post.photo;
            [cell.postImage startLoaderWithTintColor:[UIColor bubblePink]];
            [cell.postImage loadInBackgroundWithProgress:^(int progress) {
                [cell.postImage updateImageDownloadProgress:progress];
                
                if (progress == 100) {
                    [cell.postImage reveal];
                }
            }];
            
            cell.username.text = post.authorUsername;
            cell.location.text = post.locationName;
            
            UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageTapped:)];
            
            cell.textView.selectable = YES;
            cell.textView.attributedText = [self attributedMessageFromMessage:post.text];
            [cell.textView addGestureRecognizer:tapped];
            
            //[cell.like setTarget:self andAction:@selector(likePost:)];
            //[cell.comment setTarget:self andAction:@selector(commentPost:)];
            
            cell.like.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likes];
            
            NSMutableArray *likers = [NSMutableArray arrayWithArray:post.likers];
            
            if ([likers containsObject:self.appDelegate.currentUser.username]) {
                cell.like.backgroundColor = [UIColor bubblePink];
            } else {
                cell.like.backgroundColor = [UIColor lightGrayColor];
            }
            
            NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
            
            if ([commenters containsObject:self.appDelegate.currentUser.username]) {
                cell.comment.backgroundColor = [UIColor bubblePink];
            } else {
                cell.comment.backgroundColor = [UIColor lightGrayColor];
            }
            
            return cell;
            
        } else if (post.photo == nil && ![post.text isEqualToString:@""]) {
            
            TextTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextOnlyMe"];
            
            if (cell == nil) {
                
                cell = [[TextTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextOnlyMe"];
                
            }
            
            cell.image.clipsToBounds = YES;
            cell.image.layer.cornerRadius = 20;
            [cell.image setImageWithString:post.authorUsername];
            cell.image.file = self.appDelegate.currentUser.profilePicture;
            [cell.image loadInBackground];
            
            NSDate *date = [post createdAt];
            // Add 24 hours to the date becuase these posts are created 24 hours after they
            // were actually posted because there are all of the backups, which populate after 24 hours.
            date = [date dateByAddingTimeInterval:24 * 60 * 60];
            
            cell.time.timeLabel.text = [CHTime makeTimePassedFromDate:date];
            
            cell.username.text = post.authorUsername;
            cell.location.text = post.locationName;
            
            UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageTapped:)];
            
            cell.textView.selectable = YES;
            cell.textView.attributedText = [self attributedMessageFromMessage:post.text];
            [cell.textView addGestureRecognizer:tapped];
            
            //[cell.like setTarget:self andAction:@selector(likePost:)];
            //[cell.comment setTarget:self andAction:@selector(commentPost:)];
            
            cell.like.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likes];
            
            NSMutableArray *likers = [NSMutableArray arrayWithArray:post.likers];
            
            if ([likers containsObject:self.appDelegate.currentUser.username]) {
                cell.like.backgroundColor = [UIColor bubblePink];
            } else {
                cell.like.backgroundColor = [UIColor lightGrayColor];
            }
            
            NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
            
            if ([commenters containsObject:self.appDelegate.currentUser.username]) {
                cell.comment.backgroundColor = [UIColor bubblePink];
            } else {
                cell.comment.backgroundColor = [UIColor lightGrayColor];
            }
            
            return cell;
            
        }
        
    }
    
    // Just make sure that we always return something.
    UITableViewCell *nilCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"nilCell"];
    return nilCell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.postData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row <= self.postData.count && self.postData.count != 0) {
        
        BSPost *post = [self.postData objectAtIndex:indexPath.row];
        
        if (post.photo != nil && [post.text isEqualToString:@""]) {
            
            return 400;
            
        } else if (post.photo != nil && ![post.text isEqualToString:@""]) {
            
            return 500;
            
        } else if (post.photo == nil && ![post.text isEqualToString:@""]) {
            
            return 190;
            
        }
        
    }
    
    return 0;
}

#pragma mark - CHPaginatedTableViewDelegate

- (void)tableView:(CHPaginatedTableView *)tableview fetchedItems:(NSArray *)objects {
    
    [self.postData addObjectsFromArray:objects];
    
    [self.tableView reloadData];
    
}

- (PFQuery *)queryForFetch {
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = 1;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *day = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
    
    self.refreshQuery = [BSPost query];
    [self.refreshQuery includeKey:@"author"];
    [self.refreshQuery orderByDescending:@"createdAt"];
    [self.refreshQuery whereKey:@"createdAt" greaterThan:day];
    [self.refreshQuery whereKey:@"authorUsername" equalTo:self.appDelegate.currentUser.username];

    return self.refreshQuery;
    
}

- (void)pulledToRefresh {
    
    [self.postData removeAllObjects];
    
}

- (void)tableViewInitialized {
    
    UIColor *color = [CHColorHandler getUIColorFromNSString:self.user.color];
    self.tableView.refreshControl.backgroundColor = color;
    
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // Get the current size of the refresh controller
    CGRect refreshBounds = self.tableView.refreshControl.bounds;
    
    // Distance the table has been pulled >= 0
    CGFloat pullDistance = MAX(0.0, -self.tableView.refreshControl.frame.origin.y);
    
    // Half the width of the table
    //CGFloat midX = self.tableView.frame.size.width / 2.0;
    
    // Calculate the width and height of our graphics
    CGFloat bubbleHeight = self.tableView.indicatorView.bounds.size.height;
    CGFloat bubbleHeightHalf = bubbleHeight / 2.0;
    
    //CGFloat bubbleWidth = indicatorView.bounds.size.width;
    //CGFloat bubbleWidthHalf = bubbleWidth / 2.0;
    
    // Calculate the pull ratio, between 0.0-1.0
    //CGFloat pullRatio = MIN( MAX(pullDistance, 0.0), 100.0) / 100.0;
    
    // Set the Y coord of the graphics, based on pull distance
    CGFloat bubbleY = pullDistance / 2.0 - bubbleHeightHalf;
    
    // Calculate the X coord of the graphics
    CGFloat bubbleX = [UIScreen mainScreen].applicationFrame.size.width / 2;
    
    // Set the graphic's frames
    CGRect bubbleFrame = self.tableView.indicatorView.frame;
    bubbleFrame.origin.x = bubbleX;
    bubbleFrame.origin.y = bubbleY;
    
    self.tableView.indicatorView.frame = bubbleFrame;
    
    CGFloat xpoint = self.tableView.indicatorView.frame.size.width / 2;
    
    self.tableView.indicatorView.frame = CGRectOffset(self.tableView.indicatorView.frame, -xpoint, 0);
    
    // Set the enbubbleing view's frames
    refreshBounds.size.height = pullDistance;
    
    [self.tableView.refreshControl addSubview:self.tableView.indicatorView];
    
    [self.tableView.indicatorView startAnimating];
    
}

#pragma mark - IBActions

- (IBAction)segmentTapped:(id)sender {
    
    if (self.segment.selectedSegmentIndex == 0) {
        
        [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(removeSelectedSegment) userInfo:nil repeats:NO];
        
        [self performSegueWithIdentifier:@"showBuddiesSegue" sender:nil];
        
    } else if (self.segment.selectedSegmentIndex == 1) {
        
        [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(removeSelectedSegment) userInfo:nil repeats:NO];
        
        // View Photos
        PhotosCollectionViewController *photos = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotosCollectionViewController"];
        photos.user = self.user;
        
        [self.navigationController pushViewController:photos animated:YES];
        
    } else {
        
        // Message Me
        [self findCachedMessagesBetweenUsers:@[self.appDelegate.currentUser, self.user]];
        
    }
    
}

#pragma mark - Helpers

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue destinationViewController] isKindOfClass:[ViewBuddiesViewController class]]) {
        
        ViewBuddiesViewController *buddies = [segue destinationViewController];
        buddies.user = self.user;
        buddies.title = [NSString stringWithFormat:@"%@'s Buddies", self.user.username];
    }
    
}

/**
 *  Creates a new message thread with the given user.
 *
 *  @param user The user to create a message thread with.
 */
- (void)createMessageThreadWithUser:(BSUser *)user {
    
    BSMessageHead *parentNode = [BSMessageHead object];
    [parentNode setObject:self.appDelegate.currentUser forKey:@"firstUser"];
    [parentNode setObject:self.appDelegate.currentUser.username forKey:@"firstUserUsername"];
    [parentNode setObject:user forKey:@"secondUser"];
    [parentNode setObject:user.username forKey:@"secondUserUsername"];
    
    //[parentNode saveInBackground];
    
    ChatViewController *chat = [ChatViewController messagesViewController];
    chat.otherUser = user;
    chat.parentNode = parentNode;
    chat.isNewParent = YES;
    
    self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    [self.navigationController pushViewController:chat animated:YES];
    
    /*UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:chat];
    nc.navigationBar.barTintColor = [UIColor bubblePink];
    nc.navigationBar.tintColor = [UIColor whiteColor];
    nc.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    
    [self presentViewController:nc animated:YES completion:nil];*/
    
}

- (void)openMessageThreadWithUser:(BSUser *)user andParentNode:(BSMessageHead *)parentNode {
    
    ChatViewController *chat = [ChatViewController messagesViewController];
    chat.otherUser = user;
    chat.parentNode = parentNode;
    
    self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    [self.navigationController pushViewController:chat animated:YES];
    
    /*UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:chat];
    nc.navigationBar.barTintColor = [UIColor bubblePink];
    nc.navigationBar.tintColor = [UIColor whiteColor];
    nc.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    
    [self presentViewController:nc animated:YES completion:nil];*/
    
}

/**
 *  Finds cached messages between users.
 *
 *  @param users An array of two PFUser objects. Ex: @[currentUser, otherUser];
 */
- (void)findCachedMessagesBetweenUsers:(NSArray *)users {
    
    __block BSUser *user1 = [users firstObject];
    __block BSUser *user2 = [users objectAtIndex:1];
    
    // Search the cache for a head that contains both the current user and the user that we are currently looking at.
    PFQuery *query = [BSMessageHead query];
    [query whereKey:@"firstUserUsername" equalTo:user1.username];
    [query whereKey:@"secondUserUsername" equalTo:user2.username];
    [query fromPinWithName:@"MessageHeads"];
    
    PFQuery *query2 = [BSMessageHead query];
    [query2 whereKey:@"firstUserUsername" equalTo:user2.username];
    [query2 whereKey:@"secondUserUsername" equalTo:user1.username];
    [query2 fromPinWithName:@"MessageHeads"];
    
    PFQuery *full = [PFQuery orQueryWithSubqueries:@[query, query2]];
    //[full fromPinWithName:@"MessageHeads"];
    [full findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        // There isn't anything cached, so lets look on the server.
        if (objects.count == 0) {
            [self findServerMessagesBetweenUsers:@[user1, user2]];
        } else { // We found a head, lets open it.
            [self openMessageThreadWithUser:user2 andParentNode:[objects firstObject]];
        }
        
    }];
    
}

/**
 *  Finds server messages between users.
 *
 *  @param users An array of two PFUser objects. Ex: @[currentUser, otherUser];
 */
- (void)findServerMessagesBetweenUsers:(NSArray *)users {
    
    BSUser *user1 = [users firstObject];
    BSUser *user2 = [users objectAtIndex:1];
    
    // Search the cache for a head that contains both the current user and the user that we are currently looking at.
    PFQuery *query = [BSMessageHead query];
    [query whereKey:@"firstUserUsername" equalTo:user1.username];
    [query whereKey:@"secondUserUsername" equalTo:user2.username];
    
    PFQuery *query2 = [BSMessageHead query];
    [query2 whereKey:@"firstUserUsername" equalTo:user2.username];
    [query2 whereKey:@"secondUserUsername" equalTo:user1.username];
    
    PFQuery *full = [PFQuery orQueryWithSubqueries:@[query, query2]];
    [full findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        // There isn't anything on the server, lets create one.
        if (objects.count == 0) {
            [self createMessageThreadWithUser:[users objectAtIndex:1]];
        } else {
            [self openMessageThreadWithUser:[users objectAtIndex:1] andParentNode:[objects firstObject]];
        }
        
    }];
    
}

- (void)removeSelectedSegment {
    [self.segment setSelectedSegmentIndex:UISegmentedControlNoSegment];
}

- (void)setUpUserInformation {
    
    // This will appear as the title in the navigation bar
    /*UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.shadowColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    self.navigationItem.titleView = label;
    label.text = self.user.username;
    [label sizeToFit];*/
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.title = self.user.username;
    
    self.profilePicture.layer.borderWidth = 3;
    self.profilePicture.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profilePicture.clipsToBounds = YES;
    self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.height / 2;
    [self.profilePicture setImageWithString:self.user.realName color:[UIColor bubblePink]];
    self.profilePicture.file = self.user.profilePicture;
    [self.profilePicture loadInBackground];
    
    self.name.text = self.user.realName;
    self.subtitle.text = [NSString stringWithFormat:@"%zd Pops", [self.user.posts integerValue]];
    self.textView.text = self.user.aboutMe;
    
    self.buddies = [NSMutableArray arrayWithArray:self.user.buddies];
    self.photos = [NSMutableArray arrayWithArray:self.user.photos];
    //Set segmented control titles
    [self.segment setTitle:[NSString stringWithFormat:@"Buddies • %zd", self.buddies.count] forSegmentAtIndex:0];
    [self.segment setTitle:[NSString stringWithFormat:@"Photos • %zd", self.photos.count] forSegmentAtIndex:1];
    
    UIColor *color = [CHColorHandler getUIColorFromNSString:self.user.color];
    
    self.headerBackground.backgroundColor = color;
    self.navigationController.navigationBar.barTintColor = color;
    self.segment.tintColor = color;
    self.textView.tintColor = color; //Set the selectable color of links in About Me
    self.aboutMeLabel.tintColor = color;
    self.tableView.refreshControl.backgroundColor = color;
    
    [self.pointsView setLabelValue:[NSString stringWithFormat:@"%@", self.user.points]];
    self.pointsView.label.textColor = color;
    
}

- (NSAttributedString *)attributedMessageFromMessage:(NSString *)message {
    
    NSArray *messageWords = [message componentsSeparatedByString:@" "];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@""];
    
    for (NSString *word in messageWords) {
        
        NSDictionary *attributes;
        
        if ([word isEqualToString: @""]) {
            
        } else if ([word characterAtIndex:0] == '@') {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor bubblePink],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"username",
                           @"username" : [word substringFromIndex:1],
                           };
            
        } else if ([word characterAtIndex:0] == '#') {
            attributes = @{
                           NSForegroundColorAttributeName:[UIColor flatEmeraldColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"hashtag",
                           @"hashtag" : [word substringFromIndex:1],
                           };
            
        } else {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor darkGrayColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"normal",
                           };
        }
        
        NSAttributedString *subString = [[NSAttributedString alloc]
                                         initWithString:[NSString stringWithFormat:@"%@ ", word]
                                         attributes:attributes];
        
        [attributedMessage appendAttributedString:subString];
    }
    
    return attributedMessage;
}

- (void)messageTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:nil];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id wordTypes = [textView.attributedText attribute:@"wordType"
                                                  atIndex:characterIndex
                                           effectiveRange:&range];
        
        if ([wordTypes isEqualToString:@"username"]) {
            
            /*NSString *userName = [textView.attributedText attribute:@"username"
             atIndex:characterIndex
             effectiveRange:&range];*/
            
            //TODO: Handle username tap
            
        } else if ([wordTypes isEqualToString:@"hashtag"]) {
            
            NSString *hash = [textView.attributedText attribute:@"hashtag"
                                                        atIndex:characterIndex
                                                 effectiveRange:&range];
            hash = [hash lowercaseString];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenHashtag" object:nil userInfo:@{ @"hashtag" : hash }];
            
        } else {
            /*
             NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
             PFObject *obj = [self.postArray objectAtIndex:indexPath.row];
             commentObjectID = obj.objectId;
             [self performSegueWithIdentifier:@"Comments" sender:self];
             */
        }
    }
}

- (void)showOptions {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *add = [UIAlertAction actionWithTitle:@"Add as Buddy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        self.navigationItem.rightBarButtonItem.enabled = NO;
        
        [CloudCode addPendingBuddy:self.user completion:^(BOOL succeeded, NSError *error) {
            
            if (succeeded) {
                [self.user fetchInBackground];
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
            
        }];
        
    }];
    
    UIAlertAction *remove = [UIAlertAction actionWithTitle:@"Remove Buddy" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
       
        self.navigationItem.rightBarButtonItem.enabled = NO;
        
        [CloudCode removeBuddy:self.user completion:^(BOOL succeeded, NSError *error) {
            
            if (succeeded) {
                [self.user fetchInBackground];
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
            
        }];
        
    }];
    
    UIAlertAction *alreadyPending = [UIAlertAction actionWithTitle:@"Pending Buddy" style:UIAlertActionStyleDefault handler:nil];
    
    /*UIAlertAction *info = [UIAlertAction actionWithTitle:@"Request Contact Info" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       
        //TODO: Add info.
        
    }];*/
    
    UIAlertAction *block = [UIAlertAction actionWithTitle:@"Block" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        NSMutableArray *blocked = [NSMutableArray arrayWithArray:self.appDelegate.currentUser.blocked];
        
        if (blocked == nil) {
            blocked = [NSMutableArray arrayWithObject:self.user.objectId];
        } else {
            [blocked addObject:self.user.objectId];
        }
        
        self.appDelegate.currentUser.blocked = blocked;
        [self.appDelegate.currentUser saveEventually];
        
    }];
    
    UIAlertAction *unblock = [UIAlertAction actionWithTitle:@"Unblock" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        NSMutableArray *blocked = [NSMutableArray arrayWithArray:self.appDelegate.currentUser.blocked];
        
        [blocked removeObject:self.user.objectId];
        
        self.appDelegate.currentUser.blocked = blocked;
        [self.appDelegate.currentUser saveEventually];
        
    }];
    
    if ([self.appDelegate.currentUser.buddies containsObject:self.user.objectId]) {
        
        if ([self.appDelegate.currentUser.blocked containsObject:self.user.objectId]) {
            [alert addActions:@[remove, unblock, cancel]];
        } else {
            [alert addActions:@[remove, block, cancel]];
        }
        
    } else if ([self.user.pendingBuddies containsObject:self.appDelegate.currentUser.objectId]) {
        
        if ([self.appDelegate.currentUser.blocked containsObject:self.user.objectId]) {
            [alert addActions:@[alreadyPending, unblock, cancel]];
        } else {
            [alert addActions:@[alreadyPending, block, cancel]];
        }
        
    } else {
        
        if ([self.appDelegate.currentUser.blocked containsObject:self.user.objectId]) {
            [alert addActions:@[add, unblock, cancel]];
        } else {
            [alert addActions:@[add, block, cancel]];
        }

    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)closeViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
