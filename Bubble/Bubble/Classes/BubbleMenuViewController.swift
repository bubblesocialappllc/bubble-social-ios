//
//  BubbleMenuViewController.swift
//  Bubble
//
//  Created by Chayel Heinsen on 12/20/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

import Foundation
import UIKit
import Parse
import ionicons

class BubbleMenuViewController: UIViewController, BubbleMenuDelegate, RangeDelegate, LoginDelegate {
    
    var notifications: UIViewController?
    var aroundMe: UIViewController?
    var buddies: UIViewController?
    var appDelegate: AppDelegate
    
    /// Array to keep track of controllers in page menu
    var controllerArray: [UIViewController] = []
    
    var pageMenu: BubbleMenu?
    var rangeView: RangeView?
    var post: UIBarButtonItem?
    var range: Double = 0.0
    var hasSetTitleImage: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        super.init(coder: aDecoder)
    }
    
    override func viewDidLayoutSubviews() {
        
        // If we havent set the title image, we set it
        if !hasSetTitleImage {
            
            // We grab the image from a file
            let logo = UIImage(named: "bubbleLogoWhite.png");
            let imageView = UIImageView(image:logo)
            imageView.contentMode = .ScaleAspectFit
            
            // Set the frame of the imageView
            imageView.frame.size.width = 130;
            imageView.frame.size.height = 22;
            
            // We get the center of the view so we can place the image in the center
            var x :  CGFloat? = navigationController?.navigationBar.center.x
            var y :  CGFloat? = navigationController?.navigationBar.center.y
            
            x! -= imageView.frame.width/2
            y! -= imageView.frame.height
            
            y! -= 4
            
            imageView.frame.origin = CGPoint(x: x!, y: y!)
            
            navigationController?.navigationBar.addSubview(imageView)
            
            hasSetTitleImage = true;
        }
        
    }
    
    override func viewDidLoad() {
        
        //MARK: - UI Setup
        
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: UIStatusBarAnimation.Slide)
        
        // Sets up the Navigation Bar
        self.navigationController?.navigationBar.barTintColor = UIColor.bubblePink()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent = false
        
        /*
        rangeView = RangeView(frame: CGRectMake(0, 0, 30, 30))
        
        var rangeTap = UITapGestureRecognizer(target: self, action: "openRange")
        
        rangeView?.addGestureRecognizer(rangeTap)
        
        var rangeButton = UIBarButtonItem(customView: rangeView!)
        */

        post = UIBarButtonItem(image: IonIcons.imageWithIcon(ion_edit, size: 23, color: UIColor.whiteColor()), style: .Plain, target: self, action: "openPost")
        
        //self.navigationItem.setLeftBarButtonItem(rangeButton, animated: true)
        self.navigationItem.setRightBarButtonItem(post, animated: true)
       
        if (CHKeychain.objectForKey("range") != nil) {
            
            range = CHKeychain.doubleForKey("range")
            
            if (range < 1) {
                
                rangeView?.setLabelValue(NSString(format: "%.2lf", range) as String)
                
            } else {
                
                rangeView?.setLabelValue(NSString(format: "%i", Int(range)) as String)
                
            }
            
            if (range > 10) {
                range = 10;
            }
            
        } else {
            
            CHKeychain.setDouble(5.0, forKey: "range")
            rangeView?.setLabelValue("5")
            
            range = 5.0
            
        }
        
        //MARK: - Bubble Menu Setup
        
        // We start the set up here by loading all of the view controllers 
        // that should be inside the page menu
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, then add to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        notifications = self.storyboard?.instantiateViewControllerWithIdentifier("NotificationsViewController")
        notifications?.title = "Notifications"
        controllerArray.append(notifications!)
        
        aroundMe = self.storyboard?.instantiateViewControllerWithIdentifier("AroundMeViewController")
        
        aroundMe?.title = "Now"
        controllerArray.append(aroundMe!)
        
        buddies = self.storyboard?.instantiateViewControllerWithIdentifier("BuddiesViewController")
        
        buddies?.title = "Buddies"
        controllerArray.append(buddies!)
        
        // Check to see if we have an authenticated user
        if (BSUser.currentUser() == nil) {
            
            self.userIsLoggedOut()
            
        } else {
            
            if (BSUser.currentUser()!.username!.utf16.count > 15) {
                self.showPickUsername()
            }
            
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didFinishSignUpWithUser:", name: "UserDidSignUp", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "dismissAllViewControllers", name: "DismissAllViewControllers", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "openHashtag:", name: "OpenHashtag", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "userIsLoggedOut", name: "LogOutUser", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "openComment:", name: "OpenComment", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "openCommentWithImageViewer:", name: "OpenCommentWithImageViewer", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "openUser:", name: "OpenUserPage", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "openPopularity", name: "openPopularity", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "openPost", name: "OpenPost", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "newNotification:", name: "NewNotification", object: nil);
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "removeNotificationBadge", name: "RemoveNotificationBadge", object: nil)
        
    }
    
    override func viewDidAppear(animated: Bool) {

        let shouldShowTutorial = CHKeychain.objectForKey("shouldShowTutorial") as? Bool

        if shouldShowTutorial == true{
            // If we should show the tutorial, show it

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("ImageIntroViewController") as! ImageIntroViewController

            self.presentViewController(vc, animated: true, completion: nil)

            CHKeychain.setObject(false, forKey: "shouldShowTutorial")
            
        }

        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: UIStatusBarAnimation.Slide)
        
        // Initialize page menu with the controllers here because Swift is a POS
        // and freaks out if trying to load this on viewDidLoad 
        // because the frame doesn't exist yet
        pageMenu = BubbleMenu(viewControllers: controllerArray)
        
        pageMenu!.delegate = self
        
        // Set frame for page menu
        pageMenu!.view.frame = CGRectMake(0.0, self.navigationController!.navigationBar.frame.height - 46, self.view.frame.width, self.view.frame.height)
        
        // Customize page menu
        pageMenu!.scrollMenuBackgroundColor = UIColor.bubblePink()
        pageMenu!.viewBackgroundColor = UIColor.whiteColor()
        pageMenu!.selectionIndicatorColor = UIColor.whiteColor()
        pageMenu!.unselectedMenuItemLabelColor = UIColor(hex: "FFB0E6")
        pageMenu!.bottomMenuHairlineColor = UIColor.clearColor()
        pageMenu!.menuItemFont = UIFont(name: "HelveticaNeue", size: 13.0)
        pageMenu!.menuHeight = 40.0
        
        // Add page menu as subview of base view controller view
        self.view.addSubview(pageMenu!.view)
        
        range = CHKeychain.doubleForKey("range")
        
        if (range < 1) {
            
            rangeView?.setLabelValue(NSString(format: "%.2lf", range) as String)
            
        } else {
            
            rangeView?.setLabelValue(NSString(format: "%i", Int(range)) as String)
            
        }
        
        if (range > 10) {
            range = 10;
        }
        
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse {
            CLLocationManager().requestWhenInUseAuthorization()
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        
        // We set pageMenu to nil because Swift is a POS 
        // and freaks out the pageMenu on reload
        pageMenu = nil
        
    }
    
    //MARK: - BubbleMenu Delegate
    
    func menuDidFinishConfiguring(menu: BubbleMenu) {

        pageMenu!.setPage(BubbleMenuPage.last, animated: false)
        
    }
    
    //MARK: - Login Delegate
    
    func userDidLogin(user: BSUser!) {
        
        user["location"] = self.appDelegate.currentLocation
        
        user.saveInBackgroundWithBlock(nil)
        
        self.appDelegate.currentUser = user;
        
        self.appDelegate.connect = BSConnect.sharedConnect()
        
        self.appDelegate.connect.setPeerID(MCPeerID(displayName: user.username!), service: "bubble-service")
        
        self.appDelegate.connect.delegate = self.appDelegate
        
        self.appDelegate.connect.startAdvertising()
        
        let install : PFInstallation = PFInstallation.currentInstallation()
        install.addUniqueObject(user.username!, forKey: "channels")
        
        do {
            try install.save()
        } catch  {
            print("Some error from saving the installation")
        }
        
    }
    
    //MARK: - Sign Up Notification
    
    func didFinishSignUpWithUser(notification: NSNotification) {
        
        let user : BSUser = notification.userInfo!["user"] as! BSUser
        
        user.location = self.appDelegate.currentLocation
        
        user.saveInBackgroundWithBlock(nil)
        
        self.appDelegate.connect = BSConnect.sharedConnect()
        
        self.appDelegate.connect.setPeerID(MCPeerID(displayName: user.username!), service: "bubble-service")
        
        self.appDelegate.connect.delegate = self.appDelegate
        
        self.appDelegate.connect.startAdvertising()
        
        let install : PFInstallation = PFInstallation.currentInstallation()
        install.addUniqueObject(user.username!, forKey: "channels")
        
        do {
            try install.save()
        } catch {
            print("Did not save installation for some reason.")
        }
        
    }
    
    //MARK: - Range Delegate
    
    func getCurrentRange() -> Double {
        
        return range
        
    }
    
    func changeRangeViewDismissed(newRange: Double) {
        
        range = newRange
        
        if (range > 10) {
            range = 10;
        }
        
        CHKeychain.setDouble(range, forKey: "range")
        
        if (range < 1) {
            
            rangeView?.setLabelValue(NSString(format: "%.2lf", range) as String)
            
        } else {
            
            rangeView?.setLabelValue(NSString(format: "%i", Int(range)) as String)
            
        }
        
    }
    
    //MARK: - Helpers
    
    func newNotification(notification: NSNotification) {
        self.navigationController?.tabBarItem.badgeValue = "1";
    }
    
    func removeNotificationBadge() {
        self.navigationController?.tabBarItem.badgeValue = nil;
    }
    
    func openPopularity() {
        
        let rangeViewController : PopularityViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ChangeRangeViewController") as! PopularityViewController
        
        rangeViewController.delegate = self;
        
        self.presentViewController(rangeViewController, animated:true, completion:nil)
        
    }
    
    func openPost() {
        
        let messageController : RRSendMessageViewController = RRSendMessageViewController()
        messageController.presentController(self, blockCompletion: { (model, isCancel, place, shouldSavePhoto) -> Void in
            
            if !isCancel {
                
                var data : NSData?
                
                if model.photos.count > 0 {
                    
                    data = UIImageJPEGRepresentation(model.photos.firstObject! as! UIImage, 0.4)
                    
                }
                
                CloudCode.postStatus(model.text, file:data, location:self.appDelegate.currentLocation, pointOfInterest:place, shouldSavePhotoToUser:shouldSavePhoto, completion: { (saved, error) -> Void in
                    
                    if shouldSavePhoto {
                        
                        let query = BSPhoto.query()
                        query!.whereKey("user", equalTo: self.appDelegate.currentUser);
                        query!.findObjectsInBackgroundWithBlock({ (photos, error) -> Void in
                            
                            if error == nil {
                                self.appDelegate.currentUser.photos.removeAllObjects()
                               
                                if photos != nil {
                                    let photosArray = photos as! [BSPhoto]
                                    for photo in photosArray {
                                        self.appDelegate.currentUser.photos.addObject(photo.objectId!)
                                        self.appDelegate.currentUser.saveEventually(nil)
                                    }
                                }
                            }
                            
                        })
                        
                    }
                    
                })
            }
            
            self.dismissViewControllerAnimated(true, completion:nil)
            
        })
        
    }
    
    func openComment(notification: NSNotification) {
        
        let dict: NSDictionary? = notification.userInfo
        
        //var comments : CommentsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CommentsViewController") as CommentsViewController
        let post = dict?.objectForKey("object") as! BSPost
        let comments = CommentsViewController()
        comments.post = post
        let nc = UINavigationController(rootViewController: comments);
        nc.navigationBar.barTintColor = CHColorHandler.getUIColorFromNSString(post.author.color)
        self.presentViewController(nc, animated: true, completion: nil)
        
    }
    
    func openCommentWithImageViewer(notification: NSNotification) {
        
        let dict: NSDictionary? = notification.userInfo
        
        let post : BSPost = dict?.objectForKey("object") as! BSPost
        
        let query = BSPhoto.query()
        query!.whereKey("post", equalTo: post.objectId!)
        query!.getFirstObjectInBackgroundWithBlock { (object, error) -> Void in
            
            if error == nil {
                
                let handler = BSPhotoViewHandler(photo: object as! BSPhoto)
                
                let controller : EBPhotoPagesController = EBPhotoPagesController(dataSource: handler, delegate: handler)
                
                self.presentViewController(controller, animated: true, completion: nil)
            }
        }
        
    }
    
    func openHashtag(notification: NSNotification) {
        
        let dict: NSDictionary? = notification.userInfo
        
        let hashtagController : HashtagViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HashtagViewController") as! HashtagViewController
        hashtagController.hashtag = dict?.objectForKey("hashtag") as! String
        
        let nc = UINavigationController(rootViewController: hashtagController)
        nc.navigationBar.barTintColor = UIColor.bubblePink()
        self.presentViewController(nc, animated: true, completion: nil)
        
    }
    
    func openUser(notificaiton: NSNotification) {
        
        let dict: NSDictionary? = notificaiton.userInfo
        
        let userController : UserViewController = self.storyboard?.instantiateViewControllerWithIdentifier("UserViewController") as! UserViewController
        userController.user = dict?.objectForKey("user") as! BSUser;
        
        let nav : UINavigationController = UINavigationController(rootViewController: userController)
        
        self.presentViewController(nav, animated: true, completion: nil)
    }
    
    /**
    Dismisses all the view controllers from the login/signup
    */
    func dismissAllViewControllers() {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func showPickUsername() {
        
        let picker : NewFacebookUserViewController = self.storyboard?.instantiateViewControllerWithIdentifier("NewFacebookUserViewController") as! NewFacebookUserViewController
        
        self.presentViewController(picker, animated: true, completion: nil)
        
    }
    
    func userIsLoggedOut() {
        
        let login : LoginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        
        login.delegate = self
        
        let nav : UINavigationController = UINavigationController(rootViewController: login)
        
        self.presentViewController(nav, animated: true, completion: nil)
        
    }
    
}

