//
//  MessagesViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 1/14/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@import JSQMessagesViewController;
#import "ChatViewController.h"
#import "ChatSearchViewController.h"
@import SWTableViewCell;
#import "UserViewController.h"

@interface MessagesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ChatSearchDelegate, SWTableViewCellDelegate, MONActivityIndicatorViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *blankImageView;
@property (strong, nonatomic) IBOutlet UILabel *blankMessage;

@end
