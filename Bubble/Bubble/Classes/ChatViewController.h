//
//  ChatViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 1/30/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

@import JSQMessagesViewController;

@interface ChatViewController : JSQMessagesViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) BSUser *otherUser;
@property (strong, nonatomic) BSMessageHead *parentNode;
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;
@property (strong, nonatomic) NSMutableDictionary *avatars;
@property (nonatomic) BOOL isNewParent;

@end
