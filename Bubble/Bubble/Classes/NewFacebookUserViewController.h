//
//  NewFacebookUserViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/10/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSUser.h"

@protocol NewFacebookUserDelegate <NSObject>

/**
 *  Notifies the delegate that a user has chosen a username.
 *
 *  @param user     The user.
 *  @param username The username that was chosen.
 */
- (void)user:(BSUser *)user didSuccessfullyMakeUsername:(NSString *)username;

@end

@interface NewFacebookUserViewController : UIViewController

@property (weak, nonatomic) id<NewFacebookUserDelegate> delegate;
@property (strong, nonatomic) BSUser *user;
@property (strong, nonatomic) IBOutlet CHLoginTextField *username;
@property (strong, nonatomic) IBOutlet UIButton *done;

- (IBAction)pickUsername:(id)sender;

@end
