//
//  BuddiesViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HashtagViewController.h"

@interface BuddiesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CHPaginatedTableViewDelegate>

@property (weak, nonatomic) IBOutlet CHPaginatedTableView *tableView;

@end
