//
//  MessagesViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 1/14/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessagesListTableViewCell.h"

@interface MessagesViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) MONActivityIndicatorView *indicatorView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.messages = [NSMutableArray new];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_plus_round size:20 color:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(addNewMessage)];
    
    [self setupRefreshControl];
    
    self.tableView.separatorColor = [UIColor clearColor];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    PFQuery *local = [BSMessageHead query];
    [local fromPinWithName:@"MessageHeads"];
    [local findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
       
        // We don't have anything cached, let's just look on the server.
        if (objects.count == 0) {
            [self findNewMessages];
        } else {
            
            if (!error) {
                
                NSMutableArray *msgs = [NSMutableArray new];
                
                for (BSMessageHead *obj in objects) {
                    
                    NSDateComponents *components = [[NSDateComponents alloc] init];
                    components.day = -1;
                    
                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDate *day = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
                    
                    if (obj.lastMessageDate < day) {
                        [obj unpinWithName:@"MessageHeads"];
                    } else {
                        [msgs addObject:obj];
                    }
                }
                
                // We have cached message heads and then we check for new messages.
                self.messages = [NSMutableArray arrayWithArray:msgs];
                [self findNewMessages];
            }
            
        }
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate / DataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellAnimation"]) {
        cell.alpha = 0;
        
        [UIView beginAnimations:@"fade" context:NULL];
        [UIView setAnimationDuration:0.8];
        cell.alpha = 1;
        [UIView commitAnimations];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessagesListTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"messagesListCell"];
    
    if (cell == nil) {
        cell = [[MessagesListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"messagesListCell"];
    }
    
    cell.delegate = self;
    cell.leftUtilityButtons = [self leftButtons];
    cell.rightUtilityButtons = [self rightButtons];
    
    BSMessageHead *obj = [self.messages objectAtIndex:indexPath.row];
    
    if ([obj.firstUserUsername isEqualToString:self.appDelegate.currentUser.username]) {
       
        cell.username.text = obj.secondUserUsername;
        [cell.image setImageWithString:cell.username.text];
        cell.image.file = obj.secondUser.profilePicture;
        [cell.image loadInBackground];
        
    } else {
        
        cell.username.text = obj.firstUserUsername;
        [cell.image setImageWithString:cell.username.text];
        cell.image.file = obj.firstUser.profilePicture;
        [cell.image loadInBackground];
    }
    
    cell.image.clipsToBounds = YES;
    cell.image.layer.cornerRadius = 30;
    cell.lastMessage.text = obj.lastMessage;
    
    return cell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messages.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessagesListTableViewCell *cell = (MessagesListTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    BSMessageHead *obj = [self.messages objectAtIndex:indexPath.row];
    
    if ([obj.firstUserUsername isEqualToString:cell.username.text]) {
        [self openMessageThreadWithUser:obj.firstUser andParentNode:[self.messages objectAtIndex:indexPath.row]];
    } else {
        [self openMessageThreadWithUser:obj.secondUser andParentNode:[self.messages objectAtIndex:indexPath.row]];
    }
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - ChatSearchDelegate

- (void)chosenUser:(BSUser *)user {
    
    if (![user.username isEqualToString:self.appDelegate.currentUser.username]) {
        
        [self.navigationController popViewControllerAnimated:NO];
        
        [self createMessageThreadWithUser:user];
        
    }
    
}

#pragma mark - SWTableViewCell Delegate

// Click event on left utility button
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    
    // Show user was pressed
    MessagesListTableViewCell *messageCell = (MessagesListTableViewCell *)cell;
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:messageCell];
    
    BSMessageHead *head = [self.messages objectAtIndex:indexPath.row];
    
    BSUser *selectedUser;
    
    if ([head.firstUserUsername isEqualToString:messageCell.username.text]) {
        selectedUser = head.firstUser;
    } else {
        selectedUser = head.secondUser;
    }
    
    if (![selectedUser.username isEqualToString:self.appDelegate.currentUser.username]) {
        
        UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
        user.user = selectedUser;
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
        
        [self presentViewController:nc animated:YES completion:nil];
        
    }
    
}

// Click event on right utility button
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    // Delete button was pressed
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    // Delete button was pressed
    BSMessageHead *head = [self.messages objectAtIndex:indexPath.row];
    
    PFQuery *messageQuery = [BSMessage query];
    [messageQuery fromPinWithName:head.objectId];
    messageQuery.limit = 1000;
    [messageQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
       
        if (objects.count > 0) {
            
            for (BSMessage *message in objects) {
                [message unpinInBackgroundWithName:head.objectId];
            }
            
        }
        
        PFQuery *headQuery = [BSMessageHead query];
        [headQuery fromPinWithName:@"MessageHeads"];
        [headQuery whereKey:@"objectId" equalTo:head.objectId];
        [headQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            [object unpinInBackgroundWithName:@"MessageHeads"];
            [head deleteInBackground];
        }];
        
    }];
    
    [self.messages removeObjectAtIndex:indexPath.row];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    
}

// Utility button open/close event
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state {
    
    
    
}

// Prevent multiple cells from showing utilty buttons simultaneously
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

#pragma mark - MONActivityIndicatorView

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView circleBackgroundColorAtIndex:(NSUInteger)index {
    return [UIColor whiteColor];
}

#pragma mark - UIScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // Get the current size of the refresh controller
    CGRect refreshBounds = self.refreshControl.bounds;
    
    // Distance the table has been pulled >= 0
    CGFloat pullDistance = MAX(0.0, -self.refreshControl.frame.origin.y);
    
    // Half the width of the table
    //CGFloat midX = self.tableView.frame.size.width / 2.0;
    
    // Calculate the width and height of our graphics
    CGFloat bubbleHeight = self.indicatorView.bounds.size.height;
    CGFloat bubbleHeightHalf = bubbleHeight / 2.0;
    
    //CGFloat bubbleWidth = indicatorView.bounds.size.width;
    //CGFloat bubbleWidthHalf = bubbleWidth / 2.0;
    
    // Calculate the pull ratio, between 0.0-1.0
    //CGFloat pullRatio = MIN( MAX(pullDistance, 0.0), 100.0) / 100.0;
    
    // Set the Y coord of the graphics, based on pull distance
    CGFloat bubbleY = pullDistance / 2.0 - bubbleHeightHalf;
    
    // Calculate the X coord of the graphics
    CGFloat bubbleX = self.view.frame.size.width / 2;
    
    // Set the graphic's frames
    CGRect bubbleFrame = self.indicatorView.frame;
    bubbleFrame.origin.x = bubbleX;
    bubbleFrame.origin.y = bubbleY;
    
    self.indicatorView.frame = bubbleFrame;
    
    CGFloat xpoint = self.indicatorView.frame.size.width / 2;
    
    self.indicatorView.frame = CGRectOffset(self.indicatorView.frame, -xpoint, 0);
    
    // Set the enbubbleing view's frames
    refreshBounds.size.height = pullDistance;
    
    [self.refreshControl addSubview:self.indicatorView];
    
    [self.indicatorView startAnimating];
    
}

#pragma mark - Helpers

/**
 *  Creates a new message thread with the given user.
 *
 *  @param user The user to create a message thread with.
 */
- (void)createMessageThreadWithUser:(BSUser *)user {
    
    BSMessageHead *parentNode = [BSMessageHead object];
    
    parentNode.firstUser = self.appDelegate.currentUser;
    parentNode.firstUserUsername = self.appDelegate.currentUser.username;
    parentNode.secondUser = user;
    parentNode.secondUserUsername = user.username;
    
    //[parentNode saveInBackground];
    
    ChatViewController *chat = [ChatViewController messagesViewController];
    chat.otherUser = user;
    chat.parentNode = parentNode;
    chat.isNewParent = YES;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:chat];
    nc.navigationBar.barTintColor = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
    nc.navigationBar.tintColor = [UIColor whiteColor];
    nc.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    
    [self presentViewController:nc animated:YES completion:nil];
    
}

- (void)openMessageThreadWithUser:(BSUser *)user andParentNode:(BSMessageHead *)parentNode {
    
    ChatViewController *chat = [ChatViewController messagesViewController];
    chat.otherUser = user;
    chat.parentNode = parentNode;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:chat];
    nc.navigationBar.barTintColor = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
    nc.navigationBar.tintColor = [UIColor whiteColor];
    nc.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    
    [self presentViewController:nc animated:YES completion:nil];
    
}

/**
 *  This is called when a user taps the add button.
 */
- (void)addNewMessage {
    
    ChatSearchViewController *chatSearch = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatSearchViewController"];
    chatSearch.delegate = self;
    
    [self.navigationController pushViewController:chatSearch animated:YES];

}

- (void)findNewMessages {
    
    PFQuery *query = [BSMessageHead query];
    [query whereKey:@"firstUserUsername" equalTo:self.appDelegate.currentUser.username];
    
    PFQuery *query2 = [BSMessageHead query];
    [query2 whereKey:@"secondUserUsername" equalTo:self.appDelegate.currentUser.username];
    
    PFQuery *full = [PFQuery orQueryWithSubqueries:@[query, query2]];
    [full addDescendingOrder:@"lastMessageDate"];
    [full includeKey:@"firstUser"];
    [full includeKey:@"secondUser"];
    [full findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error) {
            
            self.messages = [NSMutableArray arrayWithArray:objects];
            
            for (BSMessageHead *messageHead in objects) {
                [messageHead pinInBackgroundWithName:@"MessageHeads"];
            }
            
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
            
            if (objects.count > 0) {
                self.tableView.separatorColor = [UIColor lightGrayColor];
                self.blankImageView.hidden = YES;
                self.blankMessage.hidden = YES;
            } else {
                self.tableView.separatorColor = [UIColor clearColor];
                self.blankImageView.hidden = NO;
                self.blankMessage.hidden = NO;

            }
            
        }
        
    }];
    
}

- (NSArray *)rightButtons {
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    //[rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0] title:@"More"];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] title:@"Delete"];
    
    return rightUtilityButtons;
}

- (NSArray *)leftButtons {
    
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0.07f green:0.75f blue:0.16f alpha:1.0]
                                                icon:[IonIcons imageWithIcon:ion_ios_person size:30 color:[UIColor whiteColor]]];
    
    return leftUtilityButtons;
}

- (void)setupRefreshControl {
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(findNewMessages) forControlEvents:UIControlEventValueChanged];
    [self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@""]];
    [self.refreshControl setBackgroundColor:[UIColor bubblePink]];
    [self.refreshControl setTintColor:[UIColor clearColor]];
    
    self.indicatorView = [[MONActivityIndicatorView alloc] init];
    self.indicatorView.delegate = self;
    self.indicatorView.numberOfCircles = 5;
    self.indicatorView.radius = 10;
    self.indicatorView.internalSpacing = 5;
    self.indicatorView.duration = 0.8;
    self.indicatorView.delay = 0.2;
    self.indicatorView.center = self.refreshControl.center;
    self.indicatorView.clipsToBounds = YES;
    
    [self.tableView addSubview:self.refreshControl];
    
}

@end
