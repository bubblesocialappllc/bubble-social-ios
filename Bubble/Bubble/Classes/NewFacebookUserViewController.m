//
//  NewFacebookUserViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/10/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "NewFacebookUserViewController.h"

@interface NewFacebookUserViewController ()

@end

@implementation NewFacebookUserViewController

- (void)viewDidLayoutSubviews {
    
    CGRect textFieldFrame = self.username.frame;
    textFieldFrame.size.height = 45;
    
    self.username.frame = textFieldFrame;
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.username.tintColor = [UIColor bubblePink];
    self.username.layer.borderColor = [UIColor clearColor].CGColor;
    self.username.layer.borderWidth = 3;
    self.username.layer.cornerRadius = 5;
    
    [self.username setType:CHLoginTextFieldTypeDefault withIcon:[IonIcons imageWithIcon:ion_person size:28 color:[UIColor bubblePink]]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pickUsername:(id)sender {
    
    [self.username resignFirstResponder];
    
    [KVNProgress show];
    
    // Make sure that the username is lowercase and doesn't have an trailing spaces
    self.username.text = [[self.username.text lowercaseString] removeWhiteSpacesFromString];
    
    if (self.username.text.length <= 15 && self.username.text.length >= 4) {
        
            [BSUser currentUser].username = self.username.text;
        
            [[BSUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                if (!error) {
                    
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                        [KVNProgress showSuccessWithStatus:[NSString stringWithFormat:@"Welcome %@", [PFUser currentUser].username]];
                        
                        [[BSLocationManager sharedManager] updateLocation];
                        
                        [self.delegate user:[BSUser currentUser] didSuccessfullyMakeUsername:self.username.text];
                        
                        if ([CHDevice deviceVersion] >= 8.0) {
                            
                            UIApplication *application = [UIApplication sharedApplication];
                            
                            UIMutableUserNotificationAction *accept = [UIMutableUserNotificationAction new];
                            accept.identifier = ACCEPT_BUDDY;
                            accept.title = @"Accept";
                            accept.activationMode = UIUserNotificationActivationModeBackground;
                            accept.destructive = NO;
                            accept.authenticationRequired = NO;
                            
                            UIMutableUserNotificationAction *deny = [UIMutableUserNotificationAction new];
                            deny.identifier = DENY_BUDDY;
                            deny.title = @"Deny";
                            deny.activationMode = UIUserNotificationActivationModeBackground;
                            deny.destructive = YES;
                            deny.authenticationRequired = NO;
                            
                            UIMutableUserNotificationCategory *category = [UIMutableUserNotificationCategory new];
                            category.identifier = PENDING_BUDDIES;
                            
                            NSArray *actions = @[accept, deny];
                            
                            [category setActions:actions forContext:UIUserNotificationActionContextMinimal];
                            
                            NSSet *categoriesSet = [[NSSet alloc] initWithObjects:category, nil];
                            
                            [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:categoriesSet]];
                            
                            [application registerForRemoteNotifications];
                        }
                        
                    }];
                    
                } else {
                    
                    NSString *errorString = [error userInfo][@"error"];
                    [KVNProgress showErrorWithStatus:errorString];
                    [self.username becomeFirstResponder];
                }
                
            }];
            
    } else {
            
            [KVNProgress showErrorWithStatus:@"The username you chose is either too long or too short"];
            [self.username becomeFirstResponder];
    }
    
}

@end
