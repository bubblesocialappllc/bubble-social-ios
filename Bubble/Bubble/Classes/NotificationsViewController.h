//
//  NotificationsViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationsTableViewCell.h"
#import "NotificationsPageBlankCellTableViewCell.h"

@interface NotificationsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UIImageView *blankImage;
@property (strong, nonatomic) IBOutlet UILabel *blankLabel;

@end
