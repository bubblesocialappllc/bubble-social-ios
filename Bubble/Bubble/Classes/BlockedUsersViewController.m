//
//  BlockedUsersViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/24/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BlockedUsersViewController.h"

@interface BlockedUsersViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *blocked;

@end

@implementation BlockedUsersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    PFQuery *query = [BSUser query];
    [query whereKey:@"objectId" containedIn:self.appDelegate.currentUser.blocked];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        self.blocked = [NSMutableArray arrayWithArray:objects];
        [self.tableView reloadData];
        
    }];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BlockedUsersTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"blockedUserCell"];
    
    if (cell == nil) {
        cell = [[BlockedUsersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"blockedUserCell"];
    }
    
    BSUser *user = [self.blocked objectAtIndex:indexPath.row];
    
    cell.username.text = user.username;
    
    cell.profilePicture.clipsToBounds = YES;
    cell.profilePicture.layer.cornerRadius = cell.profilePicture.frame.size.height / 2;
    [cell.profilePicture setImageWithString:user.realName];
    cell.profilePicture.file = user.profilePicture;
    [cell.profilePicture loadInBackground];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(unblockUser:)];
    
    cell.accessoryImageView.image = [IonIcons imageWithIcon:ion_close_round size:20 color:[UIColor flatAlizarinColor]];
    [cell.accessoryImageView addGestureRecognizer:tap];
    
    return cell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.blocked.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self openUserPageWithSelectedUser:[self.blocked objectAtIndex:indexPath.row]];
}

- (void)openUserPageWithSelectedUser:(BSUser *)selectedUser {
    
    if (![selectedUser.username isEqualToString:self.appDelegate.currentUser.username]) {
        
        UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
        user.user = selectedUser;
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
        
        [self presentViewController:nc animated:YES completion:nil];
        
    }
    
}

- (void)unblockUser:(UITapGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    BSUser *user = [self.blocked objectAtIndex:indexPath.row];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Are you sure you want to unblock %@", user.username] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *unblock = [UIAlertAction actionWithTitle:@"Unblock" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
       
        NSMutableArray *blocked = [NSMutableArray arrayWithArray:self.appDelegate.currentUser.blocked];
        
        [blocked removeObject:user.objectId];
        
        self.appDelegate.currentUser.blocked = blocked;
        [self.appDelegate.currentUser saveEventually];
        
        [self.blocked removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }];
    
    [alert addActions:@[unblock, cancel]];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
