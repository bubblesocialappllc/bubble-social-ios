//
//  PhotosCollectionViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/8/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@import RFQuiltLayout;
@import EBPhotoPages_BubbleSocial;
#import "BSPhotoViewHandler.h"
#import "BSPhotoCollectionViewCell.h"

@interface PhotosCollectionViewController : UICollectionViewController <RFQuiltLayoutDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CameraDelegate>

@property (strong, nonatomic) BSUser *user;

@end
