//
//  NotificationsViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "NotificationsViewController.h"

@interface NotificationsViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *data;

@end

@implementation NotificationsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.data = [NSMutableArray new];
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.table.separatorColor = [UIColor clearColor];
    [self.table setAllowsSelection:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(feedChanged:) name:@"ChangedFeed" object:nil];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, 125)];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubbleLogo"]];
    logo.contentMode = UIViewContentModeScaleAspectFill;
    
    //Set the size of the frame so that
    CGRect frame = CGRectMake([UIScreen mainScreen].applicationFrame.size.width / 2, 0, 120, 30);
    logo.frame = frame;
    logo.frame = CGRectOffset(logo.frame, -logo.frame.size.width/2, 30);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, logo.frame.size.height + 35, [[UIScreen mainScreen] applicationFrame].size.width, 20)];
    label.text = @"Bubble Social | Created with love in Orlando, FL";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0];
    
    [footer addSubview:logo];
    [footer addSubview:label];
    
    self.table.tableFooterView = footer;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource/UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellAnimation"]) {
        cell.alpha = 0;
        
        [UIView beginAnimations:@"fade" context:NULL];
        [UIView setAnimationDuration:0.8];
        cell.alpha = 1;
        [UIView commitAnimations];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    if (self.data.count > 0) {
        return self.data.count; //Return the number of notification cells there will be to allocate
    } else {
        return 1; //Return space for the blank cell
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row <= self.data.count && self.data.count != 0) {
        
        NotificationsTableViewCell *cell = [self.table dequeueReusableCellWithIdentifier:@"notificationCell"];
      
        if (cell == nil) {
            cell = [[NotificationsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"notificationCell"];
        }
        
        BSNotification *notification = [self.data objectAtIndex:indexPath.row];
        
        if ([notification.type isEqualToString:[BSNotificationType BSCommentNotification]]) {
            
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@""];
            
            NSAttributedString *sentFrom = [[NSAttributedString alloc] initWithString:notification.sentFrom attributes:@{ NSForegroundColorAttributeName : [UIColor bubblePink], NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Bold" size:15] }];
            [string appendAttributedString:sentFrom];
            
            NSAttributedString *notificationText = [[NSAttributedString alloc] initWithString:@" commented on your Pop. " attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15] }];
            [string appendAttributedString:notificationText];
            
            NSAttributedString *timeSent = [[NSAttributedString alloc] initWithString:[CHTime makeTimePassedFromDate:notification.createdAt] attributes:@{ NSForegroundColorAttributeName : [UIColor lightGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15] }];
            [string appendAttributedString:timeSent];
            
            cell.text.attributedText = string;
            
            cell.profilePicture.image = [UIImage imageNamed:@"comment"];
            cell.profilePicture.contentMode = UIViewContentModeScaleAspectFit;
            
        } else if ([notification.type isEqualToString:[BSNotificationType BSLikeNotification]]) {
            
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@""];
            
            NSAttributedString *sentFrom = [[NSAttributedString alloc] initWithString:notification.sentFrom attributes:@{ NSForegroundColorAttributeName : [UIColor bubblePink], NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Bold" size:15] }];
            [string appendAttributedString:sentFrom];
            
            NSAttributedString *notificationText = [[NSAttributedString alloc] initWithString:@" liked your Pop. " attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15] }];
            [string appendAttributedString:notificationText];
            
            NSAttributedString *timeSent = [[NSAttributedString alloc] initWithString:[CHTime makeTimePassedFromDate:notification.createdAt] attributes:@{ NSForegroundColorAttributeName : [UIColor lightGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15] }];
            [string appendAttributedString:timeSent];
            
            cell.text.attributedText = string;

            cell.profilePicture.image = [UIImage imageNamed:@"heartPink"];
            cell.profilePicture.contentMode = UIViewContentModeScaleAspectFit;
            
        } else if ([notification.type isEqualToString:[BSNotificationType BSBuddyRequestNotification]]) {
            
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@""];
            
            NSAttributedString *sentFrom = [[NSAttributedString alloc] initWithString:notification.sentFrom attributes:@{ NSForegroundColorAttributeName : [UIColor bubblePink], NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Bold" size:15] }];
            [string appendAttributedString:sentFrom];
            
            NSAttributedString *notificationText = [[NSAttributedString alloc] initWithString:@" added you as a Buddy. " attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15] }];
            [string appendAttributedString:notificationText];
            
            NSAttributedString *timeSent = [[NSAttributedString alloc] initWithString:[CHTime makeTimePassedFromDate:notification.createdAt] attributes:@{ NSForegroundColorAttributeName : [UIColor lightGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15] }];
            [string appendAttributedString:timeSent];
            
            cell.text.attributedText = string;
            
            cell.profilePicture.image = [UIImage imageNamed:@"addBuddy"];
            cell.profilePicture.contentMode = UIViewContentModeScaleAspectFit;
            
        } else if ([notification.type isEqualToString:[BSNotificationType BSResponseNotification]]) {
            
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@""];
            
            NSAttributedString *sentFrom = [[NSAttributedString alloc] initWithString:notification.sentFrom attributes:@{ NSForegroundColorAttributeName : [UIColor bubblePink], NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Bold" size:15] }];
            [string appendAttributedString:sentFrom];
            
            NSAttributedString *notificationTextFirst = [[NSAttributedString alloc] initWithString:@" also commented on " attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15] }];
            [string appendAttributedString:notificationTextFirst];
            
            NSAttributedString *notificationTextSecond = [[NSAttributedString alloc] initWithString:notification.post.authorUsername attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15] }];
            [string appendAttributedString:notificationTextSecond];
            
            NSAttributedString *timeSent = [[NSAttributedString alloc] initWithString:[CHTime makeTimePassedFromDate:notification.createdAt] attributes:@{ NSForegroundColorAttributeName : [UIColor lightGrayColor], NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:15] }];
            [string appendAttributedString:timeSent];
            
            cell.text.attributedText = string;
            
            cell.profilePicture.image = [UIImage imageNamed:@"response"];
            cell.profilePicture.contentMode = UIViewContentModeScaleAspectFit;
        }
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapCell:)];
        
        [cell addGestureRecognizer:tap];
        
        [cell setUserInteractionEnabled:YES];
        
        return cell;
    } else if (self.data.count == 0) {
        NotificationsPageBlankCellTableViewCell *blankCell = [self.table dequeueReusableCellWithIdentifier:@"NotificationsBlankCell"];
        return blankCell;
    }
    
    // Just make sure that we always return something.
    NotificationsPageBlankCellTableViewCell *blankCell = [self.table dequeueReusableCellWithIdentifier:@"NotificationsBlankCell"];
    return blankCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.data.count == 0) {
        return 250;
    } else {
        return 50;
    }
    
    return 50;
}

/*- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BSNotification *notification = [self.data objectAtIndex:indexPath.row];
    
    if ([notification.type isEqualToString:[BSNotificationType BSLikeNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"bubblesocial://post/%@", notification.post.objectId]]];
        
    } else if ([notification.type isEqualToString:[BSNotificationType BSCommentNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"bubblesocial://post/%@", notification.post.objectId]]];
        
    } else if ([notification.type isEqualToString:[BSNotificationType BSMentionNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"bubblesocial://post/%@", notification.post.objectId]]];
        
    } else if ([notification.type isEqualToString:[BSNotificationType BSResponseNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"bubblesocial://post/%@", notification.post.objectId]]];
        
    } else if ([notification.type isEqualToString:[BSNotificationType BSBuddyRequestNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"bubblesocial://buddyrequest"]];
    }
    
}*/

#pragma mark - Helpers

- (void)didTapCell:(UITapGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.table];
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint:tapPoint];
    
    BSNotification *notification = [self.data objectAtIndex:indexPath.row];
    
    if ([notification.type isEqualToString:[BSNotificationType BSLikeNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"bubblesocial://post/%@", notification.post.objectId]]];
        
    } else if ([notification.type isEqualToString:[BSNotificationType BSCommentNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"bubblesocial://post/%@", notification.post.objectId]]];
        
    } else if ([notification.type isEqualToString:[BSNotificationType BSMentionNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"bubblesocial://post/%@", notification.post.objectId]]];
        
    } else if ([notification.type isEqualToString:[BSNotificationType BSResponseNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"bubblesocial://post/%@", notification.post.objectId]]];
        
    } else if ([notification.type isEqualToString:[BSNotificationType BSBuddyRequestNotification]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"bubblesocial://view/buddyrequest"]];
    }
    
}

- (void)refresh {
    
    PFQuery *query = [BSNotification query];
    
    [query includeKey:@"post.author"]; //Include the post and the author.
    [query whereKey:@"sendTo" equalTo:self.appDelegate.currentUser.username]; //Finds the queries for the current user/
    
    NSDateComponents *oneDay = [NSDateComponents new];
    oneDay.day = -1;
    NSDate *today = [NSDate date];
    NSDate *newDate = [[NSCalendar currentCalendar]dateByAddingComponents:oneDay
                                                                   toDate: today
                                                                    options:0];
    [query whereKey:@"createdAt" greaterThanOrEqualTo:newDate]; //Query for notifications that are greater than 1 day ago.
    [query orderByDescending:@"createdAt"];
    
    //Queries the database in a block
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        self.data = [NSMutableArray arrayWithArray:objects];
        
        if (self.data.count > 0) {
            self.blankImage.hidden = YES;
            self.blankLabel.hidden = YES;
            self.table.separatorColor = [UIColor lightGrayColor];
            [self.table reloadData];
        }
    }];
    
}

- (void)feedChanged:(NSNotification *)notification {
    
    int page = 0;
    
    if ([notification.userInfo[@"page"] intValue] == page) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveNotificationBadge" object:nil];
        
        [self refresh];
        
    }
    
}

@end
