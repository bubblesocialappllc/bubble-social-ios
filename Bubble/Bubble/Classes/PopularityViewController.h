//
//  ChangeRangeViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankLoader.h"

@protocol RangeDelegate <NSObject>

/**
 *  Askes the delegate for the current set range to update the UI.
 *
 *  @return The current set range.
 */
- (double)getCurrentRange;

/**
 *  Notifies the delegate that the view has been dismissed with the set range. You should use this to update anything that may need the range.
 *
 *  @param rangeHere The range that was selected.
 */
- (void)changeRangeViewDismissed:(double)newRange;

@optional

/**
 *  Notifies the delegate that the view has been dismissed by a cancel.
 */
- (void)changeRangeViewDismissedWithCancel;

@end

@interface PopularityViewController : UIViewController

@property (weak, nonatomic) id<RangeDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *close;
@property (strong, nonatomic) IBOutlet UILabel *rank;
@property (strong, nonatomic) IBOutlet UIView *rankBorder;
@property (strong, nonatomic) IBOutlet UILabel *rankDescription;

- (IBAction)close:(id)sender;

@end
