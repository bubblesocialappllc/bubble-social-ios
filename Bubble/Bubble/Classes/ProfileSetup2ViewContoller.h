//
//  ProfileSetup2ViewContoller.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/16/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorPickerViewController.h"

@interface ProfileSetup2ViewContoller : UIViewController <UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UITextView *aboutMe;
@property (strong, nonatomic) IBOutlet UITextView *interests;

- (IBAction)continueSetup:(id)sender;

@end
