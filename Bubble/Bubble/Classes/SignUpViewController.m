//
//  SignUpViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/21/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController {
    
    CGRect viewOrigFrame;
    
}

- (void)viewDidLayoutSubviews {
    
    CGRect newFrame = self.username.frame;
    newFrame.size.height = 45;
    
    self.username.frame = newFrame;
    
    newFrame = self.password.frame;
    newFrame.size.height = 45;
    
    self.password.frame = newFrame;
    
    newFrame = self.name.frame;
    newFrame.size.height = 45;
    
    self.name.frame = newFrame;
    
    newFrame = self.email.frame;
    newFrame.size.height = 45;
    
    self.email.frame = newFrame;
    
    newFrame = self.birthday.frame;
    newFrame.size.height = 45;
    
    self.birthday.frame = newFrame;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    self.username.tintColor = [UIColor bubblePink];
    self.username.layer.borderColor = [UIColor clearColor].CGColor;
    self.username.layer.borderWidth = 3;
    self.username.layer.cornerRadius = 5;
    
    self.password.tintColor = [UIColor bubblePink];
    self.password.layer.borderColor = [UIColor clearColor].CGColor;
    self.password.layer.borderWidth = 3;
    self.password.layer.cornerRadius = 5;
    
    self.password.tintColor = [UIColor bubblePink];
    self.password.layer.borderColor = [UIColor clearColor].CGColor;
    self.password.layer.borderWidth = 3;
    self.password.layer.cornerRadius = 5;

    self.password.tintColor = [UIColor bubblePink];
    self.password.layer.borderColor = [UIColor clearColor].CGColor;
    self.password.layer.borderWidth = 3;
    self.password.layer.cornerRadius = 5;

    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [self.username setType:CHLoginTextFieldTypeDefault withIcon:[IonIcons imageWithIcon:ion_person size:28 color:[UIColor bubblePink]]];
    [self.password setType:CHLoginTextFieldTypeDefault withIcon:[IonIcons imageWithIcon:ion_ios_locked size:28 color:[UIColor bubblePink]]];
    [self.name setType:CHLoginTextFieldTypeDefault withIcon:[IonIcons imageWithIcon:ion_edit size:28 color:[UIColor bubblePink]]];
    [self.birthday setType:CHLoginTextFieldTypeDefault withIcon:[IonIcons imageWithIcon:ion_ios_calendar size:28 color:[UIColor bubblePink]]];
    [self.email setType:CHLoginTextFieldTypeDefault withIcon:[IonIcons imageWithIcon:ion_paper_airplane size:28 color:[UIColor bubblePink]]];
    
    [IonIcons label:self.close.titleLabel setIcon:ion_close_round size:20 color:[UIColor whiteColor] sizeToFit:YES];
    [self.close setImage:[IonIcons imageWithIcon:ion_close_round size:20 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
    [center addObserver:self selector:@selector(keyboardLeftScreen:) name:UIKeyboardDidHideNotification object:nil];
    
    viewOrigFrame = self.view.frame;
    
    //self.close.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField.placeholder isEqualToString:@"username"]) {
        
        [self.password becomeFirstResponder];
        
    } else if ([textField.placeholder isEqualToString:@"password"]) {
        
        [self.name becomeFirstResponder];
        
    } else if ([textField.placeholder isEqualToString:@"first and last name"]) {
        
        [self.email becomeFirstResponder];
        
     }else if ([textField.placeholder isEqualToString:@"email"]) {
        
        [self.birthday becomeFirstResponder];
        
    } else if ([textField.placeholder isEqualToString:@"birthday MM/DD/YYYY"]) {
        
        [self.birthday resignFirstResponder];
        
    }
    
    return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger length = textField.text.length + string.length;
    
    if (string.length == 0)
        length = textField.text.length - 1;
    
    if ([textField.placeholder isEqualToString:@"username"]) {
        
            // Prevent crashing undo bug
        if (range.length + range.location > textField.text.length) {
            return NO;
        }
        
        NSUInteger newLength = textField.text.length + string.length - range.length;
        return (newLength > 15) ? NO : YES;
        
    }
    
    if (![textField.placeholder isEqualToString:@"first and last name"]) {
        
        if ([textField.text containsString:@" "]) {
            
            textField.text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            
        }
        
    }
    
    if ([textField.placeholder isEqualToString:@"birthday MM/DD/YYYY"]) {
        
        if (length == 3) {
            
            textField.text = [textField.text stringByAppendingString:@"/"];
            
        } else if (length == 6) {
            
            textField.text = [textField.text stringByAppendingString:@"/"];
            
        }
        
    }
    
    return YES;
    
}

#pragma mark - IBActions

- (IBAction)close:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)signup:(id)sender {
    
    [self hideKeyboard];
    
    [KVNProgress show];
    
    if (self.username.text != nil && self.password.text != nil && self.email.text != nil && self.name.text != nil && self.birthday.text != nil) {
        
        if (![self.username.text isBlank] && ![self.password.text isBlank] && ![self.email.text isBlank] && ![self.name.text isBlank] && ![self.birthday.text isBlank]) {
            
            NSString *last = @"";
            NSScanner *scan = [NSScanner scannerWithString:self.name.text];
            [scan scanUpToString:@" " intoString:nil];
            [scan scanUpToString:@"" intoString:&last];
            last = [last removeWhiteSpacesFromString];
            
            if (last.length > 0) {
                
                NSInteger index = self.gender.selectedSegmentIndex;
                
                if (index >= 0 && index <= 1) {
                    
                    if (self.username.text.length > 3) {
                        
                        // Convert string to date object
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                        [dateFormat setDateFormat:@"MM/dd/yyyy"];
                        NSDate *birthdayDate = [dateFormat dateFromString:self.birthday.text];
                        
                        BSUser *user = [BSUser object];
                        
                        //Remove whitespace on signup
                        self.username.text = [[self.username.text lowercaseString] removeWhiteSpacesFromString];
                        self.username.text = [self.username.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        user.username = self.username.text;
                        user.password = self.password.text;
                        user.email = self.email.text;
                        user.birthday = birthdayDate;
                        user.realName = [self.name.text removeWhiteSpacesFromString];
                        user.staff = NO;
                        user.beta = NO;
                        user.points = [NSNumber numberWithInt:10];
                        user.popPoints = [NSNumber numberWithInt:0];
                        user.color = @"bubblePink";
                        user.locationHidden = NO;
                        user.phone = self.phone;
                        user.buddies = [NSMutableArray new];;
                        user.pendingBuddies = [NSMutableArray new];;
                        user.blocked = [NSMutableArray new];;
                        user.tags = [NSMutableArray new];
                        user.posts = [NSNumber numberWithInt:0];
                        
                        switch (self.gender.selectedSegmentIndex) {
                            case 0:
                                user.gender = @"male";
                                break;
                            case 1:
                                user.gender = @"female";
                                break;
                            default:
                                break;
                        }
                        
                        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            
                            if (!error) {
                                
                                //[KVNProgress showSuccessWithStatus:@"Welcome to Bubble!"];
                                [CHKeychain setObject:self.password.text forKey:@"pwd"];
                                [CHKeychain setObject:@YES forKey:@"shouldShowTutorial"];
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidSignUp" object:nil userInfo:@{@"user" : user}];
                                
                                /*if ([self.delegate respondsToSelector:@selector(didFinishSignUpWithUser:)]) {
                                    
                                    [self.delegate didFinishSignUpWithUser:user];
                                }*/
                                
                                //TODO: Push a new view to set pictures, color, whatever.
                                [KVNProgress dismiss];
                                
                                ProfileSetupViewContoller *setup = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSetupViewContoller"];
                                
                                [self.navigationController pushViewController:setup animated:YES];
                                
                            } else {
                                
                                NSString *errorString = [error userInfo][@"error"];
                                [KVNProgress showErrorWithStatus:errorString];
                                
                            }
                        }];
                        
                    } else {
                        
                        [KVNProgress showErrorWithStatus:@"Username must be more than 3 characters"];
                        
                    }
                    
                } else {
                    
                    [KVNProgress showErrorWithStatus:@"You must have a gender"];
                    
                }
                
            } else {
                
                [KVNProgress showErrorWithStatus:@"First & Last name please."];
                
            }
            
        } else {
            
            [KVNProgress showErrorWithStatus:@"Fields can't be empty"];
            
        }
        
    } else {
        
        [KVNProgress showErrorWithStatus:@"Looks like a field is empty"];
        
    }
    
}

#pragma mark - Helpers

- (void)hideKeyboard {
    
    [self.username resignFirstResponder];
    [self.password resignFirstResponder];
    [self.name resignFirstResponder];
    [self.email resignFirstResponder];
    [self.birthday resignFirstResponder];
}

- (void)keyboardOnScreen:(NSNotification *)notification {
    /*
    NSDictionary *info  = notification.userInfo;
    NSValue *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    */
    
    [UIView animateWithDuration:0.4 animations:^{
        
        CGRect newFrame = self.view.frame;
        newFrame.origin.y = -self.username.frame.origin.y + 25;
    
        self.view.frame = newFrame;
        
    }];
    
}

- (void)keyboardLeftScreen:(NSNotification *)notification {
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.view.frame = viewOrigFrame;
        
    }];
    
}

@end
