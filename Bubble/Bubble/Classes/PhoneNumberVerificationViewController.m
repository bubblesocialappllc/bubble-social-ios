//
//  PhoneNumberVerificationViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/30/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "PhoneNumberVerificationViewController.h"

@interface PhoneNumberVerificationViewController ()

@end

@implementation PhoneNumberVerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [IonIcons label:self.close.titleLabel setIcon:ion_close_round size:25 color:[UIColor bubblePink] sizeToFit:YES];
    [self.close setImage:[IonIcons imageWithIcon:ion_close_round size:25 color:[UIColor bubblePink]] forState:UIControlStateNormal];
    
    [IonIcons label:self.next.titleLabel setIcon:ion_chevron_right size:40 color:[UIColor flatAsbestosColor] sizeToFit:YES];
    [self.next setImage:[IonIcons imageWithIcon:ion_chevron_right size:40 color:[UIColor flatAsbestosColor]] forState:UIControlStateNormal];
    
    self.number.tintColor = [UIColor whiteColor];
    
    if (self.isSigningUp) {
        self.close.hidden = NO;
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.number becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)sendVerification:(id)sender {

    if (self.number.text.length != 10) {
        [KVNProgress showErrorWithStatus:@"That number doesn't look right. Ex: 4075550123"];
    } else {
        
        [CloudCode verifyPhoneNumber:self.number.text];
        
        VerifyPhoneNumberViewController *verify = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyPhoneNumberViewController"];
        
        verify.phoneNumber = self.number.text;
        verify.isSigningUp = self.isSigningUp;
        [self.navigationController pushViewController:verify animated:YES];
        
    }
    
}

- (IBAction)close:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - UITextView Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@"Phone Number"]) {
        textView.text = @"";
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    /*NSInteger length = textView.text.length + text.length;
    
    if (text.length == 0)
        length = textView.text.length - 1;*/
    
    return YES;
    
}

@end
