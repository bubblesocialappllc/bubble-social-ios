//
//  HashtagViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/3/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HashtagViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CHPaginatedTableViewDelegate>


@property (strong, nonatomic) IBOutlet CHPaginatedTableView *tableView;
@property (strong, nonatomic) NSString *hashtag;
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
@property (strong, nonatomic) IBOutlet UINavigationItem *navTitle;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *done;

- (IBAction)done:(id)sender;

@end
