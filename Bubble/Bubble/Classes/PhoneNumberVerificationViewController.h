//
//  PhoneNumberVerificationViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/30/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerifyPhoneNumberViewController.h"

@interface PhoneNumberVerificationViewController : UIViewController <UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *close;
@property (strong, nonatomic) IBOutlet UITextView *number;
@property (strong, nonatomic) IBOutlet UIButton *next;
@property BOOL isSigningUp;

- (IBAction)sendVerification:(id)sender;
- (IBAction)close:(id)sender;

@end
