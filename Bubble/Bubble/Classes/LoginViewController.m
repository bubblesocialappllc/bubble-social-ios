//
//  LoginViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/14/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLayoutSubviews {
    
    CGRect textFieldFrame = self.usernameField.frame;
    textFieldFrame.size.height = 45;
    
    self.usernameField.frame = textFieldFrame;
    
    textFieldFrame = self.passwordField.frame;
    textFieldFrame.size.height = 45;
    
    self.passwordField.frame = textFieldFrame;
    
    CGRect forgotPasswordFrame = CGRectMake(([[UIScreen mainScreen] applicationFrame].size.width / 2) - 70, [[UIScreen mainScreen] applicationFrame].size.height - 15, 140, 30);
    self.forgotPassword.frame = forgotPasswordFrame;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.facebookLogin.hidden = YES;
    
    [self.navigationController setNavigationBarHidden:YES];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    self.usernameField.delegate = self;
    self.passwordField.delegate = self;
    
    self.usernameField.tintColor = [UIColor bubblePink];
    self.usernameField.layer.borderColor = [UIColor clearColor].CGColor;
    self.usernameField.layer.borderWidth = 3;
    self.usernameField.layer.cornerRadius = 5;
    
    self.passwordField.tintColor = [UIColor bubblePink];
    self.passwordField.layer.borderColor = [UIColor clearColor].CGColor;
    self.passwordField.layer.borderWidth = 3;
    self.passwordField.layer.cornerRadius = 5;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    [self.view addGestureRecognizer:tap];

    UIImageView *bubbleIcon = [[UIImageView alloc] init];
    UIImage *bubbleIconImage = [UIImage imageNamed:@"bubbleIconWhite"];
    
    bubbleIcon.frame = CGRectMake(35, self.signUp.frame.size.height/2 - 8, 16, 16);
    
    bubbleIcon.image = bubbleIconImage;
    
    [self.signUp addSubview:bubbleIcon];
    
    [self.facebookLogin setImage:[IonIcons imageWithIcon:ion_social_facebook size:16 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    
    [self.usernameField setType:CHLoginTextFieldTypeDefault withIcon:[IonIcons imageWithIcon:ion_person size:28 color:[UIColor bubblePink]]];
    [self.passwordField setType:CHLoginTextFieldTypeDefault withIcon:[IonIcons imageWithIcon:ion_ios_locked size:28 color:[UIColor bubblePink]]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishProfileSetup) name:@"UserSetupProfile" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)user:(BSUser *)user didSuccessfullyMakeUsername:(NSString *)username {
    
    [self.delegate userDidLogin:user];
    
}

#pragma mark - SignUpDelegate

- (void)didFinishSignUpWithUser:(BSUser *)user {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    user.notifications = @{
                           @"like" : [NSNumber numberWithBool:[defaults boolForKey:@"likes_notification"]],
                           @"comment" : [NSNumber numberWithBool:[defaults boolForKey:@"comments_notification"]],
                           @"buddyRequest" : [NSNumber numberWithBool:[defaults boolForKey:@"buddyRequest_notification"]],
                           @"commentResponse" : [NSNumber numberWithBool:[defaults boolForKey:@"response_notification"]],
                           @"mention" : [NSNumber numberWithBool:[defaults boolForKey:@"mentions_notification"]],
                           };

    [user saveEventually];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UserSetupProfile" object:nil];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        NSString *name = user.realName;
        NSString *firstName = [name substringToIndex:[name rangeOfString:@" "].location];
        NSString *lastName = [name substringFromIndex:[name rangeOfString:@" "].location];
        
        firstName = [firstName removeWhiteSpacesFromString];
        lastName = [lastName removeWhiteSpacesFromString];
        
        NSLog(@"%@ %@", firstName, lastName);
        
        [SupportKit setUserFirstName:firstName lastName:lastName];
        
        PFInstallation *install = [PFInstallation currentInstallation];
        [install addUniqueObject:user.username forKey:@"channels"];
        [install saveInBackground];
        
        [[BSUser currentUser].phone fetchInBackground];
        
        [self askForNotificationPermission];
        
    }];
    
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField.placeholder isEqualToString:@"username"]) {
    
        [self.passwordField becomeFirstResponder];
    
    } else {
        
        [self login:nil];
        
    }
    
    return YES;
    
}

#pragma mark - IBActions

- (IBAction)login:(id)sender {
    
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    
    [KVNProgress show];
    
    [BSUser logInWithUsernameInBackground:self.usernameField.text password:self.passwordField.text
        block:^(PFUser *pfuser, NSError *error) {
            
            if (pfuser) {
                
                BSUser *user = (BSUser *)pfuser;
                
                [CHKeychain setObject:self.passwordField.text forKey:@"pwd"];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
                user.notifications = @{
                                       @"like" : [NSNumber numberWithBool:[defaults boolForKey:@"likes_notification"]],
                                       @"comment" : [NSNumber numberWithBool:[defaults boolForKey:@"comments_notification"]],
                                       @"buddyRequest" : [NSNumber numberWithBool:[defaults boolForKey:@"buddyRequest_notification"]],
                                       @"commentResponse" : [NSNumber numberWithBool:[defaults boolForKey:@"response_notification"]],
                                       @"mention" : [NSNumber numberWithBool:[defaults boolForKey:@"mentions_notification"]],
                                       };

                [user saveEventually];
                
                [KVNProgress dismiss];
                [self dismissViewControllerAnimated:YES completion:^{
                    
                    [KVNProgress dismiss];
                    //[KVNProgress showSuccessWithStatus:@"Welcome Back!"];
                    NSString *name = user.realName;
                    NSString *firstName = [name substringToIndex:[name rangeOfString:@" "].location];
                    NSString *lastName = [name substringFromIndex:[name rangeOfString:@" "].location];
                    
                    firstName = [firstName removeWhiteSpacesFromString];
                    lastName = [lastName removeWhiteSpacesFromString];
                    
                    [SupportKit setUserFirstName:firstName lastName:lastName];
                    
                    PFInstallation *install = [PFInstallation currentInstallation];
                    [install addUniqueObject:user.username forKey:@"channels"];
                    [install saveInBackground];
                    
                    [[BSUser currentUser].phone fetchInBackground];
                    
                    [[BSLocationManager sharedManager] updateLocation];
                    
                    [self askForNotificationPermission];
                }];

                if ([self.delegate respondsToSelector:@selector(userDidLogin:)]) {
                      [self.delegate userDidLogin:user];
                }
            
            } else {
                
                NSString *errorString = [error userInfo][@"error"];
                [KVNProgress showErrorWithStatus:errorString];
                
            }
        }];
    
}

- (IBAction)forgotPassword:(id)sender {
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    alert.customViewColor = [UIColor bubblePink];
    
    UITextField *text = [alert addTextField:@"Email"];
    
    [alert addButton:@"Reset my password" actionBlock:^(void) {
        BOOL sent = [BSUser requestPasswordResetForEmail:text.text];
        
        if (!sent) {
            [KVNProgress showErrorWithStatus:@"A user with that email couldn't be found."];
        }
        
    }];
    
    [alert showCustom:self image:[IonIcons imageWithIcon:ion_ios_locked size:20 color:[UIColor whiteColor]] color:[UIColor bubblePink] title:@"" subTitle:nil closeButtonTitle:@"Cancel" duration:0.0f];
}

- (IBAction)signup:(id)sender {
    
    PhoneNumberVerificationViewController *verify = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneNumberVerificationViewController"];
    verify.isSigningUp = YES;
    [self.navigationController pushViewController:verify animated:YES];
}

- (IBAction)loginWithFacebook:(id)sender {
    
    NSArray *permissions = @[@"public_profile",@"email",@"user_friends", @"user_birthday"];
    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissions block:^(PFUser *pfuser, NSError *error) {
        
        BSUser *user = (BSUser *)pfuser;
        
        if (!user) {
            
            NSLog(@"The user cancelled the Facebook login.");
            
        } else if (user.isNew) {
            
            FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil];
            
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                
                if (!error) {
                    
                    NSString *facebookID = result[@"id"];
                    
                    NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
                    
                    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:pictureURL];
                    
                    // Run network request asynchronously
                    [NSURLConnection sendAsynchronousRequest:urlRequest
                                                       queue:[NSOperationQueue mainQueue]
                                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                               
                                               if (connectionError == nil && data != nil) {
                                                   
                                                   // Got the profile picture
                                                   PFFile *profilePicture = [PFFile fileWithName:@"profilePic.jpg" data:data];
                                                   user.profilePicture = profilePicture;
                                                   [user saveInBackground];
                                               }
                                           }];
                    
                    // Convert string to date object
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"MM/dd/yyyy"];
                    NSDate *birthdayDate = [dateFormat dateFromString:result[@"birthday"]];
                    
                    user.email = result[@"email"];
                    user.realName = result[@"name"];
                    user.birthday = birthdayDate;
                    user.gender = result[@"gender"];
                    user.points = [NSNumber numberWithInt:10];
                    user.popPoints = [NSNumber numberWithInt:0];
                    user.color = @"bubblePink";
                    user.beta = YES;
                    user.staff = NO;
                    user.buddies = [NSMutableArray new];
                    user.pendingBuddies = [NSMutableArray new];
                    user.blocked = [NSMutableArray new];
                    user.tags = [NSMutableArray new];
                    user.posts = [NSNumber numberWithInt:0];
                    
                    [user saveInBackground];
                    
                    [self askForInformationForUser:user];
                    
                } else {
                    // An error occurred, we need to handle the error
                    [self handleAuthError:error];
                }
                
            }];
            
            /*[FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                
                if (!error) {
                    
                    NSString *facebookID = result[@"id"];
                    
                    NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
                    
                    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:pictureURL];
                    
                    // Run network request asynchronously
                    [NSURLConnection sendAsynchronousRequest:urlRequest
                                                       queue:[NSOperationQueue mainQueue]
                                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                               
                                               if (connectionError == nil && data != nil) {
                                                   
                                                   // Got the profile picture
                                                   PFFile *profilePicture = [PFFile fileWithName:@"profilePic.jpg" data:data];
                                                   user.profilePicture = profilePicture;
                                                   [user saveInBackground];
                                               }
                                           }];
                     AFNetworking NOT TESTED CODE
                     AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
                     requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
                     
                     [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                     
                     NSLog(@"Response: %@", responseObject);
                     
                     // Got the profile picture
                     PFFile *profilePicture = [PFFile fileWithName:@"profilePic.jpg" data:responseObject];
                     [user setObject:profilePicture forKey:@"ProfilePic"];
                     [user saveInBackground];
                     
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Image error: %@", error);
                     }];
                     
                     [requestOperation start];
                     
                    // Convert string to date object
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"MM/dd/yyyy"];
                    NSDate *birthdayDate = [dateFormat dateFromString:result[@"birthday"]];
                    
                    user.email = result[@"email"];
                    user.realName = result[@"name"];
                    user.birthday = birthdayDate;
                    user.gender = result[@"gender"];
                    user.points = [NSNumber numberWithInt:10];
                    user.popPoints = [NSNumber numberWithInt:0];
                    user.color = @"bubblePink";
                    user.beta = YES;
                    user.staff = NO;
                    user.buddies = [NSMutableArray new];
                    user.pendingBuddies = [NSMutableArray new];
                    user.blocked = [NSMutableArray new];
                    user.tags = [NSMutableArray new];
                    user.posts = [NSNumber numberWithInt:0];
                    
                    [user saveInBackground];
                    
                    [self askForInformationForUser:user];
                    
                } else {
                    // An error occurred, we need to handle the error
                    [self handleAuthError:error];
                }
                
            }];*/
            
        } else {
            
            NSLog(@"User logged in through Facebook!");
            
            if (user.username.length > 15) {
                
                [self askForInformationForUser:user];
                
            } else {
                
                [KVNProgress showSuccessWithStatus:[NSString stringWithFormat:@"Welcome %@", user.username]];
                
                if ([self.delegate respondsToSelector:@selector(userDidLogin:)]) {
                    [self.delegate userDidLogin:user];
                }
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
            }
            
        }
    }];
    
}

#pragma mark - Helpers

- (void)hideKeyboard {
    
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    
}

- (void)askForInformationForUser:(BSUser *)user {
    
    NewFacebookUserViewController *newFacebook = [self.storyboard instantiateViewControllerWithIdentifier:@"NewFacebookUserViewController"];
    newFacebook.user = user;
    newFacebook.delegate = self;
    
    [self.navigationController pushViewController:newFacebook animated:YES];
    
}

- (void)handleAuthError:(NSError *)error {

    [KVNProgress showErrorWithStatus:error.localizedFailureReason];
    
    /*if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            // Error requires people using you app to make an action outside your app to recover
        
        [KVNProgress showErrorWithStatus:[FBErrorUtility userMessageForError:error]];
        
    } else {
            // You need to find more information to handle the error within your app
        if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                // The user refused to log in into your app
                // This probably won't be used, but here is a log anyways
            [KVNProgress showErrorWithStatus:[FBErrorUtility userMessageForError:error]];
           
        } else {
                // All other errors that can happen need retries
                // Show the user a generic error message
            [KVNProgress showErrorWithStatus:[FBErrorUtility userMessageForError:error]];
            
        }
    }*/
}

- (void)didFinishProfileSetup {
    
    [self didFinishSignUpWithUser:[BSUser currentUser]];
    
}

- (void)askForNotificationPermission {
    
    [[BSLocationManager sharedManager] updateLocation];
    
    if ([CHDevice deviceVersion] >= 8.0) {
        
        UIApplication *application = [UIApplication sharedApplication];
        
        UIMutableUserNotificationAction *accept = [UIMutableUserNotificationAction new];
        accept.identifier = ACCEPT_BUDDY;
        accept.title = @"Accept";
        accept.activationMode = UIUserNotificationActivationModeBackground;
        accept.destructive = NO;
        accept.authenticationRequired = NO;
        
        UIMutableUserNotificationAction *deny = [UIMutableUserNotificationAction new];
        deny.identifier = DENY_BUDDY;
        deny.title = @"Deny";
        deny.activationMode = UIUserNotificationActivationModeBackground;
        deny.destructive = YES;
        deny.authenticationRequired = NO;
        
        UIMutableUserNotificationCategory *category = [UIMutableUserNotificationCategory new];
        category.identifier = PENDING_BUDDIES;
        
        NSArray *actions = @[accept, deny];
        
        [category setActions:actions forContext:UIUserNotificationActionContextMinimal];
        
        NSSet *categoriesSet = [[NSSet alloc] initWithObjects:category, nil];
        
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:categoriesSet]];
        
        [application registerForRemoteNotifications];
        
    }
    
}

@end
