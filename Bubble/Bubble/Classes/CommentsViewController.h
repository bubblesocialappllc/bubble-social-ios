//
//  CommentsViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/23/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

@import SlackTextViewController;
#import "CHTableViewCommentCellTableViewCell.h"
#import "AutoCompleteTableViewCell.h"

@interface CommentsViewController : SLKTextViewController

@property (strong, nonatomic) BSPost *post;
@property (nonatomic, getter=isPushed) BOOL pushed;

@end
