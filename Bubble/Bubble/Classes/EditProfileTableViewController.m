//
//  EditProfileTableViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/16/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "EditProfileTableViewController.h"
@import VGParallaxHeader;

@interface EditProfileTableViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;

@end

@implementation EditProfileTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.profilePic.layer.borderWidth = 3;
    self.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profilePic.clipsToBounds = YES;
    self.profilePic.layer.cornerRadius = self.profilePic.frame.size.height / 2;
    [self.profilePic setImageWithString:self.appDelegate.currentUser.realName color:[UIColor bubblePink]];
    self.profilePic.file = self.appDelegate.currentUser.profilePicture;
    [self.profilePic loadInBackground];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeProfilePicture:)];
    
    [self.profilePic addGestureRecognizer:tap];
    
    self.username.text = self.appDelegate.currentUser.username;
    [self.username setEnabled:NO];
    self.email.text = self.appDelegate.currentUser.email;
    self.aboutMe.text = self.appDelegate.currentUser.aboutMe;
    
    [self.tableView setParallaxHeaderView:self.header
                                      mode:VGParallaxHeaderModeFill // For more modes have a look in UIScrollView+VGParallaxHeader.h
                                    height:150];
    
    //self.tableView.parallaxHeader.stickyViewPosition = VGParallaxHeaderStickyViewPositionTop;
    //[self.tableView.parallaxHeader setStickyView:self.profilePic withHeight:40];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.header.backgroundColor = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
     self.username.textColor = [CHColorHandler getUIColorFromNSString:self.appDelegate.currentUser.color];
    
    PFObject *phone = self.appDelegate.currentUser.phone;
    
    if (phone != nil) {
        self.phoneNumber.text = self.appDelegate.currentUser.phone.phoneNumber;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // This must be called in order to work
    [scrollView shouldPositionParallaxHeader];
    
    // scrollView.parallaxHeader.progress - is progress of current scroll
    //NSLog(@"Progress: %f", scrollView.parallaxHeader.progress);
    
    // This is how you can implement appearing or disappearing of sticky view
    //[scrollView.parallaxHeader.stickyView setAlpha:scrollView.parallaxHeader.progress];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 3;
            break;
        default:
            return 0;
            break;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0: { // Username and About Me
            break;
        }
        case 1: { // Account
            
            switch (indexPath.row) {
                case 0: { // Color
                    break;
                }
                case 1: { // Email
                    
                    [self changeEmail];
                    break;
                }
                case 2: { // Phone Number
                    
                    [self changePhoneNumber];
                    break;
                }
                default:
                    break;
            }
            
            break;
        }
        default:
            break;
    }
    
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"" forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextField Delegate 

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    NSString *pastUsername = self.appDelegate.currentUser.username;
    
    if (textField.text.length >= 4 && textField.text.length <= 15) {
        
        self.appDelegate.currentUser.username = [textField.text removeWhiteSpacesFromString];
        [self.appDelegate.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if (error) {
                NSString *errorString = [error userInfo][@"error"];
                [KVNProgress showErrorWithStatus:errorString];
                self.appDelegate.currentUser.username = pastUsername;
                textField.text = self.appDelegate.currentUser.username;
            }
            
            
        }];
        
    } else {
        [KVNProgress showErrorWithStatus:@"The username you chose is either too long or too short"];
    }
    
}

#pragma mark - UITextView Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    
    
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    self.appDelegate.currentUser.aboutMe = textView.text;
    [self.appDelegate.currentUser saveEventually];
    
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    // Upload image
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    PFFile *file = [PFFile fileWithName:@"profilePic.jpg" data:imageData];
    
    self.appDelegate.currentUser.profilePicture = file;
    [self.appDelegate.currentUser saveInBackground];
    
    self.profilePic.image = image;
    
}

#pragma mark - Helpers

- (void)changeProfilePicture:(UIGestureRecognizer *)tap {
    
    MSAlertController *alert = [MSAlertController alertControllerWithTitle:nil message:@"Need to change your picture?" preferredStyle:MSAlertControllerStyleActionSheet];
    
    MSAlertAction *camera = [MSAlertAction actionWithTitle:@"Camera" style:MSAlertActionStyleDefault handler:^(MSAlertAction *action) {
        
        //TODO: Add Bubble Camera
        
    }];
    
    MSAlertAction *photos = [MSAlertAction actionWithTitle:@"Photos" style:MSAlertActionStyleDefault handler:^(MSAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        // Create image picker controller
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        // Set source to the camera
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        // Delegate is self
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        
        // Show image picker
        // [self presentViewController:imagePicker animated:YES];
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    
    MSAlertAction *cancel = [MSAlertAction actionWithTitle:@"Cancel" style:MSAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[camera, photos, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)changeEmail {
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    alert.customViewColor = [CHColorHandler getUIColorFromNSString:[BSUser currentUser].color];
    
    UITextField *newEmail = [alert addTextField:@"New Email"];
    
    [alert addButton:@"Change my email" actionBlock:^(void) {
        
        if ([newEmail.text isValidEmail]) {
            
            [[BSUser currentUser] setEmail:newEmail.text];
            [[BSUser currentUser] saveEventually];
            self.email.text = newEmail.text;
            
            [self showSuccessEmailChange:newEmail.text];
            
        } else {
            [KVNProgress showErrorWithStatus:@"That doesn't seem like an email."];
        }
        
    }];
    
    [alert showEdit:self.tabBarController title:@"Enter your new email" subTitle:nil closeButtonTitle:@"Cancel" duration:0.0f];
    
}

- (void)changePhoneNumber {
    
    PhoneNumberVerificationViewController *verify = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneNumberVerificationViewController"];
    verify.isSigningUp = NO;
    
    [self.navigationController pushViewController:verify animated:YES];
    
}

- (void)showSuccessEmailChange:(NSString *)newEmail {
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    alert.customViewColor = [CHColorHandler getUIColorFromNSString:[BSUser currentUser].color];
    
    [alert showSuccess:self.tabBarController title:@"Please check" subTitle:[NSString stringWithFormat:@"your email at %@ to verify your account", newEmail] closeButtonTitle:@"Alright" duration:0.0f];
    
}

@end
