//
//  ViewBuddiesViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/21/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "ViewBuddiesViewController.h"

@interface ViewBuddiesViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *buddies;

@end

@implementation ViewBuddiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.navigationController.navigationBar.barTintColor = [CHColorHandler getUIColorFromNSString:self.user.color];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    if (self.isModal) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_close_round size:20 color:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(close)];
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.user fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
    
        PFQuery *query = [BSUser query];
        [query whereKey:@"objectId" containedIn:self.user.buddies];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            self.buddies = [NSMutableArray arrayWithArray:objects];
            [self.tableView reloadData];
            
        }];
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.numberOfSections == 2) {
        
        if (indexPath.section == 0) {
            
            HasPendingBuddiesTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"PendingCell"];
            
            if (cell == nil) {
                cell = [[HasPendingBuddiesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PendingCell"];
            }
            
            NSMutableArray *pending = [NSMutableArray arrayWithArray:self.user.pendingBuddies];
            
            NSString *numberOfRequestsAsString = [NSString stringWithFormat:@"%zd", pending.count];
            
            NSString *bufferString = [NSString stringWithFormat:@"You have %@ Buddy Requests", numberOfRequestsAsString];
            
            if (pending.count == 1) {
                bufferString = [NSString stringWithFormat:@"You have %@ Buddy Request", numberOfRequestsAsString];
            }
            
            NSMutableAttributedString *numberOfRequestsLabel = [[NSMutableAttributedString alloc] initWithString: bufferString];
            
            [numberOfRequestsLabel addAttributes:@{
                                                   NSFontAttributeName : [UIFont fontWithName: @"HelveticaNeue-Bold" size:16.0]
                                                   } range:NSMakeRange(0, bufferString.length)];
            
            [numberOfRequestsLabel addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, bufferString.length)];
            
            [numberOfRequestsLabel addAttributes:@{
                                                   NSFontAttributeName : [UIFont fontWithName: @"HelveticaNeue-Bold" size:16.0]
                                                   } range:NSMakeRange(9, numberOfRequestsAsString.length)];
            
            [numberOfRequestsLabel addAttribute:NSForegroundColorAttributeName value:[UIColor bubblePink] range:NSMakeRange(9, numberOfRequestsAsString.length)];
            
            cell.textLabel.attributedText = numberOfRequestsLabel;
            
            return cell;
            
        } else {
            
            if (indexPath.row <= self.buddies.count) {
                
                UserTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"UserCell"];
                
                if (cell == nil) {
                    cell = [[UserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UserCell"];
                }
                
                BSUser *user = [self.buddies objectAtIndex:indexPath.row];
                
                cell.profilePicture.clipsToBounds = YES;
                cell.profilePicture.layer.cornerRadius = 20;
                [cell.profilePicture setImageWithString:user.realName];
                cell.profilePicture.file = user.profilePicture;
                [cell.profilePicture loadInBackground];
                
                cell.username.text = user.username;
                
                return cell;
            }
            
        }
        
    } else {
    
        if (indexPath.row <= self.buddies.count) {
            
            UserTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"UserCell"];
            
            if (cell == nil) {
                cell = [[UserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UserCell"];
            }
            
            BSUser *user = [self.buddies objectAtIndex:indexPath.row];
            
            cell.profilePicture.clipsToBounds = YES;
            cell.profilePicture.layer.cornerRadius = 20;
            [cell.profilePicture setImageWithString:user.realName];
            cell.profilePicture.file = user.profilePicture;
            [cell.profilePicture loadInBackground];
            
            cell.username.text = user.username;
            
            return cell;
        }
        
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.numberOfSections == 2) {
        
        if (section == 0) {
            return 1;
        } else {
            return self.buddies.count;
        }
        
    } else {
        return self.buddies.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSMutableArray *currentUserPendingBuddies = self.user.pendingBuddies;

    if (currentUserPendingBuddies == nil || currentUserPendingBuddies.count == 0) {
        return 1;
    } else if ([self.user.objectId isEqualToString:self.appDelegate.currentUser.objectId]) {
        return 2;
    } else {
        return 1;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.numberOfSections == 2) {
        
        if (indexPath.section == 0) {
            
        } else {
            [self openUserPageWithSelectedUser:[self.buddies objectAtIndex:indexPath.row]];
        }
        
    } else {
        [self openUserPageWithSelectedUser:[self.buddies objectAtIndex:indexPath.row]];
    }
    
}

- (void)openUserPageWithSelectedUser:(BSUser *)selectedUser {
    
    if (![selectedUser.username isEqualToString:self.appDelegate.currentUser.username]) {
        
        UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
        user.user = selectedUser;
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
        
        [self presentViewController:nc animated:YES completion:nil];
        
    }
    
}

- (void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
