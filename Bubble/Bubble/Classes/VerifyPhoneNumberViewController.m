//
//  VerifyPhoneNumberViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/30/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "VerifyPhoneNumberViewController.h"

@interface VerifyPhoneNumberViewController ()

@end

@implementation VerifyPhoneNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Ensures that the user cannot accidentally click into the wrong circle and mistype
    [self.firstCircle setUserInteractionEnabled:NO];
    [self.secondCircle setUserInteractionEnabled:NO];
    [self.thirdCircle setUserInteractionEnabled:NO];
    [self.fourthCircle setUserInteractionEnabled:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.code becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions
- (IBAction)invisibleButtonClicked:(id)sender {
    //If the invisible button around the circles is clicked, the text boxes are cleared
    self.firstCircle.textField.text=@"";
    self.secondCircle.textField.text=@"";
    self.thirdCircle.textField.text=@"";
    self.fourthCircle.textField.text=@"";
    self.code.text = @"";
    
    //Also, set the text box again as the first responder
    [self.code becomeFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger length = textField.text.length + string.length;
    BOOL isDeleting = NO;
    
    if (string.length == 0) {
        length = textField.text.length - 1;
        isDeleting = YES;
    }
    
    //Make sure we only have a max of 4 characters
    if (textField.text.length >= 4 && !isDeleting) {
        return NO;
    }
    
    if (!isDeleting) {
     
        if (length == 1)
            self.firstCircle.textField.text = string;
        else if (length == 2)
            self.secondCircle.textField.text = string;
        else if (length == 3)
            self.thirdCircle.textField.text = string;
        else if (length == 4) {
            
            self.fourthCircle.textField.text = string;
            [self verifyInputCode:self.code.text];
        
        }
    } else {
        
        if (length == 0)
            self.firstCircle.textField.text = string;
        else if (length == 1)
            self.secondCircle.textField.text = string;
        else if (length == 2)
            self.thirdCircle.textField.text = string;
        else if (length == 3)
            self.fourthCircle.textField.text = string;
        
    }
    
    return YES;

}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    self.code.text = @"";
}

#pragma mark - SignUpDelegate

- (void)didFinishSignUpWithUser:(PFUser *)user {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissAllViewControllers" object:self];
    
}

#pragma mark - Private Helpers

- (void)verifyInputCode:(NSString *)codeStr {
    
    [KVNProgress show];
    
    PFQuery *verifyQuery = [BSPhone query];
    [verifyQuery addDescendingOrder:@"createdAt"];
    [verifyQuery whereKey:@"phoneNumber" equalTo:[@"+1" stringByAppendingString:self.phoneNumber]];
    
    [verifyQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
       
        if (!error) {
            
            BSPhone *phone = (BSPhone *)object;
            
            NSInteger codeInt = self.code.text.intValue;
            NSInteger verificationCode = [phone.verificationCode integerValue];
           
            if (verificationCode == codeInt) {
                phone.verified = YES;
                [object saveEventually];
                
                [KVNProgress showSuccess];
                
                if (self.isSigningUp) {
                    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(showSignUp:) userInfo:@{@"phone" : object} repeats:NO];
                } else {
                    
                    [BSUser currentUser].phone = phone;
                    [[BSUser currentUser] saveInBackground];
                    
                    [[BSUser currentUser].phone fetchIfNeededInBackground];
                    
                    [self.navigationController popToViewController:[[self.parentViewController childViewControllers] objectAtIndex:2] animated:YES];
                }
                
            } else {
                [KVNProgress showErrorWithStatus:@"Seems to be the wrong verification code"];
                
                //If the invisible button around the circles is clicked, the text boxes are cleared
                self.firstCircle.textField.text=@"";
                self.secondCircle.textField.text=@"";
                self.thirdCircle.textField.text=@"";
                self.fourthCircle.textField.text=@"";
                self.code.text = @"";
                
                //Also, set the text box again as the first responder
                [self.code becomeFirstResponder];
            }
            
        } else {
            NSString *errorString = [error userInfo][@"error"];
            [KVNProgress showErrorWithStatus:errorString];
        }
        
    }];
}

- (void)showSignUp:(NSNotification *)notification {
    
    SignUpViewController *signUp = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    signUp.delegate = self;
    signUp.phone = [[notification userInfo] objectForKey:@"phone"];
    
    //[self presentViewController:signUp animated:YES completion:nil];
    [self.navigationController pushViewController:signUp animated:YES];
}

@end
