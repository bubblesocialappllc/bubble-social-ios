//
//  ProfileSetupViewContoller.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "ProfileSetupViewContoller.h"

@interface ProfileSetupViewContoller ()

@end

@implementation ProfileSetupViewContoller

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    /*CGSize size = image.size;
    
    CGRect cropRect = CGRectMake(0, 0, size.width, 500);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, cropRect);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);*/
    
    self.imageView.image = image;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Camera Delegate

- (void)cameraDismissedWithImage:(UIImage *)image {
    self.imageView.image = image;
}

- (void)cameraDismissedWithCancel {
    
}

#pragma mark - IBActions

- (IBAction)addProfilePhoto:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        CameraViewController *camera = [sb instantiateViewControllerWithIdentifier:@"CameraViewController"];
        
        camera.delegate = self;
        
        [self presentViewController:camera animated:YES completion:nil];
        
    }];
    
    UIAlertAction *photos = [UIAlertAction actionWithTitle:@"Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [UIImagePickerController new];
        
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = @[(NSString *)kUTTypeImage];
        picker.allowsEditing = YES;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    
    [alert addAction:camera];
    [alert addAction:photos];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)continueSetup:(id)sender {

    if (self.imageView.image) {
        
        BSUser *currentUser = [BSUser currentUser];
        
        PFFile *file = [PFFile fileWithName:@"profilePic.jpg" data:UIImageJPEGRepresentation(self.imageView.image, 1.0)];
        
        currentUser.profilePicture = file;
        [currentUser saveInBackground];
        
        ProfileSetup2ViewContoller *setup = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSetup2ViewContoller"];
        
        [self.navigationController pushViewController:setup animated:YES];
    
    } else {
        
        [KVNProgress showErrorWithStatus:@"You should pick a profile photo"];
        
    }
    
}

@end
