//
//  BSPhotoCollectionViewCell.h
//  Bubble
//
//  Created by Chayel Heinsen on 7/18/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSPhotoCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet PFImageView *image;

@end
