//
//  BlockedUsersViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/24/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlockedUsersTableViewCell.h"

@interface BlockedUsersViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
