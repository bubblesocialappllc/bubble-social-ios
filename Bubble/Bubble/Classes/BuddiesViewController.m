//
//  BuddiesViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "BuddiesViewController.h"
#import "MainTableViewCell.h"

@interface BuddiesViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *postData;
@property (strong, nonatomic) PFQuery *refreshQuery;
@property (strong, nonatomic) NSDate *lastUpdate;

@end

@implementation BuddiesViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainCell"];
    
    self.tableView.estimatedRowHeight = 50;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
   
    self.postData = [NSMutableArray new];
    
    self.tableView.fetchDelegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;


    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUser:)];
    [self.tableView addGestureRecognizer:tapGr];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(feedChanged:) name:@"ChangedFeed" object:nil];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, 125)];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubbleLogo"]];
    logo.contentMode = UIViewContentModeScaleAspectFill;
    
    //Set the size of the frame so that
    CGRect frame = CGRectMake([UIScreen mainScreen].applicationFrame.size.width/2, 0, 120, 30);
    logo.frame = frame;
    logo.frame = CGRectOffset(logo.frame, -logo.frame.size.width/2, 30);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, logo.frame.size.height+35, [[UIScreen mainScreen] applicationFrame].size.width, 20)];
    label.text = @"Bubble Social | Created with love in Orlando, FL";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0];
    
    [footer addSubview:logo];
    [footer addSubview:label];
    
    self.tableView.tableFooterView = footer;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellAnimation"]) {
        cell.alpha = 0;
        
        [UIView beginAnimations:@"fade" context:nil];
        [UIView setAnimationDuration:0.8];
        cell.alpha = 1;
        [UIView commitAnimations];
    }
    
    if (indexPath.section == self.postData.count - 1) {
        [self.tableView paginate];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section <= self.postData.count && self.postData.count != 0) {
        
        BSPost *post = [self.postData objectAtIndex:indexPath.section];
        
        MainTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MainCell"];
        
        //If the post has a photo
        if (post.photo) {
            
            cell.squareConstraint.active = YES;
            
            //We circumvent the Parse cache and directly access the NSCache to prevent photo flicker
            NSCache *cache = [NSCache new];
            UIImage *image = [cache objectForKey:post.photo.url];
            
            if (!image) {
                //[cell.image setImageWithString:post.authorUsername color:[UIColor bubblePink]];
                cell.image.file = post.photo;
                [cell.image loadInBackground];
            } else {
                cell.image.image = image;
            }
            
        } else {
            cell.image.image = [UIImage new];
            cell.squareConstraint.active = NO;
        }
        
        //The cell text
        cell.text.attributedText = [self attributedMessageFromMessage:post.text];
        
        //Adding the buttons to the footer
        [cell.like setTarget:self andAction:@selector(likePost:)];
        [cell.comment setTarget:self andAction:@selector(commentPost:)];
        
        //Set the numbers based on the post
        cell.like.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likes];
        cell.comment.commentsCountLabel.text = [NSString stringWithFormat:@"%@", post.comments];
        
        NSMutableArray *likers = [NSMutableArray arrayWithArray:post.likers];
        
        if ([likers containsObject:self.appDelegate.currentUser.username]) {
            cell.like.backgroundColor = [UIColor bubblePink];
        } else {
            cell.like.backgroundColor = [UIColor lightGrayColor];
        }
        
        NSMutableArray *commenters = [NSMutableArray arrayWithArray:post.commenters];
        
        if ([commenters containsObject:self.appDelegate.currentUser.username]) {
            cell.comment.backgroundColor = [UIColor bubblePink];
        } else {
            cell.comment.backgroundColor = [UIColor lightGrayColor];
        }
        
        //Set up for cell clicks
        UITapGestureRecognizer *cellTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCell:)];
        UITapGestureRecognizer *cellLikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(likePost:)];
        
        [cellTap setDelaysTouchesBegan:YES];
        [cellLikeTap setDelaysTouchesBegan:YES];
        
        cellTap.numberOfTapsRequired = 1;
        cellLikeTap.numberOfTapsRequired = 2;
        
        [cellTap requireGestureRecognizerToFail:cellLikeTap];
        
        [cell addGestureRecognizer:cellLikeTap];
        [cell addGestureRecognizer:cellTap];
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellDidLongHold:)];
        [cell addGestureRecognizer:longPress];


        return cell;
        
    }
    
    // Just make sureNotificationsBlankCell that we always return something.
    MePageBlankCellTableViewCell *blankCell = [self.tableView dequeueReusableCellWithIdentifier:@"BuddiesBlankCell"];;
    return blankCell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.postData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    header.backgroundColor = [UIColor whiteColor];
    
    BSPost *post = [self.postData objectAtIndex:section];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, header.frame.size.width, 4)];
    line.backgroundColor = [UIColor flatCloudsColor];
    
    PFImageView *profilePicture = [[PFImageView alloc] initWithFrame:CGRectMake(8, 8, 45, 45)];
    profilePicture.contentMode = UIViewContentModeScaleAspectFit;
    profilePicture.clipsToBounds = YES;
    profilePicture.layer.cornerRadius = profilePicture.frame.size.height / 2;
    
    UILabel *username = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 9, header.frame.size.width - 55, 25)];
    
    username.text = post.authorUsername;
    username.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    username.textColor = [UIColor darkGrayColor];
    username.tag = 1;
    
    UILabel *location = [[UILabel alloc] initWithFrame:CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.size.width + 5), 26, header.frame.size.width - 55, 25)];
    
    location.text = post.locationName;
    location.font = [UIFont systemFontOfSize:14];
    location.textColor = [UIColor lightGrayColor];
    
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(header.frame.size.width, 0, 0, 0)];
    time.text = [CHTime makeTimePassedFromDate:post.createdAt];
    time.font = [UIFont systemFontOfSize:14];
    time.textColor = [UIColor lightGrayColor];
    [time sizeToFit];
    time.frame = CGRectOffset(time.frame, -time.frame.size.width - 10, (header.frame.size.height / 2) - (time.frame.size.height / 2));
    
    profilePicture.file = post.profilePic;
    [profilePicture loadInBackground];
    
    [header addSubview:line];
    [header addSubview:profilePicture];
    [header addSubview:username];
    [header addSubview:location];
    [header addSubview:time];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapHeader:)];
    [header addGestureRecognizer:tap];
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

#pragma mark - CHPaginatedTableViewDelegate

- (void)tableView:(CHPaginatedTableView *)tableview fetchedItems:(NSArray *)objects {
    
    NSInteger beforeCount = self.postData.count;
    
    for (BSPost *post in objects) {
        
        if (![self.postData containsObject:post]) {
            [self.postData addObject:post];
        }
    }
    
    NSInteger afterCount = self.postData.count;
    
    if (afterCount != beforeCount) {
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdAt"
                                                     ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        self.postData = [[self.postData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        
        [tableview reloadData];
    }
}

- (PFQuery *)queryForFetch {
    
    self.refreshQuery = [BSPost query];
    [self.refreshQuery includeKey:@"author"];
    [self.refreshQuery orderByDescending:@"createdAt"];
    
    /*
    if (self.lastUpdate != nil) {
        [self.refreshQuery whereKey:@"updatedAt" greaterThan:self.lastUpdate];
    }
    */
    
    [self.refreshQuery whereKey:@"authorId" containedIn:self.appDelegate.currentUser.buddies];
    
    //self.lastUpdate = [NSDate date]; //Sets the current time to the last updated variable for pulling only new posts next time we call the server
    
    return self.refreshQuery;
    
}

- (void)pulledToRefresh {
    //[self.postData removeAllObjects];
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // Get the current size of the refresh controller
    CGRect refreshBounds = self.tableView.refreshControl.bounds;
    
    // Distance the table has been pulled >= 0
    CGFloat pullDistance = MAX(0.0, -self.tableView.refreshControl.frame.origin.y);
    
    // Half the width of the table
    //CGFloat midX = self.tableView.frame.size.width / 2.0;
    
    // Calculate the width and height of our graphics
    CGFloat bubbleHeight = self.tableView.indicatorView.bounds.size.height;
    CGFloat bubbleHeightHalf = bubbleHeight / 2.0;
    
    //CGFloat bubbleWidth = indicatorView.bounds.size.width;
    //CGFloat bubbleWidthHalf = bubbleWidth / 2.0;
    
    // Calculate the pull ratio, between 0.0-1.0
    //CGFloat pullRatio = MIN( MAX(pullDistance, 0.0), 100.0) / 100.0;
    
    // Set the Y coord of the graphics, based on pull distance
    CGFloat bubbleY = pullDistance / 2.0 - bubbleHeightHalf;
    
    // Calculate the X coord of the graphics
    CGFloat bubbleX = [UIScreen mainScreen].applicationFrame.size.width / 2;
    
    // Set the graphic's frames
    CGRect bubbleFrame = self.tableView.indicatorView.frame;
    bubbleFrame.origin.x = bubbleX;
    bubbleFrame.origin.y = bubbleY;
    
    self.tableView.indicatorView.frame = bubbleFrame;
    
    CGFloat xpoint = self.tableView.indicatorView.frame.size.width / 2;
    
    self.tableView.indicatorView.frame = CGRectOffset(self.tableView.indicatorView.frame, -xpoint, 0);
    
    // Set the enbubbleing view's frames
    refreshBounds.size.height = pullDistance;
    
    [self.tableView.refreshControl addSubview:self.tableView.indicatorView];
    
    [self.tableView.indicatorView startAnimating];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.tableView scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

#pragma mark - Helpers

- (void)didTapHeader:(UITapGestureRecognizer *)gr {
    UILabel *label;
    
    for (UIView *v in gr.view.subviews) {
        
        if ([v isKindOfClass:[UILabel class]]) {
            
            if (v.tag == 1) {
                label = (UILabel *)v;
            }
        }
    }
    
    
    // Get username associated with this post
    NSString *username = label.text;
    BSUser *user;
    
    // Get user associated with username
    for (BSPost *post in self.postData) {
        
        if ([username isEqualToString:post.authorUsername]) {
            user = post.author;
        }
    }
    
    if (user) {
        
        if (![user.username isEqualToString:self.appDelegate.currentUser.username]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : user }];
            
        }
    }
}

- (void)cellDidLongHold:(UILongPressGestureRecognizer *)hole {
    
    CGPoint point = [hole locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    MainTableViewCell *cell = (MainTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        //There is a conflict here with deleting Pops and having the animation for Pops, we need to come back to this
        [self.postData removeObjectAtIndex:indexPath.section];
        [self.tableView reloadData];
        
        //[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        
        [post deleteInBackground];
        
    }];
    
    UIAlertAction *report = [UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        [self reportPost:post];
        
    }];
    
    UIAlertAction *save = [UIAlertAction actionWithTitle:@"Save Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (cell.image.image && post.photo) {
            UIImageWriteToSavedPhotosAlbum(cell.image.image, nil, nil, nil);
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    if ([post.authorId isEqualToString:self.appDelegate.currentUser.objectId]) {
        [alert addAction:delete];
        
        if (cell.image.image && post.photo) {
            [alert addAction:save];
        }
        
        [alert addAction:cancel];
        
    } else {
        [alert addAction:report];
        
        if (cell.image.image && post.photo) {
            [alert addAction:save];
        }
        
        [alert addAction:cancel];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)tappedCell:(UITapGestureRecognizer *)tap {
    
    CGPoint point = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    if (post.photo != nil) {
        [self commentPostWithImageViewer:tap];
    } else {
        [self commentPost:tap];
    }
    
}

- (void)feedChanged:(NSNotification *)notification {
    
    int page = 2;
    
    if ([notification.userInfo[@"page"] intValue] == page) {
        
        if (self.appDelegate.currentUser.buddies == nil) {
            self.appDelegate.currentUser.buddies = [NSMutableArray new];
            [self.appDelegate.currentUser saveEventually];
        }
        
        [self.tableView refresh];
        
    }
    
}

- (void)refreshedLocation:(NSNotification *)notification {
    
    if (self.postData.count == 0) {
        [self.tableView refresh];
    }
    
}

- (void)openUser:(UIGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    BSUser *selectedUser = post.author;
    
    if (![selectedUser.username isEqualToString:self.appDelegate.currentUser.username]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : selectedUser }];
        
        /*UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
         user.user = selectedUser;
         
         UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
         
         [self presentViewController:nc animated:YES completion:nil];*/
        
    }
    
}

- (void)likePost:(UIGestureRecognizer *)tap {
    
    CGPoint tapPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapPoint];
    
    TextTableViewCell *cell = (TextTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    BOOL shouldLike = NO;
    
    if (![post.authorUsername isEqualToString:self.appDelegate.currentUser.username]) {
        
        NSMutableArray *likers = [NSMutableArray new];
        
        [likers setArray:post.likers];
        
        for (NSString *liker in likers) {
            
            if ([liker isEqualToString:self.appDelegate.currentUser.username]) {
                
                shouldLike = NO;
                break;
            } else {
                shouldLike = YES;
            }
            
        }
        
        if (likers.count == 0) {
            shouldLike = YES;
        }
        
        if (shouldLike) {
            
            [self likePost:post label:cell.like.likesCountLabel];
            cell.like.backgroundColor = [UIColor bubblePink];
            [cell.like setEnabled:NO];
            
        } else {
            
            cell.like.backgroundColor = [UIColor lightGrayColor];
            [self unlikePost:post label:cell.like.likesCountLabel];
            [cell.like setEnabled:NO];
            
        }
        
    }
    
}

- (void)commentPost:(UIGestureRecognizer *)tap {
    
    CGPoint pressPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pressPoint];
    
    PFObject *object = [self.postData objectAtIndex:indexPath.section];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenComment" object:nil userInfo:@{ @"object" : object }];
    
}

- (void)commentPostWithImageViewer:(UIGestureRecognizer *)tap {
    
    CGPoint pressPoint = [tap locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pressPoint];
    
    BSPost *post = [self.postData objectAtIndex:indexPath.section];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenCommentWithImageViewer" object:nil userInfo:@{ @"object" : post }];
    
}

- (void)likePost:(BSPost *)post label:(UILabel *)label {
    
    NSMutableArray *hasLikedBeforeCopy = [NSMutableArray arrayWithArray:post.hasLikedBefore];
    
    NSNumber *likes = post.likes;
    likes = [NSNumber numberWithInt:[likes intValue] + 1];
    
    label.text = [NSString stringWithFormat:@"%@", likes];
    
    NSMutableArray *likers = [NSMutableArray new];
    
    [likers setArray:post.likers];
    
    if (likers.count == 0 || likers == nil) {
        likers = [NSMutableArray arrayWithObject:self.appDelegate.currentUser.username];
    } else {
        
        BOOL hasLiked = [likers containsObject:self.appDelegate.currentUser.username];
        
        if (!hasLiked) {
            [likers addObject:self.appDelegate.currentUser.username];
        }
    }
    
    NSMutableArray *hasLikedBefore = [NSMutableArray arrayWithArray:post.hasLikedBefore];
    
    if (hasLikedBefore.count == 0 || hasLikedBefore == nil) {
        hasLikedBefore = [NSMutableArray arrayWithObject:self.appDelegate.currentUser.username];
    } else {
        
        BOOL hasLiked = [hasLikedBefore containsObject:self.appDelegate.currentUser.username];
        
        if (!hasLiked) {
            [hasLikedBefore addObject:self.appDelegate.currentUser.username];
        }
        
    }
    
    NSNumber *points = post.points;
    
    points = [NSNumber numberWithInt:[points intValue] + 1];
    
    post.likers = likers;
    post.hasLikedBefore = hasLikedBefore;
    post.likes = likes;
    post.points = points;
    
    [post saveEventually:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            
            if (![hasLikedBeforeCopy containsObject:self.appDelegate.currentUser.username]) {
                
                [CloudCode pushToChannel:post.authorUsername withAlert:[NSString stringWithFormat:@"%@ liked your post!", self.appDelegate.currentUser.username] andCategory:@"like" andViewToPush:[NSString stringWithFormat:@"bubblesocial://post/%@", post.objectId]];
                
                [CloudCode setNotification:post.objectId andType:[BSNotificationType BSLikeNotification]];
                
            }
            
        }
        
    }];
    
}

- (void)unlikePost:(BSPost *)post label:(UILabel *)label {
    
    NSNumber *likes = post.likes;
    likes = [NSNumber numberWithInt:[likes intValue] - 1];
    
    label.text = [NSString stringWithFormat:@"%@", likes];
    
    NSMutableArray *likers = [NSMutableArray new];
    
    [likers setArray:post.likers];
    
    BOOL hasLiked = [likers containsObject:self.appDelegate.currentUser.username];
    
    if (hasLiked) {
        [likers removeObject:self.appDelegate.currentUser.username];
    }
    
    NSNumber *points = post.points;
    
    points = [NSNumber numberWithInt:[points intValue] - 1];
    
    post.likers = likers;
    post.likes = likes;
    post.points = points;
    
    [post saveEventually];
    
}

- (NSAttributedString *)attributedMessageFromMessage:(NSString *)message {
    
    NSArray *messageWords = [message componentsSeparatedByString:@" "];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@""];
    
    for (NSString *word in messageWords) {
        
        NSDictionary *attributes;
        
        if ([word isEqualToString: @""]) {
            
        } else if ([word characterAtIndex:0] == '@') {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor bubblePink],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"username",
                           @"username" : [word substringFromIndex:1],
                           };
            
        } else if ([word characterAtIndex:0] == '#') {
            attributes = @{
                           NSForegroundColorAttributeName:[UIColor flatEmeraldColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"hashtag",
                           @"hashtag" : [word substringFromIndex:1],
                           };
            
        } else {
            attributes = @{
                           NSForegroundColorAttributeName : [UIColor darkGrayColor],
                           NSFontAttributeName : [UIFont systemFontOfSize:16],
                           @"wordType" : @"normal",
                           };
        }
        
        NSAttributedString *subString = [[NSAttributedString alloc]
                                         initWithString:[NSString stringWithFormat:@"%@ ", word]
                                         attributes:attributes];
        
        [attributedMessage appendAttributedString:subString];
    }
    
    return attributedMessage;
}

- (void)messageTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                    inTextContainer:textView.textContainer
                                    fractionOfDistanceBetweenInsertionPoints:nil];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id wordTypes = [textView.attributedText attribute:@"wordType"
                                                atIndex:characterIndex
                                                effectiveRange:&range];
        
        if ([wordTypes isEqualToString:@"username"]) {
            
            NSString *userName = [textView.attributedText attribute:@"username"
                                                            atIndex:characterIndex
                                                            effectiveRange:&range];
            
            PFQuery *userQuery = [BSUser query];
            [userQuery whereKey:@"username" equalTo:userName];
            [userQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenUserPage" object:nil userInfo:@{ @"user" : object }];
                
            }];
            
        } else if ([wordTypes isEqualToString:@"hashtag"]) {
            
            NSString *hash = [textView.attributedText attribute:@"hashtag"
                                                      atIndex:characterIndex
                                                      effectiveRange:&range];
            hash = [hash lowercaseString];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenHashtag" object:nil userInfo:@{ @"hashtag" : hash }];
            
        } else {
            [self tappedCell:recognizer];
        }
    }
}

- (void)reportPost:(BSPost *)post {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"What's the problem?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *inappropriate = [UIAlertAction actionWithTitle:@"Inappropriate" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *spam = [UIAlertAction actionWithTitle:@"Spamming" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *other = [UIAlertAction actionWithTitle:@"Other" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [BSReport reportPost:post withReason:action.title];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addActions:@[inappropriate, spam, other, cancel]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
