//
//  ImageIntroViewController.m
//  Bubble
//
//  Created by Tulio Troncoso on 8/18/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "ImageIntroViewController.h"

@interface ImageIntroViewController () <UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIButton *skipButton;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;

@end

@implementation ImageIntroViewController
{
    NSArray *images;
}

#pragma mark - Setup
- (void)viewDidLoad{
    [super viewDidLoad];

    // Setup scrollview
    self.scrollView.pagingEnabled = YES;
    self.scrollView.frame = self.view.frame;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.directionalLockEnabled = YES;
    CGFloat width = self.view.frame.size.width;

    [self.pageControl setNumberOfPages: [self imagesForIntro].count];

    for (int i = 0; i < [[self imagesForIntro] count]; i++)
    {

        UIImageView *imgView = [[UIImageView alloc] init];
        imgView.frame = CGRectMake(i * width, 0, width, self.view.frame.size.height);
        imgView.image = [images objectAtIndex:i];
//        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.contentMode = UIViewContentModeBottom;
        imgView.clipsToBounds = YES;
        [self.scrollView addSubview:imgView];

    }

    UITapGestureRecognizer *makeAPop = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makeAPop:)];
    makeAPop.numberOfTapsRequired = 1;
    [self.scrollView addGestureRecognizer:makeAPop];

    self.scrollView.contentSize = CGSizeMake(width * images.count, self.view.frame.size.height);

}

- (void) makeAPop:(UITapGestureRecognizer*)gr {
    if (self.pageControl.currentPage == self.pageControl.numberOfPages - 1) {
        [self dismissViewControllerAnimated:YES completion:^{

        }];
    } else {
        CGRect nextPage = self.scrollView.slk_visibleRect;
        nextPage.origin.x += self.view.frame.size.width;
        [self.scrollView scrollRectToVisible:nextPage animated:YES];
    }

}

- (void)viewDidAppear:(BOOL)animated{

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat currentOffset = scrollView.contentOffset.x;

    NSInteger page = currentOffset / 320;

    [self.pageControl setCurrentPage:page];

}
/**
 *  This method is where you want to put in the images that will be in the introduction.
 *
 *  @return An array of images that will be used for the introduction
 */
- (NSArray*)imagesForIntro{

    NSMutableArray *imagesForIntro = [NSMutableArray new];


    /**
     *  TODO: ADD YOUR IMAGE NAMES HERE
     */
    NSArray *imageNames = @[ @"Page-One",
                             @"Page-Two",
                             @"Page-Three",
                             @"Page-Four",
                             @"Page-Five",
                             @"Page-Six"];

    // Add the images to the image array
    for (NSString *imageName in imageNames) {
        [imagesForIntro addObject: [UIImage imageNamed:imageName]];
    }

    // Set the instance variable for images
    images = imagesForIntro;

    // Return the array of images
    return images;
}

#pragma mark - ScrollView Delegate

#pragma mark - IBActions
- (IBAction)didPressSkip:(UIButton*)button{
    [self dismissViewControllerAnimated:YES completion:^{

    }];}

@end
