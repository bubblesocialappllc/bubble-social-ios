//
//  UserViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/19/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatViewController.h"
#import "ViewBuddiesViewController.h"
#import "PhotosCollectionViewController.h"

/**
 *  The View Controller that shows other users on Bubble. This should be presented with a navigation controller.
 */
@interface UserViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CHPaginatedTableViewDelegate>

@property (strong, nonatomic) BSUser *user;
@property (strong, nonatomic) IBOutlet CHPaginatedTableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *header;
@property (strong, nonatomic) IBOutlet UIView *headerBackground;
@property (strong, nonatomic) IBOutlet PFImageView *profilePicture;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *subtitle;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UILabel *aboutMeLabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segment;

- (IBAction)segmentTapped:(id)sender;

@end
