//
//  ChangeRangeViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "PopularityViewController.h"

@interface PopularityViewController ()

@end

@implementation PopularityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //RankLoader *rankLoader = [[RankLoader alloc] initWithFrame:CGRectMake(0, 0, 220, 220)];
    
    //[self.view addSubview:rankLoader];
    //rankLoader.center = self.view.center;
    //[rankLoader addRankLoaderAnimation];
    
    [IonIcons label:self.close.titleLabel setIcon:ion_close_round size:20 color:[UIColor whiteColor] sizeToFit:YES];
    [self.close setImage:[IonIcons imageWithIcon:ion_close_round size:20 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    
    self.rankBorder.layer.cornerRadius = self.rankBorder.frame.size.width / 2;
    self.rankBorder.layer.borderColor = [UIColor whiteColor].CGColor;
    self.rankBorder.layer.borderWidth = 1.0f;
    self.rankBorder.clipsToBounds = YES;
    
    NSNumber *numberWithDouble = [NSNumber numberWithDouble:[CHKeychain doubleForKey:@"range"]];
    
    [PFCloud callFunctionInBackground:@"rank"
                       withParameters:@{
                                       @"bounds" : numberWithDouble
                                       }
                                block:^(id object, NSError *error) {
                                    
                                    NSString *stringOne = @"You are ranked ";
                                    NSString *stringtTwo = @" in your area based on Buddies in your Bubble and your Popularity Points, keep Popping!";
                                    NSString *composedString = [stringOne stringByAppendingString:[NSString stringWithFormat:@"%@", object]];
                                    
                                    self.rankDescription.text = [composedString stringByAppendingString:stringtTwo];
                                    self.rank.text = [NSString stringWithFormat:@"%@", object];
                                    
                                    [CHKeychain setObject:object forKey:@"mostCurrentRank"];
                                }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)close:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(changeRangeViewDismissed:)]) {
    
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }

}

@end
