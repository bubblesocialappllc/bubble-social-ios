//
//  ImageIntroViewController.h
//  Bubble
//
//  Created by Tulio Troncoso on 8/18/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageIntroViewController : UIViewController

- (IBAction)didPressSkip:(UIButton*)button;

@end
