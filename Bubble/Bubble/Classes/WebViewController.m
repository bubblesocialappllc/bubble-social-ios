//
//  WebViewController.m
//  Bubble
//
//  Created by Doug Woodrow on 2/17/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.title;
    
    // URL Request Object
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.loadURL]];
    
    // Load the request in the UIWebView
    [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
