//
//  CameraViewController.m
//  Bubble
//
//  Created by Doug Woodrow on 2/1/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "CameraViewController.h"

@interface CameraViewController ()

@end

@implementation CameraViewController

- (void)viewDidLayoutSubviews {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.camera.view.frame = CGRectMake(0, (screenRect.size.height/2 - screenRect.size.width/2), screenRect.size.width, screenRect.size.width);
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    self.camera = [[LLSimpleCamera alloc] initWithQuality:CameraQualityPhoto andPosition:CameraPositionBack];
    
    [self.camera attachToViewController:self withFrame:CGRectMake(0, (screenRect.size.height/2 - screenRect.size.width/2), screenRect.size.width, screenRect.size.width)];
    
    [self.view sendSubviewToBack:self.camera.view];
    
    //Double tap causes the camera mode to flip back to front or front to back
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchCameraPosition)];
    tap.numberOfTapsRequired = 2;
    
    [self.camera.view addGestureRecognizer:tap];
    
    //Checks the NSUserDefaults dictionary to find if the camera flash is on or off from the last used state
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    BOOL setFlash = [defaults boolForKey:@"cameraFlash"];
    
    //The switch of cameraFlash
    if (setFlash) {
        [self.camera updateFlashMode:CameraFlashOn];
        [self.flash setImage:[IonIcons imageWithIcon:ion_ios_bolt size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    } else {
        [self.camera updateFlashMode:CameraFlashOff];
        [self.flash setImage:[IonIcons imageWithIcon:ion_ios_bolt_outline size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    }
    
    [self.capture setImage:[IonIcons imageWithIcon:ion_aperture size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.flip setImage:[IonIcons imageWithIcon:ion_loop size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.close setImage:[IonIcons imageWithIcon:ion_close_circled size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.save setImage:[IonIcons imageWithIcon:ion_ios_download size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.use setImage:[IonIcons imageWithIcon:ion_checkmark_circled size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.closeVC setImage:[IonIcons imageWithIcon:ion_close_round size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    
    __weak typeof(self) weakSelf = self;
    [self.camera setOnDeviceChange:^(LLSimpleCamera *camera, AVCaptureDevice *device) {
        
        if ([camera isFlashAvailable]) {
            weakSelf.flash.enabled = YES;
        } else {
            weakSelf.flash.enabled = NO;
        }
        
    }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.camera start];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.camera stop];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)flash:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (self.camera.flash == CameraFlashOn) {
        [self.camera updateFlashMode:CameraFlashOff];
        [defaults setBool:NO forKey:@"cameraFlash"];
        [self.flash setImage:[IonIcons imageWithIcon:ion_ios_bolt_outline size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    }
    else {
        [self.camera updateFlashMode:CameraFlashOn];
        [defaults setBool:YES forKey:@"cameraFlash"];
        [self.flash setImage:[IonIcons imageWithIcon:ion_ios_bolt size:35 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    }
    
    [defaults synchronize];
}

- (IBAction)capture:(id)sender {
    
    [self.camera capture:^(LLSimpleCamera *camera, UIImage *image, NSDictionary *metadata, NSError *error) {
       
        if (!error) {
            
            // We should stop the camera, since we don't need it anymore.
            // This very important, otherwise you may experience memory crashes
            [camera stop];
            
            // Show the image
            if (camera.position == CameraPositionFront) {
                
                UIImage *flippedImage = [UIImage imageWithCGImage:image.CGImage
                                                 scale:image.scale
                                                 orientation:UIImageOrientationLeftMirrored];
                
                self.imageView.image = flippedImage;
                
            } else {
                
                self.imageView.image = image;
            }
            
            /*CGSize size = self.imageView.image.size;
            
            CGRect cropRect = CGRectMake(0, 50, size.width, 0.66 * size.height);
            
            CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, cropRect);
            UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            
            self.imageView.image = croppedImage;*/
            
            [self toggleButtons:YES];
        }
        
    } exactSeenImage:YES];
    
}

- (IBAction)flip:(id)sender {
    [self switchCameraPosition];
}

- (IBAction)closeImagePreview:(id)sender {
    
    self.imageView.image = nil;
    [self toggleButtons:NO];
    
    [self.camera start];
    
}

- (IBAction)saveToCameraRoll:(id)sender {
    
    [KVNProgress showWithStatus:@"Saving Photo"];
    UIImageWriteToSavedPhotosAlbum(self.imageView.image, self, @selector(savedImage:didFinishSavingWithError:contextInfo:), nil);
    
}

- (IBAction)useCapturedPhoto:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(cameraDismissedWithImage:)]) {
        [self dismissViewControllerAnimated:YES completion:^{
        
            [self.delegate cameraDismissedWithImage:self.imageView.image];
            
        }];
        
    } else {
        NSLog(@"You must implement cameraDismissedWithImage:");
    }
    
}

- (IBAction)dismiss:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(cameraDismissedWithCancel)]) {
        [self dismissViewControllerAnimated:YES completion:^{
            
            [self.delegate cameraDismissedWithCancel];
            
        }];
        
    }
    
}

#pragma mark - Helpers

//Causes the camera mode to flip front back to front or front to back
- (void)switchCameraPosition {
    [self.camera togglePosition];
}

- (void)toggleButtons:(BOOL)isAfterCapture {
    
    if (isAfterCapture) {
        
        self.flash.hidden = YES;
        self.flash.enabled = NO;
        self.capture.hidden = YES;
        self.capture.enabled = NO;
        self.flip.hidden = YES;
        self.flip.enabled = NO;
        self.imageView.hidden = NO;
        self.close.hidden = NO;
        self.close.enabled = YES;
        self.save.hidden = NO;
        self.save.enabled = YES;
        self.use.hidden = NO;
        self.use.enabled = YES;
        self.camera.view.hidden = YES;
        
    } else {
        
        self.flash.hidden = NO;
        self.flash.enabled = YES;
        self.capture.hidden = NO;
        self.capture.enabled = YES;
        self.flip.hidden = NO;
        self.flip.enabled = YES;
        self.imageView.hidden = YES;
        self.close.hidden = YES;
        self.close.enabled = NO;
        self.save.hidden = YES;
        self.save.enabled = NO;
        self.use.hidden = YES;
        self.use.enabled = NO;
        self.camera.view.hidden = NO;
    }
    
}

- (void)savedImage:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    if (error) {
        [KVNProgress showErrorWithStatus:error.localizedDescription];
    } else  {
        [KVNProgress showSuccess];
    }
    
}

@end
