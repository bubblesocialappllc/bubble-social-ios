//
//  LocationManagerViewController.m
//  Bubble
//
//  Created by Doug Woodrow on 3/1/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "LocationManagerViewController.h"

static NSInteger const mile = 1609;

@interface LocationManagerViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableArray *data;
@property (strong, nonatomic) SVPulsingAnnotationView *pulsingView;
@property (strong, nonatomic) UIButton *add;
@property (strong, nonatomic) UITextField *textField;
@property (nonatomic) BOOL hasMoved;

@end

@implementation LocationManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Where are you?";
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.data = [NSMutableArray new];

    [BSPlace findPlacesNearbyWithCurrentLocationWithCompletion:^(NSMutableArray *locationsNearby, NSError *error) {
        
        //Handles locations nearby
        BSPlace *place = [BSPlace new];
        place.name = self.currentCity;
        place.location = self.appDelegate.currentLocation;
        
        //Placeholder for the first cell
        BSPlace *placeholder = [BSPlace new];
        
        [self.data insertObject:placeholder atIndex:0]; //Adds a placeholder object in the array that has no data, but just acts as a placeholder for the Add a Place cell
        [self.data insertObject:place atIndex:1]; //Adds the city into the second array index
        [self.data addObjectsFromArray:locationsNearby]; //Adds all other locations into the array
        [self.tableView reloadData];
        
    }];
    
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, 275)];
    self.mapView.delegate = self;
    self.mapView.scrollEnabled = NO;
    self.mapView.rotateEnabled = NO;
    self.mapView.showsUserLocation = YES;
    [self.view addSubview:self.mapView];
    [self.view bringSubviewToFront:self.tableView];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(self.mapView.frame.size.height-20,0,0,0)];
    [self.tableView setScrollIndicatorInsets:self.tableView.contentInset];
    self.tableView.backgroundColor = [UIColor clearColor];
    
}

- (void)setMapRegion {
    MKUserLocation *userLocation = self.mapView.userLocation;
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, mile, mile);
    
    [self.mapView setRegion:region animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [self.textField resignFirstResponder];
    
}

#pragma mark - TableView Delegate/DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) { //Return the Add a Place cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addLocationCell" forIndexPath:indexPath];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = self.navigationController.navigationBar.barTintColor;
        
        return cell;
        
    } else if (indexPath.row == 1) { //Return the Locality cell
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"localityCell" forIndexPath:indexPath]; //We add another cell so that we always have room for the add location cell
        
        // Configure the cell...
        BSPlace *place = [self.data objectAtIndex:indexPath.row];
        
        if (place.vicinity == nil) {
            place.vicinity = @"";
        }
        
        cell.textLabel.text = place.name;
        cell.textLabel.textColor = self.navigationController.navigationBar.barTintColor;
        
        return cell;

    } else { //Return a location cell otherwise
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath]; //We add another cell so that we always have room for the add location cell
        
        // Configure the cell...
        BSPlace *place = [self.data objectAtIndex:indexPath.row];
        
        if (place.vicinity == nil) {
            place.vicinity = @"";
        }
        
        PFGeoPoint *location = place.location;
        
        CGFloat miles = [location distanceInMilesTo:self.appDelegate.currentLocation];
        
        NSInteger feet = miles * 5280;
        
        NSString *distance = [NSString stringWithFormat:@"%zdft", feet];
        
        cell.textLabel.text = place.name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@  %@",place.vicinity, distance];
        
        return cell;

    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        if (!self.hasMoved) {
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            self.textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 300, 44)];
            
            self.textField.delegate = self;
            self.textField.backgroundColor = [UIColor clearColor];
            self.textField.textColor = [UIColor whiteColor];
            self.textField.borderStyle = UITextBorderStyleNone;
            self.textField.tintColor = [UIColor whiteColor];
            self.textField.autocorrectionType = UITextAutocapitalizationTypeWords;
            
            self.add = [[UIButton alloc] initWithFrame:CGRectMake(self.textField.frame.size.width + 30, 0, 44, 44)];
            
            [self.add addTarget:self action:@selector(addCustomPlace) forControlEvents:UIControlEventTouchUpInside];
            
            [self.add setImage:[IonIcons imageWithIcon:ion_android_add_circle size:40 color:[UIColor whiteColor]] forState:UIControlStateNormal];
            self.add.alpha = 0;
            
            [cell addSubview:self.textField];
            [cell addSubview:self.add];
            
            POPBasicAnimation *slide = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionX];
            
            slide.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            slide.toValue = @([UIScreen mainScreen].applicationFrame.size.width + cell.textLabel.frame.size.width + 10);
            
            [cell.textLabel.layer pop_addAnimation:slide forKey:@"slide"];
            
            [self.textField becomeFirstResponder];
            
            self.hasMoved = YES;
        }
        
    } else {
    
        __strong BSPlace *place = [self.data objectAtIndex:indexPath.row];
        
        [self.delegate locationManagerDidChoosePlace:place];
        
        self.label.text = place.name;
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}

#pragma mark - MapView Delegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [self setMapRegion];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    static NSString *identifier = @"currentLocation";
    self.pulsingView = (SVPulsingAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (self.pulsingView == nil) {
        self.pulsingView = [[SVPulsingAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        self.pulsingView.annotationColor = self.navigationController.navigationBar.barTintColor;
    }
    
    self.pulsingView.canShowCallout = YES;
    
    return self.pulsingView;
}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger length = textField.text.length + string.length;
    
    if (string.length == 0) {
        length--;
    }
    
    if (length == 3) {
        
        if (self.add.alpha == 0.0) {
            
            POPBasicAnimation *fade = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
            
            fade.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            fade.fromValue = @(0.0);
            fade.toValue = @(1.0);
            
            [self.add pop_addAnimation:fade forKey:@"fade"];
            
        }
        
    } else if (length < 3) {
        
        if (self.add.alpha == 1.0) {
            
            POPBasicAnimation *fade = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
            
            fade.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            fade.fromValue = @(1.0);
            fade.toValue = @(0.0);
            
            [self.add pop_addAnimation:fade forKey:@"fade"];
        }
        
    }
    
    return YES;
    
}

#pragma mark - Helpers

- (void)addCustomPlace {
    
    [BSPlace addPlaceAtCurrentLocationWithName:self.textField.text completion:^(BSPlace *place, NSError *error) {
        
        [self.delegate locationManagerDidChoosePlace:place];
        
        self.label.text = place.name;
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
}

@end
