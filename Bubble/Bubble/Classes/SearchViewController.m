//
//  SearchViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/6/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "SearchViewController.h"
#import "CHSearch.h"

@interface SearchViewController ()

@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) AppDelegate *appDelegate;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [self.emptySearch setImage:[UIImage imageNamed:@"searchGlass"]];
    
    [self.searchBar setImage:[UIImage new] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [self.searchController.searchResultsTableView registerClass:[SearchTableViewCell class] forCellReuseIdentifier:@"SearchRow"];
    [self.searchController.searchResultsTableView registerNib:[UINib nibWithNibName:@"SearchTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SearchRow"];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchBar becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellAnimation"]) {
        cell.alpha = 0;
        
        [UIView beginAnimations:@"fade" context:NULL];
        [UIView setAnimationDuration:0.8];
        cell.alpha = 1;
        [UIView commitAnimations];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.users.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SearchTableViewCell *cell = nil;
    static NSString *identifier = @"SearchRow";
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell = [[SearchTableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    CHSearch *searchedUsers = [self.users objectAtIndex:indexPath.row];
    
    cell.index = indexPath.row;
    
    cell.profilePicture.clipsToBounds = YES;
    cell.profilePicture.layer.cornerRadius = cell.profilePicture.frame.size.height / 2;
    [cell.profilePicture setImageWithString:searchedUsers.realName color:[UIColor bubblePink]];
    cell.profilePicture.file = searchedUsers.user.profilePicture;
    [cell.profilePicture loadInBackground];
    
    cell.username.text = searchedUsers.name;
    
    [BSLocation cityNameWithLocation:searchedUsers.user.location completion:^(NSString *cityName, NSError *error) {
       
        cell.location.text = cityName;
        
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    CHSearch *searchedUser = [self.users objectAtIndex:indexPath.row];
    
    BSUser *selectedUser = searchedUser.user;
    
    if (![selectedUser.username isEqualToString:self.appDelegate.currentUser.username]) {
        
        UserViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
        user.user = selectedUser;
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:user];
        
        [self presentViewController:nc animated:YES completion:nil];
        
    }
    
}

#pragma mark - SearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    searchText = [searchText lowercaseString];
    
    PFQuery *userQuery = [BSUser query];
    [userQuery whereKey:@"username" hasPrefix:searchText];
    [userQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        self.users = [CHSearch filteredUsernamesFromUsers:objects withSubstring:searchText];
        
        [self.searchController.searchResultsTableView reloadData];
    }];
    
}

@end
