//
//  WebViewController.h
//  Bubble
//
//  Created by Doug Woodrow on 2/17/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *loadURL;

@end
