//
//  ChatSearchViewController.m
//  Bubble
//
//  Created by Chayel Heinsen on 1/30/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "ChatSearchViewController.h"
#import "CHSearch.h"
#import "SearchTableViewCell.h"

@interface ChatSearchViewController ()

@property (strong, nonatomic) NSMutableArray *users;

@end

@implementation ChatSearchViewController

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.searchBar setImage:[UIImage new] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [self.searchDisplayController.searchResultsTableView registerClass:[SearchTableViewCell class] forCellReuseIdentifier:@"SearchRow"];
    [self.searchDisplayController.searchResultsTableView registerNib:[UINib nibWithNibName:@"SearchTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SearchRow"];
    //[self.searchDisplayController.searchResultsTableView setAllowsSelection:NO];
    
}

#pragma GCC diagnostic pop

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.searchBar becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.users.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SearchTableViewCell *cell = nil;
    static NSString *identifier = @"SearchRow";
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell = [[SearchTableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    CHSearch *searchedUsers = [self.users objectAtIndex:indexPath.row];
    
    cell.index = indexPath.row;
    
    cell.profilePicture.clipsToBounds = YES;
    cell.profilePicture.layer.cornerRadius = cell.profilePicture.frame.size.height / 2;
    [cell.profilePicture setImageWithString:searchedUsers.realName color:[UIColor bubblePink]];
    cell.profilePicture.file = searchedUsers.user.profilePicture;
    [cell.profilePicture loadInBackground];

    cell.username.text = searchedUsers.name;
    
    [BSLocation cityNameWithLocation:searchedUsers.user.location completion:^(NSString *cityName, NSError *error) {
        
        cell.location.text = cityName;
        
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    CHSearch *chosenUser = [self.users objectAtIndex:indexPath.row];
    
    PFQuery *userQuery = [BSUser query];
    [userQuery whereKey:@"username" equalTo:chosenUser.name];
    [userQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        [self.delegate chosenUser:[objects firstObject]];
        
    }];
    
}

#pragma mark - SearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    searchText = [searchText lowercaseString];
    
    PFQuery *userQuery = [BSUser query];
    [userQuery whereKey:@"username" hasPrefix:searchText];
    [userQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        self.users = [CHSearch filteredUsernamesFromUsers:objects withSubstring:searchText];
        
        [self.searchBarController.searchResultsTableView reloadData];
    }];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
