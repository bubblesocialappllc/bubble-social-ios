//
//  BSSegmentedControl.swift
//  Bubble
//
//  Created by Chayel Heinsen on 3/16/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

/*

    Here is an example of how to use this class.

    Objective-C:
    
    BSSegmentedControl *segmentControl = [[BSSegmentedControl alloc] initWithFrame:CGRectMake(50, 300, 320, 50)];

    // THIS MUST BE CALLED BEFORE CUSTOMIZING!
    [segmentControl setItems:@[@"First", @"Second"]];

    segmentControl.unselectedFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
    segmentControl.selectedFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    segmentControl.unselectedTextColor = [UIColor colorWithWhite:1.0 alpha:0.8];
    segmentControl.selectedTextColor = [UIColor whiteColor];
    segmentControl.unselectedColor = [UIColor colorWithHex:@"FF1AC7"];
    segmentControl.selectedColor = [UIColor bubblePink];
    segmentControl.cursor.fillColor = [UIColor bubblePink];
    [segmentControl addTarget:self action:@selector(tapped:) forControlEvents:UIControlEventValueChanged];

    [self.view addSubview:segmentControl];
    

    - (void)tapped:(BSSegmentedControl *)sender {
        NSLog(@"selected index : %zd", sender.selectedIndex);
    }

    Swift:

    var myElements = ["First","Second"]
    var segmentControl : BSSegmentedControl = BSSegmentedControl(frame: CGRectMake(50,300,320,50))

    // THIS MUST BE CALLED BEFORE CUSTOMIZING!
    segmentControl.setItems(myElements)

    segmentControl.unselectedFont = UIFont(name: "HelveticaNeue-Light", size: 16)
    segmentControl.selectedFont = UIFont(name: "HelveticaNeue-Bold", size: 16)
    segmentControl.unselectedTextColor = UIColor(white: 1, alpha: 0.8)
    segmentControl.unselectedColor = UIColor(red: 10/255, green: 137/255, blue: 169/255, alpha: 0.8)
    segmentControl.selectedTextColor = UIColor(white: 1, alpha: 1)
    segmentControl.selectedColor = UIColor(red: 10/255, green: 137/255, blue: 169/255, alpha: 1)
    segmentControl.cursor.fillColor = UIColor.redColor()
    segmentControl.addTarget(self, action: "tapped:", forControlEvents: UIControlEvents.ValueChanged)

    self.view.addSubview(segmentControl)
    
    func tapped(sender: BSSegmentedControl) {
        println("Selected Index : \(self.selectedIndex())")
    }

*/

import UIKit

@objc @IBDesignable class BSSegmentedControl: UIControl {
    
    //MARK: - Private Properties
    
    private var views = [UIView]()
    private var labels = [UILabel]()
    private var animationChecks = [Bool]()
    private var items : [String]!
    private var currentIndex : Int = 0
    private var cursorCenterXConstraint : NSLayoutConstraint!
    private var tapGestureRecognizer : UITapGestureRecognizer!
    
    //MARK: - Public Properties
    
    /// The cursor under the segment control. If you wish to change the color, edit the fillColor property. If you need to change the size, edit its width and height with its x and y at 0. Defaults to {0,0,15,8}
    var cursor : TabIndicatorView? = TabIndicatorView(frame: CGRectMake(0, 0, 15, 8)) {
        didSet {
            
            if views.count > 0 {
                self.setItems(items)
                
                if let cur = oldValue {
                    cur.removeFromSuperview()
                }
            }
        }
    }
    
    /// The background color of the selected index. Defaults to BubblePink.
    @IBInspectable var selectedColor : UIColor? = UIColor.bubblePink() {
        didSet {
            
            if self.currentIndex <= views.count - 1 && self.currentIndex >= 0 {
                let view = views[self.currentIndex]
                view.backgroundColor = self.selectedColor
            }
        }
    }
    
    /// The background coor of the unselected index. Defaults to WhiteColor.
    @IBInspectable var unselectedColor : UIColor? = UIColor.whiteColor() {
        didSet {
            
            for index in 0..<views.count {
                
                if index != self.currentIndex {
                    let view = views[index]
                    view.backgroundColor = self.unselectedColor
                }
            }
        }
    }
    
    /// The text color of the selected index. Defaults to WhiteColor.
    @IBInspectable var selectedTextColor : UIColor? = UIColor.whiteColor() {
        didSet {
            
            if self.currentIndex <= views.count - 1 && self.currentIndex >= 0 {
                let lab = labels[self.currentIndex]
                lab.textColor = self.selectedTextColor
            }
        }
    }
    
    /// The text color of the unselected index. Defaults to BubblePink.
    @IBInspectable var unselectedTextColor : UIColor? = UIColor.bubblePink() {
        didSet {
           
            for index in 0..<views.count {
                
                if index != self.currentIndex {
                    let lab = labels[index]
                    lab.textColor = self.unselectedTextColor
                }
            }
        }
    }
    
    /// The font of the selected index. Defaults to HelveticaNeue-Bold.
    var selectedFont : UIFont? = UIFont(name: "HelveticaNeue-Bold", size: 16) {
        didSet {
           
            if self.currentIndex <= views.count - 1 && self.currentIndex >= 0 {
                let lab = labels[self.currentIndex]
                lab.font = self.selectedFont
            }
        }
    }
    
    /// The font of the unselected index. Defaults to HelveticaNeue-Light.
    var unselectedFont : UIFont? = UIFont(name: "HelveticaNeue-Light", size: 16) {
        didSet {
            
            for index in 0..<views.count {
               
                if index != self.currentIndex {
                    let lab = labels[index]
                    lab.font = self.unselectedFont
                }
            }
        }
    }
    
    /// The width of the border. Set to 0 if you don't want a border. Defaults to 1.0.
    @IBInspectable @objc var borderWidth : CGFloat = 1.0 {
        didSet {
            
            if views.count > 0 {
                
                for view in views {
                    
                    view.layer.borderColor = borderColor.CGColor
                    view.layer.borderWidth = borderWidth
                }
            }
        }
    }
    
    /// The color of the border. Defaults to BubblePink.
    @IBInspectable @objc var borderColor : UIColor = UIColor.bubblePink() {
        didSet {
            
            if views.count > 0 {
                
                for view in views {
                    
                    view.layer.borderColor = borderColor.CGColor
                    view.layer.borderWidth = borderWidth
                }
            }
        }
    }
    
    //MARK: - Overides
    
    init() {
        super.init(frame: CGRectZero)
        self.initComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initComponents()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initComponents()
    }
    
    
    //MARK: - Internal Methods
    
    // Internal access enables entities to be used within any source file from their defining module, but not in any source file outside of that module. You typically use internal access when defining an app’s or a framework’s internal structure.
    
    internal func didTap(recognizer:UITapGestureRecognizer) {
        
        if recognizer.state == UIGestureRecognizerState.Ended {
            
            let currentPoint = recognizer.locationInView(self)
            let index = indexFromPoint(currentPoint)
            selectCell(index, animate: true)
            self.sendActionsForControlEvents(UIControlEvents.ValueChanged)
        }
    }
    
    //MARK: - Private Methods
    
    // Private access restricts the use of an entity to its own defining source file. Use private access to hide the implementation details of a specific piece of functionality.
    
    private func initComponents() {
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "didTap:")
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    private func indexFromPoint(point:CGPoint) -> Int {
        
        for pos in 0..<views.count {
            
            let view = views[pos]
            
            if point.x >= view.frame.minX && point.x < view.frame.maxX {
                return pos
            }
        }
        
        return 0
    }
    
    //MARK: - Public Methods
    
    /**
    Sets the items on the segement control. You must call this first before customizing the view.
    
    - parameter items: The names of the items.
    */
    func setItems(items:[String]) {
        
        self.items = items
        var previousView : UIView?
        
        for lab in labels {
            lab.removeFromSuperview()
        }
        
        for view in views {
            view.removeFromSuperview()
        }
        
        labels.removeAll(keepCapacity: false)
        views.removeAll(keepCapacity: false)
        
        for i in 0..<items.count {
            
            let view = UIView()
            let label = UILabel()
            
            view.backgroundColor = unselectedColor
            self.addSubview(view)
            
            views.append(view)
           
            label.text = items[i]
            label.textColor = unselectedTextColor
          
            if let font = unselectedFont {
                label.font = font
            }
            view.layer.borderWidth = borderWidth;
            view.layer.borderColor = selectedColor?.CGColor
            
            view.addSubview(label)
            labels.append(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            
            var itemHeight = self.frame.height
            
            if let cur = self.cursor {
                itemHeight -= cur.frame.height
            }
            
            let centerXConstraint = NSLayoutConstraint(item:label,
                attribute:NSLayoutAttribute.CenterX,
                relatedBy:NSLayoutRelation.Equal,
                toItem:view,
                attribute:NSLayoutAttribute.CenterX,
                multiplier:1,
                constant:0)
            view.addConstraint(centerXConstraint)
            
            let centerYConstraint = NSLayoutConstraint(item:label,
                attribute:NSLayoutAttribute.CenterY,
                relatedBy:NSLayoutRelation.Equal,
                toItem:view,
                attribute:NSLayoutAttribute.CenterY,
                multiplier:1,
                constant:0)
            view.addConstraint(centerYConstraint)
            
            view.translatesAutoresizingMaskIntoConstraints = false
            
            let viewDict = [ "view" : view ] as Dictionary<String,AnyObject>
            
            let constraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[view(\(itemHeight))]", options: [], metrics: nil, views: viewDict)
            
            view.addConstraints(constraints)
            
            if let previous = previousView {
                
                let leftConstraint = NSLayoutConstraint(item:view,
                    attribute:NSLayoutAttribute.Left,
                    relatedBy:NSLayoutRelation.Equal,
                    toItem:previous,
                    attribute:NSLayoutAttribute.Right,
                    multiplier:1,
                    constant:0)
                self.addConstraint(leftConstraint)
                
                let widthConstraint = NSLayoutConstraint(item:view,
                    attribute:NSLayoutAttribute.Width,
                    relatedBy:NSLayoutRelation.Equal,
                    toItem:previous,
                    attribute:NSLayoutAttribute.Width,
                    multiplier:1,
                    constant:0)
                self.addConstraint(widthConstraint)
            
            } else {
                
                let leftConstraint = NSLayoutConstraint(item:view,
                    attribute:NSLayoutAttribute.Left,
                    relatedBy:NSLayoutRelation.Equal,
                    toItem:self,
                    attribute:NSLayoutAttribute.Left,
                    multiplier:1.0,
                    constant:0)
                self.addConstraint(leftConstraint)
            }
            
            let topConstraint = NSLayoutConstraint(item:view,
                attribute:NSLayoutAttribute.Top,
                relatedBy:NSLayoutRelation.Equal,
                toItem:self,
                attribute:NSLayoutAttribute.Top,
                multiplier:1.0,
                constant:0)
            
            self.addConstraint(topConstraint)
            previousView = view
        }
        
        if let previous = previousView {
            
            let leftConstraint = NSLayoutConstraint(item:previous,
                attribute:NSLayoutAttribute.Right,
                relatedBy:NSLayoutRelation.Equal,
                toItem:self,
                attribute:NSLayoutAttribute.Right,
                multiplier:1.0,
                constant:0)
            self.addConstraint(leftConstraint)
        }
        
        if let cur = cursor {
            
            cur.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(cur)
            
            let bottomConstraint = NSLayoutConstraint(item:cur,
                attribute:NSLayoutAttribute.Bottom,
                relatedBy:NSLayoutRelation.Equal,
                toItem:self,
                attribute:NSLayoutAttribute.Bottom,
                multiplier:1.0,
                constant:0)
            self.addConstraint(bottomConstraint)
            
            cursorCenterXConstraint = NSLayoutConstraint(item:cur,
                attribute:NSLayoutAttribute.CenterX,
                relatedBy:NSLayoutRelation.Equal,
                toItem:self,
                attribute:NSLayoutAttribute.CenterX,
                multiplier:1.0,
                constant:0)
            self.addConstraint(cursorCenterXConstraint)
            
            let viewDict = [ "cursor" : cur ] as Dictionary<String,AnyObject>
            
            var constraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[cursor(\(cur.frame.height))]", options: [], metrics: nil, views: viewDict)
            cur.addConstraints(constraints)
            constraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[cursor(\(cur.frame.width))]", options: [], metrics: nil, views: viewDict)
            cur.addConstraints(constraints)
        }
        
        selectCell(currentIndex,animate: false)
    }
    
    /**
    Selects the cell.
    
    - parameter index:   The index to select. The first index is 0.
    - parameter animate: YES to animate, otherwise NO
    */
    func selectCell(index:Int, animate:Bool) {
        
        let newView = views[index]
        let newLabel = labels[index]
        let oldView = views[currentIndex]
        let oldLabel = labels[currentIndex]
        var duration:NSTimeInterval = 0
      
        if animate {
            duration = 0.4
        }
        
        if (duration == 0 || index != currentIndex) && index < items.count {
            
            UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: [.CurveEaseInOut, .AllowUserInteraction], animations: {
                
                    self.animationChecks.append(true)
                    oldView.backgroundColor = self.unselectedColor
                    oldLabel.textColor = self.unselectedTextColor
                
                    if let font = self.unselectedFont {
                        oldLabel.font = font
                    }
                
                    newView.backgroundColor = self.selectedColor
                    newLabel.textColor = self.selectedTextColor
                
                    if let font = self.selectedFont {
                        newLabel.font = font
                    }
                
                    if let cur = self.cursor {
                        cur.center.x = newView.center.x
                    }
                },
                
                completion: { finished in
                   
                    if self.animationChecks.count == 1 {
                        
                        if let cur = self.cursor {
                            
                            self.removeConstraint(self.cursorCenterXConstraint)
                            self.cursorCenterXConstraint = NSLayoutConstraint(item:cur,
                                attribute:NSLayoutAttribute.CenterX,
                                relatedBy:NSLayoutRelation.Equal,
                                toItem:newView,
                                attribute:NSLayoutAttribute.CenterX,
                                multiplier:1.0,
                                constant:0)
                            self.addConstraint(self.cursorCenterXConstraint)
                        }
                        
                    }
                    
                    self.animationChecks.removeLast()
                    
            })
            
            currentIndex = index
        }
    }
    
    /**
    Gets the current selected index.
    
    - returns: The current selected index.
    */
    func selectedIndex() -> Int {
        return self.currentIndex
    }

}
