//
//  RangeView.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "RangeView.h"

@implementation RangeView

- (void)setLabelValue:(NSString *)value {
    
    if (!value) {
        value = @"0";
    }
    
    if ([value length] == 1) {
        
        self.label.frame = CGRectMake(10, 4, 30, 21);
        self.label.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
        
    } else if ([value length] == 4) {
        
        self.label.frame = CGRectMake(4, 4, 40, 21);
        self.label.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
        
    } else {
        
        self.label.frame = CGRectMake(5, 4, 30, 21);
        self.label.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
        
    }
    
    if ([value isEqualToString:@"0.25"]) {
        
        value = @"1/4";
        
    } else if ([value isEqualToString:@"0.50"]) {
        value = @"1/2";
    }
    
    if ([value integerValue] > 999999) {
        NSInteger v = [value integerValue] / 1000000;
        
        if (v > 9)
            self.label.frame = CGRectMake(3, 4, 30, 21);
        else
            self.label.frame = CGRectMake(5, 4, 30, 21);
        
        self.label.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        value = [NSString stringWithFormat:@"%zdM", v];
        self.label.text = value;
        return;
    }
    
    if ([value integerValue] > 999) {
        NSInteger v = [value integerValue] / 1000;
        
        if (v > 9)
            self.label.frame = CGRectMake(3, 4, 30, 21);
        else
            self.label.frame = CGRectMake(5, 4, 30, 21);
        
        
        self.label.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        value = [NSString stringWithFormat:@"%zdK", v];
        self.label.text = value;
        return;
    }
    
    if ([value integerValue] > 99) {
        self.label.frame = CGRectMake(3, 4, 30, 21);
        self.label.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        
    }
    
    self.label.text = value;
    
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
            // Initialization code
        
        [self addSubview:[[[NSBundle mainBundle] loadNibNamed:@"RoundRangeView" owner:self options:nil] objectAtIndex:0]];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(4, 5, 30, 21)];
        self.label.textColor = [UIColor bubblePink];
        
        [self addSubview:self.label];
        
    }
    return self;
}


@end
