//
//  BubbleAnnotation.h
//  Bubble
//
//  Created by Tulio Troncoso on 8/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface BubbleAnnotation : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;

- (id)initWithLocation:(CLLocationCoordinate2D)location;
+ (UIImage *)imageForAnnotationView;

@end
