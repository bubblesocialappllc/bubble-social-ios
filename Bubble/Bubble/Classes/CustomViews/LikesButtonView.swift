//
//  LikesButtonView.swift
//  Bubble
//
//  Created by Doug Woodrow on 25/10/2014.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

import UIKit

/**
*  This view should be 69x28
*/
@objc @IBDesignable public class LikesButtonView: UIView {
    
    @IBInspectable var roundness: CGFloat = 5.0 {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable public var image: UIImage? {
        didSet {
            setupView()
        }
    }
    
    var likesCountLabel: UILabel = UILabel(frame: CGRectMake(47, 3, 21, 21))
    
    var imageView: UIImageView = UIImageView(frame: CGRectMake(8, 7, 15, 15))
    
    var dot: UILabel = UILabel(frame: CGRectMake(30, 3, 10, 21));
    
    private var target: AnyObject?
    
    private var action: Selector?
    
    private var tap: UITapGestureRecognizer?
    
    private func setupView() {
        
        self.layer.cornerRadius = roundness
        
        self.backgroundColor = (UIColor.bubblePink() as UIColor)
        
        imageView.image = image
        
        dot.font = UIFont(name: "HelveticaNeue", size: 30);
        dot.textColor = UIColor.whiteColor()
        dot.text = "•"
        
        likesCountLabel.font = UIFont(name: "HelveticaNeue", size: 12);
        likesCountLabel.textColor = UIColor.whiteColor()
        likesCountLabel.text = "0"
        
        self.addSubview(imageView);
        self.addSubview(dot);
        self.addSubview(likesCountLabel)
        
        self.setNeedsDisplay()
        
    }
    
    /**
    Enables the ability to click on the view
    
    - parameter target:    The class where the action is defined.
    - parameter andAction: The method to call on the target.
    */
    @objc func setTarget(target: AnyObject, andAction: Selector) {
        
        self.target = target
        self.action = andAction
        
        self.tap = UITapGestureRecognizer(target: self.target!, action: self.action!)
        
        self.addGestureRecognizer(self.tap!);
        
    }
    
    /**
    Enables or Disables the button. Must first call setTarget:target:AndAction
    
    - parameter enabled: YES/true to Enable, otherwise NO/false
    */
    @objc func setEnabled(enabled: Bool) {
        
        if enabled {
            
            self.tap = UITapGestureRecognizer(target: self.target!, action: self.action!)
            
            self.addGestureRecognizer(self.tap!);
            
        } else {
            
            self.removeGestureRecognizer(self.tap!)
            
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupView()
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
