//
//  RankLoader.h
//
//  Code generated using QuartzCode 1.36.2 on 8/15/15.
//  www.quartzcodeapp.com
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface RankLoader : UIView

- (void)addRankLoaderAnimation;
- (void)removeAnimationsForAnimationId:(NSString *)identifier;
- (void)removeAllAnimations;

@end
