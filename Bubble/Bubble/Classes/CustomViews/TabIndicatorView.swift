//
//  TabIndicatorView.swift
//  Bubble
//
//  Created by Chayel Heinsen on 3/16/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit
import Parse

/**
*  This view was made for the cursor view of BSSegmentedControl
*/
class TabIndicatorView: UIView {
    
    init() {
        super.init(frame: CGRectZero)
        self.backgroundColor = UIColor.clearColor()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clearColor()
    }
    
    var fillColor : UIColor = UIColor.bubblePink() {
        didSet {
            fillColor.getRed(&red, green: &green, blue: &blue, alpha: &alphaColor)
            setNeedsDisplay()
        }
    }
    
    var red : CGFloat = 0, green : CGFloat = 0, blue : CGFloat = 0, alphaColor : CGFloat = 0
    
    override func drawRect(rect: CGRect) {
        
        let ctx : CGContextRef = UIGraphicsGetCurrentContext()!
        
        CGContextBeginPath(ctx)
        CGContextMoveToPoint(ctx, CGRectGetMinX(rect), CGRectGetMinY(rect));  // top left
        CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMinY(rect));  // top right
        CGContextAddLineToPoint(ctx, CGRectGetMidX(rect), CGRectGetMaxY(rect));  // bottom point
        CGContextClosePath(ctx)
        CGContextSetRGBFillColor(ctx, red, green, blue, alphaColor)
        CGContextFillPath(ctx)
        
    }
    

}