//
//  RangeView.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RangeView : UIView

@property (strong, nonatomic) UILabel *label;

- (void)setLabelValue:(NSString *)value;

@end
