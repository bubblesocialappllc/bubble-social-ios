//
//  BubbleNavigationController.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RangeView.h"

@interface BubbleNavigationController : UINavigationController

@property (strong, nonatomic) RangeView *range;

@end
