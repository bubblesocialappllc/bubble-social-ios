//
//  BSAlertView.h
//  Bubble
//
//  Created by Chayel Heinsen on 1/27/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, BSAlertStyle) {
    BSAlertStyleError = 0,
    BSAlertStyleSuccess,
    BSAlertStyleInfo
};

typedef void(^BSAlertViewCompletion)(void);

@interface BSAlertView : UIView

/**
 *  The style of the alert. Should be either BSAlertStyleError, BSAlertStyleSuccess, BSAlertStyleInfo.
 */
@property (nonatomic) BSAlertStyle style;

/**
 *  The title of the alert.
 */
@property (nonatomic, copy) NSString *title;

/**
 *  The message of the alert.
 */
@property (nonatomic, copy) NSString *message;

/**
 *  YES if the alert is currently visable, otherwise NO.
 */
@property (nonatomic, readonly) BOOL visible;

/**
 *  The font of the text. Defaults to system font.
 */
@property (strong, nonatomic) UIFont *font;

/**
 *  The alignment of the text. Defaults to NSTextAlignmentLeft.
 */
@property (nonatomic) NSTextAlignment textAlignment;

/**
 *   The amount of time the alert should be shown. Defaults to 3 seconds.
 */
@property (nonatomic) CGFloat alertDuration;

/**
 *  The amount of time the animations should be. Defaults to 0.5 seconds.
 */
@property (nonatomic) CGFloat animationDuration;

/**
 *  The color for the alert.
 */
@property (strong, nonatomic) UIColor *color;

/**
 *  The IonIcon to use for the icon. Defaults are set for all BSAlertStyle's.
 */
@property (strong, nonatomic) NSString *icon;

/**
 *  The size of the IonIcon. Defaults to 20.
 */
@property (nonatomic) CGFloat iconSize;

/**
 *  The target.
 */
@property (strong, nonatomic) id target;

/**
 *  The selector to call on the target; The selector that will be called should look like @code - (returnType)action:(id)sender
 */
@property (nonatomic) SEL action;

/**
 *  The dictionary that was received from the remote notification.
 */
@property (strong, nonatomic) NSDictionary *userInfo;

/**
 *  Initializes an alert with a style and message.
 *
 *  @param style   The style to use for the alert.
 *  @param message The message to be displayed.
 *  @param target  The target;
 *  @param action  The selector to call on the target.
 *
 *  @return A newly initialized alert.
 */
- (id)initWithStyle:(BSAlertStyle)style message:(NSString *)message target:(id)target action:(SEL)action;

/**
 *  Initializes an alert with a style and message.
 *
 *  @param style   The style to use for the alert.
 *  @param title   The title to be displayed.
 *  @param message The message to be displayed.
 *  @param target  The target;
 *  @param action  The selector to call on the target.
 *
 *  @return A newly initialized alert.
 */
- (id)initWithStyle:(BSAlertStyle)style title:(NSString *)title message:(NSString *)message target:(id)target action:(SEL)action;

/**
 *  Initializes an alert with a style and message.
 *
 *  @param style    The style to use for the alert.
 *  @param title    The title to be displayed.
 *  @param message  The message to be displayed
 *  @param icon     The IonIcon to be deplayed.
 *  @param iconSize The size of the icon.
 *  @param target  The target;
 *  @param action  The selector to call on the target.
 *
 *  @return The message to be displayed
 */
- (id)initWithStyle:(BSAlertStyle)style title:(NSString *)title message:(NSString *)message icon:(NSString *)icon iconSize:(CGFloat)iconSize target:(id)target action:(SEL)action;

/**
 *  Hides the alert.
 */
- (void)hide;

/**
 *  Shows the alert.
 */
- (void)show;

/**
 *  Shows the alert with completion block.
 *
 *  @param block The block that is called when we have finished showing the alert.
 */
- (void)showWithCompletion:(BSAlertViewCompletion)block;

@end
