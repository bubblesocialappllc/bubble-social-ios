//
//  BubbleAnnotation.m
//  Bubble
//
//  Created by Tulio Troncoso on 8/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BubbleAnnotation.h"

@implementation BubbleAnnotation

- (id)initWithLocation:(CLLocationCoordinate2D)coord {
    self = [super init];
    if (self) {
        self.coordinate = coord;
    }
    return self;
}

+ (UIImage *)imageForAnnotationView {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, 0.0f);

    UIColor* color = [UIColor whiteColor];
    UIColor* color2 = [UIColor bubblePink];

    //// Oval Drawing
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectInset(CGRectMake(0, 0, 30, 30), 2, 2)];
    [color setFill];
    [ovalPath fill];
    [color2 setStroke];
    ovalPath.lineWidth = 1;
    [ovalPath stroke];


    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

@end
