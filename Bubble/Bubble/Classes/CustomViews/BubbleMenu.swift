//
//  BubbleMenu.swift
//  Bubble
//
//  Created by Chayel Heinsen on 12/20/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

import Foundation
import UIKit

struct BubbleMenuPage {
    static var last: Int = 1
}

protocol BubbleMenuDelegate {
    
    /**
    Informs the delegate that Bubble Menu is done configuring as is able to be manipulated. If you wish to change the current page, call setPage:index here.
    
    - parameter menu: The Bubble Menu
    */
    func menuDidFinishConfiguring(menu: BubbleMenu)
    
}

@objc class MenuItemView: UIView {
    
    var titleLabel : UILabel?
    
    func setUpMenuItemView(menuItemWidth: CGFloat, menuScrollViewHeight: CGFloat, indicatorHeight: CGFloat) {
        
        titleLabel = UILabel(frame: CGRectMake(0.0, 0.0, menuItemWidth, menuScrollViewHeight - indicatorHeight))
        
        self.addSubview(titleLabel!)
    }
    
    func setTitleText(text: NSString) {
        
        if titleLabel != nil {
            titleLabel!.text = text as String
        }
    }
}

@objc class BubbleMenu: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    
    // MARK: - Properties
    
    var delegate : BubbleMenuDelegate?
    
    let menuScrollView = UIScrollView()
    let controllerScrollView = UIScrollView()
    var controllerArray : [AnyObject] = []
    var menuItems : [MenuItemView] = []
    
    var menuHeight : CGFloat = 34.0
    var menuMargin : CGFloat = 15.0
    var menuItemWidth : CGFloat = 111.0
    var selectionIndicatorHeight : CGFloat = 3.0
    
    var selectionIndicatorView : UIView = UIView()
    
    var currentPageIndex : Int = 0
    var lastPageIndex : Int = 0
    
    var selectionIndicatorColor : UIColor = UIColor.whiteColor()
    var selectedMenuItemLabelColor : UIColor = UIColor.whiteColor()

    var unselectedMenuItemLabelColor : UIColor = UIColor(hex: "FF62CC")
    var scrollMenuBackgroundColor : UIColor = UIColor.blackColor()
    var viewBackgroundColor : UIColor = UIColor.whiteColor()
    var bottomMenuHairlineColor : UIColor = UIColor.whiteColor()
    
    var menuItemFont : UIFont?
    
    var addBottomMenuHairline : Bool = true
    
    // MARK: - View life cycle
    
    init(viewControllers: [AnyObject]) {
        super.init(nibName: nil, bundle: nil)
        
        controllerArray = viewControllers
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpUserInterface()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        configureUserInterface()
        
        delegate?.menuDidFinishConfiguring(self)

        NSNotificationCenter.defaultCenter().postNotificationName("bubbleMenuDidAppear", object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        
        controllerScrollView.delegate = nil
        
    }
    
    // MARK: - UI Setup
    
    func setUpUserInterface() {
        
        // Set up menu scroll view
        menuScrollView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(menuScrollView)
        
        let viewsDictionary = ["menuScrollView":menuScrollView, "controllerScrollView":controllerScrollView]
        
        let menuScrollView_constraint_H:Array = NSLayoutConstraint.constraintsWithVisualFormat("H:|[menuScrollView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary)
        let menuScrollView_constraint_V:Array = NSLayoutConstraint.constraintsWithVisualFormat("V:[menuScrollView(\(menuHeight))]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary)
        
        self.view.addConstraints(menuScrollView_constraint_H)
        self.view.addConstraints(menuScrollView_constraint_V)
        
        // Set up controller scroll view
        controllerScrollView.pagingEnabled = true
        controllerScrollView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(controllerScrollView)
        
        let controllerScrollView_constraint_H:Array = NSLayoutConstraint.constraintsWithVisualFormat("H:|[controllerScrollView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary)
        let controllerScrollView_constraint_V:Array = NSLayoutConstraint.constraintsWithVisualFormat("V:|[controllerScrollView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary)
        
        self.view.addConstraints(controllerScrollView_constraint_H)
        self.view.addConstraints(controllerScrollView_constraint_V)
        
        // Add hairline to menu scroll view
        if addBottomMenuHairline {
            let menuBottomHairline : UIView = UIView()
            
            menuBottomHairline.translatesAutoresizingMaskIntoConstraints = false
            
            self.view.addSubview(menuBottomHairline)
            
            let menuBottomHairline_constraint_H:Array = NSLayoutConstraint.constraintsWithVisualFormat("H:|[menuBottomHairline]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["menuBottomHairline":menuBottomHairline])
            let menuBottomHairline_constraint_V:Array = NSLayoutConstraint.constraintsWithVisualFormat("V:|-\(menuHeight)-[menuBottomHairline(0.5)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["menuBottomHairline":menuBottomHairline])
            
            self.view.addConstraints(menuBottomHairline_constraint_H)
            self.view.addConstraints(menuBottomHairline_constraint_V)
            
            menuBottomHairline.backgroundColor = bottomMenuHairlineColor
        }
        
        // Disable scroll bars
        menuScrollView.showsHorizontalScrollIndicator = false
        menuScrollView.showsVerticalScrollIndicator = false
        controllerScrollView.showsHorizontalScrollIndicator = false
        controllerScrollView.showsVerticalScrollIndicator = false
        
        // Set background color behind scroll views and for menu scroll view
        self.view.backgroundColor = viewBackgroundColor
        menuScrollView.backgroundColor = scrollMenuBackgroundColor
    }
    
    func configureUserInterface() {
        
        // Add tap gesture recognizer to controller scroll view to recognize menu item selection
        let menuItemTapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleMenuItemTap:"))
        menuItemTapGestureRecognizer.numberOfTapsRequired = 1
        menuItemTapGestureRecognizer.numberOfTouchesRequired = 1
        menuItemTapGestureRecognizer.delegate = self
        controllerScrollView.addGestureRecognizer(menuItemTapGestureRecognizer)
        
        // Set delegate for controller scroll view
        controllerScrollView.delegate = self
        
        // Configure menu scroll view content size
        menuScrollView.contentSize = CGSizeMake((menuItemWidth + menuMargin) * CGFloat(controllerArray.count) + menuMargin, menuScrollView.frame.height)
        
        // Configure controller scroll view content size
        controllerScrollView.contentSize = CGSizeMake(controllerScrollView.frame.width * CGFloat(controllerArray.count), controllerScrollView.frame.height)
        
        var index : CGFloat = 0.0
        
        for controller in controllerArray {
            
            if controller.isKindOfClass(UIViewController) {
                // Configure each controllers' frame
                (controller as! UIViewController).view.frame = CGRectMake(controllerScrollView.frame.width * index, menuScrollView.frame.height, controllerScrollView.frame.width, controllerScrollView.frame.height - menuScrollView.frame.height)
                
                // Add controller as subview to controller scroll view
                controllerScrollView.addSubview((controller as! UIViewController).view)
                
                // Set up menu item for menu scroll view
                let menuItemView : MenuItemView = MenuItemView(frame: CGRectMake(menuItemWidth * index + menuMargin * (index + 1), 0.0, menuItemWidth, menuScrollView.frame.height))
                menuItemView.setUpMenuItemView(menuItemWidth, menuScrollViewHeight: menuScrollView.frame.height, indicatorHeight: selectionIndicatorHeight)
                
                // Configure menu item label font if font is set by user
                if menuItemFont != nil {
                    menuItemView.titleLabel!.font = menuItemFont
                }
                
                menuItemView.titleLabel!.textAlignment = NSTextAlignment.Center
                menuItemView.titleLabel!.textColor = unselectedMenuItemLabelColor
                
                // Set title depending on if controller has a title set
                if (controller as! UIViewController).title != nil {
                    menuItemView.titleLabel!.text = controller.title!
                } else {
                    menuItemView.titleLabel!.text = "Menu \(Int(index) + 1)"
                }
                
                // Add menu item view to menu scroll view
                menuScrollView.addSubview(menuItemView)
                menuItems.append(menuItemView)
                
                index++
            }
        }
        
        // Set selected color for title label of selected menu item
        if menuItems.count > 0 {
            
            if menuItems[currentPageIndex].titleLabel != nil {
                menuItems[currentPageIndex].titleLabel!.textColor = selectedMenuItemLabelColor
            }
        }
        
        // Configure selection indicator view
        selectionIndicatorView = UIView(frame: CGRectMake(menuMargin, menuScrollView.frame.height - selectionIndicatorHeight, menuItemWidth, selectionIndicatorHeight))
        selectionIndicatorView.backgroundColor = selectionIndicatorColor
        menuScrollView.addSubview(selectionIndicatorView)
    }
    
    
    // MARK: - Scroll view delegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        var ratio : CGFloat = 1.0
        
        if scrollView.isEqual(controllerScrollView) {
            // Calculate ratio between scroll views
            ratio = (menuScrollView.contentSize.width - self.view.frame.width) / (controllerScrollView.contentSize.width - self.view.frame.width)
            
            if menuScrollView.contentSize.width > self.view.frame.width {
                var offset : CGPoint = menuScrollView.contentOffset
                offset.x = controllerScrollView.contentOffset.x * ratio
                menuScrollView.setContentOffset(offset, animated: false)
            }
            
            // Calculate current page
            let width : CGFloat = scrollView.frame.size.width;
            let page : Int = Int((scrollView.contentOffset.x + (0.5 * width)) / width)
            
            // Update page if changed
            if page != currentPageIndex {
                lastPageIndex = currentPageIndex
                currentPageIndex = page
                BubbleMenuPage.last = currentPageIndex
                
                NSNotificationCenter.defaultCenter().postNotificationName("ChangedFeed", object: nil, userInfo: ["page" : currentPageIndex])
                
            }
            
            // Move selection indicator view when swiping
            UIView.animateWithDuration(0.15, animations: { () -> Void in
                self.selectionIndicatorView.frame = CGRectMake((self.menuMargin + self.menuItemWidth) * CGFloat(page) + self.menuMargin, self.selectionIndicatorView.frame.origin.y, self.selectionIndicatorView.frame.width, self.selectionIndicatorView.frame.height)
                
                // Switch newly selected menu item title label to selected color and old one to unselected color
                if self.menuItems.count > 0 {
                    
                    if self.menuItems[self.lastPageIndex].titleLabel != nil && self.menuItems[self.currentPageIndex].titleLabel != nil {
                        
                        self.menuItems[self.lastPageIndex].titleLabel!.textColor = self.unselectedMenuItemLabelColor
                        self.menuItems[self.currentPageIndex].titleLabel!.textColor = self.selectedMenuItemLabelColor
                    }
                }
            })
        }
    }
    
    
    // MARK: - Tap gesture recognizer selector
    
    func handleMenuItemTap(gestureRecognizer: UITapGestureRecognizer) {
        
        let tappedPoint : CGPoint = gestureRecognizer.locationInView(menuScrollView)
        
        if tappedPoint.y < menuScrollView.frame.height {
            
            // Calculate tapped page
            let itemIndex : Int = Int((tappedPoint.x - menuMargin / 2) / (menuMargin + menuItemWidth))
            
            // Update page if changed
            if itemIndex != currentPageIndex {
                lastPageIndex = currentPageIndex
                currentPageIndex = itemIndex
            }
            
            // Move selection indicator view when swiping
            UIView.animateWithDuration(0.7, animations: { () -> Void in
                let xOffset : CGFloat = CGFloat(itemIndex) * self.controllerScrollView.frame.width
                self.controllerScrollView.setContentOffset(CGPoint(x: xOffset, y: self.controllerScrollView.contentOffset.y), animated: true)
            })
        }
    }
    
    func setPage(index: NSInteger, animated: Bool) {
        
        // Update page if changed
        if index != currentPageIndex {
            lastPageIndex = currentPageIndex
            currentPageIndex = index
        }
        
        if animated {
            
            // Move selection indicator view when swiping
            UIView.animateWithDuration(0.7, animations: { () -> Void in
                let xOffset : CGFloat = CGFloat(index) * self.controllerScrollView.frame.width
                self.controllerScrollView.setContentOffset(CGPoint(x: xOffset, y: self.controllerScrollView.contentOffset.y), animated: animated)
            })
            
        } else {
            
            let xOffset : CGFloat = CGFloat(index) * self.controllerScrollView.frame.width
            self.controllerScrollView.setContentOffset(CGPoint(x: xOffset, y: self.controllerScrollView.contentOffset.y), animated: animated)
            
        }
        
    }
    
}
