//
//  RankLoader.m
//
//  Code generated using QuartzCode 1.36.2 on 8/15/15.
//  www.quartzcodeapp.com
//

#import "RankLoader.h"
#import "QCMethod.h"

@interface RankLoader ()

@property (nonatomic, strong) NSMutableDictionary * layers;


@end

@implementation RankLoader

#pragma mark - Life Cycle

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupProperties];
        [self setupLayers];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setupProperties];
        [self setupLayers];
    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    [self setupLayerFrames];
}

- (void)setBounds:(CGRect)bounds{
    [super setBounds:bounds];
    [self setupLayerFrames];
}

- (void)setupProperties{
    self.layers = [NSMutableDictionary dictionary];
    
}

- (void)setupLayers{
    CAShapeLayer * oval = [CAShapeLayer layer];
    [self.layer addSublayer:oval];
    self.layers[@"oval"] = oval;
    
    CALayer * Group = [CALayer layer];
    [self.layer addSublayer:Group];
    self.layers[@"Group"] = Group;
    {
        CAShapeLayer * oval2 = [CAShapeLayer layer];
        [Group addSublayer:oval2];
        self.layers[@"oval2"] = oval2;
        CAShapeLayer * oval3 = [CAShapeLayer layer];
        [Group addSublayer:oval3];
        self.layers[@"oval3"] = oval3;
        CAShapeLayer * oval4 = [CAShapeLayer layer];
        [Group addSublayer:oval4];
        self.layers[@"oval4"] = oval4;
    }
    
    
    [self resetLayerPropertiesForLayerIdentifiers:nil];
    [self setupLayerFrames];
}

- (void)resetLayerPropertiesForLayerIdentifiers:(NSArray *)layerIds{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    
    if(!layerIds || [layerIds containsObject:@"oval"]){
        CAShapeLayer * oval = self.layers[@"oval"];
        oval.fillColor   = nil;
        oval.strokeColor = [UIColor colorWithRed:1 green: 1 blue:1 alpha:1].CGColor;
        oval.lineWidth   = 2;
    }
    if(!layerIds || [layerIds containsObject:@"oval2"]){
        CAShapeLayer * oval2 = self.layers[@"oval2"];
        oval2.fillColor   = nil;
        oval2.strokeColor = [UIColor colorWithRed:1 green: 1 blue:1 alpha:1].CGColor;
    }
    if(!layerIds || [layerIds containsObject:@"oval3"]){
        CAShapeLayer * oval3 = self.layers[@"oval3"];
        oval3.fillColor   = nil;
        oval3.strokeColor = [UIColor colorWithRed:1 green: 1 blue:1 alpha:1].CGColor;
    }
    if(!layerIds || [layerIds containsObject:@"oval4"]){
        CAShapeLayer * oval4 = self.layers[@"oval4"];
        oval4.fillColor   = nil;
        oval4.strokeColor = [UIColor colorWithRed:1 green: 1 blue:1 alpha:1].CGColor;
    }
    
    [CATransaction commit];
}

- (void)setupLayerFrames{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    
    CAShapeLayer * oval  = self.layers[@"oval"];
    oval.frame           = CGRectMake(0.06254 * CGRectGetWidth(oval.superlayer.bounds), 0.05463 * CGRectGetHeight(oval.superlayer.bounds), 0.88639 * CGRectGetWidth(oval.superlayer.bounds), 0.89073 * CGRectGetHeight(oval.superlayer.bounds));
    oval.path            = [self ovalPathWithBounds:oval.bounds].CGPath;
    
    CALayer * Group      = self.layers[@"Group"];
    Group.frame          = CGRectMake(0.25774 * CGRectGetWidth(Group.superlayer.bounds), 0.43766 * CGRectGetHeight(Group.superlayer.bounds), 0.49599 * CGRectGetWidth(Group.superlayer.bounds), 0.12468 * CGRectGetHeight(Group.superlayer.bounds));
    
    CAShapeLayer * oval2 = self.layers[@"oval2"];
    oval2.frame          = CGRectMake(0, 0, 0.25138 * CGRectGetWidth(oval2.superlayer.bounds),  CGRectGetHeight(oval2.superlayer.bounds));
    oval2.path           = [self oval2PathWithBounds:oval2.bounds].CGPath;
    
    CAShapeLayer * oval3 = self.layers[@"oval3"];
    oval3.frame          = CGRectMake(0.37431 * CGRectGetWidth(oval3.superlayer.bounds), 0, 0.25138 * CGRectGetWidth(oval3.superlayer.bounds),  CGRectGetHeight(oval3.superlayer.bounds));
    oval3.path           = [self oval3PathWithBounds:oval3.bounds].CGPath;
    
    CAShapeLayer * oval4 = self.layers[@"oval4"];
    oval4.frame          = CGRectMake(0.74862 * CGRectGetWidth(oval4.superlayer.bounds), 0, 0.25138 * CGRectGetWidth(oval4.superlayer.bounds),  CGRectGetHeight(oval4.superlayer.bounds));
    oval4.path           = [self oval4PathWithBounds:oval4.bounds].CGPath;
    
    [CATransaction commit];
}

#pragma mark - Animation Setup

- (void)addRankLoaderAnimation{
    NSString * fillMode = kCAFillModeForwards;
    
    ////Oval animation
    CAKeyframeAnimation * ovalLineWidthAnim = [CAKeyframeAnimation animationWithKeyPath:@"lineWidth"];
    ovalLineWidthAnim.values         = @[@2, @1, @2];
    ovalLineWidthAnim.keyTimes       = @[@0, @0.449, @1];
    ovalLineWidthAnim.duration       = 3.33;
    ovalLineWidthAnim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    CAAnimationGroup * ovalUntitled1Anim = [QCMethod groupAnimations:@[ovalLineWidthAnim] fillMode:fillMode];
    ovalUntitled1Anim.repeatCount = INFINITY;
    [self.layers[@"oval"] addAnimation:ovalUntitled1Anim forKey:@"ovalUntitled1Anim"];
    
    ////Oval2 animation
    CAKeyframeAnimation * oval2OpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    oval2OpacityAnim.values                = @[@1, @0, @1];
    oval2OpacityAnim.keyTimes              = @[@0, @0.5, @1];
    oval2OpacityAnim.duration              = 2;
    
    CAAnimationGroup * oval2Untitled1Anim = [QCMethod groupAnimations:@[oval2OpacityAnim] fillMode:fillMode];
    oval2Untitled1Anim.repeatCount = INFINITY;
    [self.layers[@"oval2"] addAnimation:oval2Untitled1Anim forKey:@"oval2Untitled1Anim"];
    
    ////Oval3 animation
    CAKeyframeAnimation * oval3OpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    oval3OpacityAnim.values                = @[@1, @0, @1];
    oval3OpacityAnim.keyTimes              = @[@0, @0.5, @1];
    oval3OpacityAnim.duration              = 2;
    oval3OpacityAnim.beginTime             = 0.66;
    
    CAAnimationGroup * oval3Untitled1Anim = [QCMethod groupAnimations:@[oval3OpacityAnim] fillMode:fillMode];
    oval3Untitled1Anim.repeatCount = INFINITY;
    [self.layers[@"oval3"] addAnimation:oval3Untitled1Anim forKey:@"oval3Untitled1Anim"];
    
    ////Oval4 animation
    CAKeyframeAnimation * oval4OpacityAnim = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    oval4OpacityAnim.values                = @[@1, @0, @1];
    oval4OpacityAnim.keyTimes              = @[@0, @0.5, @1];
    oval4OpacityAnim.duration              = 2;
    oval4OpacityAnim.beginTime             = 1.33;
    
    CAAnimationGroup * oval4Untitled1Anim = [QCMethod groupAnimations:@[oval4OpacityAnim] fillMode:fillMode];
    oval4Untitled1Anim.repeatCount = INFINITY;
    [self.layers[@"oval4"] addAnimation:oval4Untitled1Anim forKey:@"oval4Untitled1Anim"];
}

#pragma mark - Animation Cleanup

- (void)updateLayerValuesForAnimationId:(NSString *)identifier{
    if([identifier isEqualToString:@"Untitled1"]){
        [QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval"] animationForKey:@"ovalUntitled1Anim"] theLayer:self.layers[@"oval"]];
        [QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval2"] animationForKey:@"oval2Untitled1Anim"] theLayer:self.layers[@"oval2"]];
        [QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval3"] animationForKey:@"oval3Untitled1Anim"] theLayer:self.layers[@"oval3"]];
        [QCMethod updateValueFromPresentationLayerForAnimation:[self.layers[@"oval4"] animationForKey:@"oval4Untitled1Anim"] theLayer:self.layers[@"oval4"]];
    }
}

- (void)removeAnimationsForAnimationId:(NSString *)identifier{
    if([identifier isEqualToString:@"Untitled1"]){
        [self.layers[@"oval"] removeAnimationForKey:@"ovalUntitled1Anim"];
        [self.layers[@"oval2"] removeAnimationForKey:@"oval2Untitled1Anim"];
        [self.layers[@"oval3"] removeAnimationForKey:@"oval3Untitled1Anim"];
        [self.layers[@"oval4"] removeAnimationForKey:@"oval4Untitled1Anim"];
    }
}

- (void)removeAllAnimations{
    [self.layers enumerateKeysAndObjectsUsingBlock:^(id key, CALayer *layer, BOOL *stop) {
        [layer removeAllAnimations];
    }];
}

#pragma mark - Bezier Path

- (UIBezierPath*)ovalPathWithBounds:(CGRect)bound{
    UIBezierPath * ovalPath = [UIBezierPath bezierPathWithOvalInRect:bound];
    return ovalPath;
}

- (UIBezierPath*)oval2PathWithBounds:(CGRect)bound{
    UIBezierPath * oval2Path = [UIBezierPath bezierPathWithOvalInRect:bound];
    return oval2Path;
}

- (UIBezierPath*)oval3PathWithBounds:(CGRect)bound{
    UIBezierPath * oval3Path = [UIBezierPath bezierPathWithOvalInRect:bound];
    return oval3Path;
}

- (UIBezierPath*)oval4PathWithBounds:(CGRect)bound{
    UIBezierPath * oval4Path = [UIBezierPath bezierPathWithOvalInRect:bound];
    return oval4Path;
}


@end