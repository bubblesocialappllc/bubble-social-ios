//
//  BubbleNavigationController.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "BubbleNavigationController.h"

@interface BubbleNavigationController ()

@end

@implementation BubbleNavigationController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
        // Do any additional setup after loading the view.
    
    for (UIViewController *viewController in self.viewControllers){
            // You need to do this because the push is not called if you created this controller as part of the storyboard
        [self addButton:viewController.navigationItem];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:@"range"])
        [self.range setLabelValue:[defaults objectForKey:@"range"]];
    else
        [defaults setObject:@"5" forKey:@"range"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Overides

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    [self addButton:viewController.navigationItem];
    [super pushViewController:viewController animated:animated];
    
}


#pragma mark - Helpers

- (void)addButton:(UINavigationItem *)item{
    
    if (item.rightBarButtonItem == nil) {
        item.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_edit size:23 color:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(showMakePost)];
    }
    
    if (item.leftBarButtonItem == nil) {
        self.range = [[RangeView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeRange)];
        
        [self.range addGestureRecognizer:tap];
        
        UIBarButtonItem *rangeViewButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.range];
        
        item.leftBarButtonItem = rangeViewButtonItem;
    }
    
}

- (void)showMakePost {
    
    [self performSegueWithIdentifier:@"MakePostSegue" sender:self];
    
}

- (void)changeRange {
    
    [self performSegueWithIdentifier:@"ChangeRangeSegue" sender:self];
    
}

@end
