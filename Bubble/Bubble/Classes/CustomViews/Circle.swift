//
//  Circle.swift
//  Bubble
//
//  Created by Chayel Heinsen on 12/30/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

import Foundation
import UIKit

@objc @IBDesignable class Circle: UIView {
    
    var textField : UITextField?
    
    // MARK: Inspectable Properties
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            setupView()
        }
    }
    
    private func setupView() {
        
        self.layer.cornerRadius = cornerRadius
        
        self.setNeedsDisplay()
        
    }
    
    private func addTextField() {
        
        self.textField = UITextField(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
        //self.textField?.backgroundColor = UIColor.blackColor()
        self.textField?.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        self.textField?.textColor = UIColor.whiteColor()
        self.textField?.textAlignment = NSTextAlignment.Center
        self.addSubview(self.textField!);
    }
    
    private func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var newLength = 0
        
        if let text = textField.text {
            newLength = text.characters.count + string.characters.count - range.length
        } else {
            newLength = string.characters.count - range.length
        }
        
        return newLength <= 1 // Bool
    }
    
    // MARK: Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        addTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
        addTextField()
    }
    
    
}

