//
//  AnimatedCircle.swift
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

import UIKit

@objc @IBDesignable class AnimatedCircle: UIView {

    @IBInspectable var fillColor : UIColor = UIColor.clearColor() {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var strokeColor : UIColor = UIColor.flatEmeraldColor() as UIColor {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var lineWidth : CGFloat = 15.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var xOffset : CGFloat = 0.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var yOffset : CGFloat = 0.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var radius : CGFloat = 100.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
        //let radius = 150.0
        
        // Create the circle layer
        let circle = CAShapeLayer()
        
        // Set the center of the circle to be the center of the view
        let center = CGPoint(x: CGRectGetMidX(self.frame) - CGFloat(radius), y: CGRectGetMidY(self.frame) - CGFloat(radius))
        
        let fractionOfCircle = 0.1 / 4.0
        
        let twoPi = 2.0 * Double(M_PI)
        
        // The starting angle is given by the fraction of the circle that the point is at, divided by 2 * Pi and less
        // We subtract M_PI_2 to rotate the circle 90 degrees to make it more intuitive (i.e. like a clock face with zero at the top, 1/4 at RHS, 1/2 at bottom, etc.)
        let startAngle = Double(fractionOfCircle) / Double(twoPi) - Double(M_PI_2)
        let endAngle = 0.0 - Double(M_PI_2)
        let clockwise: Bool = true
        
        // `clockwise` tells the circle whether to animate in a clockwise or anti clockwise direction
        circle.path = UIBezierPath(arcCenter: center, radius: CGFloat(radius), startAngle: CGFloat(startAngle), endAngle: CGFloat(endAngle), clockwise: clockwise).CGPath
        
        circle.frame = CGRectOffset(circle.frame, xOffset, yOffset);
        
        // Configure the circle
        circle.fillColor = self.fillColor.CGColor
        circle.strokeColor = self.strokeColor.CGColor
        circle.lineWidth = self.lineWidth
        
        // When it gets to the end of its animation, leave it at 0% stroke filled
        circle.strokeEnd = 0.0
        
        // Add the circle to the parent layer
        self.layer.addSublayer(circle)
        
        // Configure the animation
        let drawAnimation = CABasicAnimation(keyPath: "strokeEnd")
        drawAnimation.repeatCount = 1.0
        
        // Animate from the full stroke being drawn to none of the stroke being drawn
        drawAnimation.fromValue = NSNumber(double: fractionOfCircle)
        drawAnimation.toValue = NSNumber(float: 4.0)
        
        drawAnimation.duration = 2.5
        
        drawAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        // Add the animation to the circle
        circle.addAnimation(drawAnimation, forKey: "drawCircleAnimation")
        
        self.backgroundColor = UIColor.clearColor()
        
    }
    
    
}
