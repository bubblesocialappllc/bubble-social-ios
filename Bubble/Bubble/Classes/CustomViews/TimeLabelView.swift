//
//  TimeLabelView.swift
//  Bubble
//
//  Created by Chayel Heinsen on 10/26/14.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

import UIKit

/**
*  This view should be 90x30
*/
@objc @IBDesignable public class TimeLabelView: UIView {

    @IBInspectable public var roundness: CGFloat = 5.0 {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable public var image: UIImage? {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable public var textColor: UIColor = UIColor.lightGrayColor() {
        didSet {
            setupView()
        }
    }
    
    var timeLabel: UILabel = UILabel(frame: CGRectMake(3, 5, 57, 21))
    
    var imageView: UIImageView = UIImageView(frame: CGRectMake(63, 6, 19, 19))
    
    private var target: AnyObject?
    
    private var action: Selector?
    
    private func setupView() {
        
        self.layer.cornerRadius = roundness
        
        imageView.image = image
        
        timeLabel.font = UIFont(name: "HelveticaNeue", size: 12);
        timeLabel.textColor = textColor
        timeLabel.textAlignment = .Right
        timeLabel.text = "Just now"
        
        self.addSubview(imageView);
        self.addSubview(timeLabel)
        
        self.setNeedsDisplay()
        
    }
    
    /**
    Enables the ability to click on the view
    
    - parameter target:    The class where the action is defined.
    - parameter andAction: The method to call on the target.
    */
    @objc internal func setTarget(target: AnyObject, andAction: Selector) {
        
        self.target = target
        self.action = andAction
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self.target!, action: self.action!)
        
        self.addGestureRecognizer(tap);
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupView()
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }


}
