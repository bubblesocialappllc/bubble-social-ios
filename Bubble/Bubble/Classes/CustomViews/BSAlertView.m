//
//  BSAlertView.m
//  Bubble
//
//  Created by Chayel Heinsen on 1/27/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "BSAlertView.h"

static BOOL isPresenting;

@interface BSAlertView ()

/**
 *  The alert view.
 */
@property (strong, nonatomic) UIView *view;

/**
 *  The title label.
 */
@property (strong, nonatomic) UILabel *titleLabel;

/**
 *  The message label.
 */
@property (strong, nonatomic) UILabel *messageLabel;

/**
 *  The icon.
 */
@property (strong, nonatomic) UIImageView *imgIcon;

/**
 *  YES if the alert is visable, otherwise NO.
 */
@property (nonatomic, readwrite) BOOL visible;

/**
 *  The completion block.
 */
@property (strong, nonatomic) BSAlertViewCompletion completion;

/**
 *  Adjusts the layout for the givin screen size.
 */
- (void)adjustLayout;

/**
 *  Returns the frame for a given label.
 *
 *  @param label The label to get the frame of.
 *
 *  @return The frame of the given label.
 */
- (CGRect)frameForLabel:(UILabel *)label;

/**
 *  The Y-Origin of the view.
 *
 *  @return The Y-Origin of the alert view.
 */
- (CGFloat)originY;

@end

@implementation BSAlertView

#pragma mark - UIView Overides

- (id)init {
    
    self = [super init];
    
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                owner:self
                                options:nil];
        
        CGRect frame = self.view.frame;
        frame.size.width = CGRectGetWidth([[UIScreen mainScreen] bounds]);
        self.view.frame = frame;
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:self.view];
       
        self.alertDuration = 3.0f;
        self.animationDuration = 0.6f;
    }
    
    return self;
}

#pragma mark - Public

- (id)initWithStyle:(BSAlertStyle)style message:(NSString *)message target:(id)target action:(SEL)action {
    return [self initWithStyle:style title:nil message:message icon:@"" iconSize:20 target:target action:action];
}

- (id)initWithStyle:(BSAlertStyle)style title:(NSString *)title message:(NSString *)message target:(id)target action:(SEL)action {
    return [self initWithStyle:style title:title message:message icon:@"" iconSize:20 target:target action:action];
}

- (id)initWithStyle:(BSAlertStyle)style title:(NSString *)title message:(NSString *)message icon:(NSString *)icon iconSize:(CGFloat)iconSize target:(id)target action:(SEL)action {
    
    self = [self init];
    
    if (self) {
        
        CGRect frame = self.view.frame;
        frame.origin.y = CGRectGetHeight(self.view.frame) - [self originY];
        self.frame = frame;
        
        self.title = title;
        self.message = message;
        self.color = nil;
        self.icon = icon;
        self.iconSize = iconSize;
        self.style = style;
        self.imgIcon.clipsToBounds = NO;
        self.imgIcon.layer.cornerRadius = self.imgIcon.frame.size.height / 2;
        self.alertDuration = 3.0f;
        self.animationDuration = 0.6f;
        self.target = target;
        self.action = action;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap)];
        [self addGestureRecognizer:tap];
        
        UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
        swipe.direction = UISwipeGestureRecognizerDirectionUp;
        [self addGestureRecognizer:swipe];
        
    }
    
    return self;
    
}

- (void)setStyle:(BSAlertStyle)style {
    
    _style = style;
    UIColor *color;
    
    NSString *ionicon;
    
    switch (self.style) {
        case BSAlertStyleError: {
            
            if ([self.icon isEqualToString:@""])
                ionicon = ion_ios_close_outline;
            else
                ionicon = self.icon;
            
            if (self.color == nil)
                color = [UIColor white];
            else
                color = self.color;
            
            break;
        }
            
        case BSAlertStyleInfo: {
            
            if ([self.icon isEqualToString:@""])
                ionicon = ion_ios_information_outline;
            else
                ionicon = self.icon;
            
            if (self.color == nil)
                color = [UIColor white];
            else
                color = self.color;
            
            break;
        }
            
        case BSAlertStyleSuccess: {
            
            if ([self.icon isEqualToString:@""])
                ionicon = ion_ios_checkmark_outline;
            else
                ionicon = self.icon;
            
            if (self.color == nil)
                color = [UIColor white];
            else
                color = self.color;
            
            break;
        }
    }
    
    self.imgIcon.image = [IonIcons imageWithIcon:ionicon size:self.iconSize color:color];
    self.titleLabel.textColor = color;
    self.messageLabel.textColor = color;
}

- (void)setFont:(UIFont *)font {
    
    self.titleLabel.font = font;
    self.messageLabel.font = font;
    
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment {
  
    _textAlignment = textAlignment;
    
    self.titleLabel.textAlignment = textAlignment;
    self.messageLabel.textAlignment = textAlignment;
}

- (void)show {
    [self showWithCompletion:nil];
}

- (void)showWithCompletion:(BSAlertViewCompletion)block {
    
    if (isPresenting) {
        return;
    }
    
    isPresenting = YES;
    
    self.visible = YES;
    
    self.titleLabel.text = self.title;
    self.messageLabel.text = self.message;
    self.completion = block;
    
    [self adjustLayout];
    
    UIApplication *application = [UIApplication sharedApplication];
    
    CGRect frame = self.frame;
    frame.origin.y = -([self originY] + CGRectGetHeight(self.view.frame));
    self.frame = frame;
    
    NSInteger index = [[application keyWindow].subviews count];
    
    [[application keyWindow] insertSubview:self atIndex:index];
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = [self originY];
    
    [UIView animateWithDuration:_animationDuration animations:^{
        self.frame = viewFrame;

    } completion:^(BOOL finished) {
        [self performSelector:@selector(hide) withObject:nil afterDelay:self.alertDuration];
    }];
    
}

- (void)hide {
 
    [UIView animateWithDuration:_animationDuration animations:^{
        
        self.view.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        if (finished) {
          
            [self removeFromSuperview];
            
            isPresenting = NO;
            
            self.visible = NO;
            
            if (self.completion) {
                self.completion();
                self.completion = nil;
            }
        }
    }];
    
}

#pragma mark - Private methods

- (void)didTap {
    
    if (self.target != nil && self.action != nil) {
        SEL selector = self.action;
        IMP imp = [self.target methodForSelector:selector];
        void (*func)(id, SEL, id) = (void *)imp;
        func(self.target, selector, self);
    }
    
}

- (void)adjustLayout {
    
    CGRect frame;
    
    CGFloat titleToMessage = CGRectGetMinY(self.messageLabel.frame) - CGRectGetMaxY(self.titleLabel.frame);
    CGFloat messageToBottom = CGRectGetHeight(self.frame) - CGRectGetMaxY(self.messageLabel.frame);
    
    self.titleLabel.frame = [self frameForLabel:self.titleLabel];
    
    frame = self.messageLabel.frame;
    frame.origin.y = CGRectGetMaxY(self.titleLabel.frame) + titleToMessage;
    self.messageLabel.frame = frame;
    
    self.messageLabel.frame = [self frameForLabel:self.messageLabel];
    
    frame = self.view.frame;
    frame.size.height = CGRectGetMaxY(self.messageLabel.frame) + messageToBottom;
    self.view.frame = frame;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (CGRect)frameForLabel:(UILabel *)label {
    
    CGSize size = CGSizeZero;
    CGFloat height = 0;
    
    if ([label.text respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
       
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:label.font,
                                    NSFontAttributeName,
                                    paragraphStyle,
                                    NSParagraphStyleAttributeName, nil];
        
        height = [label.text boundingRectWithSize:size
                                          options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin
                                       attributes:attributes
                                          context:nil].size.height;
    } else {
        
        height = [label.text sizeWithFont:label.font
                        constrainedToSize:size
                        lineBreakMode:NSLineBreakByTruncatingTail].height;
    }
    
    CGRect frame = label.frame;
    frame.size.height = height;
    
    return frame;
}

#pragma GCC diagnostic pop

- (CGFloat)originY {
    
    CGFloat originY = 0;
    
    UIApplication *application = [UIApplication sharedApplication];
    
    if (!application.statusBarHidden) {
        originY = [application statusBarFrame].size.height;
    }
    
    return originY;
}

@end
