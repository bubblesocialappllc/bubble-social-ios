//
//  CommentsButtonView.swift
//  Bubble
//
//  Created by Doug Woodrow on 25/10/2014.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

import UIKit
import Parse

/**
*  This view should be 109x28
*/
@objc @IBDesignable public class CommentsButtonView: UIView {
    
    @IBInspectable public var roundness : CGFloat = 5.0 {
        didSet {
            setUpView()
        }
    }
    
    @IBInspectable public var image: UIImage? {
        didSet {
            setUpView()
        }
    }
    
    var imageView: UIImageView = UIImageView(frame: CGRectMake(8, 7, 15, 15))
    
    var commentsCountLabel: UILabel! = UILabel(frame: CGRectMake(47, 3, 21, 21))
    
    var dot: UILabel = UILabel(frame: CGRectMake(30, 3, 10, 21));
    
    private var target: AnyObject?
    
    private var action: Selector?
    
    private func setUpView() {
        
        self.layer.cornerRadius = roundness
        self.backgroundColor = (UIColor.bubblePink() as UIColor)
        
        imageView.image = image
        imageView.contentMode = .ScaleAspectFit
        
        dot.font = UIFont(name: "HelveticaNeue", size: 30);
        dot.textColor = UIColor.whiteColor()
        dot.text = "•"
        
        commentsCountLabel.font = UIFont(name: "HelveticaNeue", size: 12);
        commentsCountLabel.textColor = UIColor.whiteColor()
        commentsCountLabel.text = "0"
        
        self.addSubview(imageView);
        self.addSubview(dot);
        self.addSubview(commentsCountLabel)
        
        self.setNeedsDisplay()
        
    }
    
    /**
    Enables the ability to click on the view
    
    - parameter target:    The class where the action is defined.
    - parameter andAction: The method to call on the target.
    */
    @objc internal func setTarget(target: AnyObject, andAction: Selector) {
        
        self.target = target
        self.action = andAction
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self.target!, action: self.action!)
        
        self.addGestureRecognizer(tap);
        
    }
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setUpView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
