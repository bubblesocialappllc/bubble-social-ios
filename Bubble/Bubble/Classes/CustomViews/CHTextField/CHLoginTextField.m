//
//  CHLoginTextField.m
//  Bubble Social
//
//  Created by Chayel Heinsen on 12/14/14.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

#import "CHLoginTextField.h"

#define kLeftPadding 0
#define kVerticalPadding 12
#define kHorizontalPadding 10

@interface CHLoginTextField ()

@property CHLoginTextFieldType type;

@end

@implementation CHLoginTextField

#pragma mark - Public

- (void)setIcon:(UIImage *)image {
    [self setType:CHLoginTextFieldTypeDefault withIcon:image];
}

- (void)setType:(CHLoginTextFieldType)type withIcon:(UIImage *)image {
    
    self.type = type;
    
    UIEdgeInsets edge = [self edgeInsetsForType:type];
    
    NSString *imageName = [self backgroundImageNameForType:type];
    CGRect imageViewFrame = [self iconImageViewRectForType:type];
    
    UIImage *imagebackground = [UIImage imageNamed:imageName];
    imagebackground = [imagebackground resizableImageWithCapInsets:edge];
    
    [self setBackground:imagebackground];
    
    UIImage *icon = image;
    
        // Make an imageview to show an icon on the left side of textfield
    UIImageView *iconImage = [[UIImageView alloc] initWithFrame:imageViewFrame];
    [iconImage setImage:icon];
    [iconImage setContentMode:UIViewContentModeCenter];
    
    self.leftView = iconImage;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    [self setNeedsDisplay]; //force reload for updated editing rect for bound to take effect.
}

#pragma mark - Private

- (CGRect)textRectForBounds:(CGRect)bounds {
    
    UIEdgeInsets edge = [self edgeInsetsForType:self.type];
    
    CGFloat x = bounds.origin.x + edge.left + kLeftPadding;
    CGFloat y = bounds.origin.y + kVerticalPadding;
    
    return CGRectMake(x,y,bounds.size.width - kHorizontalPadding * 2, bounds.size.height - kVerticalPadding * 2);
    
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

- (CGRect)iconImageViewRectForType:(CHLoginTextFieldType)type {
    
    UIEdgeInsets edge = [self edgeInsetsForType:type];
  
    if (type == CHLoginTextFieldTypeRound) {
        return CGRectMake(0, 0, edge.left*2, self.frame.size.height); //to put the icon inside
    }

    return CGRectMake(0, 0, edge.left, self.frame.size.height); // default
}

- (UIEdgeInsets)edgeInsetsForType:(CHLoginTextFieldType)type {
   
    if (type == CHLoginTextFieldTypeRound) {
        return UIEdgeInsetsMake(13, 13, 13, 13);
    }
    
    return UIEdgeInsetsMake(10, 43, 10, 19); // default
}

- (NSString *)backgroundImageNameForType:(CHLoginTextFieldType)type {
  
    if (type == CHLoginTextFieldTypeRound) {
        return @"round_textfield";
    }
    
    return @"text_field"; // default
}

@end

