//
//  CHLoginTextField.h
//  Bubble Social
//
//  Created by Chayel Heinsen on 12/14/14.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    
    CHLoginTextFieldTypeDefault,
    CHLoginTextFieldTypeRound
    
} CHLoginTextFieldType;

@interface CHLoginTextField : UITextField

- (void)setIcon:(UIImage *)image;
- (void)setType:(CHLoginTextFieldType)type withIcon:(UIImage *)image;

@end
