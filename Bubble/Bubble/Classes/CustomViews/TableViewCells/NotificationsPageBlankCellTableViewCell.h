//
//  NotificationsPageBlankCellTableViewCell.h
//  Bubble
//
//  Created by Doug Woodrow on 3/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Parse;
@import Bolts;

@interface NotificationsPageBlankCellTableViewCell : UITableViewCell

@end
