//
//  MainTableViewCell.m
//  Bubble
//
//  Created by Chayel Heinsen on 7/25/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "MainTableViewCell.h"

@implementation MainTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
