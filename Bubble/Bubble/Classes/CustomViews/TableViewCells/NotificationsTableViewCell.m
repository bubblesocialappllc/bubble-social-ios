//
//  NotificationsTableViewCell.m
//  Bubble
//
//  Created by Doug Woodrow on 3/4/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "NotificationsTableViewCell.h"

@implementation NotificationsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
