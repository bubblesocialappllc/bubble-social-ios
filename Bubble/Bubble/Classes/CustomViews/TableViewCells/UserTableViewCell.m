//
//  UserTableViewCell.m
//  Bubble
//
//  Created by Chayel Heinsen on 1/12/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "UserTableViewCell.h"

@implementation UserTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
