//
//  ImageTableViewCell.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/14/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "ImageTableViewCell.h"

@implementation ImageTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.postImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.postImage setClipsToBounds:YES];
    [self.postImage sizeToFit];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
