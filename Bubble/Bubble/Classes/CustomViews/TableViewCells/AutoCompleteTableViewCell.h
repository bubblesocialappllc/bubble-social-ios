//
//  AutoCompleteTableViewCell.h
//  Bubble
//
//  Created by Chayel Heinsen on 3/23/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoCompleteTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet PFImageView *profilePicture;
@property (strong, nonatomic) IBOutlet UILabel *username;

@end
