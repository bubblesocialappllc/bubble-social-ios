//
//  ImageTableViewCell.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/14/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet UILabel *location;
@property (strong, nonatomic) IBOutlet PFImageView *image;
@property (strong, nonatomic) IBOutlet PFImageView *postImage;
@property (strong, nonatomic) IBOutlet LikesButtonView *like;
@property (strong, nonatomic) IBOutlet CommentsButtonView *comment;
@property (strong, nonatomic) IBOutlet TimeLabelView *time;

@end
