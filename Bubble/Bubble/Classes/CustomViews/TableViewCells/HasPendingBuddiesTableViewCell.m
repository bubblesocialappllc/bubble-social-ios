//
//  HasPendingBuddiesTableViewCell.m
//  Bubble
//
//  Created by Chayel Heinsen on 2/22/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "HasPendingBuddiesTableViewCell.h"

@implementation HasPendingBuddiesTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
