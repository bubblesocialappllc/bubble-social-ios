//
//  MainTableViewCell.h
//  Bubble
//
//  Created by Chayel Heinsen on 7/25/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *text;
@property (strong, nonatomic) IBOutlet PFImageView *image;
@property (strong, nonatomic) IBOutlet LikesButtonView *like;
@property (strong, nonatomic) IBOutlet CommentsButtonView *comment;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *squareConstraint;

@end
