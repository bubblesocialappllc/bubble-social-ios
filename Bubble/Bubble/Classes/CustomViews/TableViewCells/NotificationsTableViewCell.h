//
//  NotificationsTableViewCell.h
//  Bubble
//
//  Created by Doug Woodrow on 3/4/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *text;
@property (strong, nonatomic) IBOutlet PFImageView *profilePicture;

@end
