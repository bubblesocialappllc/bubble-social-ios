//
//  HasPendingBuddiesTableViewCell.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/22/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HasPendingBuddiesTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *arrow;

@end
