//
//  CHTableViewCommentCellTableViewCell.h
//  Bubble
//
//  Created by Doug Woodrow on 29/08/2014.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TimeLabelView;
@interface CHTableViewCommentCellTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet PFImageView *profilePicture;
@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet UILabel *location;
@property (strong, nonatomic) IBOutlet TimeLabelView *time;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
