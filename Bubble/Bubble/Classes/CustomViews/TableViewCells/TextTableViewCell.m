//
//  TextTableViewCell.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import "TextTableViewCell.h"

@implementation TextTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
