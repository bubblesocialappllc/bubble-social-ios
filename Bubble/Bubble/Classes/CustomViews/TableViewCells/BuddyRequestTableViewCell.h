//
//  BuddyRequestTableViewCell.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/23/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuddyRequestTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet PFImageView *profilePicture;
@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet UIView *acceptView;
@property (strong, nonatomic) IBOutlet UIView *denyView;
@property (strong, nonatomic) IBOutlet UIImageView *acceptImageView;
@property (strong, nonatomic) IBOutlet UIImageView *denyImageView;


@end
