//
//  MePageBlankCellTableViewCell.m
//  Bubble
//
//  Created by Doug Woodrow on 3/8/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import "MePageBlankCellTableViewCell.h"

@implementation MePageBlankCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
