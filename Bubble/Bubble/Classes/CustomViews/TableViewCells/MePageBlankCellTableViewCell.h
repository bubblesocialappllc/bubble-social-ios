//
//  MePageBlankCellTableViewCell.h
//  Bubble
//
//  Created by Doug Woodrow on 3/8/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MePageBlankCellTableViewCell : UITableViewCell

@end
