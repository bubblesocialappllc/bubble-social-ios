//
//  CHTableViewCommentCellTableViewCell.m
//  Bubble
//
//  Created by Doug Woodrow on 29/08/2014.
//  Copyright (c) 2014 Chayel Heinsen. All rights reserved.
//

#import "CHTableViewCommentCellTableViewCell.h"

@implementation CHTableViewCommentCellTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
