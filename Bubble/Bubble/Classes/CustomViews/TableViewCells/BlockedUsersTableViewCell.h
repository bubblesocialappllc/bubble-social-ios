//
//  BlockedUsersTableViewCell.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/24/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockedUsersTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet PFImageView *profilePicture;
@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet UIImageView *accessoryImageView;

@end
