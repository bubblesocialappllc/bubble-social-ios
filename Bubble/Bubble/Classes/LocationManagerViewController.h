//
//  LocationManagerViewController.h
//  Bubble
//
//  Created by Doug Woodrow on 3/1/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@import SVPulsingAnnotationView;

@protocol LocationManagerDelegate <NSObject>

- (void)locationManagerDidChoosePlace:(BSPlace *)place;

@end

@interface LocationManagerViewController : UIViewController <MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) MKMapView *mapView;
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) NSString *currentCity;
@property (weak, nonatomic) id<LocationManagerDelegate> delegate;

@end
