//
//  ColorPickerViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 2/15/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@import RFQuiltLayout;
#import "EditProfileTableViewController.h"

@interface ColorPickerViewController : UICollectionViewController <RFQuiltLayoutDelegate>



@end
