//
//  CameraViewController.h
//  Bubble
//
//  Created by Doug Woodrow on 2/1/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CameraDelegate <NSObject>

- (void)cameraDismissedWithImage:(UIImage *)image;

@optional

- (void)cameraDismissedWithCancel;

@end

@interface CameraViewController : UIViewController

@property (weak, nonatomic) id<CameraDelegate> delegate;
@property (strong, nonatomic) LLSimpleCamera *camera;
@property (strong, nonatomic) IBOutlet UIButton *flash;
@property (strong, nonatomic) IBOutlet UIButton *capture;
@property (strong, nonatomic) IBOutlet UIButton *flip;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *close;
@property (strong, nonatomic) IBOutlet UIButton *save;
@property (strong, nonatomic) IBOutlet UIButton *use;
@property (strong, nonatomic) IBOutlet UIButton *closeVC;

- (IBAction)flash:(id)sender;
- (IBAction)capture:(id)sender;
- (IBAction)flip:(id)sender;
- (IBAction)closeImagePreview:(id)sender;
- (IBAction)saveToCameraRoll:(id)sender;
- (IBAction)useCapturedPhoto:(id)sender;
- (IBAction)dismiss:(id)sender;

@end
