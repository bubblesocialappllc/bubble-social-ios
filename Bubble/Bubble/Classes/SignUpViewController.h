//
//  SignUpViewController.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/21/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileSetupViewContoller.h"
#import "BSUser.h"
#import "BSPhone.h"

@protocol SignUpDelegate <NSObject>

/**
 *  Notifies the delegate that we finished signing up.
 */
- (void)didFinishSignUpWithUser:(BSUser *)user;

@end

@class CHLoginTextField;
@interface SignUpViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) id<SignUpDelegate> delegate;
@property (strong, nonatomic) BSPhone *phone;
@property (strong, nonatomic) IBOutlet CHLoginTextField *username;
@property (strong, nonatomic) IBOutlet CHLoginTextField *password;
@property (strong, nonatomic) IBOutlet CHLoginTextField *name;
@property (strong, nonatomic) IBOutlet CHLoginTextField *email;
@property (strong, nonatomic) IBOutlet CHLoginTextField *birthday;
@property (strong, nonatomic) IBOutlet UISegmentedControl *gender;
@property (strong, nonatomic) IBOutlet UIButton *close;

- (IBAction)close:(id)sender;
- (IBAction)signup:(id)sender;

@end
