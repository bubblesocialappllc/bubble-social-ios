//
//  AppDelegate.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "NotificationsViewController.h"
#import "AroundMeViewController.h"
#import "BuddiesViewController.h"
#import "LoginViewController.h"
#import <SupportKit/SupportKit.h>
#import "BSConnect.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "BSPlace.h"
#import "BSUser.h"
#import "BSPhone.h"
#import "BSPost.h"
#import "BSPhoto.h"
#import "BSLocation.h"
#import "BSMessage.h"
#import "BSMessageHead.h"
#import "BSComment.h"
#import "BSPushHandler.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, BSConnectDelegate>

/**
 *  The applications window.
 */
@property (strong, nonatomic) UIWindow *window;

/**
 *  The current location of the user.
 */
@property (strong, nonatomic) PFGeoPoint *currentLocation;

/**
 *  The current logged in user.
 */
@property (strong, nonatomic) BSUser *currentUser;

/**
 *  The Multipeer Connectivity Object;
 */
@property (strong, nonatomic) BSConnect *connect;

/**
 *  Set to YES to stop showing the location error.
 */
@property BOOL stopShowingLocationError;

/**
 *  YES if is in chat view, otherwise nil.
 */
@property (readonly, getter=isInChat) BOOL inChat;

@property (strong, nonatomic) NSUserDefaults *watchDefaults;

@end

