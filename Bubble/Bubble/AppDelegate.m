//
//  AppDelegate.m
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#import <Availability.h>
#import "AppDelegate.h"
@import KVNProgress;
#import "BSAppCall.h"
#import "BSLocationManager.h"
#import "CloudCode.h"

#ifdef __IPHONE_9_0
@import WatchConnectivity;
#endif

static NSString *const sharedUserActivityType = @"com.bubblesocial.Bubble.FromWatch";
static NSString *const sharedIdentifierKey = @"com.bubblesocial.Bubble";

@interface AppDelegate () <
#ifdef __IPHONE_9_0
WCSessionDelegate
#endif
>

@property (readwrite, getter=isInChat) BOOL inChat;

@end

@implementation AppDelegate

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    
    if ([identifier isEqualToString:ACCEPT_BUDDY]) {
        
        //[CloudCode acceptBuddyWithName:[userInfo objectForKey:@"requestingUser"]];
        
    } else if ([identifier isEqualToString:DENY_BUDDY]) {
        
        //[CloudCode denyBuddyWithName:[userInfo objectForKey:@"requestingUser"]];
        
    }
    
}

    // This is a key function in the app deleagate to handle receiving remote notifications from Parse's push. It handles pushing a notification to a specific view in Bubble.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    
    //[BSPushHandler handlePush:userInfo application:application];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewNotification" object:nil userInfo:userInfo];
    
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
        MercuryNotification *notification = [MercuryNotification new];
        notification.color = [UIColor bubblePink];
        notification.soundPath = [[NSBundle mainBundle] pathForResource:@"notify" ofType:@"wav"];
        notification.text = userInfo[@"aps"][@"alert"];
        notification.action = ^(MercuryNotification *notification){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:userInfo[@"aps"][@"viewToPush"]]];
        };
        
        [[Mercury sharedInstance] postNotification:notification];
    }
    
    handler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"%@", error.localizedDescription);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];

}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Enable Local Datastore
    [Parse enableLocalDatastore];
    
    //[Parse enableDataSharingWithApplicationGroupIdentifier:@"group.com.bubblesocial.Bubble"];
    // Setup Parse
    
    // Initialize Parse Bubble credentials
    [Parse setApplicationId:@"nGqIeadsF1JJxaa17JwryHfzmAnDJeL2qKaGiBHu" clientKey:@"jXeLGJUg2TxhDkE62VEv6swOx3vkTOEiVX5dmGnC"];

//    AFNetworkReachabilityManager *netMan = [AFNetworkReachabilityManager sharedManager];
//    if (![netMan isReachable]) {
//        return YES;
//    }
        // Facebook Utilities initialize
    //[PFFacebookUtils initializeFacebookWithApplicationLaunchOptions:launchOptions]; // For some reason this is causing a crash.
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    SKTSettings *settings = [SKTSettings settingsWithAppToken:@"ez3b7awpgn56gcnn8ks0k2lrk"];
    settings.conversationAccentColor = [UIColor bubblePink];
    [SupportKit initWithSettings: settings];
    [SupportKit setDefaultRecommendations:@[@"http://bubblesocialapp.com/support"]];
    
    //[GMSServices provideAPIKey:@"AIzaSyAdy-bKkD_6XHxZGyMu3fnfj9twg7BC9JY"];
    
    self.watchDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.bubblesocial.Bubble"];
    
    if ([WCSession isSupported]) {
        WCSession *session = [WCSession defaultSession];
        session.delegate = self;
        [session activateSession];
    }
    
        // Set the tab bar colors
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor bubblePink]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor lightGrayColor] } forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor bubblePink] } forState:UIControlStateSelected];
    
    self.stopShowingLocationError = NO;
    
    if ([BSUser currentUser]) {
        
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        [currentInstallation addUniqueObject:[BSUser currentUser].username forKey:@"channels"];
        [currentInstallation saveInBackground];
        
        self.currentUser = [BSUser currentUser];
        
        NSString *name = self.currentUser.realName;
        NSString *firstName = [name substringToIndex:[name rangeOfString:@" "].location];
        NSString *lastName = [name substringFromIndex:[name rangeOfString:@" "].location];
        
        firstName = [firstName removeWhiteSpacesFromString];
        lastName = [lastName removeWhiteSpacesFromString];

        [SupportKit setUserFirstName:firstName lastName:lastName];
        [SKTUser currentUser].email = self.currentUser.email;
        //[[SKTUser currentUser] addProperties:@{  }];
        
        self.connect = [BSConnect sharedConnect];
        [self.connect setDelegate:self];
        [self.connect setPeerID:[[MCPeerID alloc] initWithDisplayName:self.currentUser.username] service:BUBBLE_MPC_SERVICE];
        
        [self.connect startAdvertising];
        
        [[BSUser currentUser].phone fetchIfNeededInBackground];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        self.currentUser.notifications = @{
                                           @"like" : [NSNumber numberWithBool:[defaults boolForKey:@"likes_notification"]],
                                           @"comment" : [NSNumber numberWithBool:[defaults boolForKey:@"comments_notification"]],
                                           @"buddyRequest" : [NSNumber numberWithBool:[defaults boolForKey:@"buddyRequest_notification"]],
                                           @"commentResponse" : [NSNumber numberWithBool:[defaults boolForKey:@"response_notification"]],
                                           @"mention" : [NSNumber numberWithBool:[defaults boolForKey:@"mentions_notification"]],
                                           };
        [self.currentUser saveEventually];
        
        UIApplication *application = [UIApplication sharedApplication];
        
        UIMutableUserNotificationAction *accept = [UIMutableUserNotificationAction new];
        accept.identifier = ACCEPT_BUDDY;
        accept.title = @"Accept";
        accept.activationMode = UIUserNotificationActivationModeBackground;
        accept.destructive = NO;
        accept.authenticationRequired = NO;
        
        UIMutableUserNotificationAction *deny = [UIMutableUserNotificationAction new];
        deny.identifier = DENY_BUDDY;
        deny.title = @"Deny";
        deny.activationMode = UIUserNotificationActivationModeBackground;
        deny.destructive = YES;
        deny.authenticationRequired = NO;
        
        UIMutableUserNotificationCategory *category = [UIMutableUserNotificationCategory new];
        category.identifier = PENDING_BUDDIES;
        
        NSArray *actions = @[accept, deny];
        
        [category setActions:actions forContext:UIUserNotificationActionContextMinimal];
        
        NSSet *categoriesSet = [[NSSet alloc] initWithObjects:category, nil];
        
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:categoriesSet]];
        
        [application registerForRemoteNotifications];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onChat) name:@"OnChat" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(offChat) name:@"OffChat" object:nil];
    
    // Set our KVNProgress Configs
    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
    
    configuration.statusColor = [UIColor flatGreenSeaColor];
    //configuration.statusFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:15.0f];
    configuration.circleStrokeForegroundColor = [UIColor bubblePink];
    //configuration.circleStrokeBackgroundColor = [UIColor colorWithWhite:1.0f alpha:0.3f];
    //configuration.circleFillBackgroundColor = [UIColor colorWithWhite:1.0f alpha:0.1f];
    //configuration.backgroundFillColor = [UIColor colorWithRed:0.173f green:0.263f blue:0.856f alpha:0.9f];
    //configuration.backgroundTintColor = [UIColor colorWithRed:0.173f green:0.263f blue:0.856f alpha:1.0f];
    configuration.successColor = [UIColor flatGreenSeaColor];
    configuration.errorColor = [UIColor flatAlizarinColor];
    //configuration.circleSize = 110.0f;
    //configuration.lineWidth = 1.0f;
    configuration.fullScreen = NO;
    
    [KVNProgress setConfiguration:configuration];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *likeNotification = [defaults objectForKey:@"like_notification"];
    
    if (!likeNotification) {
        [self registerDefaults];
    }
    
    [BSLocationManager sharedManager].didUpdateLocationBlock = ^(CLLocation *location) {
        self.currentLocation = [PFGeoPoint geoPointWithLocation:location];
        
        if ([BSUser currentUser]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshedLocation" object:nil];
            
            [BSUser currentUser].location = self.currentLocation;
        }
    };
    
    [BSLocationManager sharedManager].errorBlock = ^(NSError *error) {
        NSLog(@"%@", error);
    };
    
    return YES;
}

#pragma GCC diagnostic pop

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
    
    [[BSUser currentUser] fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        self.currentUser = (BSUser *)object;
    }];
    
    [FBSDKAppEvents activateApp];
    
    //[FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    self.stopShowingLocationError = NO;
    
    if ([BSLocationManager sharedManager].authorizationStatus != BSLocationAuthorizationStatusAuthorizedAlways) {
        [[BSLocationManager sharedManager] stopUpdatingLocation];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
   
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (self.currentUser) {
        self.currentUser.notifications = @{
                                           @"like" : [NSNumber numberWithBool:[defaults boolForKey:@"likes_notification"]],
                                           @"comment" : [NSNumber numberWithBool:[defaults boolForKey:@"comments_notification"]],
                                           @"buddyRequest" : [NSNumber numberWithBool:[defaults boolForKey:@"buddyRequest_notification"]],
                                           @"commentResponse" : [NSNumber numberWithBool:[defaults boolForKey:@"response_notification"]],
                                           @"mention" : [NSNumber numberWithBool:[defaults boolForKey:@"mentions_notification"]],
                                           };

        [self.currentUser saveEventually];
        
        if ([BSLocationManager sharedManager].authorizationStatus == BSLocationAuthorizationStatusAuthorizedAlways || [BSLocationManager sharedManager].authorizationStatus == BSLocationAuthorizationStatusAuthorizedWhenInUse) {
            [[BSLocationManager sharedManager] updateLocation];
        }
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL meantForFacebook = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                           openURL:url
                                                                 sourceApplication:sourceApplication
                                                                        annotation:annotation];
    
    if (!meantForFacebook) {
        
        if (application.applicationState != UIApplicationStateBackground) {
            return [BSAppCall handleOpenURL:url sourceApplication:sourceApplication];
        } else {
            return NO;
        }
    } else {
        return meantForFacebook;
    }

}

#pragma mark - Watch

- (void)application:(UIApplication *)application handleWatchKitExtensionRequest:(NSDictionary *)userInfo reply:(void (^)(NSDictionary *))reply {
   
    __block UIBackgroundTaskIdentifier watchKitHandler;
    watchKitHandler = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"backgroundTask"
                                                                   expirationHandler:^{
                                                                       watchKitHandler = UIBackgroundTaskInvalid;
                                                                   }];
    
    if (userInfo[@"NowFeedRequest"]) {
        
        double range = [CHKeychain doubleForKey:@"range"];
        
        if (range > 10) {
            range = 10;
        }
        
        NSDateComponents *components = [[NSDateComponents alloc] init];
        components.day = -1;
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *day = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
        
        PFQuery *query = [BSPost query];
        [query includeKey:@"author"];
        [query whereKey:@"createdAt" greaterThan:day];
        
        [query whereKey:@"location" nearGeoPoint:self.currentLocation withinMiles:range]; //Queries the posts near you
        
        if (self.currentUser.blocked != nil) {
            [query whereKey:@"authorId" notContainedIn:self.currentUser.blocked]; //Removes blocked users from the post array
        }
        
        [query orderByDescending:@"createdAt"]; //Orders the posts by created at date
        query.limit = 10;
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if (!error) {
                NSArray *array = [self mapWatchRequest:userInfo forObjects:objects];
                reply(@{@"response" : array});
            } else {
                reply(@{@"error" : error});
            }
            
            dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
            });
            
        }];
    } else if (userInfo[@"PoppinPostsFeedRequest"]) {
        double range = [CHKeychain doubleForKey:@"range"];
        
        if (range > 10) {
            range = 10;
        }
        
        NSDateComponents *components = [[NSDateComponents alloc] init];
        components.day = -1;
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *day = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
        
        PFQuery *query = [BSPost query];
        [query includeKey:@"author"];
        [query whereKey:@"createdAt" greaterThan:day];
        
        [query whereKey:@"location" nearGeoPoint:self.currentLocation withinMiles:range]; //Queries the posts near you
        
        if (self.currentUser.blocked != nil) {
            [query whereKey:@"authorId" notContainedIn:self.currentUser.blocked]; //Removes blocked users from the post array
        }
        
        [query orderByDescending:@"points"]; //Orders the posts by points
        query.limit = 10;
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if (!error) {
                NSArray *array = [self mapWatchRequest:userInfo forObjects:objects];
                reply(@{@"response" : array});
            } else {
                reply(@{@"error" : error});
            }
            
            dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
            });
            
        }];
    } else if (userInfo[@"PoppinPeopleFeedRequest"]) {
        PFQuery *query = [BSUser query];
        double range = [CHKeychain doubleForKey:@"range"];
        [query whereKey:@"location" nearGeoPoint:self.currentLocation withinMiles:range];
        [query whereKey:@"locationHidden" equalTo:[NSNumber numberWithBool:NO]];
        [query setLimit:10];
        [query orderByDescending:@"popPoints"];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if (!error) {
                NSArray *array = [self mapWatchRequest:userInfo forObjects:objects];
                reply(@{@"response" : array});
            } else {
                reply(@{@"error" : error});
            }
            
            dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
            });
            
        }];
    } else if (userInfo[@"MakePost"]) {
        
        NSArray *ar = userInfo[@"MakePost"];
        NSString *text = [ar firstObject];
        
        [CloudCode postStatus:text file:nil location:self.currentLocation pointOfInterest:[BSPlace new] shouldSavePhotoToUser:NO completion:^(BOOL succeeded, NSError *error) {
            
            dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
            });
            
        }];
    } else if (userInfo[@"GlanceRankRequest"]) {
        NSNumber *numberWithDouble = [NSNumber numberWithDouble:[CHKeychain doubleForKey:@"range"]];
        
        [PFCloud callFunctionInBackground:@"rank"
                           withParameters:@{
                                            @"bounds" : numberWithDouble
                                            }
                                    block:^(id object, NSError *error) {
                                        
                                        if (!error) {
                                            [CHKeychain setObject:object forKey:@"mostCurrentRank"];
                                            NSNumber *rank = [CHKeychain objectForKey:@"mostCurrentRank"];
                                            
                                            if (!self.watchDefaults) {
                                                self.watchDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.bubblesocial.Bubble"];
                                            }
                                            
                                            [self.watchDefaults setObject:rank forKey:@"mostCurrentRank"];
                                            [self.watchDefaults synchronize];
                                            
                                            reply(@{@"response" : @[rank]});
                                        } else {
                                            reply(@{@"resposnse" : @[error]});
                                        }
                                        
                                        dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
                                        });
                                    }];
    }

}

- (NSArray *)mapWatchRequest:(NSDictionary *)request forObjects:(NSArray *)objects {
    
    NSMutableArray *array = [NSMutableArray new];
    
    if (request[@"NowFeedRequest"] || request[@"PoppinPostsFeedRequest"]) {
        
        for (BSPost *post in objects) {
            NSMutableDictionary *dict = [NSMutableDictionary
                                         dictionaryWithDictionary:@{
                                                                    @"username" : post.authorUsername,
                                                                    @"location" : post.locationName,
                                                                    }];
            if (post.photo) {
                [dict addEntriesFromDictionary:@{@"photo" : post.photo.url}];
            }
            
            if (post.text) {
                [dict addEntriesFromDictionary:@{@"text" : post.text}];
            }
            
            [array addObject:dict];
        }
    } else if (request[@"PoppinPeopleFeedRequest"]) {
        
        for (BSUser *user in objects) {
            NSMutableDictionary *dict = [NSMutableDictionary
                                         dictionaryWithDictionary:@{
                                                                    @"username" : user.username,
                                                                    }];
            /*
            if (user.profilePicture) {
                [dict addEntriesFromDictionary:@{@"photo" : user.profilePicture.url}];
            }
            */
            [array addObject:dict];
        }

    }
    
    return array;
}

- (BOOL)application:(UIApplication *)application willContinueUserActivityWithType:(NSString *)userActivityType {
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *))restorationHandler {
    return YES;
}

- (void)session:(WCSession *)session didReceiveMessage:(NSDictionary<NSString *,id> *)message replyHandler:(nonnull void (^)(NSDictionary<NSString *,id> * _Nonnull))reply {
    
    __block UIBackgroundTaskIdentifier watchKitHandler;
    watchKitHandler = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"backgroundTask"
                                                                   expirationHandler:^{
                                                                       watchKitHandler = UIBackgroundTaskInvalid;
                                                                   }];
    
    if (message[@"NowFeedRequest"]) {
        
        double range = [CHKeychain doubleForKey:@"range"];
        
        if (range > 10) {
            range = 10;
        }
        
        PFQuery *query = [BSPost query];
        [query includeKey:@"author"];
        [query whereKey:@"location" nearGeoPoint:self.currentLocation withinMiles:range]; //Queries the posts near you
        [query orderByDescending:@"createdAt"]; //Orders the posts by created at date
        query.limit = 10;
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if (!error) {
                NSArray *array = [self mapWatchRequest:message forObjects:objects];
                reply(@{@"response" : array});
            } else {
                reply(@{@"error" : error});
            }
            
            dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
            });
            
        }];
    } else if (message[@"PoppinPostsFeedRequest"]) {
        double range = [CHKeychain doubleForKey:@"range"];
        
        if (range > 10) {
            range = 10;
        }
        
        PFQuery *query = [BSPost query];
        [query includeKey:@"author"];
        [query whereKey:@"location" nearGeoPoint:self.currentLocation withinMiles:range]; //Queries the posts near you
        [query orderByDescending:@"points"]; //Orders the posts by points
        query.limit = 10;
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if (!error) {
                NSArray *array = [self mapWatchRequest:message forObjects:objects];
                reply(@{@"response" : array});
            } else {
                reply(@{@"error" : error});
            }
            
            dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
            });
            
        }];
    } else if (message[@"PoppinPeopleFeedRequest"]) {
        PFQuery *query = [BSUser query];
        double range = [CHKeychain doubleForKey:@"range"];
        [query whereKey:@"location" nearGeoPoint:self.currentLocation withinMiles:range];
        [query whereKey:@"locationHidden" equalTo:[NSNumber numberWithBool:NO]];
        [query orderByDescending:@"popPoints"];
        query.limit = 10;
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if (!error) {
                NSArray *array = [self mapWatchRequest:message forObjects:objects];
                reply(@{@"response" : array});
            } else {
                reply(@{@"error" : error});
            }
            
            dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
            });
            
        }];
    } else if (message[@"MakePost"]) {
        
        NSArray *ar = message[@"MakePost"];
        NSString *text = [ar firstObject];
        
        [CloudCode postStatus:text file:nil location:self.currentLocation pointOfInterest:[BSPlace new] shouldSavePhotoToUser:NO completion:^(BOOL succeeded, NSError *error) {
            
            dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
            });
            
        }];
    } else if (message[@"GlanceRankRequest"]) {
        NSNumber *numberWithDouble = [NSNumber numberWithDouble:[CHKeychain doubleForKey:@"range"]];
        
        [PFCloud callFunctionInBackground:@"rank"
                           withParameters:@{
                                            @"bounds" : numberWithDouble
                                            }
                                    block:^(id object, NSError *error) {
                                        
                                        if (!error) {
                                            [CHKeychain setObject:object forKey:@"mostCurrentRank"];
                                            NSNumber *rank = [CHKeychain objectForKey:@"mostCurrentRank"];
                                            
                                            if (!self.watchDefaults) {
                                                self.watchDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.bubblesocial.Bubble"];
                                            }
                                            
                                            [self.watchDefaults setObject:rank forKey:@"mostCurrentRank"];
                                            [self.watchDefaults synchronize];
                                            
                                            reply(@{@"response" : @[rank]});
                                        } else {
                                            reply(@{@"resposnse" : @[error]});
                                        }
                                        
                                        dispatch_after( dispatch_time(DISPATCH_TIME_NOW, (int64_t)NSEC_PER_SEC * 1), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            [[UIApplication sharedApplication] endBackgroundTask:watchKitHandler];
                                        });
                                    }];
    }
}

#pragma mark - BSConnect Delegate

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state {
    
    if (state == MCSessionStateConnected) {
        
        // Send data here.
        
        
    }
    
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID {
    
    // Received data from peer.
    
}

- (BOOL)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID withContext:(NSData *)context {
    
    //TODO: We need to display to the user who is trying to connect and ask if we should allow it.
    
    return YES;
    
}

- (void)browser:(MCNearbyServiceBrowser *)browser foundSearchedPeer:(MCPeerID *)peerID {
    
    //TODO: We found the user we were looking for. Send an invitation.
    NSLog(@"Found %@", peerID.displayName);
}
 
#pragma mark - Helpers

- (void)onChat {
    self.inChat = YES;
}

- (void)offChat {
    self.inChat = NO;
}

#pragma mark - NSUserDefaults

- (void)registerDefaults {
    // this function writes default settings as settings
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    
    if (!settingsBundle) {
        NSLog(@"Could not find Settings.bundle");
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    
    for (NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
      
        if (key) {
            [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
           // NSLog(@"writing as default %@ to the key %@",[prefSpecification objectForKey:@"DefaultValue"],key);
        }
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (self.currentUser) {
        self.currentUser.notifications = @{
                                           @"like" : [NSNumber numberWithBool:[defaults boolForKey:@"likes_notification"]],
                                           @"comment" : [NSNumber numberWithBool:[defaults boolForKey:@"comments_notification"]],
                                           @"buddyRequest" : [NSNumber numberWithBool:[defaults boolForKey:@"buddyRequest_notification"]],
                                           @"commentResponse" : [NSNumber numberWithBool:[defaults boolForKey:@"response_notification"]],
                                           @"mention" : [NSNumber numberWithBool:[defaults boolForKey:@"mentions_notification"]],
                                           };

        [self.currentUser saveEventually];
    }
    
}

//#pragma mark - FICImageCacheDelegate
//
//- (void)imageCache:(FICImageCache *)imageCache wantsSourceImageForEntity:(id<FICEntity>)entity withFormatName:(NSString *)formatName completionBlock:(FICImageRequestCompletionBlock)completionBlock {
//    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        // Fetch the desired source image by making a network request
//        NSURL *requestURL = [entity sourceImageURLWithFormatName:formatName];
//        NSURLRequest *request = [NSURLRequest requestWithURL:requestURL];
//        NSLog(@"%@", requestURL);
//        AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//        
//        requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
//        
//        [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//            NSLog(@"Response: %@", responseObject);
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                completionBlock(responseObject);
//            });
//            
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            NSLog(@"Image error: %@", error);
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                completionBlock(nil);
//            });
//        }];
//        
//        [requestOperation start];
//    });
//}
//
//- (void)imageCache:(FICImageCache *)imageCache cancelImageLoadingForEntity:(id <FICEntity>)entity withFormatName:(NSString *)formatName {
//}

@end
