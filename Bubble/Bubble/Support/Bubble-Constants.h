//
//  Bubble-Constants.h
//  Bubble
//
//  Created by Chayel Heinsen on 12/7/14.
//  Copyright (c) 2014 Bubble Social App LLC. All rights reserved.
//

#ifndef Bubble_Bubble_Constants_h
#define Bubble_Bubble_Constants_h

#define SERVICE_NAME @"BUBBLESOCIALAPP"
#define GROUP_NAME @"com.bubblesocial.Bubble"
#define LIKE_NOTIFICATION @"Like"
#define COMMENT_NOTIFICATION @"Comment"
#define BUDDY_REQUEST_NOTIFICATION @"BuddyRequest"
#define ACCEPT_BUDDY @"ACCEPT_BUDDY"
#define DENY_BUDDY @"DENY_BUDDY"
#define PENDING_BUDDIES @"PENDING_BUDDIES"

#define FEED_TAB 0
#define MESSAGES_TAB 1
#define MAP_TAB 2
#define ME_TAB 3
#define SEARCH_TAB 4

// URL Schemes
#define BUBBLE @"bubble://view/aroundfeed"
#define MESSAGES @"bubble://view/messages"
#define MAP @"bubble://view/map"
#define ME @"bubble://view/me"
#define SEARCH @"bubble://view/search"
#define BUDDYREQUEST @"bubble://view/buddyrequest"

// Deprecated Macro
#define BUBBLE_DEPRECATED(x, y) __attribute__((availability(ios,introduced=x,deprecated=y)));

// Mulitpeer Connectivity
#define BUBBLE_MPC_SERVICE @"bubble-service"

#endif
